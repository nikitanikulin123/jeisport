@extends('layouts.app')
@section('assets-css')
    <link rel="stylesheet" href="{{ asset('libs/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/bootstrap-multiselect/css/bootstrap-multiselect.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/bootstrap-rating/bootstrap-rating.css') }}">
@endsection
@section('assets-js')
    <script src="{{ asset('libs/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('libs/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="{{ asset('libs/bootstrap-rating/bootstrap-rating.min.js') }}"></script>
    <script src="{{ asset('js/single_item.js') }}"></script>
@endsection
@section('cities-selector')
    @include('partials.mini._cities_selector')
@endsection
@section('content')
	<section class="trener-show">
		<div class="container-fluid">
			<div class="content">
				<div class="img-wrapper col-md-1">
					<img class="img-responsive" src="{{ $trener->setImage('180_180', 'image') }}" alt="{{ $trener->setImage('180_180', 'image') }}">
				</div>
				<div class="title col-md-5">
					<h2>{{ $trener->FullName }}</h2>
					@ReplaceBlock("partials.mini._ratingsBlock", [$type = 'trener', $item = $trener])
					@if($trener->phone)
						<div class="call-back-trener">
							<span class="button-tel">Связаться</span>
							{{--<a href="{{ $trener->phone }}">{{ $trener->phone }}</a>--}}
							<div class="toggled_block">
								<p>Оставьте свой номер телефона. Cотрудник компании PROFI.RU свяжет вас со специалистом.</p>
								<div class="input-group">
									<input type="text" id="phone11" class="form-control">
									<span class="input-group-btn">
										<button class="btn btn-default" type="button">Go!</button>
									</span>
								</div><!-- /input-group -->
							</div>
						</div>
					@endif
					@if ($trener->user_id != Auth::id())
						<a data-toggle="tooltip" data-placement="right" title="Добавить в Избранное" class="heart @if($addedToFavourites)active @endif" data-type="trener" data-id="{{ $trener->id }}"></a>
					@endif
					@foreach($trener->findFilterValues($filters, 'Специальность')->take(1) as $filterValue)
						<a href="{{ route('treners.indexByType', [$trener->city->slug, $trener->category->slug, $filterValue->slug]) }}">{{ $filterValue->title }}</a>
					@endforeach
				</div>
				<div class="discript-map col-md-6">
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</section>

	<section class="@if($trener->premium)about-human-premium @else about-human @endif">
		<div class="container-fluid">
			<div class="content">
				<div class="col-md-8">
					<div class="margin-bottom">
						<h4>Специализация</h4>
						<div class="b-filter_options_with_commas">
							@foreach($trener->findFilterValues($filters, 'Специализация') as $speciality)
								<a href="{{ route('treners.index', [$trener->city->slug, $trener->category->slug, 'filter_id' => $speciality->id]) }}">{{ trim(mb_strtolower($speciality->title)) . ', ' }}</a>
							@endforeach
						</div>
					</div>
					<div class="margin-bottom">
						<h4>Образование</h4>
						{!! $trener->education !!}
					</div>

					<div class="margin-bottom">
						<h4>Описание</h4>
						{!! $trener->description !!}
					</div>
				</div>
				@if($trener->premium)
					<div class="col-md-4">
						@if($trener->photos->filterFix('type', '!=', 'portfolio')->count())
							<h4>Сертификаты и документы</h4>
							<div class="cover-right">
								<ul>
									@foreach($trener->photos->filterFix('type', '!=', 'portfolio') as $photo)
										<li>
											<a href=""></a>
											@TagBlock([$type = "img", $var = $photo, $src = "path", $size = "130_130"])
										</li>
									@endforeach
								</ul>
							</div>
						@endif
						@if($trener->photos->filterFix('type', '=', 'portfolio')->count())
							<h4>Портфолио</h4>
							<div class="cover-right">
								<ul>
									@foreach($trener->photos->filterFix('type', '=', 'portfolio') as $photo)
										<li>
											<a href=""></a>
											@TagBlock([$type = "img", $var = $photo, $src = "path", $size = "130_130"])
										</li>
									@endforeach
								</ul>
							</div>
						@endif
						<h4>Места приема</h4>
						@if($workInClub)
							@TagBlock([$type = "map", $var = $workInClub, $src = "lat_lng"])
							@include('clubs.partials._addressBlock', [$item = $workInClub])
						@elseif($trener->premium && $trener->lat_lng)
							@TagBlock([$type = "map", $var = $trener, $src = "lat_lng"])
						@else
							@include('clubs.partials._addressBlock', [$item = $trener])
						@endif
					</div>
				@else
					<div class="col-md-4">
						@include('partials.mini._recommendedBlock', [$type = 'treners'])
						<a href=""><img src="/img/jpg/banners1.jpg" alt="" class="img-responsive"></a>
					</div>
					<div class="clearfix"></div>
					<a href="" class="reklama">
						<img src="/img/jpg/bannercenter.jpg" alt="">
					</a>
				@endif
			</div>
		</div>
	</section>

	<section class="photo-jobs">
		<div class="container-fluid">
			<div class="content">
				<div class="cover">
					<h2>Фото работ</h2>
					<div class="photo-jobs-block">
						<div class="owl-carousel photo-job-car owl-theme">
							<div class="item"><img src="/img/jpg/ras2.png" alt=""></div>
							<div class="item"><img src="/img/premium.png" alt=""></div>
							<div class="item"><img src="/img/jpg/catalog1.png" alt=""></div>
							<div class="item"><img src="/img/jpg/blog.jpg" alt=""></div>
							<div class="item"><img src="/img/logo-preload.jpg" alt=""></div>
							<div class="item"><img src="/img/jpg/ras2.png" alt=""></div>
							<div class="item"><img src="/img/slider1.jpg" alt=""></div>
							<div class="item"><img src="/img/logo-preload.jpg" alt=""></div>
							<div class="item"><img src="/img/slider1.jpg" alt=""></div>
							<div class="item"><img src="/img/jpg/blog.jpg" alt=""></div>
							<div class="item"><img src="/img/logo-preload.jpg" alt=""></div>
							<div class="item"><img src="/img/jpg/blog.jpg" alt=""></div>
							<div class="item"><img src="/img/logo-preload.jpg" alt=""></div>
							<div class="item"><img src="/img/slider1.jpg" alt=""></div>
							<div class="item"><img src="/img/jpg/blog.jpg" alt=""></div>
							<div class="item"><img src="/img/logo-preload.jpg" alt=""></div>
							<div class="item"><img src="/img/jpg/blog.jpg" alt=""></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="reviews" id="otziv">
		<div class="container-fluid">
			<div class="content">
				<div class="cover">
					<h2>Отзывы</h2>
					<div class="dropping">
						<button class="custom-btn btn write create-reviews" type="button" data-id="{{ $trener->id }}" data-toggle="collapse" data-target="#addReview" aria-expanded="false" aria-controls="collapseExample">Написать</button>
						@if(count($reviews) != 0)
							<button class="custom-btn btn date sort-reviews sort-date active" data-sort="created_at">По дате</button>
							<button class="custom-btn btn date sort-reviews sort-likes" data-sort="rating_plus">По популярности</button>
						@endif
					</div>
					<div class="collapse" id="addReview">
						@if(!$trener->ratings->filterFix('user_id', '=', Auth::id())->first())
							@ReplaceBlock("partials.mini._ratingsBlock", [$type = 'trener', $item = $trener, $noReviews = true])
						@endif
						<div class="well">
							<form action="{{ route('reviews.store', [$trener->id, 'type' => 'trener', 'parent_id' => null]) }}" class="form-horizontal ajax_form" method="POST">
								<div class="form-group col-md-6">
									<textarea name="text" class="form-control" placeholder="Комментарий" rows="6"></textarea>
								</div>
								<div class="form-group col-md-12">
									<button type="submit" class="btn btn-default">Отправить</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<ul class="comments">
					@if(count($reviews) == 0)
						<li class="no-reviews">Отзывов еще никто не оставлял! Будь первым!</li>
					@endif
					@include('partials.mini._unitsWithShowMoreBlock', [$type = 'treners.reviews', $items = $reviews])
				</ul>
			</div>
		</div>
	</section>

	@if($trener->premium)
		@if(count($stocks) > 0)
			<section class="action">
				<div class="container-fluid">
					<div class="content">
						<h2>Акции</h2>
						<div class="owl-carousel">
							@foreach($stocks as $stock)
								<div>
									<div class="dashed">
										<a class="open_stocks_modal" data-id="{{ $stock->id }}" href="" @if($stock->image)style="background: url('{{ $stock->setImage('426_206', 'image') }}')" @endif></a>
										<h3>{{ $stock->title3 }}</h3>
										<h4>{{ str_limit($stock->title4, 15) }}</h4>
										<p>{{ str_limit($stock->text) }}</p>
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</section>
		@endif

		@if($trener->priceListCategories->count() > 0)
			<div class="price-list">
				<div class="container-fluid">
					<div class="content">
						<h2>Прайс лист</h2>
						<div class="tabs">
							<ul>
								<li class="price_list" data-category-id="all" data-item_id="{{ $trener->id }}" data-type="trener">Весь прайс лист</li>
								@foreach($trener->priceListCategories as $category)
									<li class="price_list" data-category-id="{{ $category->id }}" data-item_id="{{ $trener->id }}" data-type="trener">{{ $category->title }}</li>
								@endforeach
							</ul>
						</div>
						<div class="catalog price_list_container"></div>
					</div>
				</div>
			</div>
		@endif
	@endif
@endsection