@extends('layouts.app')
@section('assets-css')
	<link rel="stylesheet" href="{{ asset('libs/bootstrap-multiselect/css/bootstrap-multiselect.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/bootstrap-rating/bootstrap-rating.css') }}">
@endsection
@section('assets-js')
	<script src="{{ asset('libs/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
	<script src="{{ asset('js/items_updater.js') }}"></script>
	<script src="{{ asset('libs/bootstrap-rating/bootstrap-rating.min.js') }}"></script>
@endsection
@section('cities-selector')
    @include('partials.mini._cities_selector')
@endsection
@section('content')

	<script class="hidden" id="ajax-category">{{ $category->id }}</script>
	<script  class="hidden" id="ajax-city">{{ $city->id }}</script>
	<script data-url="{{ route('treners.index', [$city->slug, $category->slug]) }}" data-has-type="{{ isset($filterType) ? true : false }}"  class="hidden" id="ajax-baseUrl"></script>

    <section class="trener">
        <div class="container-fluid">

			<div class="content">
				<div class="col-md-6 left">
					<div class="discript">
						<h1>{{ $seoPageTitle }} <span class="b-changed_items_amount">- {{ $treners_count }} {{ trans_choice("специалист|специалиста|специалистов", $treners_count) }}</span></h1>
						@if($seo_page)
							<h2>{{ $seo_page->page_subtitle }}</h2>
						@endif
					</div>
				</div>
				<div class="col-md-6 right">
					@if($category->show_callback_form)
						{!! Form::open(['route' => ['callbacks.create', $category->id, 'type' => 'category'], 'class' => 'form-inline trener']) !!}
							<p>Профессиональный подбор специалиста</p>
							@InputBlock([$type = "input", $item = 'name', $label = null, $var = $category, $p = "placeholder=\"Введите ваше имя\""])
							@InputBlock([$type = "input", $item = 'callback_phone', $label = null, $var = $category, $p = "placeholder=\"Введите телефон\""])
							<button type="submit" class="btn btn-default">Заказать обратный звонок</button>
						{!! Form::close() !!}
					@endif
				</div>

				<div class="col-md-12">
					<div class="selects" style="display: none;">
						@include('partials.mini._filtersList', [$type = 'filters'])
						@include('partials.mini._filtersList', [$type = 'sortFilters'])
					</div>
				</div>
				<div class="clearfix"></div>
			</div>

        </div>
    </section>

    <section class="trener-ankets">
        <div class="container-fluid">
			<div class="col-md-5 left-treners-block">
				@include('partials.mini._unitsWithShowMoreBlock', [$type = 'treners.index', $items = $treners])
			</div> <!-- end col-md-8 -->
			<div class="col-md-3 center-treners_block">
				<h2>Репетиторы</h2>
				<p>Репетитор — специалист, который занимается с клиентом изучением одной из областей науки, языка или другой теоретической базы. Также репетиторы готовят учеников к школе, поступлению вузы или прохождению обучения по смене квалификации. Репетитор всегда занимается с учеником в частном порядке без применения обучения в группах.
					Работа репетитора требует внимательности, умения работать с людьми, хорошего знания своей области и терпения.</p>
				<h3>Зачем нужны репетиторы</h3>
				<p>Репетиторство считается хорошей альтернативой классическому обучению в группах, поскольку одному человеку уделяется куда больше внимания. Репетитор способен научить своего ученика гораздо быстрее, чем учитель или преподаватель. Репетитор может исправлять ошибки ученика, предлагать ему материал в наиболее понятной форме и обучать самым подходящим образом.
					Преимущества репетиторства заключаются в индивидуальном подходе к каждому человеку. Репетитор может выработать подходящий стиль общения и преподавания для каждого из своих учеников, что позволит быстрее освоить необходимые знания и навыки. Также репетитор может заниматься с учеником любое количество часов.</p>
			</div>
			<div class="col-md-4 right-treners_block">
				@include('partials.mini._recommendedBlock', [$type = 'treners'])
			</div>
        </div>
    </section>
@endsection