@extends('layouts.app')
@section('assets-css')
    <link rel="stylesheet" href="{{ asset('libs/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endsection
@section('assets-js')
    <script src="{{ asset('js/init.js') }}"></script>
    <script src="{{ asset('libs/js/flow.min.js') }}"></script>
    <script src="{{ asset('js/initMultiple.js') }}"></script>
    <script src="{{ asset('libs/js/Sortable.min.js') }}"></script>
    <script src="{{ asset('libs/js/sortable.jquery.binding.js') }}"></script>
    <script src="{{ asset('libs/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('libs/js/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    {{--<script> var editor = CKEDITOR.replace( '', { customConfig : 'config.js', } ); </script>--}}
@endsection
@section('content')

    <?php !isset($step) ? (Request::get('step') ? $step = Request::get('step') : $step = 1) : '' ?>
    <?php isset($trener) ?: $trener = null ?>

    <section class="content-step trener">
        <div class="container-fluid">
            <div class="content">
                <div class="col-md-12">
                    <div class="spet">
                        <div class="pull-left" id="back-profile">
                            <a href="{{ route('profiles.index') }}"><span class="glyphicon glyphicon-menu-left"></span> Назад к профилю</a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="cover">
                            <h1>@if(isset($trener)) <span>{{ $trener->FullName }} @else Новый</span> специалист @endif
                            </h1>
                            <div class="bg-cover">
                                <div class="col-md @if(isset($trener)) step-success @endif @if($step == 1) active @endif ">
                                    <div class="arrow">
                                        <p>Шаг 1.</p>
                                        <a @if(isset($trener)) href="{{ route('treners.edit', [$trener->slug, 'step' => 1]) }}"
                                           @else href="{{ route('treners.create', ['step' => 1]) }}" @endif>Заполните информацию</a>
                                    </div>
                                </div>
                                <div class="col-md @if(isset($trener) && $trener->filterValues->count() > 1) step-success @endif @if($step == 2) active @endif ">
                                    <div class="arrow">
                                        <p>Шаг 2. </p>
                                        <a @if(isset($trener)) href="{{ route('treners.edit', [$trener->slug, 'step' => 2]) }}" @endif>Выберите
                                            подкатегории</a>
                                    </div>
                                </div>
                                @if(!Auth::user()->hasRoleFix('boss'))
                                    <div class="col-md @if(isset($trener) && $trener->checkWorkingDays())) step-success @endif @if($step == 3) active @endif ">
                                        <div class="arrow">
                                            <p>Шаг 3.</p>
                                            <a @if(isset($trener) && $trener->filterValues->count() > 0) href="{{ route('treners.edit', [$trener->slug, 'step' => 3]) }}" @endif>Заполните
                                                расписание</a>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-md @if(isset($trener) && $trener->image) step-success @endif @if($step == 4) active @endif "
                                     id="no-arrow">
                                    <div class="arrow">
                                        <p>Шаг 4. </p>
                                        <a @if(isset($trener) && $trener->filterValues->count() > 1) href="{{ route('treners.edit', [$trener->slug, 'step' => 4]) }}" @endif>Добавьте
                                            изображения</a>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            @if($step == 1)
                                <div class="form-step-1">
                                    {!! Form::open(['route' => isset($trener) ? ['treners.update', $trener->id, 'step' => 1] : ['treners.store', 'step' => 1], 'method' => 'POST']) !!}
                                    @if(Auth::user()->hasRoleFix('boss'))
                                        <div class="row">
                                            <div class="col-md-6">
	                                            @InputBlock([$type = "input", $item = 'name', $label = "Имя", $var = $trener])
	                                            @InputBlock([$type = "input", $item = 'sname', $label = "Фамилия", $var = $trener, $p = "placeholder=\"Введите Фамилию\""])
	                                            @InputBlock([$type = "input", $item = 'mname', $label = "Отчество", $var = $trener])
	                                            @InputBlock([$type = "select", $item = 'city_id', $label = "Город", $list = $cities, $selected = $trener, $dp = "class=\"form-control b-address_city\""])
	                                            @InputBlock([$type = "select", $item = 'category_id', $label = "Категория", $list = $categories->filterArrFix(['type' => 'trener']),
													$selected = $trener, $p = "placeholder=\"Выберите Категорию\""])
                                            </div>
                                            <div class="col-md-6">
	                                            @InputBlock([$type = "textarea", $item = 'pricelist', $label = "Расценки", $var = $trener, $p = "class=\"form-control ckeditor\""])
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
	                                            @InputBlock([$type = "textarea", $item = 'description', $label = "Описание", $var = $trener, $p = "class=\"form-control ckeditor\""])
                                            </div>
                                            <div class="col-md-6">
	                                            @InputBlock([$type = "textarea", $item = 'education', $label = "Образование", $var = $trener, $p = "class=\"form-control ckeditor\""])
                                            </div>
                                        </div>
                                    @else
                                        <div class="row">
	                                        @InputBlock([$type = "input", $item = 'name', $label = "Имя", $var = $trener, $dp = "class=\"form-group col-md-3\""])
	                                        @InputBlock([$type = "input", $item = 'sname', $label = "Фамилия", $var = $trener, $dp = "class=\"form-group col-md-3\"", $p = "placeholder=\"Введите Фамилию\""])
	                                        @InputBlock([$type = "input", $item = 'mname', $label = "Отчество", $var = $trener, $dp = "class=\"form-group col-md-3\""])
	                                        @InputBlock([$type = "select", $item = 'category_id', $label = "Категория", $list = $categories->filterArrFix(['type' => 'trener']),
												$selected = $trener, $p = "placeholder=\"Выберите Категорию\"", $dp = "class=\"form-group col-md-3\""])
                                        </div>
                                        @include('partials.mini._cityRegionStreetSelectBlock', [$obj = $trener])
                                        <div class="row">
	                                        @InputBlock([$type = "input", $item = 'house', $label = "Дом №", $var = $trener, $dp = "class=\"form-group col-md-4\"", $p = "placeholder=\"Введите номер дома\""])
	                                        @InputBlock([$type = "input", $item = 'site', $label = "Сайт (если есть)", $var = $trener, $dp = "class=\"form-group col-md-4\"", $p = "placeholder=\"Введите Фамилию\""])
	                                        @InputBlock([$type = "input", $item = 'n_email', $label = "Email для уведомлений", $var = $trener, $dp = "class=\"form-group col-md-4\"", $p = "placeholder=\"Введите Email\""])
                                        </div>
                                        <div class="row">
	                                        @InputBlock([$type = "input", $item = 'phone', $label = "Телефон", $var = $trener, $dp = "class=\"form-group col-md-4\""])
	                                        @InputBlock([$type = "input", $item = 'phone2', $label = "Телефон2", $var = $trener, $dp = "class=\"form-group col-md-4\""])
                                        </div>
                                        <div class="row">
	                                        @InputBlock([$type = "textarea", $item = 'description', $label = "Описание", $var = $trener, $dp = "class=\"form-group col-md-6\"", $p = "class=\"form-control ckeditor\""])
	                                        @InputBlock([$type = "textarea", $item = 'education', $label = "Образование", $var = $trener, $dp = "class=\"form-group col-md-6\"", $p = "class=\"form-control ckeditor\""])
                                        </div>
                                        <div class="row">
	                                        @InputBlock([$type = "textarea", $item = 'pricelist', $label = "Расценки", $var = $trener, $dp = "class=\"form-group col-md-6\"", $p = "class=\"form-control ckeditor\""])
                                            <div class="col-md-6">
	                                            @include('admin.maps._yandexMap', ['instance' => isset($trener) ? $instance = $trener : null, 'defaultCity' => \App\Models\City::where('is_main', true)->first()])
	                                            @InputBlock([$type = "input", $item = 'lat_lng', $label = null, $var = $trener, $value = '42.875989,74.603674', $p = "id=lat_lng style=\"display:none\""])
	                                            <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-md-offset-6 col-md-6 button-next" style="padding-right: 0px;">
                                        <button type="submit" class=" custom-btn btn btn-default pull-right">Далее</button>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            @endif
                            @if($step == 2 && isset($trener))
                                <div class="spet -m-2">
                                    <div class="cover">
                                        {!! Form::open(['route' => ['treners.update', $trener->id, 'step' => 2], 'method' => 'POST']) !!}
                                        <h2>Выберите подкатегории</h2>
                                        <div class="col-md-4">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs">
                                                @foreach($filters->filter(function ($item) {return $item['slug'] != 'reyting';}) as $key => $filter)
                                                    <li @if($key == 0) class="active" @endif><a href="#filter_{{ $filter->id }}" data-toggle="tab">{{ $filter->title }}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="col-md-8">
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <h3>Выберите нужные из списка</h3>
                                                @foreach($filters->filter(function ($item) {return $item['slug'] != 'reyting';}) as $key => $filter)
                                                    <div class="tab-pane @if($key == 0) active @elseif($filter->slug == 'pol') trener-gender @endif" id="filter_{{ $filter->id }}">
                                                        <div class="col-md-6">
                                                            @foreach($filter->options->slice(0, round($filter->options->count() / 2)) as $option)
		                                                        @InputBlock([$type = "checkbox", $item = 'filterValues[' . $option->id . ']', $label = $option->title,
																	$checked = isset($trener) ? $trener->filterValues->where('id', $option->id)->first() : (old('filterValues') ? old('filterValues') : true)])
                                                            @endforeach
                                                        </div>
                                                        <div class="col-md-6">
                                                            @foreach($filter->options->slice(round($filter->options->count() / 2)) as $option)
		                                                        @InputBlock([$type = "checkbox", $item = 'filterValues[' . $option->id . ']', $label = $option->title,
																	$checked = isset($trener) ? $trener->filterValues->where('id', $option->id)->first() : (old('filterValues') ? old('filterValues') : true)])
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                            @if($step && $step != 1)
                                                <a href="{{ route('treners.edit', [$trener->slug, 'step' => $step == 4 ? $step - 2 : $step - 1]) }}" class="custom-btn btn btn-default">Назад</a>
                                            @endif
                                            <button type="submit" class="custom-btn btn btn-default pull-right">Далее</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            @endif
                            @if($step == 3 && isset($trener))
                                <div class="spet -m-3">
                                    <div class="cover">
                                        {!! Form::open(['route' => ['treners.update', $trener->id, 'step' => 3], 'method' => 'POST']) !!}
		                                    <div class="col-md-4">
			                                    @include('partials.mini._dutyBlock', [$obj = $trener])
		                                    </div>
		                                    <div class="col-md-8">
			                                    @include('partials.mini._scheduleBlock', [$obj = $trener])
			                                    <div class="clearfix" style="margin-bottom: 40px;"></div>
			                                    <?php $nextStep = Auth::user()->hasRoleFix('boss') && $step - 1 == 3 ? $step - 2 : $step - 1; ?>
			                                    <a href="{{ route('treners.edit', [$trener->slug, 'step' => $nextStep]) }}" class="custom-btn btn btn-default back">Назад</a>
			                                    <button type="submit" class="custom-btn  btn btn-default pull-right">Далее</button>
		                                    </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            @endif
                            @if($step == 4 && isset($trener))
                                <div class="spet -m-4">
                                {!! Form::open(['route' => ['treners.update', $trener->id, 'step' => 'complete'], 'method' => 'POST']) !!}
	                                @InputBlock([$type = "imageUpload", $item = 'image', $label = 'Главное изображение', $var = $trener])
	                                @if(!Auth::user()->hasRoleFix('boss'))
		                                @InputBlock([$type = "imageUploadMultiple", $item = 'images', $label = 'Сертификаты', $var = $trener, $only = ''])
		                                @InputBlock([$type = "imageUploadMultiple", $item = 'portfolio', $label = 'Портфолио', $var = $trener, $only = 'portfolio'])
	                                @endif
	                                @if($step && $step != 1)
	                                    <a href="{{ route('treners.edit', [$trener->slug, 'step' => $step == 4 ? $step - 2 : $step - 1]) }}"
	                                       class="custom-btn btn btn-default">Назад</a>
	                                @endif
	                                <button type="submit" class="custom-btn btn btn-default pull-right">Завершить</button>
                                {!! Form::close() !!}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection