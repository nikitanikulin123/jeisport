@extends('layouts.app')
@section('assets-css')
	<link rel="stylesheet" href="{{ asset('libs/bootstrap-multiselect/css/bootstrap-multiselect.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/bootstrap-rating/bootstrap-rating.css') }}">
@endsection
@section('assets-js')
	<script src="{{ asset('libs/js/owl.carousel.js') }}"></script>
	<script src="{{ asset('libs/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
	<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
	<script src="{{ asset('js/items_updater.js') }}"></script>
	<script src="{{ asset('libs/bootstrap-rating/bootstrap-rating.min.js') }}"></script>
@endsection
@section('cities-selector')
    @include('partials.mini._cities_selector')
@endsection
@section('content')
	<script id="city-coords-lat">{{ stripos($category->slug, 'issyk-kul') ? $city->lat + 0.05 : $city->lat }}</script>
	<script id="city-coords-lng">{{ stripos($category->slug, 'issyk-kul') ? $city->lng + 0.7 : $city->lng }}</script>
	<script id="ajax-category">{{ $category->id }}</script>
	<script id="ajax-city">{{ $city->id }}</script>
	<script data-url="{{ route('clubs.index', [$city->slug, $category->slug]) }}" data-has-type="{{ isset($filterType) ? true : false }}" id="ajax-baseUrl"></script>
	<script>
		window.onload = function() {
			$('div.selects a').attr('rel', 'nofollow');
		};
	</script>
	
	<section class="total hidden-tab">
		<div class="container">
			<h3 class="b-changed_items_amount">{{ $clubs_count }} {{ trans_choice("место|места|мест", $clubs_count) }}</h3>
		</div>
	</section>

	<section class="trener-header hidden-tab">
		<div class="container">
			<h1>{{ $seoPageTitle }}</h1>
			@if($seo_page)
				<p>{{ $seo_page->page_subtitle }}</p>
			@endif
		</div>
	</section>

    <section class="settings -m">
	    <div class="container">
	        <ul class="nav nav-tabs mobile-filter-map" role="tablist">
        		<li role="presentation" id="t-list" class="active mobile-trener-set-list"><a href="#list" aria-controls="list" role="tab" data-toggle="tab" class="filters"><i class="fa fa-list-ul" aria-hidden="true"></i></a></li>
				<li role="presentation" id="t-list-map" class="mobile-trener-set-map"><a href="#list-map" aria-controls="list-map" role="tab" data-toggle="tab" class="maps"><i class="fa fa-map-marker" aria-hidden="true"></i></a></li>
				<li class="dropdown">
					<a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
						<i class="fa fa-th" aria-hidden="true"></i> Фильтр
					</a>
				</li>
	        </ul>

			<div class="collapse" id="collapseExample">
				<div class="selects">
					@include('partials.mini._filtersList', [$type = 'filters'])
				</div>
			</div>

			<div class="tab-content mobile_tab">
				<div role="tabpanel" class="tab-pane" id="list-map">
	                <div class="map-category" id="ymaps-map" style="width: 100%;height: 400px" data-map-zoom="{{ $city->zoom }}"></div>
				</div>
				<div role="tabpanel" class="tab-pane active" id="list">
					<section class="trener-info -m">
						<div class="human">
							@include('partials.mini._unitsWithShowMoreBlock', [$type = 'clubs.index', $items = $clubs])
						</div>
					</section>

					<section class="treiner">
						<div class="container">
							<div class="human">
								@include('partials.mini._recommendedBlock', [$type = 'clubs'])
							</div>
						</div>
					</section>

					<div class="discript">
						@if($seo_page)
							<p>{{ $seo_page->page_description }}</p>
						@endif
					</div>
				</div>
			</div>
	    </div>
    </section>
@endsection