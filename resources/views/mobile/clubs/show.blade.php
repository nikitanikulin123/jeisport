@extends('layouts.app')
@section('assets-css')
	<link rel="stylesheet" href="{{ asset('libs/css/owl.carousel.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/bootstrap-multiselect/css/bootstrap-multiselect.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/bootstrap-rating/bootstrap-rating.css') }}">
@endsection
@section('assets-js')
	<script src="{{ asset('libs/js/owl.carousel.js') }}"></script>
	<script src="{{ asset('libs/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
	<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
	<script src="{{ asset('js/items_updater.js') }}"></script>
	<script src="{{ asset('libs/bootstrap-rating/bootstrap-rating.min.js') }}"></script>
	<script src="{{ asset('js/single_item.js') }}"></script>
@endsection
@section('cities-selector')
	@include('partials.mini._cities_selector')
@endsection
@section('content')

    @if($club->photos->count() >= 1)
	    <section class="single-banner">
		    <div class="owl-carousel-1">
			    <div class="item">
				    <img src="{{ $club->setImage('1520_699', 'image') }}" alt="{{ $club->setImage('1520_699', 'image') }}">
			    </div>
			    @foreach($club->photos as $photo)
				    <div class="item">
					    <img src="{{ $club->setImage('1520_699', $photo->path) }}" alt="{{ $club->setImage('1520_699', $photo->path) }}">
				    </div>
			    @endforeach
		    </div>
	    </section>
    @else
	    <section class="slider">
		    <div class="item-no-image">
			    <img src="{{ $club->setImage('1520_699', 'image') }}" alt="{{ $club->setImage('1520_699', 'image') }}">
		    </div>
	    </section>
    @endif

    <section class="call-back">
	    <div class="container">
		    <div class="title">
			    <h1>{{ $club->title }}</h1>
			    @ReplaceBlock("partials.mini._ratingsBlock", [$type = 'club', $item = $club])
			    @if ($club->user_id != Auth::id())
				    <a data-toggle="tooltip" data-placement="right" title="Добавить в Избранное" class="heart @if($addedToFavourites)active @endif" data-type="club" data-id="{{ $club->id }}"></a>
			    @endif
		    </div>
		    @if($club->premium && $club->show_callback_form)
			    @include('partials.mini._callbackBlock', [$item = $club])
		    @endif
		    <a href="tel:{{ $club->phone }}" class="number">{{ $club->phone }}</a>
		    <a href="">Перезвонить мне</a>
	    </div>
    </section>

    <section class="adress">
	    <div class="container">
		    @foreach($club->category->filters as $filter)
			    @if(count(array_intersect($club_filter_values->lists('id')->toArray(), $filter->options->lists('id')->toArray())) > 0)
				    <div class="b-filter_options_with_commas">
						<div class="cover">
					    <h4>{{ $filter->title }}</h4>
					    @foreach($filter->options as $option)
						    @if(in_array($option->id, $club_filter_values->lists('id')->toArray()))
							    <a href="{{ route('clubs.indexByType', [$club->city->slug, $club->category->slug, $option->slug]) }}">{{ trim(mb_strtolower($option->title)) . ', ' }}</a>
						    @endif
					    @endforeach
						</div>
				    </div>
			    @endif
		    @endforeach
		    <div class="cover">
			    @include('clubs.partials._addressBlock', [$item = $club])
		    </div>
	    </div>
    </section>

    <section class="maps">
	    <div class="container">
		    <div class="img-wrapper map" id="single-map" style="height: 230px;" data-map-lat="{{ $club->lat ? $club->lat : '' }}" data-map-lng="{{ $club->lng ? $club->lng : '' }}"></div>
	    </div>
    </section>

    <section class="about-single">
	    <div class="container">
		    <div class="cover">
			    {!! $club->description !!}
		    </div>
	    </div>
    </section>

    @if(count($stocks) > 0)
	    <section class="slider-about">
		    <div class="container">
				<h5>Акции</h5>
			    <div class="slider">
				    <div class="owl-carousel-2">
					    @foreach($stocks->sortByDesc('created_at') as $stock)
						    <div class="items">
							    <a class="ajax_modal" data-action="show" data-type="stock" data-id="{{ $stock->id }}" data-forItem="club" data-forItem-id="{{ $club->id }}" href="" @if($stock->image)style="background: url('{{ $stock->setImage('426_206', 'image') }}')" @endif ></a>
							    <h2>{{ $stock->title3 }}</h2>
							    <h3>{{ str_limit($stock->title4, 15) }}</h3>
							    <p>{{ str_limit($stock->text) }}</p>
						    </div>
					    @endforeach
				    </div>
			    </div>
		    </div>
	    </section>
    @endif
	@if(count($club->priceListCategories) > 0)
		<section class="accardion">
			<div class="container">
				<h5>Прайс-лист</h5>
				<div class="accord">
					<div class="panel-group" id="accordion">
						@foreach($club->priceListCategories as $category)
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="toogle" data-toggle="collapse" data-parent="#accordion" href="#collapse_{{ $category->id }}">{{ $category->title }}</a>
									</h4>
								</div>
								<div id="collapse_{{ $category->id }}" class="panel-collapse collapse">
									@foreach($category->priceLists as $priceList)
										<div class="panel-body">
											<div class="title pull-left">
												<p>{{ $priceList->title }}</p>
												<span>{{ $priceList->price }} {{ $priceList->price_for }}</span>
											</div>
										</div>
									@endforeach
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</section>
	@endif
	@if(count($club->treners) > 0)
		<section class="photo-gallery-med">
			<div class="container">
				<div class="content">
					<h2>Специалисты</h2>
					<div class="dropping selects" style="display: none;" data-club-id="{{ $club->id }}">
						@include('partials.mini._filtersList', [$type = 'specialists', $filterType = '', $selected_filter_id = null])
					</div>
					<div class="human">
						@include('partials.mini._unitsWithShowMoreBlock', [$type = 'clubs.specialists', $items = null])
					</div>
				</div>
			</div>
		</section>
	@endif

	<section class="mob-treners-special">
		<div class="container-fluid">
			<h2>Специалисты</h2>

			<div class="owl-carousel mob-slider-special owl-theme">
				<div class="item">
					<a href="#" class="link-to-one-special"></a>
					<div class="img-wrapper">
					</div>
					<div class="title-wrapper">
						<h5 class="name-special">1Виктория Сергеевна Лесина </h5>
						<h5 class="rank-special">1Главный тренер</h5>
						<h5>
							<a href="tel:996555667788" class="tel-special">996555667788</a>
						</h5>
					</div>
				</div>
				<div class="item">
					<a href="#" class="link-to-one-special"></a>
					<div class="img-wrapper">
					</div>
					<div class="title-wrapper">
						<h5 class="name-special">2Виктория Сергеевна Лесина </h5>
						<h5 class="rank-special">2Главный тренер</h5>
						<h5>
							<a href="tel:996555667788" class="tel-special">996555667788</a>
						</h5>
					</div>
				</div>
				<div class="item">
					<a href="#" class="link-to-one-special"></a>
					<div class="img-wrapper">
					</div>
					<div class="title-wrapper">
						<h5 class="name-special">3Виктория Сергеевна Лесина </h5>
						<h5 class="rank-special">3Главный тренер</h5>
						<h5>
							<a href="tel:996555667788" class="tel-special">996555667788</a>
						</h5>
					</div>
				</div>



			</div>
		</div>
	</section>

    <section class="reviews" id="otziv">
	    <div class="container">
		    <div class="row">
			    <div class="content">
				    <div class="cover">
					    <h2>Отзывы</h2>
					    <div class="dropping">
						    <button class="btn write create-reviews" type="button" data-id="{{ $club->id }}" data-toggle="collapse" data-target="#addReview" aria-expanded="false" aria-controls="collapseExample">Написать отзыв</button>
						    @if(count($reviews) != 0)
							    <button class="btn date sort-reviews sort-date active" data-sort="created_at">По дате</button>
							    <button class="btn date sort-reviews sort-likes" data-sort="rating_plus">По популярности</button>
						    @endif
					    </div>
					    <div class="collapse" id="addReview">
						    @if(!$club->ratings->filterFix('user_id', '=', Auth::id())->first())
							    @ReplaceBlock("partials.mini._ratingsBlock", [$type = 'club', $item = $club, $noReviews = true])
						    @endif
						    <div class="well">
							    <form action="{{ route('reviews.store', [$club->id, 'type' => 'club', 'parent_id' => null]) }}" class="form-horizontal ajax_form" method="POST">
								    <div class="form-group col-md-6">
									    <textarea name="text" class="form-control" placeholder="Комментарий" rows="6"></textarea>
								    </div>
								    <div class="form-group col-md-12">
									    <button type="submit" class="btn btn-default">Отправить</button>
								    </div>
							    </form>
						    </div>
					    </div>
				    </div>
				    <ul class="comments">
					    @if(count($reviews) == 0)
						    <li class="no-reviews">Отзывов еще никто не оставлял! Будь первым!</li>
					    @endif
					    @include('partials.mini._unitsWithShowMoreBlock', [$type = 'clubs.reviews', $items = $reviews])
				    </ul>
			    </div>
		    </div>
	    </div>
    </section>

    {{--<section class="photo-gallery">--}}
	    {{--<div class="container">--}}
		    {{--<div class="gallery">--}}
			    {{--<h2>Фотографии посетителей</h2>--}}
			    {{--<div class="owl-carousel-3">--}}
				    {{--@foreach($club->treners as $trener)--}}
					    {{--<div class="items">--}}
						    {{--<img src="{{ $trener->setImage('180_180', 'image') }}" alt="{{ $trener->setImage('180_180', 'image') }}">--}}
					    {{--</div>--}}
					{{--@endforeach--}}
			    {{--</div>--}}
		    {{--</div>--}}
	    {{--</div>--}}
    {{--</section>--}}
@endsection