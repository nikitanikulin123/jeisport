@extends('layouts.app')
@section('cities-selector')
    @include('partials.mini._cities_selector')
@endsection
@section('content')
    <section class="index-banner" style="background-image: url('{{ url('img/jpg/bg1.jpg') }}');">
		<h1>Удобный выбор мест и услуг</h1>
    </section>

    <section class="search-city">
	    <div class="container">
			<form role="form" class="city">
				<div class="form-group">
					<select class="form-control select">
						@foreach($cities as $k => $city)
							<option><a href="{{ route('cities.show', [$city->slug]) }}">{{ $city->title }}</a></option>
						@endforeach
					</select>
				</div>
			</form>
		    <div class="cover">
			    <div role="form" class="special">
				    <div class="border">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Места</a></li>
							<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Специалисты</a></li>
							<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-search"></span></a></li>
						</ul>
				    </div>
			    </div>
				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="home">
						<ul>
							@foreach($categories->filterArrFix(['type' => 'club'])->sortBy('order') as $category)
								<li>
									<h2>
										<a href="{{ route('clubs.index', [stripos($category->slug, 'issyk-kul') ? 'issyk-kul' : \App\Models\City::getDefaultCitySlug($cities), $category->slug]) }}" class="title">{{ $category->title }}</a>
									</h2>
									@foreach($category->subcategories as $subcategory)
										<a href="{{ route('clubs.indexByType', [stripos($category->slug, 'issyk-kul') ? 'issyk-kul' : \App\Models\City::getDefaultCitySlug($cities), $category->slug, $subcategory->slug]) }}">{{ $subcategory->title }} </a>
									@endforeach
								</li>
							@endforeach
						</ul>
					</div>
					<div role="tabpanel" class="tab-pane" id="profile">
						<ul>
							@foreach($categories->filterArrFix(['type' => 'trener'])->sortBy('order') as $category)
								<li>
									<h2>
										<a href="{{ route('treners.index', [stripos($category->slug, 'issyk-kul') ? 'issyk-kul' : \App\Models\City::getDefaultCitySlug($cities), $category->slug]) }}" class="title">{{ $category->title }}</a>
									</h2>
									@foreach($category->subcategories as $subcategory)
										<a href="{{ route('treners.indexByType', [stripos($category->slug, 'issyk-kul') ? 'issyk-kul' : \App\Models\City::getDefaultCitySlug($cities), $category->slug, $subcategory->slug]) }}">{{ $subcategory->title }} </a>
									@endforeach
								</li>
							@endforeach
						</ul>
					</div>
					<div role="tabpanel" class="tab-pane" id="messages">
						<form role="form" class="search">
							<div class="form-group">
								<input class="form-control" type="text" placeholder="Искать">
							</div>
						</form>
					</div>
				</div>


		    </div>
	    </div>
    </section>

	<footer>
		<div class="container">
			<ul>
				{{--<li><a class="ajax_modal" href="" data-action="addClub" data-type="club">Добавить место</a></li>--}}
				{{--<li><a class="ajax_modal" href="" data-action="addTrener" data-type="trener">Добавить специалиста</a></li>--}}
				{{--<li><a href="">О проекте «JeiSport»</a></li>--}}

				<li><a href="">Для организаций</a></li>
				<li><a href="">Правила работы с отзывами</a></li>
				<li><a href="">Вакансии</a></li>
				<li><a href="">Контакты</a></li>
				<li><a href="">Обратная связь</a></li>
			</ul>
			<a href="" class="add_club">Добавить организацию</a>
			<a href="" class="add_trener">Добавить специалиста</a>
		</div>
	</footer>
@endsection