@extends('layouts.app')
@section('cities-selector')
	@include('partials.mini._cities_selector')
@endsection
@section('content')

    <section class="blog-banner">
	    <img class="gaus" src="/img/jpg/blog.jpg" alt="">
	    <div class="container">
		    <div class="cover">
			    <img src="/img/png/book.png" alt="">
			    <h1>{{ $feed->title }}</h1>
			    <p>{{ $feed->sub_title }}</p>
			    <span>{{ $feed->created_at->format('d.m.Y') }}</span>
		    </div>
	    </div>
    </section>


    <section class="dashed">
	    <div class="container">
		    <div class="cover">
			    <p>{!! $feed->body !!}</p>
			    {{--<div class="border-d">--}}
				    {{--<h3>Jei Sport решил, что самое время сдать все популярные места, где казановы подыскивают себе новых--}}
					    {{--жертв.</h3>--}}
			    {{--</div>--}}
		    </div>
	    </div>
    </section>

    @for($i = 1; $i <= 4; $i++)
	    <?php $case_title = 'case_title' . $i ?>
	    <?php $case_image = 'case_image' . $i ?>
	    <?php $case_text = 'case_text' . $i ?>
	    @if($feed->{$case_title})
		    <section class="category-blog">
			    <div class="container">
				    <div class="cover">
					    <a href=""></a>
					    <img src="{{ $feed->setImage('910_470', $feed->{$case_image}) }}" alt="{{ $feed->setImage('910_470', $feed->{$case_image}) }}">
					    <h3>{{ $feed->club->title }}</h3>
					    <p>{{ $feed->club->address }}</p>
					    <p>{{ $feed->{$case_text} }}</p>
				    </div>
			    </div>
		    </section>
	    @endif
    @endfor
@endsection