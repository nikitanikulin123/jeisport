<nav class="navbar navbar-fixed-top">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
			        data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			@section('cities-selector')
				<a class="navbar-brand" href="{{ route('home') }}">
					<img src="/img/png/logo.svg" alt="">
				</a>
			@show
			@if(Auth::check())
				<ul class="auth">
					<li class="dropdown enter">
						<a class="dropdown-toggle enter" data-toggle="dropdown" role="button"
						   aria-haspopup="true" aria-expanded="false">

							{{ Auth::user()->name }}
							<span class="caret"></span>
						</a>


					</li>
				</ul>
				<ul class="dropdown-menu custom">
					@if(!Auth::user()->hasRoleFix('user'))
						<li><a href="{{ route('profiles.index') }}">Личный кабинет</a></li>
					@endif
					@if(Auth::user()->hasRoleFix('boss'))
						<li><a href="{{ route('clubs.create') }}">Добавить заведение</a></li>
						<li><a href="{{ route('treners.create') }}">Добавить специалиста</a></li>
					@endif
					<li><a href="{{ route('profiles.index', ['type' => 'favourites']) }}">Избранное</a></li>
					<li><a href="{{ url('auth/logout') }}">Выход</a></li>
				</ul>
			@else
				<ul class="auth">
					<li class="enter"><a class="enter" href="{{ url('auth/login') }}">Вход</a></li>
				</ul>
			@endif
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<form class="navbar-form navbar-left" role="search" action="{{ route('searched') }}">
				<div class="form-group">
					<input class="form-control" placeholder="Поиск..." name="query" id="srch-term" type="text" value="{{ Request::get('query') }}">
				</div>
			</form>
			<ul class="nav navbar-nav">
				@foreach($categories as $k => $category)
					<li>
						<a href="{{ route($category->type == 'club' ? 'clubs.index' : 'treners.index', [stripos($category->slug, 'issyk-kul') ? 'issyk-kul' : \App\Models\City::getDefaultCitySlug($cities), $category->slug]) }}">{{ $category->title }}</a>
					</li>
				@endforeach

			</ul>


		</div><!-- /.navbar-collapse -->

		{{--<div class="collapse navbar-collapse pluse" id="bs-example-navbar-collapse-1-plus">--}}
			{{--<a class="ajax_modal" href="" data-action="addClub" data-type="club">Добавить место</a>--}}
			{{--<a class="ajax_modal" href="" data-action="addTrener" data-type="trener">Добавить специалиста</a>--}}
			{{--<a href="">Для организаций</a>--}}
			{{--<a href="">О проекте «JeiSport»</a>--}}
			{{--<a href="">Правила работы с отзывами</a>--}}
			{{--<a href="">Обратная связь</a>--}}
			{{--<a href="">Вакансии</a>--}}
			{{--<a href="">Контакты</a>--}}
		{{--</div> <!-- /.navbar-collapse -->--}}
	</div><!-- /.container-fluid -->
</nav>