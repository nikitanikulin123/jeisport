@extends('layouts.app')
@section('assets-css')
	<link rel="stylesheet" href="{{ asset('libs/bootstrap-multiselect/css/bootstrap-multiselect.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/bootstrap-rating/bootstrap-rating.css') }}">
@endsection
@section('assets-js')
	<script src="{{ asset('libs/js/owl.carousel.js') }}"></script>
	<script src="{{ asset('libs/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
	<script src="{{ asset('js/items_updater.js') }}"></script>
	<script src="{{ asset('libs/bootstrap-rating/bootstrap-rating.min.js') }}"></script>
@endsection
@section('cities-selector')
    @include('partials.mini._cities_selector')
@endsection
@section('content')

	<script class="hidden" id="ajax-category">{{ $category->id }}</script>
	<script class="hidden" id="ajax-city">{{ $city->id }}</script>
	<script data-url="{{ route('treners.index', [$city->slug, $category->slug]) }}" data-has-type="{{ isset($filterType) ? true : false }}" class="hidden" id="ajax-baseUrl"></script>

    <section class="total">
	    <div class="container">
		    <h3 class="b-changed_items_amount">{{ $treners_count }} {{ trans_choice("специалист|специалиста|специалистов", $treners_count) }}</h3>
	    </div>
    </section>

    <section class="trener-header">
	    <div class="container">
		    <h1>{{ $seoPageTitle }}</h1>
		    @if($seo_page)
			    <p>{{ $seo_page->page_subtitle }}</p>
		    @endif
	    </div>
    </section>

    <section class="trener-index-filter">
	    <div class="container">
			<ul class="nav nav-tabs mobile-filter-map" role="tablist">
				<li class="dropdown">
					<a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
						<i class="fa fa-list-ul" aria-hidden="true"></i>
					Фильтр
					</a>
				</li>
			</ul>

			<div class="collapse" id="collapseExample">
				<div class="selects">
				@include('partials.mini._filtersList', [$type = 'filters'])
				</div>
			</div>

			{{--<div class="tab-content">--}}
				{{--<div role="tabpanel" class="tab-pane active" id="profile">--}}
					{{--<div class="selects">--}}
						{{--@include('partials.mini._filtersList', [$type = 'filters'])--}}
					{{--</div>--}}
				{{--</div>--}}
			{{--</div>--}}
	    </div>
    </section>

    <section class="trener-info -m">
	    <div class="container">
		    <div class="human">
			    @include('partials.mini._unitsWithShowMoreBlock', [$type = 'treners.index', $items = $treners])
		    </div>
	    </div>
    </section>

    <section class="treiner">
	    <div class="container">
		    <div class="human">
			    @include('partials.mini._recommendedBlock', [$type = 'treners'])
		    </div>
	    </div>
    </section>

    <div class="discript">
	    @if($seo_page)
		    <p>{{ $seo_page->page_description }}</p>
	    @endif
    </div>
@endsection