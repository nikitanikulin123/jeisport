@yield('home_banner')
<header class="@yield('header-class') @show">
    <div class="container-fluid">

        <div class="logo">
            @section('cities-selector')
                <a class="navbar-brand" href="{{ route('home') }}">
                    <img src="/img/png/logo.svg" alt="">
                </a>
            @show
        </div>

        <div class="dropdown club">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
               aria-expanded="false">{{ str_limit($currentCategory, 14) }} <span class="caret"></span></a>
            <ul class="dropdown-menu">
                @foreach($categories as $k => $category)
                    <li>
                        <a href="{{ route($category->type == 'club' ? 'clubs.index' : 'treners.index', [stripos($category->slug, 'issyk-kul') ? 'issyk-kul' : \App\Models\City::getDefaultCitySlug($cities), $category->slug]) }}"
                           class="title">{{ $category->title }}</a>
                    </li>
                @endforeach
            </ul>
        </div>

        <div class="dropdown maps">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
               aria-expanded="false">{{ $currentCity }} <span class="caret"></span></a>
            <ul class="dropdown-menu">
                @foreach($cities as $k => $city)
                    <li><a href="{{ route('cities.show', [$city->slug]) }}">{{ $city->title }}</a></li>
                @endforeach
            </ul>
        </div>

        <div class="search">
            <form class="" role="search" action="{{ route('searched') }}">
                <div class="input-group">
                    <input class="form-control" placeholder="Поиск..." name="query" id="srch-term" type="text"
                           value="{{ Request::get('query') }}">
                </div>
            </form>
        </div>

        <div class="auth">
            @if(Auth::check())
                <div class="dropdown pull-right">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-haspopup="true" aria-expanded="false">
                        {{--<img class="avatar" src="{{ asset('img/png/user-icon.png') }}">--}}
                        {{ Auth::user()->name }}
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        @if(!Auth::user()->hasRoleFix('user'))
                            <li><a href="{{ route('profiles.index') }}">Личный кабинет</a></li>
                        @endif
                        @if(Auth::user()->hasRoleFix('boss'))
                            <li><a href="{{ route('clubs.create') }}">Добавить заведение</a></li>
                            <li><a href="{{ route('treners.create') }}">Добавить специалиста</a></li>
                        @endif
                        <li><a href="{{ route('profiles.index', ['type' => 'favourites']) }}">Избранное</a></li>
                        <li><a href="{{ url('auth/logout') }}">Выход</a></li>
                    </ul>

                </div>
            @else
                <div class="no-enter"><a href="{{ url('auth/login') }}">Вход</a></div>
            @endif
        </div>

    </div>
</header>