@if(!isset($noReviews))
	<a href="#otziv" class="otziv">
		{{ $amountOfReviews }} {{ trans_choice("отзыв|отзыва|отзывов", $amountOfReviews) }}
	</a>
@endif
<div class="rating-star-box">
	<input type="hidden" class="ratingInput" data-type="{{ $type }}" data-id="{{ $item->id }}" data-set-rating="{{ $rating }}" data-avg-rating="{{ $avgRating }}"/>
</div>
@if(!isset($noReviews))
	<span class="ratings">
		{{ $amountOfRates }} {{ trans_choice("оценка|оценки|оценок", $amountOfRates) }}
	</span>

	<script type="application/json" id="setRating">
		{!! json_encode(['value' => $rating, 'avgRating' => $avgRating]) !!}
	</script>
@endif