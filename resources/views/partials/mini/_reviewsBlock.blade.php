<?php !isset($action) ? $action = null : false ?>

<li>
	<?php foreach ($reviews as $review) renderNode($review, $action, $reviewableItem, $__env);?>
	<?php function renderNode($node, $action, $reviewableItem, $__env) { ?>
		<?php $isOwner = $node->user_id && $reviewableItem->user_id === $node->user_id ? true : false ?>
		<div class="cover @if($isOwner) official_review @endif" style="@if($action == 'showNew' && $node->viewed_at == null) background-color: lightyellow; @endif text-align: left;">
		<h3>{{ $isOwner ? "Официальный ответ - {$node->user->name}" :
			($node->user_id ? $node->user->name : 'Анонимный пользователь') }}</h3>
		<span>написано {{ \Laravelrus\LocalizedCarbon\LocalizedCarbon::instance($node->created_at)->diffForHumans() }}</span>
		@if($node->parent_id && Agent::isMobile())
			<span style="display: block;">ответ на: {{ str_limit(strip_tags($node->parent->text), 40) }}</span>
		@endif
		<p>{{ $node->text }}</p>
		<div class="like-dislike">
			@if($action != 'showNew')
				@ReplaceBlock("partials.mini._reviewLikesBlock-id_{{ $node->id }}")
			@endif
			<button class="quest custom-btn" type="button" data-toggle="collapse" data-target="#write_comment_{{ $node->id }}" aria-expanded="false" aria-controls="collapseExample">Ответить</button>
		</div>
		<div class="collapse" id="write_comment_{{ $node->id }}">
			<div class="well">
				<form action="{{ route('reviews.store', [$reviewableItem->id, 'type' => $reviewableItem instanceof \App\Models\Club ?
					'club' : 'trener', 'parent_id' => $node->id]) }}" class="form-horizontal ajax_form" method="POST">
					<div class="form-group col-md-6">
						<textarea name="text" class="form-control" placeholder="Комментарий" rows="6"></textarea>
					</div>
					<div class="form-group col-md-12">
						<button type="submit" class="custom-btn">Отправить</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	@if(count($node->children) > 0)
		@if(Agent::isMobile())
			<?php foreach ($node->children->sortByDesc('created_at') as $child) renderNode($child, $action, $reviewableItem, $__env); ?>
		@else
			<ul class="under-comments">
				<li style="-webkit-padding-start: 40px;"><?php foreach ($node->children->sortByDesc('created_at') as $child) renderNode($child, $action, $reviewableItem, $__env); ?></li>
			</ul>
		@endif
	@endif
	<?php }?>
</li>