<form action="" class="@if($type == 'sortFilters') sort-form-select @else search-form-select @endif" >
	@foreach($type == 'specialists' ? $specialistsFilters : ($type == 'sortFilters' ? $sortFilters : $filters->whereLoose('active', true)->sortBy('order')) as $filter)
		<div class="pull-left @if($filter->id == $filVars['district'] || $filter->id == $filVars['street']) not-expand @endif">
			@if($filter->id == $filVars['district'] || $filter->id == $filVars['street'])
				<a class="ajax_modal" data-action="getFilterValues" data-type="filter" data-id="{{ $filter->id }}"
				   data-forItem="city" data-forItem-id="{{ $city->id }}" data-parameters="{{ isset($selected_filter_id) ? $selected_filter_id : 'filtersModal' }}">
					@include('partials.mini._filterValuesList', [$filterSelectType = 'filter-select'])
				</a>
			@else
				@include('partials.mini._filterValuesList', [$filterSelectType = 'filter-select'])
			@endif
		</div>
	@endforeach
		<div class="clearfix"></div>
</form>