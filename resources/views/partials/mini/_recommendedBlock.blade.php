@if(isset($recommendedCategories) && count($recommendedCategories))
    @if($type == 'clubs') <h3>Выбор услуг в Бишкеке</h3> @else <h3>Специалисты Бишкека</h3> @endif
    <ul class="more-treners-group">
        @foreach($recommendedCategories as $recommendedCategory)
            <li class="treners-group-item">
                <a class="url-one-units-service" href="{{ route($type == 'clubs' ? 'clubs.index' : 'treners.index', [stripos($recommendedCategory->slug, 'issyk-kul') ? 'issyk-kul' :
						\App\Models\City::getDefaultCitySlug($cities), $recommendedCategory->slug]) }}"></a>
                <div class="img-wrapper">
                    <img src="{{ $recommendedCategory->setImage('80_80', 'image') }}" alt="{{ $recommendedCategory->setImage('80_80', 'image') }}">
                </div>
                <div class="title-wrapper">
                    <h4>{{ $recommendedCategory->title }}</h4>
                    <p>{{ $recommendedCategory->amount }} {{ trans_choice($type == 'clubs' ? "место|места|мест" : "специалист|специалиста|специалистов", $recommendedCategory->amount) }}</p>
                </div>
            </li>
        @endforeach
    </ul>
@endif

@if(isset($club))
    <a href="">
        <img class="banner" src="/img/jpg/banners.jpg" alt="">
    </a>
@endif

@if(isset($recommendedFilterValues) && count($recommendedFilterValues))
    <h3>{{ $seoPageTitle }}</h3>
    <ul class="more-treners-group">
        @foreach($recommendedFilterValues as $recommendedFilterValue)
            <li class="treners-group-item">
                <a class="url-one-units-service" href="{{ route($type == 'clubs' ? 'clubs.indexByType' : 'treners.indexByType', [stripos($category->slug, 'issyk-kul') ? 'issyk-kul' :
						\App\Models\City::getDefaultCitySlug($cities), $category->slug, $recommendedFilterValue->slug]) }}"></a>
                <div class="img-wrapper">
                    <img src="{{ $recommendedFilterValue->setImage('80_80', 'image') }}" alt="{{ $recommendedFilterValue->setImage('80_80', 'image') }}">
                </div>
                <div class="title-wrapper">
                    <h4>{{ $recommendedFilterValue->title }}</h4>
                    <p>{{ $recommendedFilterValue->amount }} {{ trans_choice($type == 'clubs' ? "место|места|мест" : "специалист|специалиста|специалистов", $recommendedFilterValue->amount) }}</p>
                </div>
            </li>
        @endforeach
    </ul>
@endif


