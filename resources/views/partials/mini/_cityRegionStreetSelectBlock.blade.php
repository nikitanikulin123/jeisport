@if(Request::segment(1) == 'admin_panel')
	<?php isset($instance) && $instance->id ? $obj = $instance : $obj = null ?>
	<?php $cities = \App\Models\City::activeOrder()->get() ?>
	<?php $regions = $instance->city ? $instance->city->regions : [] ?>
	<?php $streets = $instance->city ? $instance->city->streets : [] ?>
@endif

<div class="row">
	@InputBlock([$type = "select", $item = 'city_id', $label = "Город", $list = $cities, $selected = $obj, $p = "class=\"form-control b-address_city\"", $dp = "class=\"form-group col-md-4\""])
	@InputBlock([$type = "select", $item = 'region_id', $label = "Район", $list = $regions, $selected = $obj, $p = "class=\"form-control b-address_region\"", $dp = "class=\"form-group col-md-4\""])
	<div class="col-md-4">
		@InputBlock([$type = "select", $item = 'street_id', $label = "Улица", $list = $streets, $selected = $obj, $p = "class=\"form-control b-address_street\" placeholder=\"Выберите Улицу\""])
		@if(Request::segment(1) == 'admin_panel')
			@if($instance->address)
				<label>Другая улица</label>
				<p>{{ $instance->address }}</p>
			@endif
		@else
			<i style="color: red;font-size: 14px;">*Если вы не нашли нужную вам улицу, выберите "другая"</i>
			@InputBlock([$type = "input", $item = 'address', $label = "Введите вручную", $var = $obj, $p = "class=\"form-control b-address_hidden\" placeholder=\"Введите адрес вручную\" style=\"display:none\""])
		@endif
	</div>
</div>