<?php
	!isset($block) ? $block = null : true;
	isset($img_size) ? $sizes = explode('_', $img_size) : true;
?>

<a class="url-one-unit-list @if($block != 'treners.index') url @endif" href="{{ route($item->title ? 'clubs.show' : 'treners.show', [$item->city->slug, $item->category->slug, $item->slug]) }}" data-id="{{ $item->id }}" target="_blank"></a>
@TagBlock([$type = "img", $var = $item, $src = "image", $size = isset($img_size) ? $img_size : '230_165'])

<div class="title-wrapper">
    <h2>{{ $item instanceof \App\Models\Club ? $item->title : $item->FullName}}</h2>

	@if(!(Agent::isMobile() && $block == 'clubs.index'))
		<div class="b-filter_options_with_commas">
			<a href="{{ route($item->title ? 'clubs.index' : 'treners.index', [stripos($item->category->slug, 'issyk-kul') ? 'issyk-kul' : $item->city->slug, $item->category->slug]) }}">{{ $item->category->title }}, </a>
		    @foreach($item->filterValues->filterArrFix('id', '!=', [$filVars['male'], $filVars['female']]) as $filterValue)
		        <a href="{{ route($item->title ? 'clubs.indexByType' : 'treners.indexByType', [stripos($item->category->slug, 'issyk-kul') ? 'issyk-kul' : $item->city->slug, $item->category->slug, $filterValue->slug]) }}">{{ trim(mb_strtolower($filterValue->title)) . ', ' }}</a>
		    @endforeach
		</div>
	@endif

    <p>{{ str_limit($item->FullAddress, 40) }}</p>

    <div class="rating-info">
        <?php $amountOfReviews = $item->reviews->filterArrFix(['moderated' => true, 'parent_id' => null])->count() ?>
        <span class="otziv">{{ $amountOfReviews }} {{ trans_choice("отзыв|отзыва|отзывов", $amountOfReviews) }} </span>
        <div class="w-100"><span class="rat-t"><span class="rat" style="width:{{ $item->ratings->count() > 0 ? $item->ratings->sum('value') * 2/$item->ratings->count() : 0 }}0%; "></span></span></div>
        <span class="like">{{ $item->ratings->count() }} {{ trans_choice("оценка|оценки|оценок", $item->ratings->count()) }}</span>
    </div>
    @if(!Agent::isMobile() && $block != 'treners.index' && $block != 'clubs.specialists')
        <div class="b-textarea-ckeditor">
            <p>{{ str_limit(strip_tags(trim(preg_replace('/\s+/', ' ', $item->description))), 70) }}</p>
        </div>
    @endif

    @if(($block == 'clubs.createEdit' || $block == 'treners.createEdit') && (!isset($type) || $type != 'favourites') && $item->user_id == Auth::id())
		@if(Auth::user()->hasRoleFix('boss'))
	        <div class="clearfix"></div>
	        <a href="" class="delete ajax_modal" data-action="delete" data-type="club" data-id="{{ $item->id }}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
		@endif
        <a href="{{ route($item->title ? 'clubs.edit' : 'treners.edit', $item->slug) }}" class="edit">
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </a>
    @endif

</div>
