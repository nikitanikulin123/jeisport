<?php isset($checkedOptions) && $checkedOptions ? $checkedOptions : $checkedOptions = Request::get($filter->id, []) ?>

<select name="{{ $filter->id }}[]" @if($filter->id != $filVars['sort'] )multiple="multiple" @endif
	class="@if($filterSelectType == 'ajax-multiSelect') ajax-multiSelect @else filter-select @endif
			@if($filter->id == $filVars['district']) b-regions @elseif($filter->id == $filVars['street']) b-streets @endif" data-title="{{ $filter->title }}">
	<optgroup label="{{ $filter->title }}"></optgroup>
	<?php $resetButton = 0 ?>
	@if($type == 'sortFilters')
		<?php $filter->id == $filVars['district'] ? $options = \App\Models\Region::activeOrder()->where('city_id', $city->id)->get()->sortBy('title') :
				($filter->id == $filVars['street'] ? $options = \App\Models\Street::activeOrder()->where('city_id', $city->id)->get()->sortBy('title') :
						$options = $filter->options->whereLoose('active', true)) ?>
		@foreach($options as $option)
			<option @if(in_array($option->id, $checkedOptions) ||
				$filterType == 'district' && $option instanceof \App\Models\Region && $selected_filter_id == $option->id ||
				$filterType == 'street' && $option instanceof \App\Models\Street && $selected_filter_id == $option->id) selected <?php $resetButton = $resetButton + 1 ?>
			@endif value="{{ $option->id }}">{{ $option->title }}
			</option>
		@endforeach
	@else
		@foreach($filter->options->whereLoose('active', true) as $option)
			@if($type != 'specialists' || array_get($specialistsFilterValuesArr, $option->id))
				<option @if(in_array($option->id, $checkedOptions) ||
					($filterType != 'district' && $filterType != 'street' && $selected_filter_id == $option->id)) selected <?php $resetButton = $resetButton + 1 ?>
				@endif value="{{ $option->id }}">{{ $option->title }}
				</option>
			@endif
		@endforeach
	@endif
</select>

@if(!Request::is('*getModalContent'))
	<button class="btn btn-default b-reset-filters close-btn" style="display: @if($resetButton) inline-block @else none @endif">X</button>
@endif
