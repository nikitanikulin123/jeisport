<div id="page-preloader"><span class="spinner"></span></div>
{{--<div id="page-preloader"><img src="{{ public_path('img/pre_loading.gif') }}" alt=""></div>--}}
<div class="modal-dialog
	@if($type == 'review' && $action == 'showReview') history-user-reviews-modal
	@elseif($type == 'stock' && $action == 'show') action-modal
	@elseif($type == 'filter' && $action == 'getFilterValues') select-change
	@endif"
 role="document">
	<div class="modal-content">

		@if($type == 'club' && $action == 'addClub')
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Добавление места</h4>
			</div>
			<div class="modal-body">
				{!! Form::open(['route' => 'clubs.newClubStore', 'enctype' => 'multipart/form-data', 'id' => 'club_upload', 'class' => 'form-horizontal ajax_form']) !!}
					@InputBlock([$type = "input", $item = 'title', $label = "Название", $sdp = "class=\"col-sm-8\"", $lp = "class=\"col-sm-4 control-label\""])
					@InputBlock([$type = "input", $item = 'phone', $label = "Телефон", $sdp = "class=\"col-sm-8\"", $lp = "class=\"col-sm-4 control-label\""])
					@InputBlock([$type = "textarea", $item = 'description', $label = "Описание", $cols = 30, $rows = 5, $sdp = "class=\"col-sm-8\"", $lp = "class=\"col-sm-4 control-label\""])
					@InputBlock([$type = "select", $item = 'category_id', $label = "Категория", $list = \App\Models\Category::whereType('club')->get(),
						$sdp = "class=\"col-sm-8\"", $lp = "class=\"col-sm-4 control-label\"", $p = "placeholder=\"Выберите Категорию\""])
					@InputBlock([$type = "select", $item = 'city_id', $label = "Город", $list = \App\Models\City::all(),
						$sdp = "class=\"col-sm-8\"", $lp = "class=\"col-sm-4 control-label\"", $p = "placeholder=\"Выберите Город\""])
					@InputBlock([$type = "input", $item = 'site', $label = "Сайт (если есть)", $sdp = "class=\"col-sm-8\"", $lp = "class=\"col-sm-4 control-label\"", $p = "placeholder_=\"Введите Сайт\""])
					@InputBlock([$type = "input", $item = 'email', $label = "Ваша эл. почта", $sdp = "class=\"col-sm-8\"", $lp = "class=\"col-sm-4 control-label\"", $p = "placeholder_=\"Введите Вашу эл. почту\""])
					<div class="modal-footer">
						<button type="submit" class="custom-btn">Добавить</button>
						<button type="button" class="custom-btn" data-dismiss="modal" style="background-color: #d64848;">Отмена</button>
					</div>
				{!! Form::close() !!}
			</div>
		@endif

		@if($type == 'trener' && $action == 'addTrener')
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Добавление специалиста</h4>
			</div>
			<div class="modal-body">
				{!! Form::open(['route' => 'treners.newTrenerStore', 'enctype' => 'multipart/form-data', 'id' => 'trener_upload', 'class' => 'form-horizontal ajax_form']) !!}
					@InputBlock([$type = "input", $item = 'sname', $label = "Фамилия", $sdp = "class=\"col-sm-8\"", $lp = "class=\"col-sm-4 control-label\""])
					@InputBlock([$type = "input", $item = 'name', $label = "Имя", $sdp = "class=\"col-sm-8\"", $lp = "class=\"col-sm-4 control-label\""])
					@InputBlock([$type = "input", $item = 'mname', $label = "Отчество", $sdp = "class=\"col-sm-8\"", $lp = "class=\"col-sm-4 control-label\""])
					@InputBlock([$type = "input", $item = 'phone', $label = "Телефон", $sdp = "class=\"col-sm-8\"", $lp = "class=\"col-sm-4 control-label\""])
					@InputBlock([$type = "textarea", $item = 'description', $label = "Описание", $cols = 30, $rows = 5, $sdp = "class=\"col-sm-8\"", $lp = "class=\"col-sm-4 control-label\""])
					@InputBlock([$type = "select", $item = 'category_id', $label = "Категория", $list = \App\Models\Category::whereType('trener')->get(),
						$sdp = "class=\"col-sm-8\"", $lp = "class=\"col-sm-4 control-label\"", $p = "placeholder=\"Выберите Категорию\""])
					@InputBlock([$type = "select", $item = 'city_id', $label = "Город", $list = \App\Models\City::all(),
						$sdp = "class=\"col-sm-8\"", $lp = "class=\"col-sm-4 control-label\"", $p = "placeholder=\"Выберите Город\""])
					@InputBlock([$type = "input", $item = 'site', $label = "Сайт (если есть)", $sdp = "class=\"col-sm-8\"", $lp = "class=\"col-sm-4 control-label\"", $p = "placeholder_=\"Введите Сайт\""])
					@InputBlock([$type = "input", $item = 'email', $label = "Ваша эл. почта", $sdp = "class=\"col-sm-8\"", $lp = "class=\"col-sm-4 control-label\"", $p = "placeholder_=\"Введите Вашу эл. почту\""])
					<div class="modal-footer">
						<button type="submit" class="custom-btn">Добавить</button>
						<button type="button" class="custom-btn" data-dismiss="modal" style="background-color: #d64848;">Отмена</button>
					</div>
				{!! Form::close() !!}
			</div>
		@endif

		@if($type == 'stock' && ($action == 'create' || $action == 'edit'))
			<?php isset($item) ? $stock = $item : $stock = null ?>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">@if(isset($stock)) <span>{{ $stock->title3 }} @else <i>Новая акция </i>для "{{ $forItem->title }}"</span>@endif</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::open(['route' => isset($stock) ? ['stocks.update', $stock->id, 'club_id' => $forItem->id] : ['stocks.store', 'club_id' => $forItem->id], 'method' => 'POST', 'class' => 'ajax_form']) !!}
						<div class="row">
							<div class="col-md-6" style="text-align: left">
								@InputBlock([$type = "imageUpload", $item = 'image', $label = "Изображение", $var = $stock])
							</div>
							<div class="col-md-6" style="text-align: left">
								@InputBlock([$type = "input", $item = 'title3', $label = "Заголовок", $var = $stock])
								@InputBlock([$type = "input", $item = 'title4', $label = "Подзаголовок", $var = $stock])
								@InputBlock([$type = "textarea", $item = 'text', $label = "Описание", $var = $stock, $p = "class=\"form-control ckeditor\""])
							</div>
						</div>
						<button type="submit" class="btn btn-default pull-right">Сохранить</button>
					{!! Form::close() !!}
				</div>
			</div>

			<script src="{{ asset('js/init.js') }}"></script>
			<script src="{{ asset('libs/js/flow.min.js') }}"></script>
		@endif

		@if($type == 'stock' && $action == 'show')
			<?php $stock = $item ?>
			<div class="cover">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="discript">
					<h3>{{ $stock->title3 }}</h3>
					<p>{{ $stock->text }}</p>
				</div>
				<div class="address-wrapper">
					@include('clubs.partials._addressBlock', [$item = $forItem])
					<a class="tel" href="tel:{{ $forItem->phone }}">
						<i class="glyphicon glyphicon-phone"></i>
						{{ $forItem->phone }}
					</a>
				</div>
				@if($stock->image)
					@TagBlock([$type = "img", $var = $stock, $src = "image", $size = "898_380", $p = "class=\"img-resposnsive\""])
				@endif
				<div class="maps" id="stock-map" data-map-lat="{{ $stock->stockable->lat ? $stock->stockable->lat : '' }}" data-map-lng="{{ $stock->stockable->lng ? $stock->stockable->lng : '' }}" style="height: 300px;width: 100%;"></div>
			</div>

			<script> $(function () {getStockMap()}); </script>
		@endif

		@if($type == 'priceListCategory' && ($action == 'create' || $action == 'edit'))
			<?php isset($item) ? $priceListCategory = $item : $priceListCategory = null ?>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">@if(isset($priceListCategory)) <span>{{ $priceListCategory->title }} @else Новый прайслист для "{{ $forItem->title }}"</span>@endif</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::open(['route' => isset($priceListCategory) ? ['priceListCategories.update', $priceListCategory->id, 'club_id' => $forItem->id] : ['priceListCategories.store', 'club_id' => $forItem->id], 'method' => 'POST', 'class' => 'ajax_form']) !!}
						<div class="row">
							<div class="col-md-6" style="width: 100%">
								@InputBlock([$type = "input", $item = 'title', $label = "Заголовок1", $var = $priceListCategory])
								@InputBlock([$type = "fileUpload", $item = 'file', $label = "Файл текущего прайслиста", $var = $priceListCategory])
							</div>
						</div>
						<button type="submit" class="btn btn-default pull-right">Сохранить</button>
					{!! Form::close() !!}
				</div>
			</div>

			<script src="{{ asset('js/init.js') }}"></script>
			<script src="{{ asset('libs/js/flow.min.js') }}"></script>
		@endif

		@if($type == 'priceList' && ($action == 'create' || $action == 'edit'))
			<?php isset($item) ? $priceList = $item : $priceList = null ?>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">@if(isset($priceList)) <span>{{ $priceList->title }} @else Новый кейс для "{{ $forItem->title }}"</span>@endif</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::open(['route' => isset($priceList) ? ['priceLists.update', $priceList->id, 'priceListCategory_id' => $forItem->id] : ['priceLists.store', 'priceListCategory_id' => $forItem->id], 'method' => 'POST', 'class' => 'ajax_form']) !!}
						<div class="row">
							<div class="col-md-6" style="width: 100%">
								@InputBlock([$type = "input", $item = 'title', $label = "Заголовок", $var = $priceList])
								@InputBlock([$type = "input", $item = 'price', $label = "Стоимость", $var = $priceList])
								@InputBlock([$type = "select", $item = 'currency_id', $label = "Валюта", $list = \App\Models\Currency::all(),
									$selected = $priceList, $p = "placeholder=\"Выберите Валюту\""])
								@InputBlock([$type = "input", $item = 'price_for', $label = "Единица измерения", $var = $priceList])
							</div>
						</div>
						<button type="submit" class="btn btn-default pull-right">Сохранить</button>
					{!! Form::close() !!}
				</div>
			</div>
		@endif

		@if($type == 'priceList' && $action == 'show')
			<?php $priceList = $item ?>
			<div class="cover">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				@TagBlock([$type = "img", $var = $forItem, $src = "image", $size = "60_60"])
				<div class="discript">
					<h3>{{ $forItem->prefix ? $forItem->prefix : '' }} {{ $forItem->title }} {{ $forItem->postfix ? $forItem->postfix : '' }}</h3>
				</div>
				<div class="adress">
					@include('clubs.partials._addressBlock', [$item = $forItem])
					<h3>
						<a href="tel:{{ $forItem->phone }}">
							<i class="glyphicon glyphicon-phone"></i>
							{{ $forItem->phone }}
						</a>
					</h3>
				</div>
				<div class="rating-info">
					<?php $amountOfReviews = $forItem->reviews->filterArrFix(['moderated' => true, 'parent_id' => null])->count() ?>
					<span class="otziv">{{ $amountOfReviews }} {{ trans_choice("отзыв|отзыва|отзывов", $amountOfReviews) }} </span>
					<div class="w-100"><span class="rat-t"><span class="rat" style="width:{{ $forItem->ratings->count() > 0 ? $forItem->ratings->sum('value') * 2/$forItem->ratings->count() : 0 }}0%; "></span></span></div>
					<span class="like">{{ $forItem->ratings->count() }} {{ trans_choice("оценка|оценки|оценок", $forItem->ratings->count()) }}</span>
				</div>
				<div>
					<p>{{ $priceList->priceListCategory->title }}---></p>
					<p>{{ $priceList->title }}</p>
					<p>{{ $priceList->price }} {{ $priceList->currency->title }} - {{ $priceList->price_for }}</p>
				</div>
			</div>
		@endif

		@if($type == 'club' && $action == 'uploadPriceList')
			<?php isset($item) ? $club = $item : $club = null ?>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Загрузить файл прайслиста</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					{!! Form::open(['route' => ['clubs.update', $item->id, 'step' => 'uploadPriceList'], 'method' => 'POST', 'class' => 'ajax_form']) !!}
						<div class="row">
							<div class="col-md-6" style="text-align: left">
								@InputBlock([$type = "fileUpload", $item = 'p_file', $label = "Файл прайслиста", $var = $club])
							</div>
						</div>
						<button type="submit" class="btn btn-default pull-right">Сохранить</button>
					{!! Form::close() !!}
				</div>
			</div>

			<script src="{{ asset('js/init.js') }}"></script>
			<script src="{{ asset('libs/js/flow.min.js') }}"></script>
		@endif

		@if($type == 'review' && $action == 'showReview')
			<?php $allReviews = \App\Models\Review::whereId($item->id)->first()->DescendantsAndSelf()->with(['reviewable', 'user', 'parent'])->get() ?>
			<?php $reviews = $allReviews->toHierarchy() ?>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h2>Отзывы</h2>
							{!! Form::open(['route' => ['reviews.store', $forItem->id, 'type' => 'club', 'parent_id' => null], 'method' => 'POST', 'class' => 'form-horizontal comments_form']) !!}
								<div class="form-group col-md-12">
									<textarea name="text" class="form-control" placeholder="Комментарий" rows="6"></textarea>
								</div>
								<div class="form-group col-md-12">
									<button type="submit" class="custom-btn btn btn-default">Отправить</button>
								</div>
							{!! Form::close() !!}
					<ul class="comments">
						@include('partials.mini._reviewsBlock', [$action = 'showNew'])
					</ul>

			@foreach($allReviews as $checkedReview)
				@if($checkedReview->viewed_at == null)
					<?php $checkedReview->update(['viewed_at' => \Carbon\Carbon::now()]) ?>
				@endif
			@endforeach
		@endif

		@if($type == 'filter' && $action == 'getFilterValues')
			<?php parse_str(urldecode($parameters), $parameters) ?>

			{!! Form::open() !!}
				@include('partials.mini._filterValuesList', [$filterSelectType = 'ajax-multiSelect', $type = 'sortFilters', $filterType = null,
					$filter = $item, $city = $forItem, $filVars = \App\Models\Vars::specialFilterVarsByIds(), $checkedOptions = array_get($parameters, $item->id)])
				<a href="" class="custom-btn btn btn-default pull-right b-filter-apply" data-filter-id="{{ $item->id }}">Применить</a>
			{!! Form::close() !!}

			<link rel="stylesheet" href="{{ asset('libs/jquery-multiselect/css/multi-select.css') }}">
			<script src="{{ asset('libs/jquery-multiselect/js/jquery.multi-select.js') }}"></script>
			<script src="{{ asset('libs/js/jquery.quicksearch.js') }}"></script>
			<script src="{{ asset('js/initJQueryMultiSelect.js') }}"></script>
		@endif

		@if($type == 'map' && $action == 'show')
			<div class="cover">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="maps" id="stock-map" data-map-obj="{{ isset($parameters) ? $parameters : '' }}" style="height: 300px;width: 100%;"></div>
			</div>

			<script> $(function () {getStockMap()}); </script>
		@endif

		@if($action == 'delete')

			<?php $type == 'priceListCategory' ? $route = 'priceListCategorie' : $route = $type ?>
			<?php $text = [
					'priceListCategory' => 'прайслист',
					'priceList' => 'кейс прайслиста',
					'callback' => 'заявку на обратный звонок',
				] ?>

			<div class="modal-header del-modal-text-lk">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Вы уверены, что хотите удалить {{ array_get($text, $type) }} безвозвратно?</h4>
			</div>
			{!! Form::open(['route' => [$route . 's.delete', $item->id], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'response_form']) !!}
				<div class="modal-footer del-modal-buttons-lk">
					<button type="submit" class="btn btn-primary send_response">Удалить</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
				</div>
			{!! Form::close() !!}
		@endif

		@if($action == 'toFavourites')
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				@if($type == 'club')
					<h4 class="modal-title">@if($item->favourites->where('user_id', Auth::id())->first()) Заведение добавлено в "Избранное" @else Заведение удалено из "Избранного"@endif</h4>
				@elseif($type == 'trener')
					<h4 class="modal-title">@if($item->favourites->where('user_id', Auth::id())->first()) Специалист добавлен в "Избранное" @else Специалист удален из "Избранного"@endif</h4>
				@endif
			</div>
		@endif
	</div>
</div>