<?php $page = 1; $dashedType = str_replace(["."], '_', $type); ?>

<div class="clearfix"></div>
@if($type === 'clubs.reviews')
	@ReplaceBlock("partials.mini._unitsListFiltered-id_{{ $dashedType }}")
@else
	<ul class="b-units-list @if($type === 'clubs.createEdit' || $type === 'treners.createEdit') m-zavedenie @endif">
		@ReplaceBlock("partials.mini._unitsListFiltered-id_{{ $dashedType }}")
	</ul>
@endif
<div class="clearfix"></div>
@ReplaceBlock("partials.mini._showMoreBlock-id_{{ $dashedType }}")
