<div class="ballon">
    <div class="left">
        <div class="img-wrapper">
            <img src="{{ $club->setImage('160_115', 'image') }}" alt="{{ $club->setImage('160_115', 'image') }}" class="">
        </div>
        <p><a href="{{ route('clubs.show', [$club->city->slug, $club->category->slug, $club->slug]) }}">{{ $club->photos->count() }} фото</a></p>
    </div>
    <div class="right">
        <div class="title">
            <h2>
                <a href="{{ route('clubs.show', [$club->city->slug, $club->category->slug, $club->slug]) }}">{{ $club->title }}</a>
            </h2>
            <p class="revies"><a href="{{ route('clubs.show', [$club->city->slug, $club->category->slug, $club->slug]) }}" class="revies">{{ $club->reviews->count() }} {{ trans_choice('отзыв|отзыва|отзывов', $club->reviews->count()) }}</a></p>
            <ul>
                <li class="star"></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
            <div class="clearfix"></div>
            <p class="city">{{ $club->address }}</p>
            <p class="phone"><a href="tel:{{ $club->phone }}" class="phone">{{ $club->phone }}</a></p>
            <a href="{{ route('clubs.show', [$club->city->slug, $club->category->slug, $club->slug]) }}" class="seemore">Подробнее</a>
        </div>
    </div>
</div>
