{!! Form::open(['route' => ['callbacks.create', $club->id, 'type' => 'club'], 'class' => 'form-inline club ajax_form']) !!}
	@InputBlock([$type = "input", $item = 'name', $label = null, $var = $club, $p = "placeholder=\"Введите ваше имя\""])
	@InputBlock([$type = "input", $item = 'callback_phone', $label = null, $var = $club, $p = "placeholder=\"Введите телефон\""])
	<button type="submit" class="btn btn-default">Заказать обратный звонок</button>
{!! Form::close() !!}