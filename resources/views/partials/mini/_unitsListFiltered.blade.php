<?php $type == 'clubs.createEdit' || $type == 'treners.createEdit' ? $img_size = '160_115' : $img_size = '132_132' ?>

@if($type === 'clubs.reviews' || $type === 'treners.reviews')
	@include('partials.mini._reviewsBlock', [$reviewableItem = $type === 'clubs.reviews' ? $club : $trener])
@else
	@forelse($items ? $items->take($page * env('CONFIG_PAGINATE', 1)) : new \App\Custom\CustomCollection() as $filteredItem)
		<li @if($type == 'clubs.specialists') style="float: left;width: 50%;margin-bottom: 60px;" @endif>
			@include('partials.mini._unitsList', [$item = $filteredItem, $block = $type])
		</li>
	@empty
		@if($type == 'clubs.createEdit')
			<li class="no-items"><h3>Не добавлено заведений</h3></li>
		@elseif($type == 'treners.createEdit')
			<li class="no-items"><h3>Не добавлено специалистов</h3></li>
		@elseif(isset($points))
			<li class="no-items"><h3>Не найдено заведений на заданном участке карты</h3></li>
		@else
			<li class="no-items"><h3>Не найдено специалистов</h3></li>
		@endif
	@endforelse
@endif

<script type="application/json" id="commonInfo">
	{!! json_encode(['type' => $type]) !!}
</script>
@if(isset($points))
	<script type="application/json" id="points">
		{!! json_encode($points) !!}
	</script>
@endif
@if(isset($resultedFilteredArr))
	<script type="application/json" id="filters">
		{!! json_encode($resultedFilteredArr) !!}
	</script>
@endif