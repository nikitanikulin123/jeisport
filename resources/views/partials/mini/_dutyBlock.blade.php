<h3>График работы</h3>
<div class="form-groups form-group @if(isset($instance)) b-admin_panel @endif">
	@InputBlock([$type = "radio", $item = 'set_schedule', $label = 'Будние дни с', $value = 1, $var = null, $checked = true])
	@InputBlock([$type = "radio", $item = 'set_schedule', $label = 'Будние и выходные дни с', $value = 2, $var = null, $checked = false])
	@InputBlock([$type = "radio", $item = 'set_schedule', $label = 'Круглосуточно', $value = 3, $var = null, $checked = false])
</div>
<?php $classForStaticDiv = isset($instance) ? 'form-groups form-group datepicker input-group' : 'form-groups form-group' ?>
@InputBlock([$type = "inputTime", $item = 'schedule_time_from', $label = null, $value = "09:00", $dp = "class=\"{$classForStaticDiv}\"",
	$sdp = "class=\"b-timepicker\"", $p = "class=\"form-control schedule_time_from add-on\""])
@InputBlock([$type = "inputTime", $item = 'schedule_time_to', $label = null, $value = "18:00", $dp = "class=\"{$classForStaticDiv}\"",
	$sdp = "class=\"b-timepicker\"", $p = "class=\"form-control schedule_time_to add-on\""])
<a class="custom-btn btn btn-default set_schedule" href="" data-action="create" data-type="stock" data-forItem="club"
   data-forItem-id="{{ !isset($instance) ? $obj->id : '' }}">Применить</a>

@if(isset($instance))
	<script src="{{ asset('js/custom_functions.js') }}"></script>
	<script src="{{ asset('libs/js/jquery.maskedinput.min.js') }}"></script>
	<script src="{{ asset('js/custom.js') }}"></script>
@endif