<div class="col-md-6">
	<h3>Будние дни</h3>
	<div class="col-md-6">
		@InputBlock([$type = "checkbox", $item = 'work_in_mon', $label = 'Понедельник', $var = $obj, $lp = "class=\"control control--checkbox\""])
		@InputBlock([$type = "inputTime", $item = 'mon_time_from', $label = null, $var = $obj, $dp = "class=\"form-group date time-from\"", $sdp = "class=\"b-timepicker\"", $p = "class=\"form-control add-on\""])
		@InputBlock([$type = "inputTime", $item = 'mon_time_to', $label = null, $var = $obj, $dp = "class=\"form-group date time-to\"", $sdp = "class=\"b-timepicker\"", $p = "class=\"form-control add-on\"", $to = true])
	</div>
	<div class="col-md-6">
		@InputBlock([$type = "checkbox", $item = 'work_in_tue', $label = 'Вторник', $var = $obj, $lp = "class=\"control control--checkbox\""])
		@InputBlock([$type = "inputTime", $item = 'tue_time_from', $label = null, $var = $obj, $dp = "class=\"form-group date time-from\"", $sdp = "class=\"b-timepicker\"", $p = "class=\"form-control add-on\""])
		@InputBlock([$type = "inputTime", $item = 'tue_time_to', $label = null, $var = $obj, $dp = "class=\"form-group date time-to\"", $sdp = "class=\"b-timepicker\"", $p = "class=\"form-control add-on\"", $to = true])
	</div>
	<div class="col-md-6">
		@InputBlock([$type = "checkbox", $item = 'work_in_wed', $label = 'Среда', $var = $obj, $lp = "class=\"control control--checkbox\""])
		@InputBlock([$type = "inputTime", $item = 'wed_time_from', $label = null, $var = $obj, $dp = "class=\"form-group date time-from\"", $sdp = "class=\"b-timepicker\"", $p = "class=\"form-control add-on\""])
		@InputBlock([$type = "inputTime", $item = 'wed_time_to', $label = null, $var = $obj, $dp = "class=\"form-group date time-to\"", $sdp = "class=\"b-timepicker\"", $p = "class=\"form-control add-on\"", $to = true])
	</div>
	<div class="col-md-6">
		@InputBlock([$type = "checkbox", $item = 'work_in_thu', $label = 'Четверг', $var = $obj, $lp = "class=\"control control--checkbox\""])
		@InputBlock([$type = "inputTime", $item = 'thu_time_from', $label = null, $var = $obj, $dp = "class=\"form-group date time-from\"", $sdp = "class=\"b-timepicker\"", $p = "class=\"form-control add-on\""])
		@InputBlock([$type = "inputTime", $item = 'thu_time_to', $label = null, $var = $obj, $dp = "class=\"form-group date time-to\"", $sdp = "class=\"b-timepicker\"", $p = "class=\"form-control add-on\"", $to = true])
	</div>
	<div class="col-md-6">
		@InputBlock([$type = "checkbox", $item = 'work_in_fri', $label = 'Пятница', $var = $obj, $lp = "class=\"control control--checkbox\""])
		@InputBlock([$type = "inputTime", $item = 'fri_time_from', $label = null, $var = $obj, $dp = "class=\"form-group date time-from\"", $sdp = "class=\"b-timepicker\"", $p = "class=\"form-control add-on\""])
		@InputBlock([$type = "inputTime", $item = 'fri_time_to', $label = null, $var = $obj, $dp = "class=\"form-group date time-to\"", $sdp = "class=\"b-timepicker\"", $p = "class=\"form-control add-on\"", $to = true])
	</div>
</div>
<div class="col-md-6">
	<h3>Выходные</h3>
	<div class="col-md-6">
		@InputBlock([$type = "checkbox", $item = 'work_in_sat', $label = 'Суббота', $var = $obj, $lp = "class=\"control control--checkbox\""])
		@InputBlock([$type = "inputTime", $item = 'sat_time_from', $label = null, $var = $obj, $dp = "class=\"form-group date time-from\"", $sdp = "class=\"b-timepicker\"", $p = "class=\"form-control add-on\""])
		@InputBlock([$type = "inputTime", $item = 'sat_time_to', $label = null, $var = $obj, $dp = "class=\"form-group date time-to\"", $sdp = "class=\"b-timepicker\"", $p = "class=\"form-control add-on\"", $to = true])
	</div>
	<div class="col-md-6">
		@InputBlock([$type = "checkbox", $item = 'work_in_sun', $label = 'Воскресенье', $var = $obj, $lp = "class=\"control control--checkbox\""])
		@InputBlock([$type = "inputTime", $item = 'sun_time_from', $label = null, $var = $obj, $dp = "class=\"form-group date time-from\"", $sdp = "class=\"b-timepicker\"", $p = "class=\"form-control add-on\""])
		@InputBlock([$type = "inputTime", $item = 'sun_time_to', $label = null, $var = $obj, $dp = "class=\"form-group date time-to\"", $sdp = "class=\"b-timepicker\"", $p = "class=\"form-control add-on\"", $to = true])
	</div>
</div>