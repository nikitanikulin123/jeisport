<div class="search-link">
    <div class="container">
        <a href="{{ route('home') }}" class="logo"><img src="/img/png/logo.png" alt=""></a>
        <span class="search-place">Места и услуги </span>
        <span class="search-city">в Бишкеке </span>

        <form action="" class="search-form-input">
            <input type="text" placeholder="Поиск">
        </form>
        @if(Auth::check())
            <span class=""><a href="{{ route('profile.show') }}">{{ Auth::user()->name }}</a></span>
            <a href="/auth/logout" class="logout">Выход</a>
        @else
            <a class="login">Вход</a>
        @endif
    </div>
</div>