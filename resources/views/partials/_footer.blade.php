@if(!Agent::isMobile())
	<footer>
	    <div class="container">
	        <div class="row">

	            <div class="col-md-1">
	                <a href="">
						<img src="/img/png/logo.svg" alt="">
	                </a>
	            </div>

	            <div class="col-md-offset-2 col-md-3">
		            <p><a class="ajax_modal" href="" data-action="addClub" data-type="club">Добавить место</a></p>
		            <p><a class="ajax_modal" href="" data-action="addTrener" data-type="trener">Добавить специалиста</a></p>
	                <p><a href="">Для организаций</a></p>
	                <p><a href="{{ route('about') }}">О проекте «JS.kg»</a></p>
	            </div>

	            <div class="col-md-3">
	                <p><a href="">Правила работы с отзывами</a></p>
	                <p><a href="">Обратная связь</a></p>
	                <p><a href="">Вакансии</a></p>
	                <p><a href="{{ route('contacts') }}">Контакты</a></p>
	            </div>

	            <div class="col-md-3">
	                <h6>Мы в соц сетях</h6>
					<a href=""><img src="/img/png/fb.png" alt=""></a>
					<a href=""><img src="/img/png/gl.png" alt=""></a>
					<a href=""><img src="/img/png/in.png" alt=""></a>
					<a href=""><img src="/img/png/ins.png" alt=""></a>
					<a href=""><img src="/img/png/tv.png" alt=""></a>
					<a href=""><img src="/img/png/tw.png" alt=""></a>
					<a href=""><img src="/img/png/vb.png" alt=""></a>
					<a href=""><img src="/img/png/wifi.png" alt=""></a>
	            </div>
	        </div>
	    </div>
	</footer>
@endif

@if(env('APP_METRICS') === null || env('APP_METRICS') !== false)
    {!! $adminVars->snipets !!}
@endif

<script src="{{ asset('libs/js/jquery.min.js') }}"></script>
<script src="{{ asset('libs/js/bootstrap.js') }}"></script>
<script src="{{ asset('libs/jquery-bbq/jquery.ba-bbq.min.js') }}"></script>
{{--<script src="{{ asset('libs/slick/slick.min.js') }}"></script>--}}
<script src="{{ asset('libs/js/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('js/custom_functions.js') }}"></script>
<script src="{{ asset('libs/alertifyjs/js/alertify.min.js') }}"></script>
@yield('assets-js')
<script src="{{ asset('js/common.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>