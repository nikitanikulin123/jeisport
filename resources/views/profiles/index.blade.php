@extends('layouts.app')
@section('assets-css')
    <link rel="stylesheet" href="{{ asset('libs/colorbox/colorbox.css') }}">
@endsection
@section('assets-js')
    <script src="{{ asset('libs/colorbox/jquery.colorbox-min.js') }}"></script>
    <script src="{{ asset('libs/admin/js/init-colorbox.js') }}"></script>
    <script src="{{ asset('libs/highcharts/js/highcharts.js') }}"></script>
    <script src="{{ asset('libs/highcharts/modules/exporting.js') }}"></script>
    <script src="{{ asset('js/items_updater.js') }}"></script>
@endsection
@section('content')

    <section class="history-user">
        <div class="container-fluid">
            <div class="content">
                <ul class="tab-unit">
                    @if($type == 'favourites')
                        <li class="active">
                            <a href="">
                                <i class="fa fa-user" aria-hidden="true"></i>Избранное
                            </a>
                        </li>
                    @else
                        <li class="@if(!Request::get('type') || Request::get('type') == 'my_company')active @endif">
                            <a href="{{ route('profiles.index', ['type' => 'my_company']) }}">
                                <i class="fa fa-user" aria-hidden="true"></i>
	                            @if(Auth::user()->hasRoleFix('boss')) Моя компания @else Мои специалисты @endif
                            </a>
                        </li>
                        @if(Auth::user()->hasRoleFix('boss'))
                            <li class="@if(Request::get('type') == 'reports')active @endif">
                                <a href="{{ route('profiles.index', ['type' => 'reports']) }}">
                                    <i class="fa fa-pie-chart" aria-hidden="true"></i>Отчеты
                                </a>
                            </li>
                        @endif
                        {{--<li class="@if(Request::get('type') == 'payments')active @endif">--}}
                            {{--<a href="{{ route('profiles.index', ['type' => 'payments']) }}">--}}
                                {{--<i class="fa fa-pie-chart" aria-hidden="true"></i>Платежи--}}
                            {{--</a>--}}
                        {{--</li>--}}

                        {{--<li>--}}
                        {{--<a href="{{ route('profiles.index', ['type' => 'favourites']) }}">--}}
                        {{--<i class="fa fa-info" aria-hidden="true"></i>Помощь и поддержка--}}
                        {{--</a>--}}
                        {{--</li>--}}
                    @endif
                </ul>

                @if($type == null || $type == 'my_company' || $type == 'favourites')
                    <div class="cover">
	                    @if(Auth::user()->hasRoleFix('boss'))
	                        <div class="col-md-6">
	                            <div class="pull-left">
	                                <h3>Заведения</h3>
	                            </div>
	                            @if($type != 'favourites' && Auth::user()->hasRoleFix('boss'))
	                                <div class="pull-right">
	                                    <a href="{{ route('clubs.create') }}" class="add-club">+ добавить заведение</a>
	                                </div>
	                            @endif
		                        @if(isset($clubs) && $clubs)
		                            @include('partials.mini._unitsWithShowMoreBlock', [$type = 'clubs.createEdit', $items = $clubs])
		                        @endif
	                        </div>
	                    @endif
                        <div class="col-md-6">
                            <div class="pull-left">
                                <h3>Специалисты</h3>
                            </div>
                            @if($type != 'favourites' && Auth::user()->hasRoleFix('boss'))
                                <div class="pull-right">
                                    <a href="{{ route('treners.create') }}" class="add-club">+ добавить специалиста</a>
                                </div>
                            @endif
	                        @if(isset($treners) && $treners)
	                            @include('partials.mini._unitsWithShowMoreBlock', [$type = 'treners.createEdit', $items = $treners])
	                        @endif
                        </div>
                    </div>
                @endif

                @if($type == 'payments')
                    <div class="payment">
                        <div class="cover">
                            <div class="col-md-6">
                                <div class="your-payment">
                                    <p>Лицевой счет</p>
                                    <h6>21617719</h6>
                                </div>

                                <div class="your-payment" style="padding-left: 0">
                                    <h5>Пополнение баланса:</h5>
                                    <p>Введите сумму для пополнения Вашего баланса:</p>
                                    <div class="col-md-6" style="padding-top: 5px; ">
                                        <form class="form-horizontal" role="form">
                                            <div class="form-group">
                                                <div class="col-sm-6" style="padding-right: 0; padding-left: 0;">
                                                    <input type="text" class="form-control" id="" placeholder="">
                                                </div>
                                                <label for="" class="col-sm-2 control-label"
                                                       style=" padding-left: 0;padding-top: 4px; font-weight: normal; color: #d0d0d0; font-size: 17px;">сом</label>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="your-payment col-md-6">
                                    <p>Ваш баланс</p>
                                    <h6>2 869 сом</h6>

                                </div>
                                <div class="your-payment col-md-6">
                                    <p>Ваш номер лицевого счета: </p>
                                    <h6> 21617719</h6>
                                </div>

                                <div class="your-payment col-md-12">
                                    <h5>Выберите способ пополнения:</h5>
                                    <p>Платежные системы:</p>
                                    <a href="" style="margin: 9px 6px; display: inline-block;"><img src="/img/png/visa.png" alt=""></a>
                                    <a href="" style="margin: 9px 6px;display: inline-block;"><img src="/img/png/master.png" alt=""></a>
                                    <a href="" style="margin: 9px 6px;display: inline-block;"><img src="/img/png/webmoney.png" alt=""></a>
                                </div>
                                {{--<div class="col-md-7" style="padding: 0;">--}}

                                {{--</div>--}}
                                {{--<div class="clearfix" style="margin-bottom: 20px;"></div>--}}
                                {{--<div class="col-md-5" style="padding-left: 0">--}}
                                    {{--<p>Терминал оплаты:</p>--}}
                                {{--</div>--}}

                            </div>
                        </div>
                    </div>
                @endif

                @if($type == 'reports')
                    <div class="col-md-12">
                        <?php $show = Request::get('show')?>
                        <?php $item_id = Request::get('item_id')?>
                        <div class="reports">
                            <ol class="breadcrumb">
	                            @if($show && $show != 'index')
	                                <li><a href="{{ route('profiles.index', ['type' => 'reports', 'show' => 'index']) }}">{{ $clubs->filterArrFix(['id' => $item_id])->first()->title }}</a></li>
	                                <li>@if($show == 'visits')Кол-во посещений @elseif($show == 'callback')Заявки на обратный звонок @elseif($show == 'reviews')Отчёты @endif</li>
	                            @endif
                                <li class="active"><a href="{{ route('profiles.index', ['type' => 'reports', 'show' => 'index']) }}">Назад к профилю</a></li>
                            </ol>
                        </div>
                        @if($show == null || $show == 'index')
                            <div class="reports">
	                            @foreach($clubs->whereLoose('premium', true) as $club)

	                                <div class="col-md-6">
                                        <div class="title-report">
                                            <a href="{{ route('clubs.show', [$club->city->slug, $club->category->slug, $club->slug]) }}">
                                                <h3>{{ $club->title }}</h3>
                                            </a>
                                        </div>
	                                    <div class="item-report">
                                            <span>Кол-во посещений: {{ $club->visits->sum('visits') }}</span>
                                            <a href="{{ route('profiles.index', ['type' => 'reports', 'show' => 'visits', 'item_id' => $club->id]) }}" class="custom-btn btn btn-default">Посмотреть</a>
	                                    </div>
	                                    <div class="item-report">
                                            <span>Заявки на обрат ный звонок: {{ $club->callbacks->count() }}</span>
                                            <a href="{{ route('profiles.index', ['type' => 'reports', 'show' => 'callback', 'item_id' => $club->id]) }}" class="custom-btn btn btn-default">Посмотреть</a>
	                                    </div>
	                                    <div class="item-report">
                                            <span>Отзывы: {{ $club->reviews->count() }}</span>
                                            <a href="{{ route('profiles.index', ['type' => 'reports', 'show' => 'reviews', 'item_id' => $club->id]) }}" class="custom-btn btn btn-default">Посмотреть</a>
	                                    </div>
	                                </div>
								@endforeach
                            </div>
                        @elseif($show == 'visits')
	                        <?php $visits = $clubs->filterArrFix(['id' => $item_id])->first()->visits->sortBy('visited_at') ?>
		                        <div id="b-plot_container" data-xaxis="{{ $visits->implode('visited_at', ',') }}" data-yaxis="{{ $visits->implode('visits', ',') }}"></div>
                        @elseif($show == 'callback')
                            <div class="reports call-back col-md-6">
                                <?php $callbacksForClub = $clubs->filterArrFix(['id' => $item_id])->first()->callbacks->sortByDesc('created_at') ?>
                                @foreach($callbacksForClub as $callback)
	                                {!! Form::open(['route' => ['callbacks.checked', $callback->id], 'method' => 'POST']) !!}
	                                    <div class="item" style="@if($callback->checked)  @endif">
		                                    <h6>{{ $callback->name }}</h6>
	                                        <p>{{ $callback->phone }}</p>
		                                    <a class="ajax_modal close" data-action="delete" data-type="callback" data-id="{{ $callback->id }}" href="" ></a>
		                                    {{--<a href="{{ route('callbacks.checked', $callback->id) }}" class="open"></a>--}}
	                                    </div>
		                                @InputBlock([$type = "input", $item = 'description', $label = null, $var = $callback, $p = "placeholder=\"Укажите примечание\""])
		                                <button type="submit" class="custom-btn btn btn-success">Применить</button>
	                                {!! Form::close() !!}
								@endforeach
                            </div>
                        @elseif($show == 'reviews')
                            <div class="reports reviews">
                                <ul class="col-md-6">
	                                <?php $reviewsForClub = $clubs->filterArrFix(['id' => $item_id])->first()->reviews->whereLoose('moderated', true)->load(['user'])->toHierarchy()->sortByDesc('created_at') ?>
	                                @foreach($reviewsForClub as $review)
	                                    <?php $descendants = $review->listAllChildren() ?>
	                                    <?php $descendants->count() > 0 ? $subReview = $descendants->first() : $subReview = $review ?>

	                                    <li>
		                                    <a class="ajax_modal" data-action="showReview" data-type="review" data-id="{{ $review->id }}"
		                                       data-forItem="club" data-forItem-id='{{ $review->reviewable_id }}' href="">
			                                    <h6>{{ $subReview->user->name }}</h6>
			                                    <span class="badge">{{ $descendants->where('viewed_at', null)->count() }}</span>
			                                    <span>{{ $review->text }}</span>
			                                    <span>{{ $subReview->text }}</span>
			                                    {{--<a href="" class="close"></a>--}}
		                                    </a>
	                                    </li>
									@endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection


