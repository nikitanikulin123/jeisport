@extends('layouts.app')
@section('assets-css')
	<link rel="stylesheet" href="{{ asset('libs/bootstrap-multiselect/css/bootstrap-multiselect.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/bootstrap-rating/bootstrap-rating.css') }}">
@endsection
@section('assets-js')
	<script src="{{ asset('libs/js/owl.carousel.js') }}"></script>
	<script src="{{ asset('libs/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
	<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
	<script src="{{ asset('js/items_updater.js') }}"></script>
	<script src="{{ asset('libs/bootstrap-rating/bootstrap-rating.min.js') }}"></script>
@endsection
@section('cities-selector')
    @include('partials.mini._cities_selector')
@endsection
@section('content')

	<script id="city-coords-lat">{{ $city->lat }}</script>
	<script id="city-coords-lng">{{ $city->lng }}</script>
	<script id="ajax-category">{{ $category->id }}</script>
	<script id="ajax-city">{{ $city->id }}</script>
	<script data-url="{{ route('clubs.index', [$city->slug, $category->slug]) }}" data-has-type="{{ isset($filterType) ? true : false }}" id="ajax-baseUrl"></script>

    <script>
        window.onload = function() {
            $('div.selects a').attr('rel', 'nofollow');
        };
    </script>
    <div class="map-body-club" id="ymaps-map" data-map-zoom="{{ $city->zoom }}"></div>

    <section class="filter-category-on-map">
        <div class="container-fluid">
            <div class="opacity">
                <div class="col-md-9">
                    <div class="discript">
                        <h1>{{ $seoPageTitle }} <span class="b-changed_items_amount">- {{ $clubs_count }} {{ trans_choice("место|места|мест", $clubs_count) }}</span></h1>
                        @if($seo_page)
                            <h2>{{ $seo_page->page_subtitle }}</h2>
                        @endif
                    </div>
                </div>
                {{--<div class="col-md-3">--}}
                    {{--<div class="img-wrapper">--}}
                        {{--<a href="">--}}
                            {{--<img src="/img/jpg/cat1.jpg" alt="">--}}
                        {{--</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="clearfix"></div>
                <div class="col-md-12">
                    <div class="selects" style="display: none;">
                        @include('partials.mini._filtersList', [$type = 'filters'])
                    </div>
                </div>
                <div class="clearfix"></div>

            </div>
        </div>
    </section>

    <section class="club">
        <div class="container-fluid">
            <div class="col-md-5">
                <div class="top-line sort-filters" style="display: none;">
                    @include('partials.mini._filtersList', [$type = 'sortFilters'])
                </div>
	            @include('partials.mini._unitsWithShowMoreBlock', [$type = 'clubs.index', $items = $clubs])
                <div class="b-units-service">
                    @include('partials.mini._recommendedBlock', [$type = 'clubs'])
                    <div class="discript">
                        @if($seo_page)
                            <p>{{ $seo_page->page_description }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection