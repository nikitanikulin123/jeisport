<ul>
    @foreach($priceLists->load('currency') as $item)
        <li class="images">
            <a></a>
            <div class="title">
                <h4>{{ str_limit($item->title, 30) }}</h4>
	            <a class="ajax_modal" data-action="show" data-type="priceList" data-id="{{ $item->id }}" data-forItem="{{ $type }}" data-forItem-id="{{ $item_id }}" href="">
                    <p>{{ $item->price }} {{ $item->currency->title }} - {{ $item->price_for }}</p>
	            </a>
            </div>
        </li>
    @endforeach
</ul>