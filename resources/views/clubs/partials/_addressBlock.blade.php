<div class="address">
	<ul>
		<li>
			<span>Адрес:</span>
			@if($item->region)
				район <a href="{{ route($item->title ? 'clubs.indexByDistrict' : 'treners.indexByDistrict', [$item->city->slug, $item->category->slug, $item->region->slug]) }}">{{ $item->region->title }}</a>,
			@endif
			@if($item->street && $item->street->title != 'другая')
				улица <a href="{{ route($item->title ? 'clubs.indexByStreet' : 'treners.indexByStreet', [$item->city->slug, $item->category->slug, $item->street->slug]) }}">{{ $item->street->title }}</a> -
			@else
				{{ $item->address }}
			@endif
			@if($item->house)
				{{ $item->house }}
			@endif
		</li>
		@if($item->site)
			<li><span>Сайт:</span>
				<a href="{{ $item->site }}" rel="nofollow">{{ $item->site }}</a>
			</li>
		@endif
		@if($item instanceof \App\Models\Trener)
		<li><span>Цена:</span>
			не указана</li>
		@endif
		@if($item->checkWorkingDays())
			<li><span>Время работы:</span>
				<?php $schedule = $item->getCurrentDayInfo('schedule') ?>
				<?php $show = true ?>
				@if(count($schedule) > 1)
					@foreach($schedule as $scheduledDay)

							{{ count($scheduledDay) > 1 ? head(array_keys($scheduledDay)) . ' - ' . last(array_keys($scheduledDay)) : head(array_keys($scheduledDay))}}:
							{{ $item->setDateFormat(head($scheduledDay) . '_time_from') }} - {{ $item->setDateFormat(head($scheduledDay) . '_time_to') }}

					@endforeach
				@elseif(count($schedule) == 1)
					@if($item->setDateFormat(head($schedule->first()) . '_time_from') == '00:00' && $item->setDateFormat(head($schedule->first()) . '_time_to') == '23:59')
						Круглосуточно
						<?php $show = false ?>
					@else
						Ежедневно: {{ $item->setDateFormat(head($schedule->first()) . '_time_from') }} - {{ $item->setDateFormat(head($schedule->first()) . '_time_to') }}
					@endif
				@endif

			</li>
		@endif
		<li>
			@if(isset($show))
				<?php
					$currentTime = LocalizedCarbon::now();
					$todayTimeFrom = LocalizedCarbon::parse($item->{$item->getCurrentDayInfo('todayTimeFrom')});
					$todayTimeTo = LocalizedCarbon::parse($item->{$item->getCurrentDayInfo('todayTimeTo')});
					$todayTimeFrom > $todayTimeTo ? $todayTimeTo = $todayTimeTo->addHours(24) : false;
				?>
				@if($item->{$item->getCurrentDayInfo('workToday')})
					@if($currentTime < $todayTimeFrom && $currentTime < $todayTimeTo)
						<span>Откроется:</span> {{ $todayTimeFrom->diffForHumans($currentTime) }}
					@elseif($currentTime > $todayTimeFrom && $currentTime > $todayTimeTo)
						<span>Уже закрыто сегодня, откроется:</span> {{ $item->getNextWorkDay()->diffForHumans($currentTime) }}
					@else
						<span>Открыто сейчас, закроется:</span> {{ $todayTimeTo->diffForHumans($currentTime) }}
					@endif
				@else
					<span>Сегодня не работает, откроется:</span> {{ $item->getNextWorkDay()->diffForHumans($currentTime) }}
				@endif
			@endif
		</li>
	</ul>
</div>