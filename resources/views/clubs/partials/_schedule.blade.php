<section class="schedule">
    <div class="container">
        <div class="row">
            <div class="content">
                <h2>Расписание</h2>
                <div class="dropping">
                    <form class="form-inline">
                        <div class="form-group">
                            <select class="form-control">
                                <option>cегодня, чт 26 мая</option>
                                <option>cегодня, пт 27 мая</option>
                                <option>cегодня, сб 28 мая</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control">
                                <option>Весь день</option>
                                <option>Пол дня</option>
                                <option>Пол часа</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="catalog">
                    <ul>
                        <li class="col-md-6 col-sm-12">
                            <a href=""></a>
                            <div class="img-wrapper">
                                <img src="/img/jpg/ras1.png" alt="">
                            </div>
                            <div class="title-wrapper">
                                <h3>Angry Birds в кино </h3>
                                <p>Мультфильмы, приключенческие фильмы, комедии</p>
                                <span class="disable">11:10</span>
                                <span class="disable">11:50</span>
                                <span class="disable">13:20</span>
                                <span>16:20</span>
                                <span>18:30</span>
                                <span>20:40</span>
                                <span>21:55</span>
                                <div class="cover">
                                    <h6>87 -136</h6>
                                    <ul class="stars">
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-6 col-sm-12">
                            <a href=""></a>
                            <div class="img-wrapper">
                                <img src="/img/jpg/ras2.png" alt="">
                            </div>
                            <div class="title-wrapper">
                                <h3>Angry Birds в кино </h3>
                                <p>Мультфильмы, приключенческие фильмы, комедии</p>
                                <span class="disable">11:10</span>
                                <span class="disable">11:50</span>
                                <span class="disable">13:20</span>
                                <span>16:20</span>
                                <span>18:30</span>
                                <span>20:40</span>
                                <div class="cover">
                                    <h6>87 -136</h6>
                                    <ul class="stars">
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-6 col-sm-12">
                            <a href=""></a>
                            <div class="img-wrapper">
                                <img src="/img/jpg/ras3.png" alt="">
                            </div>
                            <div class="title-wrapper">
                                <h3>Angry Birds в кино </h3>
                                <p>Мультфильмы, приключенческие фильмы, комедии</p>
                                <span class="disable">11:10</span>
                                <span class="disable">11:50</span>
                                <span class="disable">13:20</span>
                                <span>16:20</span>
                                <span>18:30</span>
                                <span>20:40</span>
                                <div class="cover">
                                    <h6>87 -136</h6>
                                    <ul class="stars">
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-6 col-sm-12">
                            <a href=""></a>
                            <div class="img-wrapper">
                                <img src="/img/jpg/ras4.png" alt="">
                            </div>
                            <div class="title-wrapper">
                                <h3>Angry Birds в кино </h3>
                                <p>Мультфильмы, приключенческие фильмы, комедии</p>
                                <span class="disable">11:10</span>
                                <span class="disable">11:50</span>
                                <span class="disable">13:20</span>
                                <span>16:20</span>
                                <span>18:30</span>
                                <span>20:40</span>
                                <div class="cover">
                                    <h6>87 -136</h6>
                                    <ul class="stars">
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="seemore">
                        <a href="">Показать еще</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>