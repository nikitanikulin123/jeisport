@extends('layouts.app')
@section('assets-css')
    <link rel="stylesheet" href="{{ asset('libs/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endsection
@section('assets-js')
    <script src="{{ asset('js/init.js') }}"></script>
    <script src="{{ asset('libs/js/flow.min.js') }}"></script>
    <script src="{{ asset('js/initMultiple.js') }}"></script>
    <script src="{{ asset('libs/js/Sortable.min.js') }}"></script>
    <script src="{{ asset('libs/js/sortable.jquery.binding.js') }}"></script>
    <script src="{{ asset('libs/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('libs/js/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    {{--<script> var editor = CKEDITOR.replace( '', { customConfig : 'config.js', } ); </script>--}}
@endsection
@section('content')

    <?php !isset($step) ? (Request::get('step') ? $step = Request::get('step') : $step = 1) : '' ?>
    <?php isset($club) ?: $club = null ?>

    <section class="content-step">
        <div class="container-fluid">
            <div class="content">
                <div class="col-md-12">
                    <div class="spet">
                        <div class="pull-left" id="back-profile">
                            <a href="{{ route('profiles.index') }}"><span class="glyphicon glyphicon-menu-left"></span>Назад
                                к профилю</a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="cover">
                            <h1>@if(isset($club)) <span>{{ $club->title }} @else Новое</span> заведение @endif</h1>
                            <div class="bg-cover">
                                <div class="col-md @if(isset($club)) step-success @endif @if($step == 1) active @endif ">
                                    <div class="arrow">
                                        <p>Шаг 1.</p>
                                        <a @if(isset($club)) href="{{ route('clubs.edit', [$club->slug, 'step' => 1]) }}"
                                           @else href="{{ route('clubs.create', ['step' => 1]) }}" @endif>Заполните
                                            информацию</a>
                                    </div>
                                </div>
                                <div class="col-md @if(isset($club) && $club->filterValues->count() > 0) step-success @endif @if($step == 2) active @endif ">
                                    <div class="arrow">
                                        <p>Шаг 2. </p>
                                        <a @if(isset($club)) href="{{ route('clubs.edit', [$club->slug, 'step' => 2]) }}" @endif>Выберите
                                            подкатегории</a>
                                    </div>
                                </div>
                                <div class="col-md @if(isset($club) && $club->checkWorkingDays()) step-success @endif @if($step == 3) active @endif ">
                                    <div class="arrow">
                                        <p>Шаг 3.</p>
                                        <a @if(isset($club) && $club->filterValues->count() > 0) href="{{ route('clubs.edit', [$club->slug, 'step' => 3]) }}" @endif>Заполните
                                            расписание</a>
                                    </div>
                                </div>
                                <div class="col-md @if(isset($club) && $club->image) step-success @endif @if($step == 4) active @endif ">
                                    <div class="arrow">
                                        <p>Шаг 4. </p>
                                        <a @if(isset($club) && $club->checkWorkingDays()) href="{{ route('clubs.edit', [$club->slug, 'step' => 4]) }}" @endif>Добавьте
                                            изображения</a>
                                    </div>
                                </div>
                                <div class="col-md @if(isset($club) && ($club->stocks->first() || $club->priceListCategories->first())) step-success @endif @if($step == 5) active @endif "
                                     id="no-arrow">
                                    <div class="arrow">
                                        <p>Шаг 5. </p>
                                        <a @if(isset($club) && $club->image) href="{{ route('clubs.edit', [$club->slug, 'step' => 5]) }}" @endif>Добавьте
                                            акции и прайс-листы</a>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            @if($step == 1)
                                <div class="form-step-1">
                                    {!! Form::open(['route' => isset($club) ? ['clubs.update', $club->id, 'step' => 1] : ['clubs.store', 'step' => 1], 'method' => 'POST']) !!}
                                    <div class="row">
                                        @InputBlock([$type = "input", $item = 'title', $label = "Название", $var = $club, $dp = "class=\"form-group col-md-3\""])
	                                    @InputBlock([$type = "input", $item = 'prefix', $label = "Префикс", $var = $club, $dp = "class=\"form-group col-md-3\""])
	                                    @InputBlock([$type = "input", $item = 'postfix', $label = "Постфикс", $var = $club, $dp = "class=\"form-group col-md-3\""])
	                                    @InputBlock([$type = "select", $item = 'category_id', $label = "Категория", $list = $categories->filterArrFix(['type' => 'club']),
	                                        $selected = $club, $p = "placeholder=\"Выберите Категорию\"", $dp = "class=\"form-group col-md-3\""])
                                    </div>
                                    @include('partials.mini._cityRegionStreetSelectBlock', [$obj = $club])
                                    <div class="row">
                                        @InputBlock([$type = "input", $item = 'house', $label = "Дом №", $var = $club, $p = "placeholder=\"Введите номер дома\"", $dp = "class=\"form-group col-md-4\""])
                                        @InputBlock([$type = "input", $item = 'site', $label = "Сайт (если есть)", $var = $club, $p = "placeholder=\"Введите адрес сайта\"", $dp = "class=\"form-group col-md-4\""])
                                        @InputBlock([$type = "input", $item = 'n_email', $label = "Email для уведомлений", $var = $club, $p = "placeholder=\"Введите Email\"", $dp = "class=\"form-group col-md-4\""])
                                    </div>
                                    <div class="row">
                                        @InputBlock([$type = "input", $item = 'phone', $label = "Телефон", $var = $club, $dp = "class=\"form-group col-md-4\""])
                                        @InputBlock([$type = "input", $item = 'phone2', $label = "Телефон2", $var = $club, $dp = "class=\"form-group col-md-4\""])
	                                    @InputBlock([$type = "multiSelect", $item = 'trener_id[]', $label = "Привязать специалистов", $list = \App\Models\Trener::where('user_id', Auth::id()),
	                                        $selected = $club ? $club->treners() : null, $dp = "class=\"form-group col-md-4\""])
                                    </div>
                                    <div class="bottom-form-step-1">
                                        <div class="row">
                                            @InputBlock([$type = "textarea", $item = 'description', $label = "Описание", $var = $club, $p = "class=\"ckeditor\"", $dp = "class=\"form-group col-md-6\""])
                                            <div class="col-md-6">
                                                @include('admin.maps._yandexMap', ['instance' => isset($club) ? $instance = $club : null, 'defaultCity' => \App\Models\City::where('is_main', true)->first()])
	                                            @InputBlock([$type = "input", $item = 'lat_lng', $label = null, $var = $club, $value = '42.875989,74.603674', $p = "id=lat_lng style=\"display:none\""])
                                                <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-offset-6 col-md-6 button-next" style="padding-right: 0px;">
                                        <button type="submit" class="custom-btn btn btn-default pull-right">Далее
                                        </button>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            @endif
                            @if($step == 2 && isset($club))
                                <div class="spet -m-2">
                                    <div class="cover">
                                        {!! Form::open(['route' => ['clubs.update', $club->id, 'step' => 2], 'method' => 'POST']) !!}
                                        <h2>Выберите подкатегории</h2>
                                        <div class="col-md-4">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs">
                                                @foreach($filters->filter(function ($item) {return $item['id'] != 100 && $item['id'] != 101 && $item['id'] != 102 && $item['id'] != 105;}) as $key => $filter)
                                                    <li @if($key == 0) class="active" @endif>
                                                        <a href="#filter_{{ $filter->id }}" data-toggle="tab">{{ $filter->title }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="col-md-8">
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <h3>Выберите нужные из списка</h3>
                                                @foreach($filters as $key => $filter)
                                                    <div class="tab-pane @if($key == 0) active @endif" id="filter_{{ $filter->id }}">
                                                        <div class="col-md-6">
                                                            @foreach($filter->options->slice(0, round($filter->options->count() / 2)) as $option)
		                                                        @InputBlock([$type = "checkbox", $item = 'filterValues[' . $option->id . ']', $label = $option->title,
		                                                            $checked = isset($club) ? $club->filterValues->where('id', $option->id)->first() : (old('filterValues') ? old('filterValues') : true)])
                                                            @endforeach
                                                        </div>
                                                        <div class="col-md-6">
                                                            @foreach($filter->options->slice(round($filter->options->count() / 2)) as $option)
		                                                        @InputBlock([$type = "checkbox", $item = 'filterValues[' . $option->id . ']', $label = $option->title,
																	$checked = isset($club) ? $club->filterValues->where('id', $option->id)->first() : (old('filterValues') ? old('filterValues') : true)])
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <a href="{{ route('clubs.edit', [$club->slug, 'step' => $step - 1]) }}"
                                               class="custom-btn btn btn-default back">Назад</a>
                                            <button type="submit" class="custom-btn btn btn-default pull-right">Далее
                                            </button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            @endif
                            @if($step == 3 && isset($club))
                                <div class="spet -m-3">
                                    <div class="cover">
                                        {!! Form::open(['route' => ['clubs.update', $club->id, 'step' => 3], 'method' => 'POST']) !!}
                                        <div class="col-md-4">
	                                        @include('partials.mini._dutyBlock', [$obj = $club])
                                        </div>
                                        <div class="col-md-8">
	                                        @include('partials.mini._scheduleBlock', [$obj = $club])
                                            <div class="clearfix" style="margin-bottom: 40px;"></div>
                                            <a href="{{ route('clubs.edit', [$club->slug, 'step' => $step - 1]) }}" class="custom-btn btn btn-default back">Назад</a>
                                            <button type="submit" class="custom-btn btn btn-default pull-right">Далее</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            @endif
                            @if($step == 4 && isset($club))
                                <div class="spet -m-4">
                                    {!! Form::open(['route' => ['clubs.update', $club->id, 'step' => 4], 'method' => 'POST']) !!}
		                                @InputBlock([$type = "imageUpload", $item = 'image', $label = 'Главное изображение', $var = $club])
		                                @InputBlock([$type = "imageUploadMultiple", $item = 'images', $label = 'Дополнительные изображения', $var = $club])
	                                    <div class="col-md-6">
	                                        <a href="{{ route('clubs.edit', [$club->slug, 'step' => $step - 1]) }}" class="custom-btn btn btn-default back">Назад</a>
	                                    </div>
	                                    <div class="col-md-6">
	                                        <button type="submit" class="custom-btn btn btn-default pull-right">Далее
	                                        </button>
	                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            @endif
                            @if($step == 5 && isset($club))
                                <div class="spet -m-5">
                                    <div class="col-md-5">
                                        <h3>Акции</h3>
                                        <a class="custom-btn btn btn-default ajax_modal acii" href="" data-action="create" data-type="stock"
                                           data-forItem="club" data-forItem-id="{{ $club->id }}">+ Добавить акцию</a>
                                        @foreach($club->stocks as $stock)
                                            <div class="item">
                                                <div class="img-wrapper">
                                                    <img src="{{ $stock->setImage('150_150', 'image') }}" alt="{{ $stock->setImage('150_150', 'image') }}">
                                                </div>
                                                <div class="title">
                                                    <h4>{{ $stock->title3 }}</h4>
                                                    <h5>{{ $stock->title4 }}</h5>
                                                    <p>{{ str_limit(strip_tags($stock->text), 60) }}</p>
                                                    <a class="glyphicon glyphicon-pencil ajax_modal" href="" data-action="edit" data-type="stock"
                                                       data-id="{{ $stock->id }}" data-forItem="club" data-forItem-id="{{ $club->id }}"></a>
                                                    <a class="glyphicon glyphicon-remove ajax_modal" href="" data-action="delete" data-type="stock"
                                                       data-id="{{ $stock->id }}"></a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-offset-1 col-md-6">
                                        <h3>Прайслисты</h3>
                                        <a class="custom-btn btn btn-default ajax_modal price" href="" data-action="create" data-type="priceListCategory"
                                           data-forItem="club" data-forItem-id="{{ $club->id }}">+ Добавить прайслист</a>
                                        @foreach($club->priceListCategories as $priceListCategory)
                                            <div class="row" style="margin-bottom: 20px;">
                                                <div class="col-md-4">
                                                    <h4>{{ $priceListCategory->title }}</h4>
                                                </div>
                                                <div class="col-md-5">
                                                    @foreach($priceListCategory->priceLists as $priceList)
                                                        <div class="row">
                                                            <div class="col-md-7 foot">
                                                                <p>{{ $priceList->title }}</p>
                                                            </div>
                                                            <div class="col-md-1 foot">
	                                                            <a class="glyphicon glyphicon-pencil ajax_modal" href="" data-action="edit" data-type="priceList"
                                                                        data-id="{{ $priceList->id }}" data-forItem="priceListCategory" data-forItem-id="{{ $priceListCategory->id }}"></a>
                                                            </div>
                                                            <div class="col-md-1 foot">
	                                                            <a class="glyphicon glyphicon-remove ajax_modal" href="" data-action="delete" data-type="priceList"
                                                                    data-id="{{ $priceList->id }}"></a>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <div class="col-md-1 set">
                                                    <a class="glyphicon glyphicon-plus ajax_modal" href=""
                                                       data-action="edit" data-type="priceList"
                                                       data-forItem="priceListCategory"
                                                       data-forItem-id="{{ $priceListCategory->id }}"></a>
                                                </div>
                                                <div class="col-md-1 set">
                                                    <a class="glyphicon glyphicon-pencil ajax_modal" href=""
                                                       data-action="edit" data-type="priceListCategory"
                                                       data-id="{{ $priceListCategory->id }}"
                                                       data-forItem="club"
                                                       data-forItem-id="{{ $club->id }}"></a>
                                                </div>
                                                <div class="col-md-1 set">
                                                    <a class="glyphicon glyphicon-remove ajax_modal" href=""
                                                       data-action="delete" data-type="priceListCategory"
                                                       data-id="{{ $priceListCategory->id }}"></a>
                                                </div>
                                            </div>
                                        @endforeach
                                        <a class="glyphicon glyphicon-plus ajax_modal" href=""
                                           data-action="uploadPriceList" data-type="club"
                                           data-id="{{ $club->id }}">Загрузить прайслист</a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6" style="padding-bottom: 40px;">
                                        <a href="{{ route('clubs.edit', [$club->slug, 'step' => $step - 1]) }}"
                                           class="custom-btn btn-default back">Назад</a>
                                    </div>
                                    <div class="col-md-6" style="padding-bottom: 40px;">
                                        <a href="{{ route('clubs.edit', [$club->slug, 'step' => 'complete']) }}"
                                           class="custom-btn btn btn-default finish" style="float: right;">Завершить</a>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection