@extends('layouts.app')
@section('assets-css')
	<link rel="stylesheet" href="{{ asset('libs/css/owl.carousel.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/bootstrap-multiselect/css/bootstrap-multiselect.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/bootstrap-rating/bootstrap-rating.css') }}">
@endsection
@section('assets-js')
	<script src="{{ asset('libs/js/owl.carousel.js') }}"></script>
	<script src="{{ asset('libs/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
	<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
	@if($club->premium) <script src="{{ asset('js/items_updater.js') }}"></script> @endif
	<script src="{{ asset('libs/bootstrap-rating/bootstrap-rating.min.js') }}"></script>
	<script src="{{ asset('js/single_item.js') }}"></script>
@endsection
@section('cities-selector')
	@include('partials.mini._cities_selector')
@endsection
@section('content')
    <section class="show-one-club" >
        <div class="container-fluid">
			<ul class="anjor-one-club">
				<li class=""><a class="go_to" href="#description">Описание и контакты</a></li>
				@if(count($reviews) != 0)
					<li><a  class="go_to" href="#reviews">Отзывы</a></li>
				@endif
				@if($club->premium && count($club->priceListCategories) != 0)
					<li><a  class="go_to" href="#priceListCategories">Прайс-лист</a></li>
				@endif
				@if(count($stocks) != 0)
					<li><a  class="go_to" href="#stocks">Акции</a></li>
				@endif
				@if($club->premium && $club->show_callback_form)
					<li class="pull-right">
						<a href="tel:{{ $club->phone }}">{{ $club->phone }}</a>
						@if($club->phone2)
							<a href="tel2:{{ $club->phone2 }}">{{ $club->phone2 }}</a>
						@endif
					</li>
				@endif
			</ul>
        </div>
    </section>

    @if($club->premium)
	    @if($club->photos->count() >= 1)
	        <section class="slider">
	            <div class="owl-carousel2 club-show">
		            @TagBlock([$type= 'img', $var = $club, $src = "image", $size = "1520_699", $dp = "class=\"item\"", $p = "class=\"owl-lazy\" data-src={$club->setImage('1520_699', 'image')}"])
	                @foreach($club->photos as $photo)
			            @TagBlock([$type= 'img', $var = $photo, $src = 'path', $size = "1520_699", $dp = "class=\"item\"", $p = "class=\"owl-lazy\" data-src={$club->setImage('1520_699', $photo->path)}"])
	                @endforeach
	            </div>
	        </section>
	    @else
	        <section class="slider">
	            <div class="item-no-image">
	                <img src="{{ $club->setImage('1520_699', 'image') }}" alt="{{ $club->setImage('1520_699', 'image') }}">
	            </div>
	        </section>
	    @endif
    @endif

    <section id="description" class="item-show @if($club->premium) premium-show-club @else free-show-club @endif">
	    <a name="description"></a>
        <div class="container-fluid">
			<div class="wrapper-margin-top">
				<div class="title">
					<h1>{{ $club->prefix ? $club->prefix : '' }} {{ $club->title }} {{ $club->postfix ? $club->postfix : '' }}</h1>
					@ReplaceBlock("partials.mini._ratingsBlock", [$type = 'club', $item = $club])
					@if ($club->user_id != Auth::id())
						<a data-toggle="tooltip" data-placement="right" title="Добавить в Избранное" class="heart @if($addedToFavourites)active @endif" data-type="club" data-id="{{ $club->id }}"></a>
					@endif
				</div>
				<div class="contact-show-one-blade">
					<div class="col-md-6 left">
						@if(!$club->premium)
							<div class="wrapper">
								<a class="loops" href="">
									<img class="owl-lazy" data-src="{{ $club->setImage('505_240', 'image') }}" src="{{ $club->setImage('505_240', 'image') }}" alt="{{ $club->setImage('505_240', 'image') }}">
								</a>
							</div>
						@endif
						<div class="b-textarea-ckeditor">
							{!! $club->description !!}
						</div>
						@foreach($club->category->filters as $filter)
							@if(count(array_intersect($club_filter_values->lists('id')->toArray(), $filter->options->lists('id')->toArray())) > 0)
								<div class="filter-option-show-club">
									<h4>{{ $filter->title }}</h4>
									@foreach($filter->options as $key => $option)
										@if(in_array($option->id, $club_filter_values->lists('id')->toArray()))
											<a href="{{ route('clubs.indexByType', [$club->city->slug, $club->category->slug, $option->slug]) }}">{{ trim(mb_strtolower($option->title)) . ', ' }}</a>
										@endif
									@endforeach
								</div>
							@endif
						@endforeach

						@if(!$club->premium)
							@include('clubs.partials._addressBlock', [$item = $club])
							@if(count($stocks) > 0)
								<div class="shares">
									<h3>Акции</h3>
									<a name="stocks"></a>
									<ul>
										@foreach($stocks->sortByDesc('created_at')->take(3) as $stock)
											<li>
												<a class="ajax_modal" data-action="show" data-type="stock" data-id="{{ $stock->id }}" data-forItem="club" data-forItem-id="{{ $club->id }}" href=""  ></a>
												@if($stock->image)
												<img  src="{{ $stock->setImage('426_206', 'image') }}"   alt="">
												@endif
												<h4>{{ $stock->title3 }}</h4>
												<h5>{{ str_limit($stock->title4, 15) }}</h5>
												<p>{{ str_limit($stock->text) }}</p>
											</li>
										@endforeach
									</ul>
								</div>
							@endif
						@endif
					</div>
					<div class="col-md-6 right">
						@if($club->premium)
							<div class="number-club" id="callbacks">
								<a href="tel:{{ $club->phone }}">{{ $club->phone }}</a>
								@if($club->phone2)
									<a href="tel2:{{ $club->phone2 }}">{{ $club->phone2 }}</a>
								@endif
							</div>
							@if($club->premium && $club->show_callback_form)
								@include('partials.mini._callbackBlock', [$item = $club])
							@endif
							@TagBlock([$type = "map", $var = $club, $src = "lat_lng"])
							<div class="address">
								@include('clubs.partials._addressBlock', [$item = $club])
							</div>
						@else
							@TagBlock([$type = "map", $var = $club, $src = "lat_lng"])
							<div class="services">
								@include('partials.mini._recommendedBlock', [$type = 'clubs'])
								<a href="" class="banner">
									<img  src="/img/jpg/banners1.jpg" alt="">
								</a>
							</div>
						@endif
					</div> {{--col-md-6 right--}}

					@if(!$club->premium)
						<div class="clearfix"></div>
						<div class="banner-center">
							<a class="" href="">
								<img src="/img/jpg/bannercenter.jpg" alt="">
							</a>
						</div>
					@endif

				</div> {{--maps-about--}}

			</div>
        </div>
    </section>

    @if($club->premium && $club->priceListCategories->count() > 0)
	    <section class="price-list" id="priceListCategories">
		    <div class="container-fluid">
			    <div class="content">
				    <h2>Прайс лист</h2>
				    <div class="tabs">
					    <ul>
						    <li class="price_list" data-category-id="all" data-item_id="{{ $club->id }}" data-type="club">Весь прайс лист</li>
						    @foreach($club->priceListCategories as $category)
							    <li class="price_list" data-category-id="{{ $category->id }}" data-item_id="{{ $club->id }}" data-type="club">{{ $category->title }}</li>
							    @if($category->file)
								    @TagBlock([$type = "file", $var = $category, $src = "file", $text = "Прайслист {$category->title}", $p = 'target="_blank"'])
							    @endif
						    @endforeach
					    </ul>
				    </div>
				    <div class="price_list_container"></div>
				    @if($club->p_file)
					    @TagBlock([$type = "file", $var = $club, $src = "p_file", $text = "Прайслист {$category->title}", $p = 'target="_blank"'])
				    @endif
			    </div>
		    </div>
	    </section>
    @endif

    @if($club->premium && $club->treners->count() > 0)
	    <section class="specialist">
		    <div class="container-fluid">
				<div class="content">
					<h2>Специалисты</h2>
					<div class="dropping selects" style="display: none;" data-club-id="{{ $club->id }}">
						@include('partials.mini._filtersList', [$type = 'specialists', $filterType = '', $selected_filter_id = null])
					</div>
					<div class="human">
						@include('partials.mini._unitsWithShowMoreBlock', [$type = 'clubs.specialists', $items = null])
					</div>
				</div>
		    </div>
	    </section>
    @endif

    @if($club->premium && count($stocks) > 0)
	    <section class="action" id="stocks">
		    <div class="container-fluid">
				<div class="content">
					<h2>Акции</h2>
					<div class="owl-carousel">
						@foreach($stocks as $stock)
							<div>
								{{--@if($stock->image)style="background: url('{{ $stock->setImage('426_206', 'image') }}') center" @endif--}}
								<div class="dashed">
									<a class="ajax_modal" data-action="show" data-type="stock" data-id="{{ $stock->id }}" data-forItem="club" data-forItem-id="{{ $club->id }}" href=""></a>
									<h3>{{ $stock->title3 }}</h3>
									<h4>{{ str_limit($stock->title4, 15) }}</h4>
									<p>{{ str_limit($stock->text) }}</p>
								</div>
							</div>
						@endforeach
					</div>
				</div>
		    </div>
	    </section>
    @endif

	{{--<section class="treners-special">--}}
		{{--<div class="container-fluid">--}}
			{{--<div class="content">--}}
				{{--<h2>Специалисты</h2>--}}

				{{--<div class="item-one-special">--}}
					{{--<div class="img-wrapper">--}}

					{{--</div>--}}
					{{--<div class="title-wrapper">--}}
						{{--<a href="#" class="name-special">Виктория Сергеевна Лесина </a>--}}
						{{--<h5 class="rank-special">Главный тренер</h5>--}}
						{{--<h5>--}}
							{{--<a href="#" class="review-info">Отзывы и доп. информация</a>--}}
						{{--</h5>--}}
						{{--<h5>--}}
							{{--<a href="tel:996555667788" class="tel-special">996555667788</a>--}}
						{{--</h5>--}}
					{{--</div>--}}
				{{--</div>--}}

				{{--<div class="item-one-special">--}}
					{{--<div class="img-wrapper">--}}

					{{--</div>--}}
					{{--<div class="title-wrapper">--}}
						{{--<a href="#" class="name-special">Виктория Сергеевна Лесина </a>--}}
						{{--<h5 class="rank-special">Главный тренер</h5>--}}
						{{--<h5>--}}
							{{--<a href="#" class="review-info">Отзывы и доп. информация</a>--}}
						{{--</h5>--}}
						{{--<h5>--}}
							{{--<a href="tel:996555667788" class="tel-special">996555667788</a>--}}
						{{--</h5>--}}
					{{--</div>--}}
				{{--</div>--}}

			{{--</div>--}}
		{{--</div>--}}
	{{--</section>--}}

    <section class="reviews" id="reviews">
        <div class="container-fluid">
			<div class="content">
				<h2>Отзывы</h2>
				@if(!Auth::check())
					<i class="pls-auch">*Для того чтобы написать отзыв, пожалуйста, авторизуйтесь</i>
				@endif
				<div class="dropping">
					<button class="btn write create-reviews" type="button" data-id="{{ $club->id }}" data-toggle="collapse" data-target="#addReview" aria-expanded="false" aria-controls="collapseExample">Написать</button>
					@if(count($reviews) != 0)
						<button class="custom-btn btn date sort-reviews sort-date active" data-sort="created_at">По дате</button>
						<button class="custom-btn btn date sort-reviews sort-likes" data-sort="rating_plus">По популярности</button>
					@endif
				</div>
				<div class="collapse" id="addReview">
					@if(!$club->ratings->filterFix('user_id', '=', Auth::id())->first())
						@ReplaceBlock("partials.mini._ratingsBlock", [$type = 'club', $item = $club, $noReviews = true])
					@endif
					<div class="well">
						<form action="{{ route('reviews.store', [$club->id, 'type' => 'club', 'parent_id' => null]) }}" class="form-horizontal ajax_form" method="POST">
							<div class="form-group col-md-6">
								<textarea name="text" class="form-control" placeholder="Комментарий" rows="6"></textarea>
							</div>
							<div class="form-group col-md-12">
								<button type="submit" class="custom-btn">Отправить</button>
							</div>
						</form>
					</div>
				</div>
				<ul class="comments">
					@if(count($reviews) == 0)
						<li class="no-reviews">Отзывов еще никто не оставлял! Будь первым!</li>
					@endif
					@include('partials.mini._unitsWithShowMoreBlock', [$type = 'clubs.reviews', $items = $reviews])
				</ul>
			</div>
        </div>
    </section>
@endsection