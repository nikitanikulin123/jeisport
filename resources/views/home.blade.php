@section('header-class') home-header @endsection
@section('home_banner')
    <?php $sliderNum = array_rand([1 => 1, 2 => 2]) ?>
    <section class="banner-index" style="background-image: url('{{ url("img/slider{$sliderNum}.jpg") }}');">
        <div class="container">
            <div class="row">
                <h1>Добавь свою<br><span>организацию</span></h1>
                <p> Быстрая служба поддержки</p>
                <p> Неограниченные возможности сервиса</p>
                <a class="ajax_modal custom-btn" href="" data-action="addClub" data-type="club">+ Добавить</a>
            </div>
        </div>
    </section>
@endsection

@section('cities-selector')
    @include('partials.mini._cities_selector')
@endsection

@extends('layouts.app')

@section('content')
    <section class="category-club-trener">
        <div class="container-fluid">
            <div class="col-md-offset-3 col-md-6">
                <h2>Все организации и специалисты с отзывами и рейтингом в городе Бишкек</h2>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-8">
                {{--<div class="col-md-offset-3 col-md-6">--}}
                    {{--<h3>Заведения</h3>--}}
                {{--</div>--}}
                <div class="clearfix"></div>
                <div class="categori-option">
                    @foreach($categories->filterArrFix(['type' => 'club'])->sortBy('order') as $category)
                        <div class="col-md-6">
                            <h4>
                                <a href="{{ route('clubs.index', [stripos($category->slug, 'issyk-kul') ? 'issyk-kul' : \App\Models\City::getDefaultCitySlug($cities), $category->slug]) }}" class="title">{{ $category->title }}</a>
                            </h4>
                            @foreach($category->subcategories as $subcategory)
                                <a href="{{ route('clubs.indexByType', [stripos($category->slug, 'issyk-kul') ? 'issyk-kul' : \App\Models\City::getDefaultCitySlug($cities), $category->slug, $subcategory->slug]) }}">{{ $subcategory->title }} </a>
                            @endforeach
                        </div>
                    @endforeach
                </div>

                <div class="clearfix"></div>
            </div>
            <div class="col-md-4">
                {{--<div class="col-md-offset-3 col-md-6">--}}
                    {{--<h3>Специалисты</h3>--}}
                {{--</div>--}}
                <div class="clearfix"></div>
                <div class="categori-option">
                    @foreach($categories->filterArrFix(['type' => 'trener'])->sortBy('order') as $category)
                        <div class="col-md-12">
                            <h4>
                                <a href="{{ route('treners.index', [stripos($category->slug, 'issyk-kul') ? 'issyk-kul' : \App\Models\City::getDefaultCitySlug($cities), $category->slug]) }}" class="title">{{ $category->title }}</a>
                            </h4>
                            @foreach($category->subcategories as $subcategory)
                                <a href="{{ route('treners.indexByType', [stripos($category->slug, 'issyk-kul') ? 'issyk-kul' : \App\Models\City::getDefaultCitySlug($cities), $category->slug, $subcategory->slug]) }}">{{ $subcategory->title }} </a>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <section class="paper">
        <div class="container">
            <h2>Интересные статьи</h2>
            <div id="macy-container">
                @foreach($feeds->where('city_id', $cities->where('title', $currentCity)->first()->id) as $feed)
                    <div class="grid-item">
                        <a href="{{ route('feeds.show', [$feed->city->slug, $feed->slug]) }}"></a>
	                    @TagBlock([$type = "img", $var = $feed, $src = "image", $size = "350_null", $mod = 'resize'])
                        <h3>{{ $feed->title }}</h3>
                        <p>{{ str_limit(strip_tags($feed->body)) }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection

@section('assets-js')
    <script src="{{ asset('libs/js/massonry.js') }}"></script>
@endsection