<div class="container" style="margin-top: 5px;margin-bottom: 15px;">
	<form action="{{ isset($request) && $request->get('type') == 'categories' ? route('attributes.store', [$item->id, 'type' => 'categories']) :
		(isset($request) && $request->get('type') == 'club' ? route('attributes.store', [$item->id, 'type' => 'club']) :
		route('attributes.store', [$item->id, 'type' => 'trener'])) }}" method="POST" class="store_attributes">
		<?php isset($request) && $request->get('type') == 'categories' ? $filters = $item->filters : $filters = $item->category->filters ?>
		@foreach($filters->filter(function ($item) {return $item['id'] != 100 && $item['id'] != 101 && $item['id'] != 102;}) as $filter)
			<div class="col-md-6">
                <h3>{{ $filter->title }}</h3>
                @foreach($filter->options as $option)
                    <div class="checkbox"><label> <input name="attrs[]" @if(in_array($option->id, $filter_values->lists('id')->toArray())) checked @endif value="{{ $option->id }}" type="checkbox"> {{ $option->title }} </label></div>
                @endforeach
            </div>
        @endforeach
        <div class="col-md-12">
            <button type="submit" class="btn btn-default">Сохранить</button>
        </div>
    </form>
</div>

<script>
    $('.store_attributes').on('submit', function(){
        var self = $(this);
        $.post(self.attr('action'), self.serialize(), function(data){
            alert('Сохранено!');
        });
        return false;
    });
</script>