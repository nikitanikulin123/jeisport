@if(Request::get('type'))
	<a class="btn btn-primary" href="{{ $model->createUrl(['type='. $request]) }}">Добавить категорию</a>
@elseif(Request::path() == 'admin_panel/clubs')
	<a class="btn btn-primary" href="{{ $model->createUrl() }}">Добавить заведение</a>
@elseif(Request::path() == 'admin_panel/treners')
	<a class="btn btn-primary" href="{{ $model->createUrl() }}">Добавить специалиста</a>
@endif

<div class="margin-top-wrapper">
    <div class="panel panel-default">
        <div class="panel-body">
            <p>Записей: {{ $rows->total() }}</p>
            <p>Активных: {{ $count_active }}</p>
	        @if(!isset($request)) <p>Ожидают модерации: {{ $count_unmoderated }}</p> @endif
        </div>
    </div>
</div>
<div class="margin-top-wrapper">
    <table class="table table-condensed">
        <thead>
        <tr>
            <th>#</th>
	        @if(isset($request))
	            <th>Название категории</th>
	            <th>Активен</th>
	            <th>Дата создания</th>
	            <th>Подкатегории</th>
	            <th>Порядок вывода</th>
			@else
		        <th>@if(Request::path() == 'admin_panel/clubs')Название заведения @else ФИО специалиста @endif</th>
		        <th>Акции</th>
		        <th>Отзывы</th>
		        <th>Прайс лист</th>
		        <th>Категория</th>
		        <th>Город</th>
		        <th>Активен</th>
		        <th>Промодерирован</th>
		        <th>Дата создания</th>
		        <th>Подкатегории</th>
		        <th>Приоритет</th>
		        <th>SEO</th>
	        @endif
        </tr>
        @if(!isset($request))
	        <form action="">
	            <tr class="filters">
	                <th><input name="id" value="{{ Request::get('id') }}" type="text" class="form-control" placeholder="#"></th>
	                <th><input name="title" value="{{ Request::get('title') }}" type="text" class="form-control" placeholder=""></th>
	                <th></th>
	                <th></th>
	                <th></th>
	                <th>{!! Form::select('category_id', Request::path() == 'admin_panel/clubs' ? $categories->filter(function ($item) {return $item['type'] == 'club';})->lists('title', 'id')
	                    : $categories->filter(function ($item) {return $item['type'] == 'trener';})->lists('title', 'id'), Request::get('category_id'), ['class' => 'form-control', 'placeholder' => 'Выберите']) !!}</th>
	                <th>{!! Form::select('city_id', $cities->lists('title', 'id'), Request::get('city_id'), ['class' => 'form-control', 'placeholder' => 'Выберите']) !!}</th>
	                <th>{!! Form::select('active', [0 => 'Нет', 1 => 'Да'], Request::get('active'), ['class' => 'form-control', 'placeholder' => 'Выберите']) !!}</th>
	                <th>{!! Form::select('moderated', [0 => 'Нет', 1 => 'Да'], Request::get('moderated'), ['class' => 'form-control', 'placeholder' => 'Выберите']) !!}</th>
	                <th>
	                    <button type="submit" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> Фильтр</button>
	                </th>
	            </tr>
	        </form>
        @endif
        </thead>
        <tbody>
        @foreach($rows as $row)
            <tr class="@if($row->moderated) success @elseif($row->active) warning @else danger @endif">
                <td>{{ $row->id }}</td>
	            @if(!isset($request))
		            @if(isset($row->title))
			            @if($row->category && $row->city)
		                    <td><a target="_blank" href="{{ route('clubs.show', [$row->city->slug, $row->category->slug, $row->slug]) }}">{{ $row->title }}</a></td>
			            @else
				            <td>{{ $row->title }}</td>
			            @endif
		                <td><a href="{{ url('admin_panel/stocks?club_id=' . $row->id) }}"><span class="glyphicon glyphicon-eye-open"></span></a> {{ $row->stocks->count() }}</td>
		                <td><a href="{{ url('admin_panel/reviews?club_id=' . $row->id) }}"><span class="glyphicon glyphicon-eye-open"></span></a> {{ $row->reviews->count() }}</td>
		                <td><a href="{{ url('admin_panel/price_list_categories?club_id=' . $row->id) }}"><span class="glyphicon glyphicon-eye-open"></span></a> {{ $row->priceListCategories->count() }}</td>
		            @else
			            <td>{{ $row->FullName }}</td>
			            <td><a href="{{ url('admin_panel/stocks?trener_id=' . $row->id) }}"><span class="glyphicon glyphicon-eye-open"></span></a> {{ $row->stocks->count() }}</td>
			            <td><a href="{{ url('admin_panel/reviews?trener_id=' . $row->id) }}"><span class="glyphicon glyphicon-eye-open"></span></a> {{ $row->reviews->count() }}</td>
			            <td><a href="{{ url('admin_panel/price_list_categories?trener_id=' . $row->id) }}"><span class="glyphicon glyphicon-eye-open"></span></a> {{ $row->priceListCategories->count() }}</td>
		            @endif
	                @if($row->category)
	                    <td>{{ $row->category->title }}</td>
					@else
			            <td></td>
		            @endif
		            @if($row->city)
		                <td>{{ $row->city->title }}</td>
		            @else
			            <td></td>
		            @endif
		            <td class="editable"><a class="@if(!$row->active) editable-empty @endif editable-click" href="#"
	                                        data-type="checklist"
	                                        data-emptytext="нет"
	                                        data-name="active"
	                                        data-source="{ 1 : 'да' }"
	                                        data-value="{{ $row->active }}"
	                                        data-pk="{{ $row->id }}"
	                                        data-url="{{ route('admin.units.update', ['type' => $row->title ? 'club' : 'trener']) }}"
	                    >{!! $row->active ? 'да' : 'нет' !!}</a></td>
		            <td>{!! $row->moderated ? 'да' : 'нет' !!}</td>
		            <td>{{ $row->created_at->format('d.m.Y H:i:s') }}</td>
	                <td>
	                    <a href="{{ route('attributes.index', [$row->id, 'type' => isset($row->title) ? 'club' : 'trener']) }}" class="add-attrs" title="Добавить значения атрибутов"><span class="glyphicon glyphicon-plus"></span></a>
		                {{ $row->filterValues->count() }}
	                </td>
			            <td>{{ $row->priority }}</td>
		            <td>@if($seo = $row->metas->first())<a href="{{ url(Admin::model(\App\Models\Meta::class)->editUrl($seo->id)) }}" class="btn btn-default glyphicon glyphicon-eye-open"></a>@endif</td>
	                <td>
	                    <div class="text-right pull-right">
	                        <a href="{{ $model->editUrl($row->id) }}" class="btn btn-default btn-sm" data-toggle="tooltip" title="" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a>
	                        {!! Form::open(['route' => ['admin.model.destroy', $row->title ? 'clubs' : 'treners', $row->id], 'method' => 'DELETE', 'style' => 'display:inline-block']) !!}
	                        <button class="btn btn-danger btn-sm btn-delete" data-toggle="tooltip" title="" data-original-title="Удалить">
	                            <i class="fa fa-times"></i>
	                        </button>
	                        {!! Form::close() !!}
	                    </div>
	                </td>
	            @else
		            <td>{{ $row->title }}</td>
		            <td>{{ $row->active ? 'да' : '-' }}</td>
		            <td>{{ $row->created_at->format('d.m.Y H:i:s') }}</td>
		            <td>
			            <a href="{{ route('attributes.index', [$row->id, 'type' => 'categories']) }}" class="add-attrs" title="Добавить значения атрибутов"><span class="glyphicon glyphicon-plus"></span></a>
			            {{ $row->filterValues->count() }}
		            </td>
		            <td>{{ $row->order }}</td>
		            <td>
			            <div class="text-right pull-right">
				            <a href="{{ $model->editUrl($row->id) }}" class="btn btn-default btn-sm" data-toggle="tooltip" title="" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a>
				            {!! Form::open(['route' => ['admin.model.destroy', 'categories', $row->id], 'method' => 'DELETE', 'style' => 'display:inline-block']) !!}
				            <button class="btn btn-danger btn-sm btn-delete" data-toggle="tooltip" title="" data-original-title="Удалить">
					            <i class="fa fa-times"></i>
				            </button>
				            {!! Form::close() !!}
			            </div>
		            </td>
	            @endif
            </tr>
        @endforeach
        </tbody>
    </table>
	@if(!isset($request))
	    {!! $rows->appends([
	        'id' => Request::get('id'),
	        'title' => Request::get('title'),
	        'category_id' => Request::get('category_id'),
	    ])->render() !!}
	@else
		{!! $rows->appends([
			'id' => Request::get('id'),
			'title' => Request::get('title'),
			'type' => $request,
		])->render() !!}
	@endif
</div>