@extends('layouts.app')
@section('content')
	@include('partials._messages')
	<div class="restart-pass" style="background-image: url(/img/png/avtorization.png);">
		<div class="container">
			<form method="POST" action="/password/email"  class="b-login_form col-md-4 col-md-offset-4">
				{!! csrf_field() !!}
				<h2>Восстановление пароля</h2>
				<div class="form-group">
					<input class="form-control" placeholder="Email"  type="email" name="email" value="{{ old('email') }}">
				</div>
				<br>
				<div>
					<button type="submit" class="custom-btn">Восстановить пароль</button>
				</div>
			</form>
		</div>
	</div>
@endsection