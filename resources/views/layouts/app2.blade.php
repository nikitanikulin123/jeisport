<!DOCTYPE html>
<html lang="ru">
<head>
	<link rel="icon" href="/img/favicon/favicon.ico">
	<title>{{ isset($metatitle) && $metatitle ? $metatitle : config('admin.title') }}</title>
	<meta name="description" content="{{ isset($metadesc) ? $metadesc : null }}">
	<meta name="keywords" content="{{ isset($metakeyw) ? $metakeyw : null }}">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    @yield('assets-css')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
@include('partials._header')
@include('partials._alerts')
@yield('content')
@include('partials._modal')
@if(!Agent::isMobile())
    <footer>
        <div class="container">
            <div class="row">

                <div class="col-md-3">
                    <a href="">
                        <img src="/logo_footer.png" alt="">
                        <img class="hover" src="/logo.png" alt="">
                    </a>
                </div>

                <div class="col-md-3">
                    <p><a class="ajax_modal" href="" data-action="addClub" data-type="club">Добавить место</a></p>
                    <p><a class="ajax_modal" href="" data-action="addTrener" data-type="trener">Добавить специалиста</a></p>
                    <p><a href="">Для организаций</a></p>
                    <p><a href="">О проекте «JeiSport»</a></p>
                </div>

                <div class="col-md-3">
                    <p><a href="">Правила работы с отзывами</a></p>
                    <p><a href="">Обратная связь</a></p>
                    <p><a href="">Вакансии</a></p>
                    <p><a href="">Контакты</a></p>
                </div>

                <div class="col-md-3">
                    <p>Мы в соц сетях</p>
                    <ul>
                        <li><a href=""><img src="/img/png/fb.png" alt=""></a></li>
                        <li><a href=""><img src="/img/png/gl.png" alt=""></a></li>
                        <li><a href=""><img src="/img/png/in.png" alt=""></a></li>
                        <li><a href=""><img src="/img/png/ins.png" alt=""></a></li>
                        <li><a href=""><img src="/img/png/tv.png" alt=""></a></li>
                        <li><a href=""><img src="/img/png/tw.png" alt=""></a></li>
                        <li><a href=""><img src="/img/png/vb.png" alt=""></a></li>
                        <li><a href=""><img src="/img/png/wifi.png" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
@endif

@yield('assets-js')
</body>
</html>