@extends('layouts.app')
@section('content')
        <!-- search-link -->

@include('partials._top_navbar')

<section class="history-user">
    <div class="container">
        <h1>История <span> вашей активности</span></h1>
        <div class="row">
            <div class="col-md-6">
                <h4>Просмотры</h4>
                <div class="history-col">
                    <a href="#" class="pic">
                        <img src="/img/jpg/blog-img2.jpg" alt="">
                    </a>
                    <div class="text">
                        <a class="title">Lomonosov Bar</a>
                        <a href="">Ночные клубы > </a>
                        <a href="">Танцы , </a>
                        <a href="">Адресс: пр Манаса 23</a>
                        <i class="date">23.12.2016</i>
                    </div>
                </div>

                <div class="history-col">
                    <a href="#" class="pic">
                        <img src="/img/jpg/blog-img2.jpg" alt="">
                    </a>
                    <div class="text">
                        <a href="#" class="title">Lomonosov Bar</a>
                        <a href="">Ночные клубы > </a>
                        <a href="">Танцы , </a>
                        <a href="">Адресс: пр Манаса 23</a>
                        <i class="date">23.12.2016</i>
                    </div>
                </div>

                <div class="history-col">
                    <a href="#" class="pic">
                        <img src="/img/jpg/blog-img2.jpg" alt="">
                    </a>
                    <div class="text">
                        <a href="#" href="#" class="title">Lomonosov Bar</a>
                        <a href="">Ночные клубы > </a>
                        <a href="">Танцы , </a>
                        <a href="">Адресс: пр Манаса 23</a>
                        <i class="date">23.12.2016</i>
                    </div>
                </div>

                <div class="history-col">
                    <a href="#" class="pic">
                        <img src="/img/jpg/blog-img2.jpg" alt="">
                    </a>
                    <div class="text">
                        <a href="#" class="title">Lomonosov Bar</a>
                        <a href="">Ночные клубы > </a>
                        <a href="">Танцы , </a>
                        <a href="">Адресс: пр Манаса 23</a>
                        <i class="date">23.12.2016</i>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <h4>Оценки</h4>
                <div class="history-col">
                    <a href="#" class="pic">
                        <img src="/img/jpg/blog-img2.jpg" alt="">
                    </a>
                    <div class="text">
                        <a class="title">Lomonosov Bar</a>
                        <span class="reiting">Ваша оценка: </span>
                        <ul>
                            <li class="star"></li>
                            <li class="star"></li>
                            <li class="star"></li>
                            <li class="star"></li>
                            <li class="star active"></li>
                        </ul>
                        <i class="date">23.12.2016</i>
                    </div>
                </div>
                <div class="history-col">
                    <a href="#" class="pic">
                        <img src="/img/jpg/blog-img2.jpg" alt="">
                    </a>
                    <div class="text">
                        <a class="title">Lomonosov Bar</a>
                        <span class="reiting">Ваша оценка: </span>
                        <ul>
                            <li class="star"></li>
                            <li class="star"></li>
                            <li class="star"></li>
                            <li class="star"></li>
                            <li class="star active"></li>
                        </ul>
                        <i class="date">23.12.2016</i>
                    </div>
                </div>
                <div class="history-col">
                    <a href="#" class="pic">
                        <img src="/img/jpg/blog-img2.jpg" alt="">
                    </a>
                    <div class="text">
                        <a class="title">Lomonosov Bar</a>
                        <span class="reiting">Ваша оценка: </span>
                        <ul>
                            <li class="star"></li>
                            <li class="star"></li>
                            <li class="star"></li>
                            <li class="star"></li>
                            <li class="star active"></li>
                        </ul>
                        <i class="date">23.12.2016</i>
                    </div>
                </div>
                <div class="history-col">
                    <a href="#" class="pic">
                        <img src="/img/jpg/blog-img2.jpg" alt="">
                    </a>
                    <div class="text">
                        <a class="title">Lomonosov Bar</a>
                        <span class="reiting">Ваша оценка: </span>
                        <ul>
                            <li class="star"></li>
                            <li class="star"></li>
                            <li class="star"></li>
                            <li class="star"></li>
                            <li class="star active"></li>
                        </ul>
                        <i class="date">23.12.2016</i>
                    </div>
                </div>
                <div class="history-col">
                    <a href="#" class="pic">
                        <img src="/img/jpg/blog-img2.jpg" alt="">
                    </a>
                    <div class="text">
                        <a class="title">Lomonosov Bar</a>
                        <span class="reiting">Ваша оценка: </span>
                        <ul>
                            <li class="star"></li>
                            <li class="star"></li>
                            <li class="star"></li>
                            <li class="star"></li>
                            <li class="star active"></li>
                        </ul>
                        <i class="date">23.12.2016</i>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

@endsection


















