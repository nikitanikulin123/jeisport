@extends('layouts.app')
@section('content')
    <section class="billing">
        <div class="container">
            <div class="row">
				<div class="pull-left" id="back-profile">
					<a href="{{ route('profiles.index') }}">Назад к профилю</a>
				</div>
				<div class="clearfix"></div>
				<div class="cover">
					@if(Request::get('premium'))
						{!! Form::open(['route' => ['billing.makePremium', $billedItem->id, 'type' => $type, 'premium' => 1], 'method' => 'POST']) !!}
							<div class="col-md-offset-1 col-md-4 premium">
								<div class="img-left">
									<h3>Тарифный план Premium</h3>
								</div>
								<div class="radio-button">
									<h6>Выберите срок размещения</h6>
									@foreach($billingVars->premiumClub as $key => $val)
										@InputBlock([$type = "radio", $item = 'duration', $label = $key . ' ' . trans_choice("месяц|месяца|месяцев", $key) . ($key == 3 && !$billedItem->used_trial ? ' (бесплатно)' : ''),
											$value = $key, $var = $billedItem, $checked = $key == 3 ? true : false, $dp = "class=\"radio\"", $lp = "class=\"radio-label\"", $p = "id=duration-{$key}"])
									@endforeach
								</div>
							</div>
							<div class="col-md-offset-1 col-md-4 premium">
								<div class="radio-button pay">
									<div class="pay">
										<h6>Выберите способ пополнения</h6>
										<span>терминал оплаты</span>
										<a href="">
											<img src="/img/mobilnik.png" alt="">
										</a>
									</div>
									<button type="submit" class="free">Выбрать</button>
								</div>
							</div>
						{!! Form::close() !!}
					@else
						<h2>Выберите тарифный план для заведения "<span>{{ $billedItem->title }}</span>"</h2>
						<div class="rateplans" style="overflow: hidden;">
							{!! Form::open(['route' => ['billing.makePremium', $billedItem->id, 'type' => $type, 'premium' => 0], 'method' => 'POST']) !!}
							<div class="col-md-offset-1 col-md-4 free">
								<div class="img-left"><h3>Тарифный план Free</h3></div>

								<div class="closes">
									<h6>Недостатки</h6>
									<ul>
										<li>Формы заявки на обратный звонок</li>
										<li>Отсутствие статистики посещаемости
											вашей странице</li>
										<li>Наличие рекламы конкурентов на
											вашей странице</li>
										<li>Возможность загружать только
											одну фотографию</li>
										<li>Нет возможности добавлять
											специалистов вашего заведения</li>
										<li>Нет возможности добавлять прайс листы,
											акции вашего заведения</li>
										<li>Отсутствует тех. поддержка и
											персональный менеджер</li>
									</ul>
								</div>
								<div class="checked">
									<h6>Преимущества</h6>
									<ul>
										<li>Описание предоставляемых услуг</li>
										<li>Отображение на карте вашего заведения</li>
										<li>Контакты вашего заведения</li>
										<li>Неограниченный период размещения</li>
										<li>Отзывы пользователей о вашем заведении</li>
									</ul>
								</div>
								<button type="submit" class="free">Выбрать</button>
							</div>
							{!! Form::close() !!}

							{!! Form::open(['route' => ['billing.makePremium', $billedItem->id, 'type' => $type, 'premium' => 1], 'method' => 'POST']) !!}
							<div class="col-md-offset-1 col-md-4 premium">
								<div class="img-left">
									<h3>Тарифный план Premium</h3>
								</div>
								<div class="checked">
									<h6>Преимущества</h6>
									<ul>
										<li>Оптимизация под поисковые системы</li>
										<li>Выгодная позиция в результатах поиска</li>
										<li>Персональный менеджер (24/7)</li>
										<li>Отзывчивая тех. Поддержка</li>
										<li>Уведомления на почту и в личный кабинет,
											о заявках на обратный звонок</li>
										<li>Неограниченное количество фотографий</li>
										<li>Возможность добавлять специалистов
											заведения</li>
										<li>Возможность добавлять прайс лист</li>
										<li>Возможность добавлять акции вашего
											заведения</li>
										<li>Отсутствие рекламы на вашей странице</li>
										<li>Отображение на карте вашего заведения</li>
										<li>Подробная статистика посещаемости
											вашего заведения</li>
										<li>Отзывы пользователей о вашем заведении</li>
									</ul>
								</div>
								<button type="submit" class="premium">Выбрать</button>
							</div>
							{!! Form::close() !!}
						</div>
					@endif
				</div>
            </div>
        </div>
    </section>
@endsection