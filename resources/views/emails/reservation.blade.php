<b>Имя:</b> {{ $name }} <br/>
<b>Телефон:</b> {{ $phone }} <br/>
<b>E-mail:</b> {{ $email }} <br/>
<b>Номер:</b> {{ \App\Models\Room::find($room_id)->title }} <br/>
<b>Количество человек:</b> {{ $people }} <br/>
<b>Дата заселения:</b> {{ $arrival_date }} <br/>
<b>Дата выезда:</b> {{ $departure_date }} <br/>
<b>IP-адрес:</b> {{ $ip_address }} <br/>
<b>Дата:</b> {{ \Carbon\Carbon::now()->format('d.m.Y H:m:s') }} <br/>