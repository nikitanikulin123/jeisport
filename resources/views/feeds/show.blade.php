@extends('layouts.app')
@section('cities-selector')
	@include('partials.mini._cities_selector')
@endsection
@section('content')
    <section class="interesting-item">
        <div class="slider-blog">
            <div class="item">
                <img class="img-wrapper blur" src="{{ $feed->setImage('1520_580', 'image') }}" alt="{{ $feed->setImage('1520_580', 'image') }}">
                <div class="title-wrapper">
                    <img src="/img/png/book.png" alt="/img/png/book.png">
                    <h1>{{ $feed->title }}</h1>
                    <h2>{{ $feed->sub_title }}</h2>
                    <p>{{ $feed->created_at->format('d.m.Y') }}</p>
                </div>
            </div>
        </div>
    </section>

    <section class="interesting-item-discript">
        <div class="container-fluid">

                <div class="content">
                    <div class="col-md-12">

                    <div class="dashed">
                        {!! $feed->body !!}
                    </div>

	                @for($i = 1; $i <= 4; $i++)
						<?php $case_title = 'case_title' . $i ?>
						<?php $case_image = 'case_image' . $i ?>
						<?php $case_text = 'case_text' . $i ?>
						@if($feed->{$case_title})
		                    <div class="category">
                                <div class="border">
                                    <div class="img-wrapper">
                                        @if($feed->{$case_image})
                                            <img src="{{ $feed->setImage('910_470', $feed->{$case_image}) }}" alt="{{ $feed->setImage('910_470', $feed->{$case_image}) }}">
                                        @else
                                            <img src="https://placehold.it/910x470" alt="https://placehold.it/910x470">
                                        @endif
                                            <h4>{{ $feed->{$case_title} }}</h4>
                                    </div>

                                    @if($feed->club_id != 0 && $feed->club)
                                        <div class="discript">
                                            <a href="{{ route('clubs.show', [$feed->club->city->slug, $feed->club->category->slug, $feed->club->slug]) }}"></a>
                                            <h3>{{ $feed->club->title }}</h3>
                                            <p>{{ $feed->club->address }}</p>
                                        </div>
                                    @endif

                                    <div class="discript">
                                        <p>{{ $feed->{$case_text} }}</p>
                                    </div>
                                </div>
		                    </div>
						@endif
	                @endfor

	                @if(count($club_feeds) > 0)
						@foreach($club_feeds->whereLoose('club.moderated', true) as $club_feed)
			                <div class="category">
                                <div class="border">
                                    <a href="{{ route('clubs.show', [$club_feed->club->city->slug, $club_feed->club->category->slug, $club_feed->club->slug]) }}"></a>
                                    <div class="img-wrapper">
                                        <?php $club_feed->image ? $image = $club_feed->image : $image = $club_feed->club->image ?>
                                        <img src="{{ $club_feed->setImage('910_470', $image) }}" alt="{{ $club_feed->setImage('910_470', $image) }}">
                                        <h4>{{ $club_feed->title }}</h4>
                                    </div>
                                    <div class="discript">

                                        <h3>{{ $club_feed->club->title }}</h3>
                                        <h5>{{ $club_feed->club->address }}</h5>
                                        <p>{!! $club_feed->body !!}</p>
                                    </div>

                                </div>
			                </div>

		                @endforeach
	                @endif

                    @if(count($similars) > 0)
                    <div class="clearfix"></div>
                        <div class="paper">
                            <div class="container">
                                <h2>Похожие статьи</h2>
                                <div id="macy-container">
                                    @foreach($similars as $similar)
                                        <div class="grid-item">
                                            <a href="{{ route('feeds.show', [$similar->city->slug, $similar->slug]) }}"></a>
                                            <img src="{{ $similar->setImage('248_190', 'image') }}" alt="{{ $similar->setImage('248_190', 'image') }}">
                                            <h3>{{ $similar->title }}</h3>
                                            <p>{{ str_limit(strip_tags($similar->body)) }}</p>

                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    @endif
                    </div>
                </div>

        </div>
    </section>
@endsection

@section('assets-js')
    <script src="{{ asset('libs/js/massonry.js') }}"></script>
@endsection