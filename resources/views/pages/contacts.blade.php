@extends('layouts.app')
@section('cities-selector')
    @include('partials.mini._cities_selector')
@endsection
@section('content')
    <section class="contact">
        <div class="container-fluid">
          	<div class="content">
				@if($contacts)
					<h1>Контакты</h1>
					<div class="col-md-8">
						@if($contacts->google_map_code && $contacts->google_map_code != '[]')
							@include('admin.maps._googleMap', [$instance = $contacts, $type = 'static', $size = ['100%', '450px']])
						@endif
					</div>

					<div class="col-md-4">
						<h3>Связаться с нами</h3>
						<ul>
							<li><i class="fa fa-map-marker" aria-hidden="true"></i><span>{{ $contacts->address }}</span></li>
							<li><i class="fa fa-mobile" aria-hidden="true"></i><span>{{ implode(',', $contacts->phone) }}</span></li>
							<li><i class="fa fa-envelope" aria-hidden="true"></i><span>{{ $contacts->email }}</span></li>
						</ul>

						<h3>Обратная связь</h3>
						{!! Form::open(['route' => ['callbacks.create', 0, 'type' => 'contacts'], 'class' => '']) !!}
							@InputBlock([$type = "input", $item = 'name', $label = null, $p = "placeholder=\"Введите ваше имя\""])
							@InputBlock([$type = "input", $item = 'callback_phone', $label = null, $p = "placeholder=\"Введите телефон\""])
							@InputBlock([$type = "textarea", $item = 'description', $label = null, $p = "placeholder=\"Текст сообщения\""])
							<button type="submit" class="custom-btn btn btn-default">Отправить</button>
						{!! Form::close() !!}
					</div>

				@endif

			</div>
        </div>
    </section>
@endsection