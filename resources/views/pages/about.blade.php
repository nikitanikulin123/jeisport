@extends('layouts.app')
@section('cities-selector')
    @include('partials.mini._cities_selector')
@endsection
@section('content')
    <section class="about-js">
        <div class="container-fluid">
            <div class="content">
                <div class="col-md-12">
	                @if(isset($page) && $page)
	                    <h1>{{ $page->title }}</h1>
	                    <div class="cover left">
	                        <div class="left">
		                        @TagBlock([$type = "img", $var = $page, $src = "image", $size = "600_400"])
	                            <div class="title">
		                            {!! $page->section !!}
	                            </div>
	                        </div>
	                        <div class="clearfix"></div>
	                        <div class="des">
		                        {!! $page->sub_section !!}
	                        </div>
	                    </div>
	                    <div class="cover right">
	                        <div class="left">
		                        @TagBlock([$type = "img", $var = $page, $src = "image2", $size = "600_400"])
	                            <div class="title">
		                            {!! $page->section2 !!}
	                            </div>
	                        </div>
	                        <div class="clearfix"></div>
		                    <div class="des">
			                    {!! $page->sub_section2 !!}
		                    </div>
	                    </div>
					@else
		                <h1>О нас</h1>
					@endif
                </div>
            </div>
        </div>
    </section>
@endsection