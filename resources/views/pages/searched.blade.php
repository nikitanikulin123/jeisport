@extends('layouts.app')
@section('content')
    <section class="searched">
        <div class="container-fluid">
			<div class="cover">
				<h2 class="title">По запросу "{{ $query }}" найдено {{ count($clubs) + count($treners) }} {{ trans_choice("совпадение|совпадения|совпадений", count($clubs) + count($treners)) }}.
					@if(count($clubs)) Заведений: {{ count($clubs) }} @endif
					@if(count($treners)) Специалистов: {{ count($treners) }} @endif
				</h2>
				@if(count($clubs))
					<div class="col-md-12">
						<div class="pull-left">
							<h3>Заведения</h3>
						</div>
						<div class="clearfix"></div>
						<ul class="b-units-list m-zavedenie" id="sidebar">
							@foreach($clubs as $club)
								<li>
									@include('partials.mini._unitsList', [$item = $club, $block = 'clubs.createEdit', $img_size = '160_115'])
								</li>
							@endforeach
						</ul>
					</div>
				@endif
				@if(count($treners))
					<div class="col-md-6">
						<div class="pull-left">
							<h3>Специалисты</h3>
						</div>
						<div class="clearfix"></div>
						<ul class="b-units-list m-zavedenie" id="sidebar">
							@foreach($treners as $trener)
								<li>
									@include('partials.mini._unitsList', [$item = $trener, $block = 'treners.createEdit', $img_size = '125_125'])
								</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
        </div>
    </section>
@endsection


