<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('metas', function (Blueprint $table) {
			$table->integer('metable_id')->after('metadesc');
			$table->string('metable_type')->after('metable_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('metas', function (Blueprint $table) {
			$table->dropColumn('metable_id');
			$table->dropColumn('metable_type');
		});
	}
}
