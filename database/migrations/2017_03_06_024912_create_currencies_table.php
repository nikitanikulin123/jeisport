<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('currencies', function (Blueprint $table) {
			$table->engine = "InnoDB";
			$table->increments('id');
			$table->string('title');
			$table->boolean('active');
			$table->integer('order');
			$table->timestamps();
		});

		DB::statement('ALTER TABLE callbacks ENGINE = InnoDB');
		DB::statement('ALTER TABLE seo_street_categories ENGINE = InnoDB');
		DB::statement('ALTER TABLE visits ENGINE = InnoDB');

		DB::statement("SET FOREIGN_KEY_CHECKS=0");
		Schema::table('price_lists', function (Blueprint $table) {
			$table->integer('currency_id')->after('price')->unsigned();

			$table->foreign('currency_id')->references('id')->on('currencies')->onUpdate('cascade')->onDelete('cascade');
		});

		Schema::table('seo_street_categories', function (Blueprint $table) {
			$table->integer('street_id')->unsigned()->change();
			$table->integer('category_id')->unsigned()->change();

			$table->foreign('street_id')->references('id')->on('streets')->onUpdate('cascade')->onDelete('cascade');
			$table->foreign('category_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('cascade');
		});
		DB::statement("SET FOREIGN_KEY_CHECKS=1");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('price_lists', function (Blueprint $table) {
			$table->dropForeign('price_lists_currency_id_foreign');
			$table->dropColumn('currency_id');
		});

		Schema::drop('currencies');

		Schema::table('seo_street_categories', function (Blueprint $table) {
			$table->dropForeign('seo_street_categories_street_id_foreign');
			$table->dropForeign('seo_street_categories_category_id_foreign');
		});
	}
}
