<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPageSubtitleToSeoTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('seo_city_categories', function (Blueprint $table) {
			$table->string('page_subtitle')->after('page_title');
		});

		Schema::table('seo_city_filters', function (Blueprint $table) {
			$table->string('page_subtitle')->after('page_title');
		});

		Schema::table('seo_city_filter_values', function (Blueprint $table) {
			$table->string('page_subtitle')->after('page_title');
		});

		Schema::table('seo_region_categories', function (Blueprint $table) {
			$table->string('page_subtitle')->after('page_title');
		});

		Schema::table('seo_street_categories', function (Blueprint $table) {
			$table->string('page_subtitle')->after('page_title');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('seo_city_categories', function (Blueprint $table) {
			$table->dropColumn('page_subtitle');
		});

		Schema::table('seo_city_filters', function (Blueprint $table) {
			$table->dropColumn('page_subtitle');
		});

		Schema::table('seo_city_filter_values', function (Blueprint $table) {
			$table->dropColumn('page_subtitle');
		});

		Schema::table('seo_region_categories', function (Blueprint $table) {
			$table->dropColumn('page_subtitle');
		});

		Schema::table('seo_street_categories', function (Blueprint $table) {
			$table->dropColumn('page_subtitle');
		});
	}
}
