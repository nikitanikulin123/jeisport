<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToClubFilterValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");
        Schema::table('club_filter_value', function (Blueprint $table) {
            $table->integer('club_id')->unsigned()->change();
        });
        Schema::table('club_filter_value', function (Blueprint $table) {
            $table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
        });

        Schema::table('club_filter_value', function (Blueprint $table) {
            $table->integer('filter_value_id')->unsigned()->change();
        });
        Schema::table('club_filter_value', function (Blueprint $table) {
            $table->foreign('filter_value_id')->references('id')->on('filter_values')->onDelete('cascade');
        });

        Schema::table('filter_values', function (Blueprint $table) {
            $table->integer('filter_id')->unsigned()->change();
        });
        Schema::table('filter_values', function (Blueprint $table) {
            $table->foreign('filter_id') ->references('id')->on('filters')->onDelete('cascade');
        });
        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('club_filter_value', function (Blueprint $table) {
            $table->dropForeign('club_filter_value_club_id_foreign');
            $table->dropForeign('club_filter_value_filter_value_id_foreign');
        });
        Schema::table('filter_values', function (Blueprint $table) {
            $table->dropForeign('filter_values_filter_id_foreign');
        });
    }
}
