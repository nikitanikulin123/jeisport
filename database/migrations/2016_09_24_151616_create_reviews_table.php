<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('parent_id')->nullable();
            $table->integer('lft')->nullable();
            $table->integer('rgt')->nullable();
            $table->integer('depth')->nullable();

            $table->boolean('moderated');
            $table->string('name');
            $table->integer('user_id')->nullable();
            $table->integer('club_id')->unsigned();
            $table->text('text');
            $table->integer('rating_plus');
            $table->integer('rating_minus');

            $table->timestamps();
        });


        Schema::table('reviews', function (Blueprint $table) {
//            $table->foreign('user_id')
//                ->references('id')->on('users')
//                ->onDelete('cascade');
            $table->foreign('club_id')
                ->references('id')->on('clubs')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reviews', function (Blueprint $table) {
//            $table->dropForeign('reviews_user_id_foreign');
            $table->dropForeign('reviews_club_id_foreign');
        });
        Schema::drop('reviews');
    }
}
