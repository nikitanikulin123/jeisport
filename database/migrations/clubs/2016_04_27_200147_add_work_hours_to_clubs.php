<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWorkHoursToClubs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table) {
            //Понедельник
            $table->tinyInteger('work_in_mon');
            $table->time('mon_time_from');
            $table->time('mon_time_to');
            //Вторник
            $table->tinyInteger('work_in_tue');
            $table->time('tue_time_from');
            $table->time('tue_time_to');
            //Среда
            $table->tinyInteger('work_in_wed');
            $table->time('wed_time_from');
            $table->time('wed_time_to');
            //Четверг
            $table->tinyInteger('work_in_thu');
            $table->time('thu_time_from');
            $table->time('thu_time_to');
            //Пятница
            $table->tinyInteger('work_in_fri');
            $table->time('fri_time_from');
            $table->time('fri_time_to');
            //Суббота
            $table->tinyInteger('work_in_sat');
            $table->time('sat_time_from');
            $table->time('sat_time_to');
            //Воскресенье
            $table->tinyInteger('work_in_sun');
            $table->time('sun_time_from');
            $table->time('sun_time_to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropColumn('work_in_mon');
            $table->dropColumn('mon_time_from');
            $table->dropColumn('mon_time_to');

            $table->dropColumn('work_in_tue');
            $table->dropColumn('tue_time_from');
            $table->dropColumn('tue_time_to');


            $table->dropColumn('work_in_wed');
            $table->dropColumn('wed_time_from');
            $table->dropColumn('wed_time_to');


            $table->dropColumn('work_in_thu');
            $table->dropColumn('thu_time_from');
            $table->dropColumn('thu_time_to');


            $table->dropColumn('work_in_fri');
            $table->dropColumn('fri_time_from');
            $table->dropColumn('fri_time_to');


            $table->dropColumn('work_in_sat');
            $table->dropColumn('sat_time_from');
            $table->dropColumn('sat_time_to');


            $table->dropColumn('work_in_sun');
            $table->dropColumn('sun_time_from');
            $table->dropColumn('sun_time_to');

        });
    }
}
