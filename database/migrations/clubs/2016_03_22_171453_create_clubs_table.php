<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('images');
            $table->string('phone');
            $table->string('address');
            $table->string('lat_lng');
            $table->string('site');
            $table->text('description');
            $table->integer('priority');
            $table->boolean('active');

            $table->string('metatitle')->nullable();
            $table->string('metakeyw')->nullable();
            $table->text('metadesc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clubs');
    }
}
