<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFileToPricelistCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('price_list_categories', function (Blueprint $table) {
			$table->string('file')->after('max');
		});

		Schema::table('clubs', function (Blueprint $table) {
			$table->boolean('show_callback_form')->after('email_to_admin');
		});

		Schema::table('treners', function (Blueprint $table) {
			$table->boolean('show_callback_form')->after('email_to_admin');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('price_list_categories', function (Blueprint $table) {
			$table->dropColumn('file');
		});

		Schema::table('clubs', function (Blueprint $table) {
			$table->dropColumn('show_callback_form');
		});

		Schema::table('treners', function (Blueprint $table) {
			$table->dropColumn('show_callback_form');
		});
	}
}
