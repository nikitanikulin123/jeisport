<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('price');
            $table->string('price_for');
            $table->integer('price_list_category_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('price_lists', function (Blueprint $table) {
            $table->foreign('price_list_category_id')->references('id')->on('price_list_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('price_lists');
    }
}
