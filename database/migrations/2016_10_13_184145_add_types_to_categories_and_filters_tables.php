<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypesToCategoriesAndFiltersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('categories', function (Blueprint $table) {
			$table->string('type')->after('slug');
		});

		Schema::table('filters', function (Blueprint $table) {
			$table->string('type')->after('slug');
			$table->dropColumn('category_id');
		});

		Schema::create('category_filter', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('category_id');
			$table->integer('filter_id');
			$table->timestamps();
		});

//		Schema::table('category_filter', function (Blueprint $table) {
//			$table->unique(['category_id', 'filter_id']);
//
//			$table->foreign('category_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('cascade');
//			$table->foreign('filter_id')->references('id')->on('filters')->onUpdate('cascade')->onDelete('cascade');
//		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('categories', function (Blueprint $table) {
			$table->dropColumn('type');
		});

		Schema::table('filters', function (Blueprint $table) {
			$table->dropColumn('type');
			$table->integer('category_id')->after('title');
		});

		Schema::drop('category_filter');
	}
}
