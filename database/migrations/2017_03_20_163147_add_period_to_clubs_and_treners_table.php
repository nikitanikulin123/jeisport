<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPeriodToClubsAndTrenersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('clubs', function (Blueprint $table) {
			$table->integer('period')->after('premium');

			$table->dropColumn('metatitle');
			$table->dropColumn('metakeyw');
			$table->dropColumn('metadesc');
		});

		Schema::table('treners', function (Blueprint $table) {
			$table->integer('period')->after('premium');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('clubs', function (Blueprint $table) {
			$table->dropColumn('period');

			$table->string('metatitle')->after('premium');
			$table->string('metakeyw')->after('metatitle');
			$table->text('metadesc')->after('metakeyw');
		});

		Schema::table('treners', function (Blueprint $table) {
			$table->dropColumn('period');
		});
	}
}
