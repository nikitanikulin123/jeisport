<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImagesToRecommendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('recommends', function (Blueprint $table) {
			$table->string('image_filter_value')->after('amount_by_filter_value');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('recommends', function (Blueprint $table) {
			$table->dropColumn('image_filter_value');
		});
	}
}
