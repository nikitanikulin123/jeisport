<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSuffixAndPrefixToClubsAndTrenersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('clubs', function (Blueprint $table) {
			$table->string('prefix')->after('title');
			$table->string('postfix')->after('prefix');
		});

		Schema::table('treners', function (Blueprint $table) {
			$table->string('prefix')->after('mname');
			$table->string('postfix')->after('prefix');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('clubs', function (Blueprint $table) {
			$table->dropColumn('prefix');
			$table->dropColumn('postfix');
		});

		Schema::table('treners', function (Blueprint $table) {
			$table->dropColumn('prefix');
			$table->dropColumn('postfix');
		});
	}
}
