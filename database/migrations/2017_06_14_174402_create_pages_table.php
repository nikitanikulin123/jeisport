<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('pages', function (Blueprint $table) {
			$table->engine = "InnoDB";
			$table->increments('id');
			$table->string('type');
			$table->string('title');
			$table->string('image');
			$table->text('section');
			$table->text('sub_section');
			$table->string('image2');
			$table->text('section2');
			$table->text('sub_section2');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}
}
