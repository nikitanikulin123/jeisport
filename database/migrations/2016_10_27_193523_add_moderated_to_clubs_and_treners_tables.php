<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModeratedToClubsAndTrenersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('clubs', function (Blueprint $table) {
			$table->boolean('moderated')->after('active');
		});

		Schema::table('treners', function (Blueprint $table) {
			$table->boolean('moderated')->after('active');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('clubs', function (Blueprint $table) {
			$table->dropColumn('moderated');
		});

		Schema::table('treners', function (Blueprint $table) {
			$table->dropColumn('moderated');
		});
	}
}
