<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeoCityFilterValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo_city_filter_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id');
            $table->integer('filter_value_id');
            $table->string('page_title');
            $table->text('page_description');
            $table->string('metatitle');
            $table->text('metakeyw');
            $table->text('metadesc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seo_city_filter_values');
    }
}
