<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhone2ToClubsAndTrenersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('clubs', function (Blueprint $table) {
			$table->string('phone2')->after('phone');
		});

		Schema::table('treners', function (Blueprint $table) {
			$table->string('phone2')->after('phone');
		});

		Schema::table('callbacks', function (Blueprint $table) {
			$table->text('description')->after('closed');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('clubs', function (Blueprint $table) {
			$table->dropColumn('phone2');
		});

		Schema::table('treners', function (Blueprint $table) {
			$table->dropColumn('phone2');
		});

		Schema::table('callbacks', function (Blueprint $table) {
			$table->dropColumn('description');
		});
	}
}
