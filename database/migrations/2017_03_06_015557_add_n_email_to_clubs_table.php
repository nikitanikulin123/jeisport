<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNEmailToClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('clubs', function (Blueprint $table) {
			$table->string('n_email')->after('email');
			$table->string('p_file')->after('image');
		});

		Schema::table('treners', function (Blueprint $table) {
			$table->string('n_email')->after('email');
			$table->string('p_file')->after('image');
		});

		Schema::table('cities', function (Blueprint $table) {
			$table->integer('zoom');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('clubs', function (Blueprint $table) {
			$table->dropColumn('n_email');
			$table->dropColumn('p_file');
		});

		Schema::table('treners', function (Blueprint $table) {
			$table->dropColumn('n_email');
			$table->dropColumn('p_file');
		});

		Schema::table('cities', function (Blueprint $table) {
			$table->dropColumn('zoom');
		});
	}
}
