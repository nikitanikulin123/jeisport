<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStreetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('streets', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->string('slug');
			$table->integer('order');
			$table->tinyInteger('active');
			$table->integer('city_id');
			$table->timestamps();
		});

		Schema::table('regions', function (Blueprint $table) {
			$table->integer('order')->after('slug');
			$table->tinyInteger('active')->after('order');
		});

		Schema::table('clubs', function (Blueprint $table) {
			$table->integer('street_id');
		});

		Schema::table('treners', function (Blueprint $table) {
			$table->integer('street_id')->after('region_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('streets');

		Schema::table('regions', function (Blueprint $table) {
			$table->dropColumn('order');
			$table->dropColumn('active');
		});

		Schema::table('clubs', function (Blueprint $table) {
			$table->dropColumn('street_id');
		});

		Schema::table('treners', function (Blueprint $table) {
			$table->dropColumn('street_id');
		});
	}
}
