<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecommendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('recommends', function (Blueprint $table) {
			$table->engine = "InnoDB";
			$table->increments('id');
			$table->string('type');
			$table->integer('city_id')->unsigned();
			$table->integer('category_id')->unsigned();
			$table->integer('amount_by_category');
			$table->integer('filter_value_id')->unsigned();
			$table->integer('amount_by_filter_value');
			$table->timestamps();

			$table->foreign('city_id')->references('id')->on('cities')->onUpdate('cascade')->onDelete('cascade');
			$table->foreign('category_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('cascade');
			$table->foreign('filter_value_id')->references('id')->on('filter_values')->onUpdate('cascade')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recommends');
	}
}
