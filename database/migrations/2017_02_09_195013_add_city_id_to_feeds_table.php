<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCityIdToFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		DB::statement("SET FOREIGN_KEY_CHECKS=0");
		Schema::table('feeds', function (Blueprint $table) {
			$table->integer('city_id')->unsigned()->after('order');

			$table->foreign('city_id')->references('id')->on('cities')->onUpdate('cascade')->onDelete('cascade');
		});
		DB::statement("SET FOREIGN_KEY_CHECKS=1");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('feeds', function (Blueprint $table) {
			$table->dropForeign('feeds_city_id_foreign');
			$table->dropColumn('city_id');
		});
	}
}
