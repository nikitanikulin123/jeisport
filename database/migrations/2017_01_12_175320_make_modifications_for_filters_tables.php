<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeModificationsForFiltersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('avg_ratings', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->float('average_value', 2, 1);
			$table->morphs('rateable');
			$table->timestamps();

			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});

		Schema::table('price_lists', function (Blueprint $table) {
			$table->integer('price')->change();
		});

		Schema::table('price_list_categories', function (Blueprint $table) {
			$table->integer('min')->after('title');
			$table->integer('max')->after('min');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('avg_ratings');

		Schema::table('price_lists', function (Blueprint $table) {
			$table->string('price')->change();
		});

		Schema::table('price_list_categories', function (Blueprint $table) {
			$table->dropColumn('min');
			$table->dropColumn('max');
		});
	}
}
