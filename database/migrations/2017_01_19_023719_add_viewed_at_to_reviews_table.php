<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewedAtToReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('reviews', function (Blueprint $table) {
		    $table->boolean('closed')->after('rating_minus');
		    $table->dateTime('viewed_at')->after('closed')->nullable();
	    });
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reviews', function (Blueprint $table) {
			$table->dropColumn('closed');
			$table->dropColumn('viewed_at');
		});
	}
}
