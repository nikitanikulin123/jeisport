<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHouseToClubsAndTrenersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('clubs', function (Blueprint $table) {
			$table->string('house')->after('address');
		});

		Schema::table('treners', function (Blueprint $table) {
			$table->string('house')->after('address');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('clubs', function (Blueprint $table) {
			$table->dropColumn('house');
		});

		Schema::table('treners', function (Blueprint $table) {
			$table->dropColumn('house');
		});
	}
}
