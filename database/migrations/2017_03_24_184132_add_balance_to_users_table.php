<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBalanceToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('users', function (Blueprint $table) {
		    $table->integer('personal_bill');
		    $table->integer('balance');
	    });

	    Schema::table('clubs', function (Blueprint $table) {
		    $table->renameColumn('period', 'duration');
	    });

	    Schema::table('clubs', function (Blueprint $table) {
		    $table->boolean('used_trial')->after('duration');
	    });

	    Schema::table('treners', function (Blueprint $table) {
		    $table->renameColumn('period', 'duration');
	    });

	    Schema::table('treners', function (Blueprint $table) {
		    $table->boolean('used_trial')->after('duration');
	    });

	    Schema::create('billing_logs', function (Blueprint $table) {
		    $table->engine = "InnoDB";
		    $table->increments('id');
		    $table->integer('user_id');
		    $table->morphs('billable');
		    $table->string('description');
		    $table->integer('duration');
		    $table->integer('change');
		    $table->integer('balance');
		    $table->boolean('active');
		    $table->boolean('expired');
		    $table->dateTime('started_at');
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('users', function (Blueprint $table) {
		    $table->dropColumn('personal_bill');
		    $table->dropColumn('balance');
	    });

	    Schema::table('clubs', function (Blueprint $table) {
		    $table->renameColumn('duration', 'period');
		    $table->dropColumn('used_trial');
	    });

	    Schema::table('treners', function (Blueprint $table) {
		    $table->renameColumn('duration', 'period');
		    $table->dropColumn('used_trial');
	    });

	    Schema::drop('billing_logs');
    }
}
