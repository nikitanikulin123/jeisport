<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMapToTrenersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('treners', function (Blueprint $table) {
			$table->string('lat_lng')->after('email');
			$table->float('lat', 14, 12)->after('lat_lng');
			$table->float('lng', 14, 12)->after('lat');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('treners', function (Blueprint $table) {
			$table->dropColumn('lat_lng');
			$table->dropColumn('lat');
			$table->dropColumn('lng');
		});
	}
}
