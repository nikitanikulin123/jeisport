<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationsForAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		DB::statement("SET FOREIGN_KEY_CHECKS=0");
//		Schema::table('clubs', function (Blueprint $table) {
//			$table->integer('user_id')->unsigned()->change();
//			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
//		});
//
//		Schema::table('treners', function (Blueprint $table) {
//			$table->integer('user_id')->unsigned()->change();
//			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
//		});

		Schema::table('likes', function (Blueprint $table) {
			$table->integer('user_id')->unsigned()->change();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});

		Schema::table('ratings', function (Blueprint $table) {
			$table->integer('user_id')->unsigned()->change();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});

		Schema::table('reviews', function (Blueprint $table) {
			$table->integer('user_id')->unsigned()->change();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});

		Schema::table('regions', function (Blueprint $table) {
			$table->integer('city_id')->unsigned()->change();
			$table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
		});

		Schema::table('seo_city_categories', function (Blueprint $table) {
			$table->integer('city_id')->unsigned()->change();
			$table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
			$table->integer('category_id')->unsigned()->change();
			$table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
		});

		Schema::table('seo_city_filters', function (Blueprint $table) {
			$table->integer('filter_value_id')->unsigned()->change();
			$table->foreign('filter_value_id')->references('id')->on('filter_values')->onDelete('cascade');
			$table->integer('category_id')->unsigned()->change();
			$table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
		});

		Schema::table('seo_city_filter_values', function (Blueprint $table) {
			$table->integer('city_id')->unsigned()->change();
			$table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
			$table->integer('filter_value_id')->unsigned()->change();
			$table->foreign('filter_value_id')->references('id')->on('filter_values')->onDelete('cascade');
		});

		Schema::table('seo_region_categories', function (Blueprint $table) {
			$table->integer('region_id')->unsigned()->change();
			$table->foreign('region_id')->references('id')->on('regions')->onDelete('cascade');
			$table->integer('category_id')->unsigned()->change();
			$table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
		});
		DB::statement("SET FOREIGN_KEY_CHECKS=1");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
//		Schema::table('clubs', function (Blueprint $table) {
//			$table->dropForeign('clubs_user_id_foreign');
//		});
//
//		Schema::table('treners', function (Blueprint $table) {
//			$table->dropForeign('treners_user_id_foreign');
//		});

		Schema::table('likes', function (Blueprint $table) {
			$table->dropForeign('likes_user_id_foreign');
		});

		Schema::table('ratings', function (Blueprint $table) {
			$table->dropForeign('ratings_user_id_foreign');
		});

		Schema::table('reviews', function (Blueprint $table) {
			$table->dropForeign('reviews_user_id_foreign');
		});

		Schema::table('regions', function (Blueprint $table) {
			$table->dropForeign('regions_city_id_foreign');
		});

		Schema::table('seo_city_categories', function (Blueprint $table) {
			$table->dropForeign('seo_city_categories_city_id_foreign');
			$table->dropForeign('seo_city_categories_category_id_foreign');
		});

		Schema::table('seo_city_filters', function (Blueprint $table) {
			$table->dropForeign('seo_city_filters_filter_value_id_foreign');
			$table->dropForeign('seo_city_filters_category_id_foreign');
		});

		Schema::table('seo_city_filter_values', function (Blueprint $table) {
			$table->dropForeign('seo_city_filter_values_city_id_foreign');
			$table->dropForeign('seo_city_filter_values_filter_value_id_foreign');
		});

		Schema::table('seo_region_categories', function (Blueprint $table) {
			$table->dropForeign('seo_region_categories_region_id_foreign');
			$table->dropForeign('seo_region_categories_category_id_foreign');
		});
	}
}
