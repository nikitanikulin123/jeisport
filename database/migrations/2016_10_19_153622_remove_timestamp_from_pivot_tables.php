<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTimestampFromPivotTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('category_filter', function (Blueprint $table) {
			$table->dropColumn('id');
			$table->dropTimestamps();
		});
		Schema::table('category_filter_value', function (Blueprint $table) {
			$table->dropColumn('id');
			$table->dropTimestamps();
		});
		Schema::table('club_filter_value', function (Blueprint $table) {
			$table->dropColumn('id');
			$table->dropTimestamps();
		});

		// foreign keys
		DB::statement("SET FOREIGN_KEY_CHECKS=0");
		Schema::table('category_filter', function (Blueprint $table) {
			$table->integer('category_id')->unsigned()->change();
			$table->integer('filter_id')->unsigned()->change();

			$table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
			$table->foreign('filter_id')->references('id')->on('filters')->onDelete('cascade');

			$table->primary(['category_id', 'filter_id']);
		});

		Schema::table('category_filter_value', function (Blueprint $table) {
			$table->integer('category_id')->unsigned()->change();
			$table->integer('filter_value_id')->unsigned()->change();

			$table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
			$table->foreign('filter_value_id')->references('id')->on('filter_values')->onDelete('cascade');

			$table->primary(['category_id', 'filter_value_id']);
		});

		Schema::table('club_filter_value', function (Blueprint $table) {
			$table->primary(['club_id', 'filter_value_id']);
		});
		DB::statement("SET FOREIGN_KEY_CHECKS=1");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// foreign keys
		Schema::table('category_filter', function (Blueprint $table) {
			$table->dropForeign('category_filter_category_id_foreign');
			$table->dropForeign('category_filter_filter_id_foreign');
		});
		Schema::table('category_filter_value', function (Blueprint $table) {
			$table->dropForeign('category_filter_value_category_id_foreign');
			$table->dropForeign('category_filter_value_filter_value_id_foreign');
		});

		DB::statement("ALTER TABLE  category_filter DROP PRIMARY KEY;");
		Schema::table('category_filter', function (Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
		});
		DB::statement("ALTER TABLE  category_filter_value DROP PRIMARY KEY;");
		Schema::table('category_filter_value', function (Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
		});
		DB::statement("ALTER TABLE  club_filter_value DROP PRIMARY KEY;");
		Schema::table('club_filter_value', function (Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
		});
	}
}
