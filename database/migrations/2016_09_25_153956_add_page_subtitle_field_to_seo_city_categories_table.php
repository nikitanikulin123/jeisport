<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPageSubtitleFieldToSeoCityCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seo_city_categories', function (Blueprint $table) {
            $table->text('page_subtitle');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seo_city_categories', function (Blueprint $table) {
            $table->dropColumn('page_subtitle');
        });
    }
}
