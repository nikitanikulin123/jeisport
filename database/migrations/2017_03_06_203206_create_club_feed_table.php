<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubFeedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('club_feeds', function (Blueprint $table) {
			$table->engine = "InnoDB";
			$table->increments('id');
			$table->integer('club_id')->unsigned();
			$table->integer('feed_id')->unsigned();
			$table->string('image');
			$table->string('title');
			$table->string('sub_title');
			$table->text('body');
			$table->boolean('active');
			$table->integer('order');
			$table->timestamps();

			$table->foreign('club_id')->references('id')->on('clubs')->onUpdate('cascade')->onDelete('cascade');
			$table->foreign('feed_id')->references('id')->on('feeds')->onUpdate('cascade')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('club_feeds');
	}
}
