<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeClubIdToMorphInAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('reviews', function (Blueprint $table) {
			$table->dropForeign('reviews_club_id_foreign');
			$table->dropColumn('club_id');
			$table->dropColumn('name');
			$table->morphs('reviewable');
		});

		Schema::table('stocks', function (Blueprint $table) {
			$table->dropForeign('stocks_club_id_foreign');
			$table->dropColumn('club_id');
			$table->morphs('stockable');
		});

		Schema::table('price_list_categories', function (Blueprint $table) {
			$table->dropForeign('price_list_categories_club_id_foreign');
			$table->dropColumn('club_id');
			$table->morphs('listable');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement("SET FOREIGN_KEY_CHECKS=0");
		Schema::table('reviews', function (Blueprint $table) {
			$table->string('name')->after('moderated');
			$table->integer('club_id')->after('user_id')->unsigned();
			$table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
			$table->dropColumn('reviewable_id');
			$table->dropColumn('reviewable_type');
		});

		Schema::table('stocks', function (Blueprint $table) {
			$table->integer('club_id')->after('image')->unsigned();
			$table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
			$table->dropColumn('stockable_id');
			$table->dropColumn('stockable_type');
		});

		Schema::table('price_list_categories', function (Blueprint $table) {
			$table->integer('club_id')->after('title')->unsigned();
			$table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
			$table->dropColumn('listable_id');
			$table->dropColumn('listable_type');
		});
		DB::statement("SET FOREIGN_KEY_CHECKS=1");
	}
}
