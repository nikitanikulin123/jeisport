<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('visits', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('visits');
			$table->date('visited_at');
			$table->morphs('visitable');
			$table->timestamps();
		});

		Schema::create('callbacks', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->integer('phone');
			$table->boolean('checked');
			$table->boolean('closed');
			$table->morphs('callbackable');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('visits');
		Schema::drop('callbacks');
	}
}
