<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCasesToFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('feeds', function (Blueprint $table) {
			$table->dropColumn('metatitle');
			$table->dropColumn('metakeyw');
			$table->dropColumn('metadesc');
			$table->dropColumn('similars');

			$table->integer('club_id')->after('id');

			$table->string('case_title1')->after('body');
			$table->string('case_image1')->after('case_title1');
			$table->text('case_text1')->after('case_image1');

			$table->string('case_title2')->after('case_text1');
			$table->string('case_image2')->after('case_title2');
			$table->text('case_text2')->after('case_image2');

			$table->string('case_title3')->after('case_text2');
			$table->string('case_image3')->after('case_title3');
			$table->text('case_text3')->after('case_image3');

			$table->string('case_title4')->after('case_text3');
			$table->string('case_image4')->after('case_title4');
			$table->text('case_text4')->after('case_image4');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('feeds', function (Blueprint $table) {
			$table->string('metatitle')->after('order');
			$table->string('metakeyw')->after('metatitle');
			$table->string('metadesc')->after('metakeyw');
			$table->string('similars')->after('metadesc');

			$table->dropColumn('club_id');

			$table->dropColumn('case_title1');
			$table->dropColumn('case_image1');
			$table->dropColumn('case_text1');

			$table->dropColumn('case_title2');
			$table->dropColumn('case_image2');
			$table->dropColumn('case_text2');

			$table->dropColumn('case_title3');
			$table->dropColumn('case_image3');
			$table->dropColumn('case_text3');

			$table->dropColumn('case_title4');
			$table->dropColumn('case_image4');
			$table->dropColumn('case_text4');
		});
	}
}
