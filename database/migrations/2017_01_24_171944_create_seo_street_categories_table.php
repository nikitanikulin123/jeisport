<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeoStreetCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('seo_street_categories', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('street_id');
			$table->integer('category_id');
			$table->string('page_title');
			$table->text('page_description');
			$table->string('metatitle');
			$table->text('metakeyw');
			$table->text('metadesc');
			$table->timestamps();
		});

		Schema::table('seo_city_categories', function (Blueprint $table) {
			$table->dropColumn('page_subtitle');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('seo_street_categories');

		Schema::table('seo_city_categories', function (Blueprint $table) {
			$table->string('page_subtitle')->after('page_title');
		});
	}
}
