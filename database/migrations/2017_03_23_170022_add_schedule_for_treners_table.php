<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScheduleForTrenersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('treners', function (Blueprint $table) {
			//�����������
			$table->tinyInteger('work_in_mon')->after('deactivation_date');
			$table->time('mon_time_from')->after('work_in_mon');
			$table->time('mon_time_to')->after('mon_time_from');
			//�������
			$table->tinyInteger('work_in_tue')->after('mon_time_to');
			$table->time('tue_time_from')->after('work_in_tue');
			$table->time('tue_time_to')->after('tue_time_from');
			//�����
			$table->tinyInteger('work_in_wed')->after('tue_time_to');
			$table->time('wed_time_from')->after('work_in_wed');
			$table->time('wed_time_to')->after('wed_time_from');
			//�������
			$table->tinyInteger('work_in_thu')->after('wed_time_to');
			$table->time('thu_time_from')->after('work_in_thu');
			$table->time('thu_time_to')->after('thu_time_from');
			//�������
			$table->tinyInteger('work_in_fri')->after('thu_time_to');
			$table->time('fri_time_from')->after('work_in_fri');
			$table->time('fri_time_to')->after('fri_time_from');
			//�������
			$table->tinyInteger('work_in_sat')->after('fri_time_to');
			$table->time('sat_time_from')->after('work_in_sat');
			$table->time('sat_time_to')->after('sat_time_from');
			//�����������
			$table->tinyInteger('work_in_sun')->after('sat_time_to');
			$table->time('sun_time_from')->after('work_in_sun');
			$table->time('sun_time_to')->after('sun_time_from');

			$table->tinyInteger('email_to_admin')->after('n_email');
		});

		Schema::table('clubs', function (Blueprint $table) {
			$table->tinyInteger('email_to_admin')->after('n_email');
		});

		Schema::table('categories', function (Blueprint $table) {
			$table->tinyInteger('show_callback_form')->after('type');
		});

		Schema::table('users', function (Blueprint $table) {
			$table->integer('limit_of_items');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('treners', function (Blueprint $table) {
			$table->dropColumn('work_in_mon');
			$table->dropColumn('mon_time_from');
			$table->dropColumn('mon_time_to');

			$table->dropColumn('work_in_tue');
			$table->dropColumn('tue_time_from');
			$table->dropColumn('tue_time_to');


			$table->dropColumn('work_in_wed');
			$table->dropColumn('wed_time_from');
			$table->dropColumn('wed_time_to');


			$table->dropColumn('work_in_thu');
			$table->dropColumn('thu_time_from');
			$table->dropColumn('thu_time_to');


			$table->dropColumn('work_in_fri');
			$table->dropColumn('fri_time_from');
			$table->dropColumn('fri_time_to');


			$table->dropColumn('work_in_sat');
			$table->dropColumn('sat_time_from');
			$table->dropColumn('sat_time_to');


			$table->dropColumn('work_in_sun');
			$table->dropColumn('sun_time_from');
			$table->dropColumn('sun_time_to');

			$table->dropColumn('email_to_admin');
		});

		Schema::table('clubs', function (Blueprint $table) {
			$table->dropColumn('email_to_admin');
		});

		Schema::table('categories', function (Blueprint $table) {
			$table->dropColumn('show_callback_form');
		});

		Schema::table('users', function (Blueprint $table) {
			$table->dropColumn('limit_of_items');
		});
	}
}
