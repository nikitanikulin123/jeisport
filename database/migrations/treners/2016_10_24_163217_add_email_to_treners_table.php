<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailToTrenersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('treners', function (Blueprint $table) {
			$table->integer('user_id')->after('id');
			$table->string('email')->after('address');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('treners', function (Blueprint $table) {
			$table->dropColumn('user_id');
			$table->dropColumn('email');
		});
	}
}
