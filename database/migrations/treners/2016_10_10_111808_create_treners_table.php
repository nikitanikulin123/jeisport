<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrenersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treners', function (Blueprint $table) {
	        $table->increments('id');
	        $table->string('name');
	        $table->string('sname');
	        $table->string('mname');
	        $table->string('slug');
	        $table->string('image');
	        $table->string('phone');
	        $table->string('address');
	        $table->string('site');
	        $table->text('description');
	        $table->text('education');
	        $table->text('pricelist');
	        $table->integer('priority');
	        $table->integer('category_id');
	        $table->integer('city_id');
	        $table->integer('region_id');
	        $table->boolean('active');
	        $table->boolean('premium');
	        $table->date('deactivation_date');
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('treners');
    }
}
