<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilterValueTrenerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('filter_value_trener', function (Blueprint $table) {
			$table->integer('filter_value_id');
			$table->integer('trener_id');
		});

		DB::statement("SET FOREIGN_KEY_CHECKS=0");
		Schema::table('filter_value_trener', function (Blueprint $table) {
			$table->integer('filter_value_id')->unsigned()->change();
			$table->integer('trener_id')->unsigned()->change();

			$table->foreign('filter_value_id')->references('id')->on('filter_values')->onDelete('cascade');
			$table->foreign('trener_id')->references('id')->on('treners')->onDelete('cascade');

			$table->primary(['trener_id', 'filter_value_id']);
		});
		DB::statement("SET FOREIGN_KEY_CHECKS=1");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('filter_value_trener');
	}
}
