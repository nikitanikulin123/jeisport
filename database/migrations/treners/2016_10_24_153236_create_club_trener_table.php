<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubTrenerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('club_trener', function (Blueprint $table) {
			$table->integer('club_id');
			$table->integer('trener_id');
		});

		DB::statement("SET FOREIGN_KEY_CHECKS=0");
		Schema::table('club_trener', function (Blueprint $table) {
			$table->integer('club_id')->unsigned()->change();
			$table->integer('trener_id')->unsigned()->change();

			$table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
			$table->foreign('trener_id')->references('id')->on('treners')->onDelete('cascade');

			$table->primary(['trener_id', 'club_id']);
		});
		DB::statement("SET FOREIGN_KEY_CHECKS=1");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('club_trener');
	}
}
