<?php

use Illuminate\Database\Seeder;

class ClubsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 4; $i++):
            \App\Models\Club::create([
                'title' => $faker->sentence(4),
                'lat_lng' => "42.828474,74.619637",
                'image' => '3ef9f499171e5a5e178fe96ee416a0eb.jpg',
                'phone' => $faker->phoneNumber,
                'address' => $faker->streetAddress,
                'site' => $faker->url,
                'slug' => $faker->slug(),
                'active' => true,
                'category_id' => 1,
                'city_id' => 1
            ]);
        endfor;
    }
}
