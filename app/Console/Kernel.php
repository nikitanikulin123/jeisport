<?php

namespace App\Console;

use App\Console\Commands\CheckBillingLogs;
use App\Console\Commands\CountRecommends;
use App\Console\Commands\FillSeoTables;
use App\Console\Commands\FillUsersPersonalBill;
use App\Console\Commands\GenerateSiteMap;
use App\Console\Commands\Inspire;
use App\Console\Commands\SetFixedFilters;
use App\Console\Commands\SetPremium;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Inspire::class,
        SetFixedFilters::class,
	    FillUsersPersonalBill::class,
	    CheckBillingLogs::class,
	    CountRecommends::class,
	    SetPremium::class,
	    GenerateSiteMap::class,
	    FillSeoTables::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Disable checking if club is expired
//	    $schedule->command('billingLogs:check')->everyFiveMinutes();
	    $schedule->command('recommends:count')->hourly();
	    $schedule->command('siteMap:generate')->daily();
	    $schedule->command('seoTables:fill')->daily();
    }
}
