<?php

namespace App\Console\Commands;

use App\Models\BillingLog;
use App\Models\Club;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckBillingLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'billingLogs:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
	public function handle()
	{
		\DB::connection()->disableQueryLog();
		$counter = 0;

		foreach (BillingLog::whereActive(true)->whereExpired(false)->get() as $log) {
			switch (true) {
				case ($log->billable_type === Club::class && ($log->description === 'premium' || $log->description === 'premium_trial') && $log->updated_at->addDays($log->duration) < Carbon::now()):
					$log->billable ? $log->billable->update(['premium' => false, 'duration' => 0]) : false;
					$log->update(['started_at' => $log->updated_at, 'expired' => true]);

					$counter = $counter + 1;
					$this->info("{$counter}. Expired log with id: {$log->id}");
					break;
			}
		}

		foreach (BillingLog::whereActive(false)->get() as $log) {
			if($log->updated_at->addDays(30) < Carbon::now()) {
				$log->update(['started_at' => $log->updated_at, 'expired' => true]);

				$counter = $counter + 1;
				$this->info("{$counter}. Expired log with id: {$log->id}");
			}
		}
	}
}
