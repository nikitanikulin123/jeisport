<?php

namespace App\Console\Commands;

use App\Http\Controllers\BaseController;
use App\Models\BaseModel;
use App\Models\SeoRegionCategory;
use App\Models\SeoStreetCategory;
use App\Models\Street;
use App\Models\Vars;
use File;
use Illuminate\Console\Command;
use App;
use App\Http\Requests;
use App\Models\Category;
use App\Models\City;
use Illuminate\Http\Request as policyRequest;

class FillSeoTables extends Command
{
    protected $signature = 'seoTables:fill';
    protected $description = 'Command description';
    protected $counter = 0;

    public function __construct()
    {
        parent::__construct();
    }

	public function handle()
	{
		\DB::connection()->disableQueryLog();
		$cities = City::active()->where('slug', '!=', 'issyk-kul')->get();
		$categories = Category::active()->get();

		foreach($cities as $city) {
			foreach($categories as $category) {
				$this->addSeo('street', ['city' => $city, 'category' => $category, 'seoModel' => 'SeoStreetCategory', 'on' => 'на улице']);
				$this->addSeo('region', ['city' => $city, 'category' => $category, 'seoModel' => 'SeoRegionCategory', 'on' => 'в районе']);
			}
		}
	}

	protected function addSeo($type, $params = null)
	{
		if(array_get($params, 'category') && stripos($params['category']->slug, 'issyk-kul'))
			$params['city'] = BaseModel::makeModel('city')->where('slug', 'issyk-kul')->first();

		$model = BaseModel::makeModel($type)->where(['city_id' => $params['city']->id]);

		$model->chunk(100, function ($items) use ($type, $params) {
			foreach ($items as $item) {
				$this->createSeo($item, $type, $params);
			}
		});
	}

	protected function createSeo($item, $type, $params = null) {
		$seoModel = BaseModel::makeModel($params['seoModel'],  true);
		$on = $params['on'];

		if($seo = $seoModel->where(["{$type}_id" => $item->id, 'category_id' => $params['category']->id])->first())
			$word = 'Skipped';
		else {
			$categoryTitle = mb_strtolower($params['category']->title);
			$seo = $seoModel->create(["{$type}_id" => $item->id, 'category_id' => $params['category']->id,
				'page_title' => "{$params['category']->title} {$on} {$item->title}",
				'page_subtitle' => "Все {$categoryTitle} {$on} {$item->title} ({$params['city']->title}) с адресами, телефонами с фото и режимом работы",
				'page_description' => "",
				'metatitle' => "{$params['category']->title} {$on} {$item->title} ({$params['city']->title}) с отзывами, фото, адресами - Js.kg",
				'metadesc' => "Все {$categoryTitle} {$on} {$item->title} ({$params['city']->title}) - адреса, телефоны, фото, отзывы посетителей, часы работы - Js.kg",
				'metakeyw' => "{$categoryTitle} {$on} {$item->title}, {$categoryTitle} {$on} {$item->title} {$params['city']->title}",
			]);
			$word = 'Added';
		}

		$this->counter++;
		$this->info("{$this->counter}. {$word} {$type} seo item for {$params['category']->type} with id: {$seo->id}");
	}
}
