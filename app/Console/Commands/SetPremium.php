<?php

namespace App\Console\Commands;

use App\Http\Controllers\BaseController;
use App\Models\BillingLog;
use App\Models\Club;
use App\Models\Trener;
use App\Models\Vars;
use App\Models\Widget;
use Carbon\Carbon;
use Illuminate\Http\Request as policyRequest;
use SleepingOwl\Admin\Repository\BaseRepository;
use App\Models\Category;
use App\Models\City;
use App\Models\Filter;
use App\Models\FilterValue;
use App\Models\Recommend;
use Illuminate\Console\Command;

class SetPremium extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'premium:set';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
	public function handle()
	{
		\DB::connection()->disableQueryLog();
		$counter = 0;

		Club::moderated()->chunk(100, function ($clubs) use($counter) {
			foreach ($clubs as $club) {
				$duration = 3;
				$user = $club->user;
				$description = 'premium_trial';
				$sum = 0;

				$balance = $user->balance - $sum;
				$billingLog = BillingLog::create(['user_id' => $user->id, 'description' => $description, 'duration' => $duration * 30,
					'change' => $sum, 'balance' => $balance, 'active' => true]);
				$club->billingLog()->save($billingLog);
				$club->update(['premium' => true, 'duration' => $duration, 'used_trial' => true]);

				$counter = $counter + 1;
				$this->info("{$counter}. Premium trial period has been given to user with id: {$club->id}");
			}
		});
	}
}
