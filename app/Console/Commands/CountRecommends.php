<?php

namespace App\Console\Commands;

use App\Http\Controllers\BaseController;
use App\Models\BaseModel;
use App\Models\Club;
use App\Models\Trener;
use App\Models\Vars;
use App\Models\Widget;
use Carbon\Carbon;
use Illuminate\Http\Request as policyRequest;
use SleepingOwl\Admin\Repository\BaseRepository;
use App\Models\Category;
use App\Models\City;
use App\Models\Filter;
use App\Models\FilterValue;
use App\Models\Recommend;
use Illuminate\Console\Command;

class CountRecommends extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recommends:count';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
	public function handle()
	{
		\DB::connection()->disableQueryLog();
		$counter = 0;
		$types = ['club', 'trener'];
		$filVars = Vars::specialFilterVarsByIds();

		foreach ($types as $type) {
			foreach (City::all() as $city) {
				Category::where('type', $type)->chunk(1, function ($categories) use($type, $city, $filVars, $counter) {
					foreach ($categories as $category) {
						$filters = $type == 'club' ?
							$category->filters->filter(function ($item) use ($filVars) {return $item['id'] != $filVars['ratings'] && $item['id'] != $filVars['price'] && $item['id'] != $filVars['schedule'];}) :
							$category->filters->filter(function ($item) use ($filVars) {return $item['id'] != $filVars['gender'] && $item['id'] != $filVars['ratings_T'];});
						foreach ($filters as $filter) {
							foreach ($filter->options as $filterValue) {
								$counter = $counter + 1;
								$dataArr = ['type' => $type, 'city_id' => $city->id, 'category_id' => $category->id, 'filter_value_id' => $filterValue->id];
								$recommend = Recommend::where($dataArr)->first();

								if($recommend && $recommend->updated_at->format('d-m-Y H') == Carbon::now()->format('d-m-Y H'))
									$this->info("{$counter}. Skipped recommend with id: {$recommend->id}");
								else {
									$model = BaseModel::makeModel($type);
									$amount_by_category = $model->moderated()->where(['city_id' => $city->id, 'category_id' => $category->id])->count();
									$amount_by_filter_value = $model->moderated()->where(['city_id' => $city->id, 'category_id' => $category->id])
										->whereHas("filterValues", function ($query) use ($filterValue) {
											$query->where('id', $filterValue->id);
										})->count();
									$item = $model->where(['moderated' => true, 'city_id' => $city->id, 'category_id' => $category->id])->orderByRaw("RAND()")->first();
									$updateArr = ['amount_by_category' => $amount_by_category, 'amount_by_filter_value' => $amount_by_filter_value, 'image_filter_value' => $item ? $item->image : 'nophoto.jpg'];

									if($recommend) {
										$recommend->update(array_add($updateArr, 'updated_at', Carbon::now()));
										$this->info("{$counter}. Updated recommend with id: {$recommend->id} -- {$type} - {$city->id} - {$category->id} - {$filter->id} - {$filterValue->id}");
									} else {
										$dataArr = array_merge($dataArr, $updateArr);
										$recommend = Recommend::create($dataArr);
										$this->info("{$counter}. Created recommend with id: {$recommend->id} -- {$type} - {$city->id} - {$category->id} - {$filter->id} - {$filterValue->id}");
									}
								}
							}
						}
					}
				});
			}
		}
	}
}
