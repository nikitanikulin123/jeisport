<?php

namespace App\Console\Commands;

use App\Http\Controllers\BaseController;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class FillUsersPersonalBill extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fill:bills';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Request $request)
    {
        foreach (User::where('personal_bill', 0)->get() as $user) {
            $user->personal_bill = (new BaseController($request))->generateCode();
            $user->save();
        }
    }
}
