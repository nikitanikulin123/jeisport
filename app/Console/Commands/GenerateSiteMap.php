<?php

namespace App\Console\Commands;

use App\Http\Controllers\BaseController;
use App\Models\BaseModel;
use App\Models\Vars;
use File;
use Illuminate\Console\Command;
use App;
use App\Http\Requests;
use App\Models\Category;
use App\Models\City;
use Illuminate\Http\Request as policyRequest;

class GenerateSiteMap extends Command
{
    protected $signature = 'siteMap:generate';
    protected $description = 'Command description';
    protected $counter = 0;
    protected $sitemapCounter  = 0;

    public function __construct()
    {
        parent::__construct();
    }

	public function handle()
	{
		\DB::connection()->disableQueryLog();

		$sitemap = App::make("sitemap");

		$cities = City::active()->where('slug', '!=', 'issyk-kul')->get();
		$categories = Category::active()->get();

		$this->includeToSiteMap($sitemap, $cities, '', false, 1, 'daily');
		$this->includeToSiteMap($sitemap, $categories->filterFix('slug', 'like', 'issyk-kul'), 'issyk-kul', false, 0.8, 'daily');
		foreach($cities as $city) {
			$origCity = $city;

			$this->includeToSiteMap($sitemap, $categories->filterFix('slug', 'not like', 'issyk-kul'), $city->slug, false, 0.8, 'daily');
			$this->includeToSiteMap($sitemap, 'feed', $city->slug . '/feeds', false, 0.4, 'daily', ['city' => $city]);
			foreach($categories as $category) {
				if(stripos($category->slug, 'issyk-kul'))
					$city = BaseModel::makeModel('city')->where('slug', 'issyk-kul')->first();
				else
					$city = $origCity;

				$this->includeToSiteMap($sitemap, 'region', $city->slug . '/' . $category->slug . '/district', false, 0.4, 'daily', ['city' => $city]);
				$this->includeToSiteMap($sitemap, 'street', $city->slug . '/' . $category->slug . '/street', false, 0.4, 'daily', ['city' => $city]);
				$this->includeToSiteMap($sitemap, 'filterValue', $city->slug . '/' . $category->slug . '/type', false, 0.4, 'daily', ['category' => $category]);
				$this->includeToSiteMap($sitemap, 'club', $city->slug . '/' . $category->slug, false, 0.4, 'daily', ['city' => $city, 'category' => $category]);
			}
		}

		if (!empty($sitemap->model->getItems()))
			$this->storeSiteMap($sitemap, $this->sitemapCounter);

		$sitemap->store('sitemapindex','sitemap');
	}

	protected function includeToSiteMap($sitemap, $type, $prefix, $modified, $priority, $freq, $params = null)
	{
		if(is_string($type)) {
			if(count($params)) {
				$filVars = Vars::specialFilterVarsByIds();
				$model = BaseModel::makeModel($type);

				if($type == 'filterValue')
					$model = $model->active()->whereHas('filter', function ($query) use($filVars, $params) {
						return $query->whereHas('categories', function ($q) use($params) {
							return $q->where('id',  $params['category']->id);
						})->where('id', '!=', $filVars['ratings'])->where('id', '!=', $filVars['price'])->where('id', '!=', $filVars['schedule']);
					})->orderBy('created_at', 'desc');
				else {
					$model = $model->active();
					if(array_get($params, 'city'))
						$model = $model->where('city_id', $params['city']->id);
					if(array_get($params, 'category'))
						$model = $model->where('category_id', $params['category']->id);
				}
			}

			$model->chunk(100, function ($items) use ($sitemap, $type, $prefix, $modified, $priority, $freq) {
				$this->addToSiteMap($sitemap, $items, $prefix, $modified, $priority, $freq);
			});
		} else
			$this->addToSiteMap($sitemap, $type, $prefix, $modified, $priority, $freq);

		return $sitemap;
	}

	protected function addToSiteMap($sitemap, $items, $prefix, $modified, $priority, $freq) {
		foreach ($items as $item) {
			if ($this->counter / 5000 == $this->sitemapCounter + 1)
			{
				$this->storeSiteMap($sitemap, $this->sitemapCounter);
				$this->sitemapCounter++;
			}

			$sitemap->add(secure_url($prefix . '/' . $item->slug), $modified ? $modified : $item->created_at, $priority, $freq);
			$this->counter++;
			$this->info("{$this->counter}. Added to sitemap item with id: {$item->id}");
		}

		return $sitemap;
	}

	protected function storeSiteMap($sitemap, $sitemapCounter) {
		if(!is_dir('public_html/sitemaps'))
			File::makeDirectory('public_html/sitemaps');

		$sitemap->store('xml',"sitemaps/sitemap-{$sitemapCounter}");
		$sitemap->addSitemap(secure_url("sitemaps/sitemap-{$sitemapCounter}.xml"));
		$sitemap->model->resetItems();

		return true;
	}
}
