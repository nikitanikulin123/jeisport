<?php

namespace App\Console\Commands;

use App\Models\Filter;
use App\Models\FilterValue;
use App\Models\Vars;
use Illuminate\Console\Command;

class SetFixedFilters extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fixedFilters:set';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
	public function handle()
	{
		$filNames = Vars::specialFilterNamesByVars();

		$number = 0;

		foreach ($filNames['filters'] as $filterId => $filter) {
			if (!Filter::where('id', $filterId)->first()) {
				if(is_array($filter))
					$newFilterArr = ['id' => $filterId, 'title' => $filter[0], 'description' => $filter[1], 'active' => true,
						'type' => count($filter) == 3 ? $filter[2] : 'trener'];
				else
					$newFilterArr = ['id' => $filterId, 'title' => $filter, 'active' => true, 'type' => 'club'];
				$newFilter = Filter::create($newFilterArr);
				$number = $number + 1;
				$this->info($number . '. Created filter id: ' . $newFilter->id);

				$filtersOptions = $filNames['filtersOptions'];
				if (isset($filtersOptions[$filterId])) {
					foreach ($filtersOptions[$filterId] as $optionId => $option) {
						if (!FilterValue::where('id', $optionId)->first()) {
							$newOption = FilterValue::create(['id' => $optionId, 'title' => $option, 'filter_id' => $newFilter->id,'active' => true]);
							$number = $number + 1;
							$this->info($number . '. Created filters option id: ' . $newOption->id);
						}
					}
				}
			}

		}
	}
}
