<?php

namespace App\Listeners;

use App\Events\SendNotificationEvent;
use App\Events\UserRegisteredEvent;
use App\Models\Widget;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendNotificationListener
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param SendNotificationEvent $event
     */
    public function handle(SendNotificationEvent $event)
    {
	    Mail::queue('emails.sendNotification', ['notification' => $event->message], function ($msg) use ($event) {
		    $msg->to(explode(',', $event->message['recipientEmail']));
		    if(array_get($event->message, 'bccEmail'))
		        $msg->bcc(explode(',', $event->message['bccEmail']));
		    
		    if($event->message['type'] == 'newTrener') {
		        $msg->subject('Добавлен новый специалист!');
		    }
		    if($event->message['type'] == 'newClub') {
			    $msg->subject('Добавлено новое заведение!');
		    }
		    if($event->message['type'] == 'clubForModeration') {
			    $msg->subject('Новое заведение отправлено на модерацию!');
		    }
		    if($event->message['type'] == 'trenerForModeration') {
			    $msg->subject('Информация о новом специалисте отправлена на модерацию!');
		    }
		    if($event->message['type'] == 'moderatedClub') {
			    $msg->subject('Ваше заведение прошло модерацию и будет отображено в общем каталоге!');
		    }
		    if($event->message['type'] == 'moderatedTrener') {
			    $msg->subject('Информация о специалисте прошла модерацию и будет отображена в общем каталоге!');
		    }
		    if($event->message['type'] == 'callback') {
			    $msg->subject('Заказан обратный звонок!');
		    }
	    });
    }
}
