<?php

/*
 * This is a simple example of the main features.
 * For full list see documentation.
 */

Admin::model(\App\Models\Administrator::class)->title('Администраторы')->display(function ()
{
	$display = AdminDisplay::table();
	$display->columns([
		Column::string('username')->label('Имя пользователя'),
		Column::string('name')->label('Имя'),
        Column::lists('roles.name')->label('Группы пользователя')
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('username', 'Имя пользователя')->required()->unique(),
		FormItem::text('name', 'Имя')->required(),
		FormItem::password('password', 'Пароль'),
        FormItem::multiselect('roles', 'Группы')->model(\App\Role::class)->display('name')
	]);
	return $form;
});