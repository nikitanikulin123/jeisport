<?php

Admin::model(\App\Models\Filter::class)->title('Фильтры')->display(function () {
    $display = AdminDisplay::datatablesAsync()->order([[0, 'desc']])->attributes(['stateSave' => false,]);
	$display->with('categories');
    $display->apply(function($query){
        if (Request::get('category_id'))
            $query->whereHas("categories", function ($query) {
	            $query->where('id', Request::get('category_id'));
            });
	    if(Request::get('type') == 'club')
		    $query->whereType('club');
	    else
		    $query->whereType('trener');
    });
    $display->columns([
	    Column::string('id')->label('#'),
        Column::string('title')->label('Название'),
        Column::string('description')->label('Описание'),
        Column::string('slug')->label('ЧПУ'),
        Column::custom()->label('Активен?')->callback(function ($e) {
            return $e->active ? '&check;' : '-';
        })->orderable(false),
	    Column::custom()->label('Категории')->callback(function ($e) {
		    $categories = '';
		    foreach($e->categories as $category) {
			    $link = Admin::model(\App\Models\Filter::class)->displayUrl() . '?category_id=' . $category->id . (Request::get('type') ? '&type=' . Request::get('type') : '');
			    $categories = $categories . '<li>' . $category->title .
				    '<a href="' . url($link) . '">
				        <i class="fa fa-filter" data-toggle="tooltip" title="" data-original-title="Показать подобные записи"></i>
				    </a>' . '</li>';
		    }
		    return ('<ul class="list-unstyled">' . $categories . '</ul>');
	    })->orderable(false),
	    Column::count('options')->label('Опции фильтра')->append(
		    Column::filter('filter_id')->model(\App\Models\FilterValue::class)
	    )->orderable(false),
	    Column::string('order')->label('Порядок вывода'),
//        Column::order()->orderable(false)
    ]);
    return $display;
})->createAndEdit(function ($id) {
    $form = AdminForm::form();
    $form->items([
        FormItem::columns()->columns([
            [
                FormItem::checkbox('active', 'Активен?')->defaultValue(true),
	            FormItem::radio('type', 'Тип')->options(['club' => 'Заведение', 'trener' => 'Специалист'])->required(),
                FormItem::text('title', 'Название')->required(),
                FormItem::text('description', 'Описание'),
//                FormItem::select('category_id', 'Категория')->model(\App\Models\Category::class)->display('title')->required(),
            ], [
		        FormItem::text('order', 'Порядок вывода(задаётся автоматически при создании)'),
            ],
        ]),
    ]);
    return $form;
});