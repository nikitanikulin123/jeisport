<?php

$request = Request::get('type');

Admin::model(\App\Models\Review::class)->title('Отзывы о заведениях')->display(function () use($request) {
	$display = AdminDisplay::datatablesAsync()->order([[0, 'desc']])->attributes(['stateSave' => false,]);
    $display->apply(function ($query) use($request) {
	    if (Request::has('club_id'))
		    $query->where('reviewable_id', Request::get('club_id'));
	    elseif (Request::has('trener_id'))
		    $query->where('reviewable_id', Request::get('trener_id'));
	    elseif ($request == 'club')
            $query->where('reviewable_type', \App\Models\Club::class);
	    elseif ($request == 'trener')
		    $query->where('reviewable_type', \App\Models\Trener::class);
    });
    $display->parameters([
//        'club_id' => Request::get('club_id')
    ]);
    $display->with('reviewable', 'user');
    $display->filters([]);
//    $display->scope('forClub');
    $display->columns([
        Column::string('id')->label('#'),
        Column::string('user.name')->label('Пользователь')->orderable(false),
	    $request == 'club' || Request::has('club_id') ?
		    Column::string('reviewable.title')->label('Название заведения')->orderable(false) :
		    Column::string('reviewable.fullName')->label('Имя специалиств')->orderable(false),
        Column::string('text')->label('Текст отзыва'),
        Column::string('rating_plus')->label('Положительных оценок'),
        Column::string('rating_minus')->label('Отрицательных оценок'),
        Column::custom()->label('Проверен?')->callback(function ($e) {
            return $e->moderated ? '&check;' : '-';
        })->orderable(false),
    ]);
    return $display;
})->createAndEdit(function ($id) {
    $form = AdminForm::form();
    $form->items([
        FormItem::columns()->columns([
            [
//                FormItem::hidden('club_id'),
                FormItem::checkbox('moderated')->label('Проверен?')->defaultValue(true),
//                FormItem::select('user_id')->model(\App\User::class)->display('name')->label('Пользователь'),
//                FormItem::select('club_id')->model(\App\Models\Club::class)->display('title')->label('Заведение')->required(),
//                FormItem::text('rating_plus')->label('Положительных оценок'),
//                FormItem::text('rating_minus')->label('Отрицательных оценок'),
            ], [

            ],
        ]),
        FormItem::textarea('text')->label('Текст отзыва')
    ]);
    return $form;
})->create(null);