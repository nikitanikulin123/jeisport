<?php

Admin::model(\App\Models\Feed::class)->title('Интересные статьи')->display(function () {
	$display = AdminDisplay::datatablesAsync()->order([[0, 'desc']])->attributes(['stateSave' => false,]);
    $display->with('city', 'metas', 'club_feeds');
    $display->columns([
	    Column::string('id')->label('#'),
        Column::image('AdminImage')->label('Изображение на заднем фоне'),
        Column::string('title')->label('Заголовок'),
        Column::string('slug')->label('ЧПУ'),
        Column::custom()->label('Описание')->callback(function ($i) {
            return str_limit(strip_tags($i->body));
        })->orderable(false),
        Column::custom()->label('Активен?')->callback(function ($e) {
            return $e->active ? '&check;' : '-';
        })->orderable(false),
	    Column::string('city.title')->label('Город')->orderable(false),
	    Column::count('club_feeds')->label('Кейсы заведений')->append(
		    Column::filter('feed_id')->model(\App\Models\ClubFeed::class)
	    )->orderable(false),
	    Column::custom()->label('SEO')->callback(function ($e) {
		    if($seo = $e->metas->first())
		        return '<a href="'. url(Admin::model(\App\Models\Meta::class)->editUrl($seo->id)) . '" class="btn btn-default glyphicon glyphicon-eye-open"></a>';
		    return null;
	    })->orderable(false),
        Column::order()->orderable(false)
    ]);
    return $display;
})->createAndEdit(function ($id) {
    $form = AdminForm::form();
    $form->items([
        FormItem::columns()->columns([
            [
                FormItem::checkbox('active', 'Активен?')->defaultValue(true),
	            FormItem::image('AdminImage', 'Изображение на заднем фоне')->required(),
                FormItem::text('title', 'Заголовок')->required(),
                FormItem::text('sub_title', 'Подзаголовок')->required(),
                FormItem::text('slug', 'ЧПУ (генерируется автоматически)'),
	            FormItem::remoteSelect('city_id', 'Город')->model(\App\Models\City::class)->display('title')->required(),
	            FormItem::text('case_title1', 'Заголовок кейса 1'),
	            FormItem::textarea('case_text1', 'Текст кейса 1'),
	            FormItem::image('AdminCaseImage1', 'Изображение кейса 1'),
	            FormItem::text('case_title3', 'Заголовок кейса 3'),
	            FormItem::textarea('case_text3', 'Текст кейса 3'),
	            FormItem::image('AdminCaseImage3', 'Изображение кейса 3'),
            ], [
		        FormItem::remoteSelect('club_id', 'Заведение')->model(\App\Models\Club::class)->display('title'),
		        FormItem::ckeditor('body', 'Описание')->required(),
		        FormItem::text('case_title2', 'Заголовок кейса 2'),
		        FormItem::textarea('case_text2', 'Текст кейса 2'),
		        FormItem::image('AdminCaseImage2', 'Изображение кейса 2'),
		        FormItem::text('case_title4', 'Заголовок кейса 4'),
		        FormItem::textarea('case_text4', 'Текст кейса 4'),
		        FormItem::image('AdminCaseImage4', 'Изображение кейса 4'),
            ],
        ]),

    ]);
    return $form;
});