<?php

$type = Request::get('type');

Admin::model(\App\Models\SeoCityCategory::class)->title('СЕО страницы (Город/Категория)')->display(function () use($type) {
	$display = AdminDisplay::datatablesAsync()->order([[0, 'desc']])->attributes(['stateSave' => false,]);
	$display->with(['city', 'category']);
	$display->apply(function($query) use($type) {
		if($type == 'club')
			$query->whereHas('category', function ($query) {
				$query->where('type', 'club');
			});
		else
			$query->whereHas('category', function ($query) {
				$query->where('type', 'trener');
			});
	});
    $display->columns([
	    Column::string('id')->label('#'),
        Column::string('page_title')->label('Заголовок страницы'),
        Column::custom()->label('Подзаголовок страницы')->callback(function($i){
            return str_limit($i->page_subtitle, 50);
        })->orderable(false),
	    Column::custom()->label('Описание страницы')->callback(function($i){
		    return str_limit($i->page_description, 50);
	    })->orderable(false),
        Column::string('category.title')->label('Категория')->orderable(false),
        Column::string('city.title')->label('Город')->orderable(false),
    ]);
    return $display;
})->createAndEdit(function ($id) use($type) {
    $form = AdminForm::form();
	if (Request::is('*/edit') && !is_null($id)) {
		$seo = \App\Models\SeoCityCategory::findOrFail($id);
	}
    $form->items([
        FormItem::columns()->columns([
            [
	            FormItem::remoteSelect('city_id', 'Город')->model(\App\Models\City::class)->display('title')->required(),
	            $type == 'club' || (isset($seo) && $seo->category && $seo->category->type == 'club') ?
		            FormItem::remoteSelect('category_id', 'Категория')->model(\App\Models\Category::class)->filter(['type', 'club'])->display('title')->required() :
		            FormItem::remoteSelect('category_id', 'Категория')->model(\App\Models\Category::class)->filter(['type', 'trener'])->display('title')->required(),
                FormItem::text('page_title', 'Заголовок страницы')->required(),
                FormItem::textarea('page_subtitle', 'Подзаголовок страницы')->required(),
                FormItem::textarea('page_description', 'Описание страницы')->required(),
            ], [
                FormItem::textarea('metatitle', 'Metatitle'),
                FormItem::textarea('metakeyw', 'Metakeyw'),
                FormItem::textarea('metadesc', 'Metadesc'),
            ],
        ]),
    ]);
    return $form;
});