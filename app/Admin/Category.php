<?php

use App\Models\Category;
use App\Models\Vars;
use SleepingOwl\Admin\AssetManager\AssetManager;

$request = Request::get('type');

Admin::model(\App\Models\Category::class)->title('Категории')->display(function () use ($request) {
	if($request == 'club') {
		$rows = Category::whereType('club')->orderBy('active')->orderBy('created_at', 'DESC')->paginate(15);
		$count_active = Category::whereType('club')->where('active', true)->count();
	}
	elseif($request == 'trener') {
		$rows = Category::whereType('trener')->orderBy('active')->orderBy('created_at', 'DESC')->paginate(15);
		$count_active = Category::whereType('trener')->where('active', true)->count();
	}
	$model = Admin::model(Category::class);
		AssetManager::addStyle(url('libs/admin/css/admin-style.css'));
		AssetManager::addStyle(url('libs/bootstrap3-editable/css/bootstrap-editable.css'));
		AssetManager::addStyle(url('libs/colorbox/colorbox.css'));
		AssetManager::addScript(url('libs/bootstrap3-editable/js/bootstrap-editable.min.js'));
		AssetManager::addScript(url('libs/colorbox/jquery.colorbox-min.js'));
		AssetManager::addScript(url('libs/admin/js/init-colorbox.js'));
		AssetManager::addScript(url('libs/admin/js/custom.js'));
		AssetManager::addScript('admin::default/js/bootbox.js');
		AssetManager::addScript('admin::default/js/columns/control.js');
	return view('admin.index', compact('rows', 'model', 'count_active', 'request'));

})->createAndEdit(function ($id) use ($request) {
    $form = AdminForm::form();
    $slug = null;
    if (!is_null($id)) {
        $category = \App\Models\Category::findOrFail($id);
    }
    $form->items([
        FormItem::columns()->columns([
            [
                FormItem::checkbox('active', 'Активен?')->defaultValue(true),
	            $request == 'trener' || (isset($category) && $category->type == 'trener') ?
	                FormItem::checkbox('show_callback_form', 'Показывать форму обратной связи?') : '',
	            FormItem::radio('type', 'Тип')->options(['club' => 'Заведение', 'trener' => 'Специалист'])->required(),
                FormItem::text('title', 'Название')->required(),
                FormItem::text('slug', 'ЧПУ'),
            ], [
		        FormItem::checkbox('specialFilters', 'Автоматически добавить статичные фильтры?')->defaultValue(true),
		        $request == 'club' || (isset($category) && $category->type == 'club') ?
			        FormItem::remoteMultiSelect('filters', 'Фильтры')->model(\App\Models\Filter::class)->display('title')->addDisplay(['description', ' - '])->filter(['type', 'club']) :
		        ($request == 'trener' || (isset($category) && $category->type == 'trener') ?
			        FormItem::remoteMultiSelect('filters', 'Фильтры')->model(\App\Models\Filter::class)->display('title')->addDisplay(['description', ' - '])->filter(['type', 'trener']) : ''),
		        FormItem::text('order', 'Порядок вывода(задаётся автоматически при создании)'),
            ],
        ]),
    ]);
    return $form;
});