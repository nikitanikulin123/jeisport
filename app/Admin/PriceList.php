<?php

$request = Request::get('type');

Admin::model(\App\Models\PriceList::class)->title('Кейсы')->display(function () use ($request) {
	$display = AdminDisplay::datatablesAsync()->order([[0, 'desc']])->attributes(['stateSave' => false,]);
	$display->apply(function ($query) use ($request) {
		if (Request::has('price_list_category_id'))
			$query->where('price_list_category_id', Request::get('price_list_category_id'));
		else {
			if ($request == 'club')
				$query->whereHas('priceListCategory', function ($q) {
					$q->where('listable_type', \App\Models\Club::class);
				});
		elseif ($request == 'trener')
			$query->whereHas('priceListCategory', function ($q) {
				$q->where('listable_type', \App\Models\Trener::class);
			});
		}
	});
    $display->parameters([
        'price_list_category_id' => Request::get('price_list_category_id')
    ]);
    $display->with('priceListCategory', 'currency');
    $display->filters([]);
//    $display->scope('forClub');
    $display->columns([
        Column::string('id')->label('#'),
        Column::string('title')->label('Название'),
        Column::string('price')->label('Стоимость'),
        Column::string('currency.title')->label('Валюта')->orderable(false),
        Column::string('price_for')->label('Единица измерения'),
	    Column::string('priceListCategory.title')->label('Название прайс-листа')->append(
		    Column::filter('price_list_category_id')->model(\App\Models\PriceList::class)
	    )->orderable(false),
    ]);
    return $display;
})->createAndEdit(function () {

    $form = AdminForm::form();
    $form->items([
        FormItem::columns()->columns([
            [
                FormItem::text('title')->label('Заголовок кейса')->required(),
                FormItem::text('price')->label('Стоимость')->required(),
	            FormItem::select('currency_id', 'Валюта')->model(\App\Models\Currency::class)->display('title'),
                FormItem::text('price_for')->label('Единица измерения')->required(),
                FormItem::select('price_list_category_id', 'Категория кейса')->model(\App\Models\PriceListCategory::class)->display('title'),
            ], [

            ],
        ]),
    ]);
    return $form;
});