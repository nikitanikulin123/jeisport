<?php

Admin::model(\App\Models\Page::class)->title('Статичные страницы')->display(function () {
	$display = AdminDisplay::datatablesAsync()->order([[0, 'desc']])->attributes(['stateSave' => false,]);
    $display->columns([
	    Column::string('id')->label('#'),
	    Column::string('type')->label('Тип страницы'),
	    Column::string('title')->label('Заголовок'),
    ]);
    return $display;
})->createAndEdit(function ($id) {
    $form = AdminForm::form();
    $form->items([
        FormItem::columns()->columns([
            [
	            FormItem::text('title', 'Заголовок')->required(),
            ], [
		        FormItem::text('type', 'Тип страницы')->required(),
            ],
        ]),
	    FormItem::columns()->columns([
		    [
			    FormItem::image('AdminImage', 'Изображение первой секции'),
			    FormItem::ckeditor('section', 'Текст первой секции'),
			    FormItem::ckeditor('sub_section', 'Текст под первой секцией'),

		    ], [
			    FormItem::image('AdminImage2', 'Изображение второй секции'),
			    FormItem::ckeditor('section2', 'Текст второй секции'),
			    FormItem::ckeditor('sub_section2', 'Текст под второй секцией'),
		    ],
	    ]),

    ]);
    return $form;
});