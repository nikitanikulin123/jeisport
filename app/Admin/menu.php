<?php

if (Request::segment(1) == 'admin_panel') {
	$cities = \App\Models\City::all();
	$contacts = \App\Models\Contact::first()->getKey();
} else {
	$cities = new \App\Custom\CustomCollection;
	$contacts = new \App\Custom\CustomCollection();
}

Admin::menu()->url('/')->label('Главная страница')->icon('fa-dashboard');

Admin::menu()->label('Администраторы и роли')->icon('fa-cogs')->items(function () {
	Admin::menu(\App\Models\Administrator::class)->label('Администраторы')->icon('fa-users');
	Admin::menu(\App\Role::class)->label('Роли')->icon('fa-users');
	Admin::menu(\App\Permission::class)->label('Права доступа')->icon('fa-users');
});

Admin::menu()->label('Управление пользователями')->icon('fa-cogs')->items(function () {
	Admin::menu(\App\User::class)->label('Список пользователей')->icon('fa-users');
});

Admin::menu(\App\Models\City::class)->label('Города')->icon('fa-globe');
Admin::menu()->label('Районы')->icon('fa-cogs')->items(function () use ($cities) {
	foreach ($cities as $city) {
		Admin::menu(\App\Models\Region::class)->url('regions?city_id=' . $city->id)->label($city->title)->icon('fa-bars');
	}
});
Admin::menu()->label('Улицы')->icon('fa-cogs')->items(function () use ($cities) {
	foreach ($cities as $city) {
		Admin::menu(\App\Models\Street::class)->url('streets?city_id=' . $city->id)->label($city->title)->icon('fa-bars');
	}
});
Admin::menu(\App\Models\Currency::class)->label('Валюты')->icon('fa-globe');
Admin::menu(\App\Models\Callback::class)->label('Обратный звонок')->url('callbacks')->icon('fa-filter');

Admin::menu()->label('Управление заведениями')->icon('fa-cogs')->items(function () {
	Admin::menu()->label('Фильтры')->icon('fa-filter')->items(function () {
		Admin::menu(\App\Models\Filter::class)->url('filters?type=club')->label('Фильтры')->icon('fa-filter');
		Admin::menu(\App\Models\FilterValue::class)->label('Опции фильтров')->url('filter_values?type=club')->icon('fa-filter');
	});
	Admin::menu(\App\Models\Category::class)->url('categories?type=club')->label('Категории заведений')->icon('fa-barcode');
	Admin::menu(\App\Models\Club::class)->label('Заведения')->icon('fa-bars');
	Admin::menu(\App\Models\Stock::class)->url('stocks?type=club')->label('Акции заведений')->icon('fa-bars');
	Admin::menu(\App\Models\Review::class)->url('reviews?type=club')->label('Отзывы о заведениях')->icon('fa-bars');
	Admin::menu()->label('Прайс листы заведений')->icon('fa-cogs')->items(function () {
		Admin::menu(\App\Models\PriceListCategory::class)->url('price_list_categories?type=club')->label('Прайс листы заведений')->icon('fa-bars');
		Admin::menu(\App\Models\PriceList::class)->url('price_lists?type=club')->label('Кейсы заведений')->icon('fa-bars');
	});
});

Admin::menu()->label('Управление специалистами')->icon('fa-cogs')->items(function () {
	Admin::menu()->label('Фильтры')->icon('fa-filter')->items(function () {
		Admin::menu(\App\Models\Filter::class)->url('filters?type=trener')->label('Фильтры')->icon('fa-filter');
		Admin::menu(\App\Models\FilterValue::class)->label('Опции фильтров')->url('filter_values?type=trener')->icon('fa-filter');
	});
	Admin::menu(\App\Models\Category::class)->url('categories?type=trener')->label('Категории специалистов')->icon('fa-barcode');
	Admin::menu(\App\Models\Trener::class)->label('Специалисты')->icon('fa-bars');
	Admin::menu(\App\Models\Stock::class)->url('stocks?type=trener')->label('Акции от специалистов')->icon('fa-bars');
	Admin::menu(\App\Models\Review::class)->url('reviews?type=trener')->label('Отзывы о специалистах')->icon('fa-bars');
	Admin::menu()->label('Прайс листы специалистов')->icon('fa-cogs')->items(function () {
		Admin::menu(\App\Models\PriceListCategory::class)->url('price_list_categories?type=trener')->label('Прайс листы специалистов')->icon('fa-bars');
		Admin::menu(\App\Models\PriceList::class)->url('price_lists?type=trener')->label('Кейсы специалистов')->icon('fa-bars');
	});
});

Admin::menu()->label('Интересные статьи')->icon('fa-cogs')->items(function () {
	Admin::menu(\App\Models\Feed::class)->label('Статьи')->icon('fa-bars');
	Admin::menu(\App\Models\ClubFeed::class)->label('Кейсы статей')->icon('fa-bars');
});

Admin::menu()->label('СЕО настройки заведений')->icon('fa-cogs')->items(function () {
	Admin::menu(\App\Models\SeoCityCategory::class)->url('seo_city_categories?type=club')->label('Город/Категория')->icon('fa-bars');
	Admin::menu(\App\Models\SeoCityFilterValue::class)->url('seo_city_filter_values?type=club')->label('Город/Значение фильтра')->icon('fa-bars');
	Admin::menu(\App\Models\SeoRegionCategory::class)->url('seo_region_categories?type=club')->label('Район/Категория')->icon('fa-bars');
	Admin::menu(\App\Models\SeoStreetCategory::class)->url('seo_street_categories?type=club')->label('Улица/Категория')->icon('fa-bars');
});

Admin::menu()->label('СЕО настройки специалистов')->icon('fa-cogs')->items(function () {
	Admin::menu(\App\Models\SeoCityCategory::class)->url('seo_city_categories?type=trener')->label('Город/Категория')->icon('fa-bars');
	Admin::menu(\App\Models\SeoCityFilterValue::class)->url('seo_city_filter_values?type=trener')->label('Город/Значение фильтра')->icon('fa-bars');
	Admin::menu(\App\Models\SeoRegionCategory::class)->url('seo_region_categories?type=trener')->label('Район/Категория')->icon('fa-bars');
	Admin::menu(\App\Models\SeoStreetCategory::class)->url('seo_street_categories?type=trener')->label('Улица/Категория')->icon('fa-bars');
});

Admin::menu(\App\Models\Page::class)->label('Статичные страницы')->icon('fa-bars');

Admin::menu()->label('Настройки')->icon('fa-cogs')->items(function () use($contacts) {
	Admin::menu()->url('contacts/' . $contacts . '/edit')->label('Контакты')->icon('fa-phone');
	Admin::menu(\App\Models\Widget::class)->label('Виджеты')->icon('fa-sliders');
	Admin::menu(\App\Models\Meta::class)->label('Мета данные')->icon('fa-file-o');
});