<?php

$request = Request::get('type');

Admin::model(\App\Models\PriceListCategory::class)->title('Прайс лист')->display(function () use ($request) {
	$display = AdminDisplay::datatablesAsync()->order([[0, 'desc']])->attributes(['stateSave' => false,]);
    $display->apply(function ($query) use ($request) {
        if (Request::has('club_id'))
			$query->where('listable_id', Request::get('club_id'));
	    elseif (Request::has('trener_id'))
		    $query->where('listable_id', Request::get('trener_id'));
	    elseif ($request == 'club')
		    $query->where('listable_type', \App\Models\Club::class);
	    elseif ($request == 'trener')
		    $query->where('listable_type', \App\Models\Trener::class);
    });
    $display->parameters([
//        'club_id' => Request::get('club_id')
    ]);
	$display->with('listable');
    $display->filters([]);
//    $display->scope('forClub');
    $display->columns([
        Column::string('id')->label('#'),
        Column::string('title')->label('Название'),
	    Column::count('priceLists')->label('Записи')->append(
		    Column::filter('price_list_category_id')->model(\App\Models\PriceList::class)
	    )->orderable(false),
        $request == 'club' || Request::has('club_id') ?
	        Column::string('listable.title')->label('Название заведения')->orderable(false) :
	        Column::string('listable.fullName')->label('Имя')->orderable(false),
    ]);
    return $display;
})->createAndEdit(function ($id)  use ($request) {
	if (Request::is('*/edit') && !is_null($id)) {
		$priceList = \App\Models\PriceListCategory::findOrFail($id);
	}

    $form = AdminForm::form();
    $form->items([
        FormItem::columns()->columns([
            [
                FormItem::text('title')->label('Название категории')->required(),
	            $request == 'club' || (isset($priceList) && $priceList->listable_type == 'App\Models\Club') ?
		            FormItem::remoteSelect('listable_id', 'Клуб')->model(\App\Models\Club::class)->filter(['active', true])->display('title')->required() :
		            FormItem::remoteSelect('listable_id', 'Специалист')->model(\App\Models\Trener::class)->filter(['active', true])->display('FullName')->required(),
	            $request == 'club' ? FormItem::hidden('listable_type')->defaultValue('App\Models\Club') : FormItem::hidden('listable_type')->defaultValue('App\Models\Trener'),
            ], [

            ],
        ]),
    ]);
    return $form;
});