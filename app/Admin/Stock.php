<?php

$request = Request::get('type');

Admin::model(\App\Models\Stock::class)->title('Акции заведений')->display(function () use ($request) {
	$display = AdminDisplay::datatablesAsync()->order([[0, 'desc']])->attributes(['stateSave' => false,]);
	$display->apply(function ($query) use($request) {
		if (Request::has('club_id'))
			$query->where('stockable_id', Request::get('club_id'));
		elseif (Request::has('trener_id'))
			$query->where('stockable_id', Request::get('trener_id'));
		elseif ($request == 'club')
			$query->where('stockable_type', 'App\Models\Club');
		elseif ($request == 'trener')
			$query->where('stockable_type', 'App\Models\Trener');
    });
    $display->parameters([
//        'club_id' => Request::get('club_id')
    ]);
    $display->with('stockable');
    $display->filters([]);
//    $display->scope('forClub');
    $display->columns([
        Column::string('id')->label('#'),
        Column::string('title3')->label('Название'),
        Column::string('title4')->label('Подзаголовок'),
        Column::string('text')->label('Текст акции'),
        Column::image('AdminImage')->label('Изображение'),
	    $request == 'club' || Request::has('club_id') ?
	        Column::string('stockable.title')->label('Название')->orderable(false) :
	        Column::string('stockable.fullName')->label('Имя')->orderable(false),
    ]);
    return $display;
})->createAndEdit(function ($id) use ($request) {
	if (Request::is('*/edit') && !is_null($id)) {
		$stock = \App\Models\Stock::findOrFail($id);
	}

	$form = AdminForm::form();
    $form->items([
        FormItem::columns()->columns([
            [
                FormItem::text('title3')->label('Название')->required(),
                FormItem::text('title4')->label('Подзаголовок'),
                FormItem::textarea('text')->label('Текст акции'),
            ], [
		        FormItem::image('AdminImage')->label('Изображение'),
                $request == 'club' || (isset($stock) && $stock->stockable_type == 'App\Models\Club') ?
		            FormItem::remoteSelect('stockable_id', 'Клуб')->model(\App\Models\Club::class)->filter(['active', true])->display('title')->required() :
		            FormItem::remoteSelect('stockable_id', 'Специалист')->model(\App\Models\Trener::class)->filter(['active', true])->display('FullName')->required(),
		        $request == 'club' ? FormItem::hidden('stockable_type')->defaultValue('App\Models\Club') : FormItem::hidden('stockable_type')->defaultValue('App\Models\Trener'),
            ],
        ]),
    ]);
    return $form;
});