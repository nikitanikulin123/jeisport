<?php

/*
 * Describe you custom displays, columns and form items here.
 *
 *		Display::register('customDisplay', '\Foo\Bar\MyCustomDisplay');
 *
 *		Column::register('customColumn', '\Foo\Bar\MyCustomColumn');
 *
 * 		FormItem::register('customElement', \Foo\Bar\MyCustomElement::class);
 *
 */

FormItem::register('remoteSelect', App\Custom\Admin\FormItems\RemoteSelect::class);
FormItem::register('remoteMultiSelect', App\Custom\Admin\FormItems\RemoteMultiSelect::class);