<?php

use App\Models\Category;
use App\Models\City;
use App\Models\Trener;
use SleepingOwl\Admin\AssetManager\AssetManager;

Admin::model(\App\Models\Trener::class)->title('Специалисты')->display(function () {
	$categories = Category::all();
    $rows = Trener::with('category', 'metas')->filtered()->orderBy('active')->orderBy('created_at', 'DESC')->paginate(15);
	$count_active = Trener::where('active', true)->count();
	$count_unmoderated = Trener::where('moderated', false)->count();
    $model = Admin::model(Trener::class);
    $cities = City::all();
	AssetManager::addStyle(url('libs/admin/css/admin-style.css'));
	AssetManager::addStyle(url('libs/bootstrap3-editable/css/bootstrap-editable.css'));
	AssetManager::addStyle(url('libs/colorbox/colorbox.css'));
	AssetManager::addScript(url('libs/bootstrap3-editable/js/bootstrap-editable.min.js'));
	AssetManager::addScript(url('libs/colorbox/jquery.colorbox-min.js'));
	AssetManager::addScript(url('libs/admin/js/init-colorbox.js'));
	AssetManager::addScript(url('libs/admin/js/custom.js'));
	AssetManager::addScript('admin::default/js/bootbox.js');
	AssetManager::addScript('admin::default/js/columns/control.js');
    return view('admin.index', compact('rows', 'model', 'count_active', 'count_unmoderated', 'categories', 'cities'));
})->createAndEdit(function ($id) {
	AssetManager::addStyle(url('libs/admin/css/admin-style.css'));
	AssetManager::addScript(url('js/custom_functions.js'));
	AssetManager::addScript(url('js/custom.js'));
	AssetManager::addScript(url('libs/admin/js/custom.js'));
    $defaultCity = \App\Models\City::where('is_main', true)->first();

    AssetManager::addScript('https://api-maps.yandex.ru/2.1/?lang=ru_RU');
    $form = AdminForm::form();
    $form->items([
        FormItem::columns()->columns([
            [
                FormItem::checkbox('active', 'Активен')->defaultValue(true),
                FormItem::checkbox('moderated', 'Промодерирован'),
                FormItem::checkbox('premium', 'Премиум?'),
	            FormItem::remoteSelect('user_id', 'Автор')->model(\App\User::class)->display('name')->addDisplay(['email', ' - ']),
                FormItem::text('name', 'Имя')->required(),
                FormItem::text('sname', 'Фамилия')->required(),
                FormItem::text('mname', 'Отчество'),
                FormItem::text('slug', 'ЧПУ'),
	            FormItem::remoteSelect('category_id', 'Категория')->model(Category::class)->filter(['type', 'trener'])->display('title')->required(),
		        FormItem::hidden('cityRegionStreet'),
	            FormItem::custom()->display(function ($instance) {
		            return view('partials.mini._cityRegionStreetSelectBlock', ['instance' => $instance, 'b_counter' => 0]);
	            }),
	            FormItem::text('house', 'Дом'),
	            FormItem::text('phone', 'Телефон'),
	            FormItem::text('phone2', 'Телефон2'),
                FormItem::text('site', 'Сайт (если есть)'),
	            FormItem::text('n_email', 'Email для уведомлений'),
	            FormItem::custom()->display(function ($instance) use ($defaultCity) {
		            return view('admin.maps._yandexMap', ['instance' => $instance, 'defaultCity' => $defaultCity, 'b_counter' => 0]);
	            }),
	            FormItem::text('lat_lng', 'Координаты')->defaultValue('42.875989,74.603674'),
            ],
	        [
		        FormItem::image('AdminImage', 'Главное изображения')->required(),
		        FormItem::images('AdminCertificates', 'Сертификаты'),
		        FormItem::images('AdminPortfolio', 'Портфолио'),
		        FormItem::ckeditor('description', 'Описание'),
		        FormItem::ckeditor('education', 'Образование'),
		        FormItem::ckeditor('pricelist', 'Расценки'),
	        ]
        ]),

	    FormItem::custom()->display(function ($instance) {
		    return view('partials.mini._dutyBlock', ['instance' => $instance, 'b_counter' => 0]);
	    }),
	    FormItem::columns()->columns([
		    [
			    FormItem::checkbox('work_in_mon', 'Понедельник')->defaultValue(true),
		    ],
		    [
			    FormItem::time('mon_time_from')->defaultValue('08:00')->validationRule('date_format:H:i'),
		    ],
		    [
			    FormItem::time('mon_time_to')->defaultValue('17:00')->validationRule('date_format:H:i'),
		    ],
		    [],
	    ]),
	    FormItem::columns()->columns([
		    [
			    FormItem::checkbox('work_in_tue', 'Вторник')->defaultValue(true),
		    ],
		    [
			    FormItem::time('tue_time_from')->defaultValue('08:00')->validationRule('date_format:H:i'),
		    ],
		    [
			    FormItem::time('tue_time_to')->defaultValue('17:00')->validationRule('date_format:H:i'),
		    ],
		    [],
	    ]),
	    FormItem::columns()->columns([
		    [
			    FormItem::checkbox('work_in_wed', 'Среда')->defaultValue(true),
		    ],
		    [
			    FormItem::time('wed_time_from')->defaultValue('08:00')->validationRule('date_format:H:i'),
		    ],
		    [
			    FormItem::time('wed_time_to')->defaultValue('17:00')->validationRule('date_format:H:i'),
		    ],
		    [],
	    ]),
	    FormItem::columns()->columns([
		    [
			    FormItem::checkbox('work_in_thu', 'Четверг')->defaultValue(true),
		    ],
		    [
			    FormItem::time('thu_time_from')->defaultValue('08:00')->validationRule('date_format:H:i'),
		    ],
		    [
			    FormItem::time('thu_time_to')->defaultValue('17:00')->validationRule('date_format:H:i'),
		    ],
		    [],
	    ]),
	    FormItem::columns()->columns([
		    [
			    FormItem::checkbox('work_in_fri', 'Пятница')->defaultValue(true),
		    ],
		    [
			    FormItem::time('fri_time_from')->defaultValue('08:00')->validationRule('date_format:H:i'),
		    ],
		    [
			    FormItem::time('fri_time_to')->defaultValue('17:00')->validationRule('date_format:H:i'),
		    ],
		    [],
	    ]),
	    FormItem::columns()->columns([
		    [
			    FormItem::checkbox('work_in_sat', 'Суббота')->defaultValue(true),
		    ],
		    [
			    FormItem::time('sat_time_from')->defaultValue('08:00')->validationRule('date_format:H:i'),
		    ],
		    [
			    FormItem::time('sat_time_to')->defaultValue('17:00')->validationRule('date_format:H:i'),
		    ],
		    [],
	    ]),
	    FormItem::columns()->columns([
		    [
			    FormItem::checkbox('work_in_sun', 'Воскресенье')->defaultValue(true),
		    ],
		    [
			    FormItem::time('sun_time_from')->defaultValue('08:00')->validationRule('date_format:H:i'),
		    ],
		    [
			    FormItem::time('sun_time_to')->defaultValue('17:00')->validationRule('date_format:H:i'),
		    ],
		    [],
	    ]),
    ]);
    return $form;
})->delete();