<?php

Admin::model(\App\Models\ClubFeed::class)->title('Кейсы статей')->display(function () {
	$display = AdminDisplay::datatablesAsync()->order([[0, 'desc']])->attributes(['stateSave' => false,]);
    $display->with('club', 'feed');
	$display->filters([
		Filter::related('feed_id')->model(\App\Models\Feed::class)->display('title')
	]);
    $display->columns([
	    Column::string('id')->label('#'),
        Column::image('AdminImage')->label('Изображение на заднем фоне'),
        Column::string('title')->label('Заголовок'),
        Column::string('sub_title')->label('Подзаголовок'),
        Column::custom()->label('Описание')->callback(function ($i) {
            return str_limit(strip_tags($i->body));
        })->orderable(false),
	    Column::string('club.title')->label('Заведение')->orderable(false),
	    Column::string('feed.title')->label('Статья')->orderable(false),
        Column::custom()->label('Активен?')->callback(function ($e) {
            return $e->active ? '&check;' : '-';
        })->orderable(false),
        Column::order()->orderable(false)
    ]);
    return $display;
})->createAndEdit(function ($id) {
    $form = AdminForm::form();
    $form->items([
        FormItem::columns()->columns([
            [
                FormItem::checkbox('active', 'Активен?')->defaultValue(true),
                FormItem::text('title', 'Заголовок')->required(),
                FormItem::text('sub_title', 'Подзаголовок')->required(),
		        FormItem::ckeditor('body', 'Описание')->required(),
            ], [
		        FormItem::image('AdminImage', 'Изображение на заднем фоне'),
		        FormItem::remoteSelect('club_id', 'Заведение')->model(\App\Models\Club::class)->display('title')->required(),
		        FormItem::remoteSelect('feed_id', 'Статья')->model(\App\Models\Feed::class)->display('title')->required(),
            ],
        ]),

    ]);
    return $form;
});