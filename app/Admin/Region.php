<?php

Admin::model(\App\Models\Region::class)->title('Районы')->display(function () {
	$display = AdminDisplay::datatablesAsync()->order([[0, 'desc']])->attributes(['stateSave' => false,]);
    $display->with(['city']);
	$display->filters([
		Filter::related('city_id')->model(\App\Models\City::class)->display('title')
	]);
    $display->columns([
	    Column::string('id')->label('#'),
        Column::string('title')->label('Название'),
        Column::string('slug')->label('ЧПУ'),
        Column::string('city.title')->label('Город')->orderable(false),
	    Column::custom()->label('Активен?')->callback(function ($e) {
		    return $e->active ? '&check;' : '-';
	    })->orderable(false),
    ]);
    return $display;
})->createAndEdit(function () {
    $form = AdminForm::form();
    $form->items([
        FormItem::columns()->columns([
            [
	            FormItem::checkbox('active', 'Активен')->defaultValue(true),
                FormItem::text('title', 'Название')->required(),
                FormItem::text('slug', 'ЧПУ'),
                FormItem::remoteSelect('city_id', 'Город')->model(\App\Models\City::class)->display('title')->required(),
            ], [

            ],
        ]),
    ]);
    return $form;
});