<?php

Admin::model(\App\Models\Currency::class)->title('Валюты')->display(function () {
	$display = AdminDisplay::datatablesAsync()->order([[0, 'desc']])->attributes(['stateSave' => false,]);
    $display->columns([
	    Column::string('id')->label('#'),
        Column::string('title')->label('Название'),
        Column::custom()->label('Активен?')->callback(function ($e) {
            return $e->active ? '&check;' : '-';
        })->orderable(false),
        Column::order()->orderable(false),
    ]);
    return $display;
})->createAndEdit(function ($id) {
    $form = AdminForm::form();
    $form->items([
        FormItem::columns()->columns([
            [
                FormItem::checkbox('active', 'Активен?')->defaultValue(true),
                FormItem::text('title', 'Название')->required(),
            ], [

            ],
        ]),
    ]);
    return $form;
})->delete(null);