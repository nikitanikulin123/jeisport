<?php

$request = Request::get('type');

Admin::model(\App\Models\FilterValue::class)->title('Опции фильтров')->display(function () use($request){
	$display = AdminDisplay::datatablesAsync()->order([[0, 'desc']])->attributes(['stateSave' => false,]);

    $display->apply(function ($query) use($request){
        if (Request::get('filter_id'))
            $query->where('filter_id', Request::get('filter_id'));
	    elseif($request == 'club')
		    $query->whereHas('filter', function ($query) {
			    $query->where('type', 'club');});
        elseif($request == 'trener')
	        $query->whereHas('filter', function ($query) {
		        $query->where('type', 'trener');});
    });

	$display->with('filter');
    $display->filters([

    ]);
    $display->columns([
	    Column::string('id')->label('#'),
        Column::string('title')->label('Название опции'),
        Column::string('slug')->label('SLUG'),
        Column::custom()->label('Активен?')->callback(function ($e) {
            return $e->active ? '&check;' : '-';
        })->orderable(false),
        Column::string('filter.title')->label('Фильтр')->append(
	        Column::filter('filter_id')->model(\App\Models\FilterValue::class)
        )->orderable(false),
        Column::order()->orderable(false)
    ]);
    return $display;
})->createAndEdit(function ($id) use($request) {
	$filters = \App\Models\Filter::all();
    $form = AdminForm::form();
    $form->items([
        FormItem::columns()->columns([
            [
                FormItem::checkbox('active', 'Активен?')->defaultValue(true),
                FormItem::text('title', 'Название опции')->required(),
                FormItem::text('slug', 'SLUG (генерируется автоматически)'),

	            $request == 'club' || $request == 'trener' ?
		            ($request == 'club' || (isset($category) && $category->type == 'club') ?
			            FormItem::select('filter_id', 'Фильтры')->options($filters->filter(function ($item) {
				            return $item['type'] == 'club';
			            })->lists('title', 'id')->toArray())->required() :
			            ($request == 'trener' || (isset($category) && $category->type == 'trener') ?
				            FormItem::select('filter_id', 'Фильтры')->options($filters->filter(function ($item) {
					            return $item['type'] == 'trener';
				            })->lists('title', 'id')->toArray())->required() : '')
                    ) :
                    FormItem::select('filter_id', 'Фильтр')->model(\App\Models\Filter::class)->display('title')->required(),
            ], [

            ],
        ]),
    ]);
    return $form;
});