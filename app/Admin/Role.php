<?php


Admin::model(\App\Role::class)->title('Роли')->display(function ()
{
	$display = AdminDisplay::table();
	$display->columns([
		Column::string('name')->label('Название'),
		Column::string('display_name')->label('Отображаемое название'),
		Column::string('description')->label('Описание'),
        Column::lists('perms.name')->label('Права доступа')
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('name', 'Название')->required()->unique(),
		FormItem::text('display_name', 'Отображаемое название')->required(),
		FormItem::text('description', 'Описание')->required(),
        FormItem::multiselect('perms', 'Права доступа')->model(\App\Permission::class)->display('name')
	]);
	return $form;
});