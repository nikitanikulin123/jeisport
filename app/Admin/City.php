<?php

use SleepingOwl\Admin\AssetManager\AssetManager;

Admin::model(\App\Models\City::class)->title('Города')->display(function () {
	$display = AdminDisplay::datatablesAsync()->order([[0, 'desc']])->attributes(['stateSave' => false,]);
    $display->with('regions', 'streets');
    $display->columns([
	    Column::string('id')->label('#'),
        Column::string('title')->label('Название'),
        Column::string('slug')->label('ЧПУ'),
        Column::custom()->label('Основной город')->callback(function($i){
            return $i->is_main ? '&check;' : '-';
        })->orderable(false),
        Column::custom()->label('Активен?')->callback(function ($e) {
            return $e->active ? '&check;' : '-';
        })->orderable(false),
	    Column::count('regions')->label('Районы')->append(
		    Column::filter('city_id')->model(\App\Models\Region::class)
	    )->orderable(false),
	    Column::count('streets')->label('Улицы')->append(
		    Column::filter('city_id')->model(\App\Models\Street::class)
	    )->orderable(false),
    ]);
    return $display;
})->createAndEdit(function ($id) {
	AssetManager::addScript(url('js/custom_functions.js'));
	AssetManager::addScript('https://api-maps.yandex.ru/2.1/?lang=ru_RU');
	$defaultCity = \App\Models\City::where('is_main', true)->first();

	$form = AdminForm::form();
    $form->items([
        FormItem::columns()->columns([
            [
                FormItem::checkbox('active', 'Активен')->defaultValue(true),
                FormItem::checkbox('is_main', 'Основной город')->validationRule('unique:cities,is_main,' . $id),
                FormItem::text('title', 'Название')->required(),
                FormItem::text('slug', 'ЧПУ'),
                FormItem::text('zoom', 'Приближение'),
            ], [

            ],
        ]),
	    FormItem::custom()->display(function ($instance) use ($defaultCity) {
		    return view('admin.maps._yandexMap', ['instance' => $instance, 'defaultCity' => $defaultCity, 'b_counter' => 0]);
	    }),
        FormItem::text('lat_lng', 'Координаты')->required()->defaultValue('42.875989,74.603674'),
        FormItem::text('corners', 'Кооринаты углов')->required()
    ]);
    return $form;
});