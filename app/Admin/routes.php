<?php

Route::get('', ['as' => 'admin.home', function () {
	return Admin::view(null, config('app.site_name'));
}]);
Route::get('clubs/attributes/{club_id}', '\App\Http\Controllers\Admin\PopupsController@getAttributes')->name('attributes.index');
Route::post('clubs/attributes/{club_id}', '\App\Http\Controllers\Admin\PopupsController@postAttributes')->name('attributes.store');

Route::get('formItems/remoteSelect/{model}/{field}', '\App\Custom\Admin\FormItems\FormItemController@remoteSelect')->name('admin.formitems.remoteSelect');