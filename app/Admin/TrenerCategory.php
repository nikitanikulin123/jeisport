<?php

Admin::model(\App\Models\TrenerCategory::class)->title('Категории специалистов')->display(function () {
    $display = AdminDisplay::datatablesAsync();
    $display->apply(function ($query) {
        $query->orderBy('order')->orderBy('created_at', 'desc');
    });
    $display->with();
    $display->filters([

    ]);
    $display->columns([
        Column::string('id')->label('#'),
        Column::string('title')->label('Название'),
        Column::string('slug')->label('ЧПУ'),
//        Column::action('show')->label('Фильтры')->icon('fa-certificate')->style('long')->url(function ($instance) {
//            return url('admin_panel/filters?category_id=' . $instance->id);
//        }),
        Column::custom()->label('Активен?')->callback(function ($e) {
            return $e->active ? '&check;' : '-';
        }),
        Column::order()
    ]);
    return $display;
})->createAndEdit(function ($id) {
    $form = AdminForm::form();
//    $slug = null;
//    if (!is_null($id)) {
//        $category = \App\Models\Category::findOrFail($id);
//    }
    $form->items([
        FormItem::columns()->columns([
            [
                FormItem::checkbox('active', 'Активен?')->defaultValue(true),
                FormItem::text('title', 'Название')->required(),
                FormItem::text('slug', 'ЧПУ'),
            ], [
//                isset($category) ? FormItem::multiselect('subcategories', 'Подкатегории')->options($category->filterOptions()->lists('filter_values.title', 'filter_values.id')->toArray()) : ''
            ],
        ]),
    ]);
    return $form;
});