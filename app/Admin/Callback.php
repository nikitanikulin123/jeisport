<?php

Admin::model(\App\Models\Callback::class)->title('Обратный звонок')->display(function () {
	$display = AdminDisplay::datatablesAsync()->order([[0, 'desc']])->attributes(['stateSave' => false,]);
    $display->with(['callbackable']);
    $display->columns([
	    Column::string('id')->label('#'),
        Column::string('name')->label('Имя'),
        Column::string('phone')->label('Телефон'),
	    Column::custom()->label('Проверен?')->callback(function ($e) {
		    return $e->checked ? '&check;' : '-';
	    })->orderable(false),
	    Column::custom()->label('Закрыт?')->callback(function ($e) {
		    return $e->closed ? '&check;' : '-';
	    })->orderable(false),
	    Column::custom()->label('Тип')->callback(function ($e) {
		    return $e->callbackable_type == \App\Models\Club::class ? 'Заведение' :
			        ($e->callbackable_type == \App\Models\Category::class ? 'Категория' : 'Страница Контактов');
	    })->orderable(false),
	    Column::string('callbackable.title')->label('Название')->orderable(false),
	    Column::string('description')->label('Описание'),
    ]);
    return $display;
})->createAndEdit(function () {
    $form = AdminForm::form();
    $form->items([
        FormItem::columns()->columns([
            [
	            FormItem::checkbox('checked', 'Проверен?'),
	            FormItem::checkbox('closed', 'Закрыт?'),
                FormItem::text('name', 'Имя')->required(),
                FormItem::text('phone', 'Телефон')->required(),
            ], [
		        FormItem::textarea('description', 'Описание'),
            ],
        ]),
    ]);
    return $form;
});