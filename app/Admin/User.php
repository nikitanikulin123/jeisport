<?php

Admin::model(\App\User::class)->title('Пользователи сайта')->display(function ()
{
	$display = AdminDisplay::datatablesAsync()->order([[0, 'desc']])->attributes(['stateSave' => false,]);
	$display->with('roles');
	$display->columns([
		Column::string('id')->label('#'),
		Column::string('name')->label('Имя'),
		Column::string('email')->label('Email'),
        Column::datetime('created_at')->label('Дата регистрации'),
        Column::string('limit_of_items')->label('Лимит заведений'),
		Column::lists('roles.display_name')->label('Роль')->orderable(false),
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('name', 'Имя')->required(),
		FormItem::text('email', 'Email')->required()->unique(),
		FormItem::text('limit_of_items', 'Лимит заведений')->defaultValue(3),
		FormItem::select('AdminRole', 'Роль')->options(\App\Role::where('name', '!=', 'admin')->get()->lists('description', 'id')->all())->display('name'),
	]);
	return $form;
});