<?php

namespace App;

use App\Http\Controllers\BaseController;
use App\Models\Club;
use App\Models\Trener;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Http\Request as policyRequest;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, EntrustUserTrait;

	protected $with = ['roles'];

    protected $dates = [
        'activation_request_date'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	    'id', 'name', 'email', 'password', 'photo', 'uid', 'network', 'identity', 'activated',
	    'activation_token', 'remember_token', 'limit_of_items', 'personal_bill', 'balance'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

	public function setRolesAttribute($perms)
	{
		$this->roles()->detach();
		if (!$perms) return;
		if (!$this->exists) $this->save();

		$this->roles()->attach($perms);
	}

	public function roles()
	{
		return $this->belongsToMany(Role::class);
	}

	public function setAdminRoleAttribute($value)
	{
		$this->roles()->detach();
		if (!$value) return;
		if (!$this->exists) {
			$this->save();
			$this->update(['personal_bill' => (new BaseController(new policyRequest))->generateCode()]);
			$response = Password::sendResetLink(['email' => $this->email], function (Message $message) {
				$message->subject('Для Вас создан аккаунт на сайте js.kg!');
			});
		}

		$this->roles()->attach($value);
	}

	public function getAdminRoleAttribute()
	{
		if ($this->exists && count($this->roles))
			return $this->roles->first()->id;
		return null;
	}

	public function clubs()
	{
		return $this->hasMany(Club::class);
	}

	public function treners()
	{
		return $this->hasMany(Trener::class);
	}

	public function hasRoleFix($role){
		return $this->roles->filter(function($item) use($role){
			return $item['name'] == $role;
		})->count();
	}
}
