<?php

namespace App\Http\Middleware;

use App\Models\Company;
use App\Models\Resumes\Resume;
use App\Models\Training;
use App\Models\Vacancies\Vacancy;
use App\User;
use Closure;
use Illuminate\Contracts\Auth\Guard;

class LimitMiddleware
{
	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$user = \Auth::user();
		$identifier = array_get(explode('-', $request->segment(1)), 0);
		$object = $identifier == 'c' ? 'clubs' : 'treners';
		$count = count($user->{$object});

		if ($count >= $user->limit_of_items)
			return redirect()->back()->with('error', 'Исчерпан лимит заведений!');

		return $next($request);
	}
}
