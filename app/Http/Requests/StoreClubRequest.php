<?php

namespace App\Http\Requests;

use Auth;
use Carbon\Carbon;

class StoreClubRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
	    return true;
//        return Auth::check() && Auth::user()->hasRoleFix('employers');
    }

    /**
     * Set custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
//            'position' => 'должность'
        ];
    }

    /**
     * Set custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
	    $rules = [];

	    if(Request::url() == route('clubs.newClubStore') || (Request::url() == route('clubs.store') ||
			    ((Request::url() == route('clubs.update', $this->route('id'))) && Request::get('step') == 1))) {
		    $rules = array_merge($rules, [
			    'title' => 'required|max:255',
			    'prefix' => 'max:255',
			    'postfix' => 'max:255',
			    'phone' => 'required|max:255',
			    'phone2' => 'max:255',
			    'description' => 'required',
			    'category_id' => 'required|exists:categories,id',
			    'city_id' => 'required|exists:cities,id',
			    'site' => 'max:255',
			    'n_email' => 'max:255|email',
		    ]);
	    }

	    if(Request::url() == route('clubs.newClubStore'))
		    $rules = array_merge($rules, [
			    'email' => 'required|max:255|email',
		    ]);

	    if((Request::url() == route('clubs.store') || Request::url() == route('clubs.update', $this->route('id'))) && Request::get('step') == 1) {
		    $rules = array_merge($rules, [
			    'lat_lng' => 'required|max:255',
			    'region_id' => 'exists:regions,id',
			    'street_id' => 'required|exists:streets,id',
			    'house' => 'max:255',
			    'address' => 'max:255',
		    ]);
	    }

//	    if(Request::url() == route('clubs.update', $this->route('id')) && Request::get('step') == 3) {
//		    $rules = array_merge($rules, [
//			    'mon_time_to' => 'required_with:work_in_mon|after:mon_time_from',
//			    'tue_time_to' => 'required_with:work_in_tue|after:tue_time_from',
//			    'wed_time_to' => 'required_with:work_in_wed|after:wed_time_from',
//			    'thu_time_to' => 'required_with:work_in_thu|after:thu_time_from',
//			    'fri_time_to' => 'required_with:work_in_fri|after:fri_time_from',
//			    'sat_time_to' => 'required_with:work_in_sat|after:sat_time_from',
//			    'sun_time_to' => 'required_with:work_in_sun|after:sun_time_from',
//		    ]);
//	    }

	    if(Request::url() == route('clubs.update', $this->route('id')) && Request::get('step') == 4) {
		    $rules = array_merge($rules, [
			    'image' => 'required|max:255',
		    ]);
	    }

	    return $rules;
    }
}
