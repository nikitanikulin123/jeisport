<?php

namespace App\Http\Requests;

use Auth;
use Carbon\Carbon;

class StoreTrenerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
	    return true;
//        return Auth::check() && Auth::user()->hasRoleFix('employers');
    }

    /**
     * Set custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
//            'position' => 'должность'
        ];
    }

    /**
     * Set custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
	    $rules = [];
	    $isBoss = Auth::check() && Auth::user()->hasRoleFix('boss');

	    if(Request::url() == route('treners.newTrenerStore') || (Request::url() == route('treners.store') ||
			    ((Request::url() == route('treners.update', $this->route('id'))) && Request::get('step') == 1))) {
		    $rules = [
			    'sname' => 'required|max:255',
			    'name' => 'required|max:255',
			    'mname' => 'max:255',
			    'city_id' => 'required|exists:cities,id',
			    'category_id' => 'required|exists:categories,id',
			    'description' => 'required',
		    ];
		    if(!$isBoss)
			    $rules = array_merge($rules, [
				    'phone' => 'required|max:255',
				    'phone2' => 'max:255',
				    'site' => 'max:255',
				    'n_email' => 'max:255|email',
			    ]);
	    }

	    if(Request::url() == route('treners.newTrenerStore'))
		    $rules = array_merge($rules, [
			    'email' => 'required|max:255|email',
		    ]);

	    if((Request::url() == route('treners.store') || Request::url() == route('treners.update', $this->route('id')))
		    && Request::get('step') == 1 && !$isBoss) {
		    $rules = array_merge($rules, [
			    'lat_lng' => 'required|max:255',
			    'region_id' => 'exists:regions,id',
			    'street_id' => 'required|exists:streets,id',
			    'house' => 'max:255',
			    'address' => 'max:255',
		    ]);
	    }

	    if(Request::url() == route('treners.update', $this->route('id')) && Request::get('step') == 4) {
		    $rules = array_merge($rules, [
			    'image' => 'required|max:255',
		    ]);
	    }

	    return $rules;
    }
}
