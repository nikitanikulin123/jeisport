<?php
if (env('SSL_ENABLED'))
    URL::forceSchema('https');

// Authentication routes...
Route::post('social/auth', 'Auth\AuthController@postSocialAuth');
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::get('auth/activate', ['as' => 'auth.activate', 'uses' => 'Auth\AuthController@activate']);
Route::get('auth/send_activation/{user_id}', ['as' => 'auth.send_activation', 'uses' => 'Auth\AuthController@sendActivation'])->where('user_id', '[0-9]+');

//2gis images parser for firms
Route::get('2gis/parse', 'Api\ApiController@twoGisParse')->name('2gis.parse');
Route::get('2gis/form', 'Api\ApiController@twoGisForm');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', ['as' => 'password.reset', 'uses' => 'Auth\PasswordController@getReset']);
Route::post('password/reset', 'Auth\PasswordController@postReset');

//HOME
Route::get('/', ['as' => 'home', 'uses' => 'PagesController@getHome']);



//ImageCache
Route::get('fitImage', 'BaseController@fitImage')->name('fitImage');
Route::get('resizeImage', 'BaseController@resizeImage')->name('resizeImage');

Route::group(['prefix' => 'admin/ajax', 'namespace' => 'Admin'], function () {
    Route::post('units', ['as' => 'admin.units.update', 'uses' => 'AjaxController@unitsUpdate']);
});

//--------------------------------------------------------------------------------------------------------------

// AJAX
Route::get('json/get_objects', ['as' => 'json.get_objects', 'uses' => 'PushItemsController@getObjects']);
Route::group(['prefix' => 'ajax'], function () {
    Route::post('modal/getModalContent', 'AjaxController@getModalContent')->name('ajax.getModalContent');
    Route::get('address/getRegionAndCityLists', 'AjaxController@getRegionAndCityLists')->name('ajax.getRegionAndCityLists');

    Route::post('get_shown_items', 'PushItemsController@getShownItems')->name('ajax.get_shown_items');
    Route::post('balloon', 'PushItemsController@getBalloonContent');
//	Route::post('stocks/show', 'StocksController@show');
    Route::post('price_list', 'AjaxController@getPriceList');
    Route::get('showMore', 'AjaxController@showMore')->name('ajax.showMore');

    Route::post('uploadFile', ['as' => 'uploadFile', 'uses' => 'AjaxController@uploadFile']);
    Route::post('uploadImage', ['as' => 'uploadImage', 'uses' => 'AjaxController@uploadImage']);
});

// Callbacks
Route::post('callbacks/{id}', 'CallbacksController@create')->name('callbacks.create')->where('id', '[0-9]+');

Route::group(['middleware' => 'auth'], function () {

    // Profiles
    Route::get('profiles/index', 'ProfileController@index')->name('profiles.index');
//	Route::get('profiles/agencies/{club_slug}', 'ProfileController@clubShow')->name('profiles.clubShow');

    // Trainers
    Route::group(['middleware' => 'role:boss'], function () {
        Route::get('t-create', 'TrenersController@create')->name('treners.create');
        Route::post('t-store', 'TrenersController@store')->name('treners.store');
        Route::post('t-{id}/delete', 'TrenersController@destroy')->name('treners.delete')->where('id', '[0-9]+');
    });
    Route::get('t-{trener_slug}/edit', 'TrenersController@edit')->name('treners.edit');
    Route::post('t-{id}/update', 'TrenersController@update')->name('treners.update')->where('id', '[0-9]+');

    // Clubs
    Route::group(['middleware' => 'role:boss'], function () {
        Route::group(['middleware' => 'limit_of_items'], function () {
            Route::get('c-create', 'ClubsController@create')->name('clubs.create');
            Route::post('c-store', 'ClubsController@store')->name('clubs.store');
        });
        Route::get('c-{club_slug}/edit', 'ClubsController@edit')->name('clubs.edit');
        Route::post('c-{id}/update', 'ClubsController@update')->name('clubs.update')->where('id', '[0-9]+');
        Route::post('c-{id}/delete', 'ClubsController@destroy')->name('clubs.delete')->where('id', '[0-9]+');
    });

    // BillingLogs
    Route::get('billing/c-{id}', 'BillingLogsController@index')->name('billing.index')->where('id', '[0-9]+');
    Route::post('billing/c-{id}/makePremium', 'BillingLogsController@makePremium')->name('billing.makePremium')->where('id', '[0-9]+');

    // Stocks
    Route::post('stocks/store', 'StocksController@store')->name('stocks.store');
    Route::post('stocks/{id}/update', 'StocksController@update')->name('stocks.update')->where('id', '[0-9]+');
    Route::post('stocks/{id}/delete', 'StocksController@destroy')->name('stocks.delete')->where('id', '[0-9]+');

    // PriceListCategories
    Route::post('priceListCategories/store', 'PriceListCategoriesController@store')->name('priceListCategories.store');
    Route::post('priceListCategories/{id}/update', 'PriceListCategoriesController@update')->name('priceListCategories.update')->where('id', '[0-9]+');
    Route::post('priceListCategories/{id}/delete', 'PriceListCategoriesController@destroy')->name('priceListCategories.delete')->where('id', '[0-9]+');

    // PriceList
    Route::post('priceLists/store', 'PriceListController@store')->name('priceLists.store');
    Route::post('priceLists/{id}/update', 'PriceListController@update')->name('priceLists.update')->where('id', '[0-9]+');
    Route::post('priceLists/{id}/delete', 'PriceListController@destroy')->name('priceLists.delete')->where('id', '[0-9]+');

    // Reviews
    Route::post('reviews/{id}/store', 'ReviewsController@store')->name('reviews.store')->where('user_id', '[0-9]+');
    Route::get('ajax/reviews/likeUp', 'ReviewsController@likeUp')->name('reviews.likeUp');
    Route::get('ajax/reviews/likeDown', 'ReviewsController@likeDown')->name('reviews.likeDown');

    // Callback
    Route::post('callbacks/{id}/checked', 'CallbacksController@checked')->name('callbacks.checked')->where('id', '[0-9]+');
    Route::post('callbacks/{id}/delete', 'CallbacksController@destroy')->name('callbacks.delete')->where('id', '[0-9]+');
});

// Common
Route::get('ajax/reviews/rated', 'ReviewsController@rated')->name('reviews.rated');
Route::get('ajax/reviews/toFavourites', 'ReviewsController@toFavourites')->name('reviews.toFavourites');

// Feeds
Route::get('{city}/feeds/{feed}', 'PagesController@getFeedShow')->name('feeds.show');

// Trainers
Route::get('{city}/t-{trener_category}', 'TrenersController@index')->name('treners.index');
Route::get('{city}/t-{trener_category}/district/{district_slug}', 'TrenersController@index')->name('treners.indexByDistrict');
Route::get('{city}/t-{trener_category}/street/{street_slug}', 'TrenersController@index')->name('treners.indexByStreet');
Route::get('{city}/t-{trener_category}/type/{filterValue_slug}', 'TrenersController@index')->name('treners.indexByType');
Route::get('{city}/t-{category}/{trener_slug}', 'TrenersController@show')->name('treners.show');
Route::post('newTrenerStore', 'TrenersController@newTrenerStore')->name('treners.newTrenerStore');

// Clubs
Route::get('{city}/{category}', 'ClubsController@index')->name('clubs.index');
Route::get('{city}/{category}/district/{district_slug}', 'ClubsController@index')->name('clubs.indexByDistrict');
Route::get('{city}/{category}/street/{street_slug}', 'ClubsController@index')->name('clubs.indexByStreet');
Route::get('{city}/{category}/type/{filterValue_slug}', 'ClubsController@index')->name('clubs.indexByType');
Route::get('{city}/{category}/{club_slug}', 'ClubsController@show')->name('clubs.show');
Route::post('newClubStore', 'ClubsController@newClubStore')->name('clubs.newClubStore');

// Pages
Route::get('searched', 'PagesController@getSearched')->name('searched');
Route::get('contacts', 'PagesController@getContacts')->name('contacts');
Route::get('about', 'PagesController@getAbout')->name('about');

// City - Region
Route::get('{city}', 'CitiesController@show')->name('cities.show');

