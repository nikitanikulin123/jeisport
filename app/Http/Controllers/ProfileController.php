<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Club;
use App\Models\Favourite;
use App\Models\Trener;
use Illuminate\Http\Request;
use Auth;

class ProfileController extends BaseController
{
	public function index(Request $request)
	{
		$type = $request->get('type');
		if(($type == 'favourites')) {
			$favouriteClubsId = Favourite::where(['user_id' => Auth::id(), 'favouritable_type' => Club::class])->lists('favouritable_id')->toArray();
			$favouriteTrenersId = Favourite::where(['user_id' => Auth::id(), 'favouritable_type' => Trener::class])->lists('favouritable_id')->toArray();
			$treners = Trener::active()->whereIn('id', $favouriteTrenersId)->get();
			$clubs = Club::active()->whereIn('id', $favouriteClubsId)->get();
		} else {
			if (Auth::user()->hasRoleFix('trener') || Auth::user()->hasRoleFix('boss'))
				$treners = Trener::activeOnly()->forAuthUser()->with(['visits', 'callbacks'])->orderBy('id', 'desc')->paginate(env('CONFIG_PAGINATE', 1));
			if (Auth::user()->hasRoleFix('boss'))
				$clubs = Club::activeOnly()->forAuthUser()->with(['visits', 'callbacks'])->orderBy('id', 'desc')->paginate(env('CONFIG_PAGINATE', 1));
		}

		return view('profiles.index', compact('clubs', 'treners', 'type'));
	}

	public function steps(Request $request)
	{
		return view('profiles.steps');
	}

    public function show(Request $request)
    {
        return view('users.profile');
    }
}
