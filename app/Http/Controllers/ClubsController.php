<?php

namespace App\Http\Controllers;

use App\Custom\CustomCollection;
use App\Http\Requests;
use App\Models\AvgRating;
use App\Models\BaseModel;
use App\Models\Club;
use App\Models\Favourite;
use App\Models\Filter;
use App\Models\Photo;
use App\Models\Rating;
use App\Models\Recommend;
use App\Models\Region;
use App\Models\Review;
use App\Models\Street;
use App\Models\Trener;
use App\Models\Vars;
use App\Models\Visit;
use App\User;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Http\Request;
use App\Http\Requests\StoreClubRequest;
use App\Events\SendNotificationEvent;
use Event;
use Auth;
use Session;
use Request as UrlRequest;

class ClubsController extends BaseController
{
	public function index(Request $request, $city = null, $category = null, $parameter = null)
	{
		$filVars = Vars::specialFilterVarsByIds();
		$cities = $this->cities;
		$categories = $this->categories;
		$city = $cities->filterArrFix(['slug' => $city])->first();
		$category = $categories->filterArrFix(['slug' => $category])->first();
		if (!$city)
			throw new NotFoundHttpException;
		if (!$category || stripos($category->slug, 'issyk-kul') && $city->slug != 'issyk-kul')
			throw new NotFoundHttpException;
		$clubs = Club::moderated()->forCategory($category->id)->forCity($city->id)->with('street', 'region', 'photos', 'avgRatings')->paginate(env('CONFIG_PAGINATE', 1));
		$filters = $category->filters;
		$sortFilters = Filter::all()->filterArrFix('id', '=', [$filVars['sort'], $filVars['district'], $filVars['street']]);
		list($recommendedCategories, $recommendedFilterValues) = $this->recommended('club', $city, $category);

		$filterType = UrlRequest::segment(3) ? UrlRequest::segment(3) : null;
		$selectFilterAndSeoArr = $this->selectFilterAndSeo($parameter, $filterType, $category, $city, 'club')->collapse();
		$selected_filter_id = $selectFilterAndSeoArr->get('selected_filter_id');
		$seo_page = $selectFilterAndSeoArr->get('seo_page');
		$clubs_count = $selectFilterAndSeoArr->get('items_count');
		if(!$parameter)
			$selected_filter_id = $request->get('filter_id');
		if ($seo_page) {
			$seoPageTitle = $seo_page->page_title;
			$metatitle = $seo_page->metatitle;
			$metakeyw = $seo_page->metakeyw;
			$metadesc = $seo_page->metadesc;
		} else
			$seoPageTitle = 'Заголовок страницы по умолчанию';

		$objects = [];
		foreach ($clubs as $club) {
			$objects[] = [
				'type' => 'Feature',
				'id' => $club->id,
				'geometry' => [
					'type' => 'Point',
					'coordinates' => [$club->lat, $club->lng]
				],
				'properties' => [
					"balloonContent" => "{$club->title}, id: {$club->id}",
					"clusterCaption" => "{$club->title}",
					"hintContent" => "{$club->title}"
				]
			];
		}
		$points = [
			'count' => $clubs->total(),
			'type' => 'FeatureCollection',
			'features' => $objects
		];

		return view('clubs.index', compact('allClubs', 'clubs', 'seoPageTitle', 'filters', 'city', 'clubs_count',
			'category', 'points', 'seo_page', 'metatitle', 'metakeyw', 'metadesc',
			'filterType', 'selected_filter_id', 'sortFilters', 'recommendedCategories', 'recommendedFilterValues',
			'exceptedFilterValues'
		));
	}

	/**
	 * Show the form for creating a new vacancy.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(Request $request)
	{
		$regions = [];
		$streets = [];

		return view('clubs.createEdit', compact('categories', 'cities', 'regions', 'streets'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param StoreClubRequest $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreClubRequest $request)
	{
		if($request->get('step') == 1) {
			$request->merge([
				'user_id' => Auth::id(),
				'email' => Auth::user()->email,
				'active' => true,
                'premium' => 1
			]);
			$club = Club::create($request->all());

			$treners = Trener::whereIn('id', $request->get('trener_id'))->get();
			$club->treners()->sync($treners);

			return redirect()->route('clubs.edit', [$club->slug, 'step' => $request->get('step') + 1])->with('success', 'Информация успешно сохранена. Пожалуйста, заполните следующие данные');
		}
	}

	public function newClubStore(StoreClubRequest $request)
	{
		$user = User::where('email', $request->get('email'))->first();

		$request->merge([
			'user_id' => $user ? $user->id : 0,
			'image' => 'nophoto.jpg',
			'lat_lng' => '42.87599689166824,74.60371691534351',
		]);
		$club = Club::create($request->all());

		$request->merge([
			'type' => 'newClub',
			'recipientEmail' => $club->email,
			'bccEmail' => $this->adminVars->adminEmail,
			'data' => $club,
		]);

		Event::fire(new SendNotificationEvent($request->all()));

		return $this->ajaxResponse(1, 'Заведение успешно сохранёно и отправлено на модерацию!');
	}

    public function show($city, $category_slug, $club_slug)
    {
	    $cities = $this->cities;
	    $categories = $this->categories;
	    $city = $cities->filterArrFix(['slug' => $city])->first();
	    $category = $categories->filterArrFix(['slug' => $category_slug])->first();
	    $club = Club::findBySlugOrFail($club_slug);
        if (!$club || (!$club->moderated && $club->user_id != Auth::id()))
            throw new NotFoundHttpException;
	    if (!$city || $club->city->id != $city->id)
		    throw new NotFoundHttpException;
	    if (!$category || $club->category->id != $category->id)
		    throw new NotFoundHttpException;
	    if(!$club->premium)
	        list($recommendedCategories, $recommendedFilterValues) = $this->recommended('club', $city, $category);
        $club_filter_values = $club->filterValues;
	    list($amountOfRates, $rating, $avgRating, $reviews, $amountOfReviews) = $this->getReviewsAndRatings($club);
	    $stocks = $club->stocks;
	    $addedToFavourites = $club->favourites->where('user_id', Auth::id())->first();

	    $allVisits = Visit::where('visitable_id', $club->id)->where('visitable_type', Club::class)->orderBy('visited_at', 'asc')->get();
	    $expiredVisits = $allVisits->filter(function ($item) {return $item['visited_at'] <= Carbon::now()->subDays(30)->toDateString();});
	    if ($expiredVisits->count() > 0) {
		    foreach ($expiredVisits as $expiredVisit)
			    $expiredVisit->delete();
	    }
	    if ($visit = $allVisits->where('visited_at', Carbon::now()->toDateString())->first()) {
		    $visit->increment('visits');
	    } else {
		    $visit = Visit::create(['visits' => 1, 'visited_at' => Carbon::now()]);
		    $club->visits()->save($visit);
	    }

	    if(count($club->treners))
	        $specialistsFilters = $club->treners->first()->category->filters;
	    else
		    $specialistsFilters = new CustomCollection();

	    $specialistsFilterValuesArr = [];
	    foreach ($club->treners as $singleItem) {
		    $filterValuesArr = count($singleItem->filterValues) ? $singleItem->filterValues->lists('title', 'id')->toArray() : [];
		    foreach ($filterValuesArr as $key => $val) {
			    $specialistsFilterValuesArr[$key] = $val;
		    }
	    }

	    $seoPageTitle = 'Заголовок страницы по умолчанию';

        return view('clubs.show', compact('club', 'amountOfRates', 'rating', 'avgRating', 'filters', 'city', 'category',
	        'club_filter_values', 'reviews', 'amountOfReviews', 'stocks', 'specialistsFilters', 'specialistsFilterValuesArr',
	        'recommendedCategories', 'recommendedFilterValues', 'seoPageTitle', 'addedToFavourites'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
	public function edit(Request $request, $slug)
	{
		$club = Club::findBySlugOrFail($slug);
		$regions = Region::where('city_id', $club->city_id)->get()->sortBy('title');
		$streets = Street::where('city_id', $club->city_id)->get()->sortBy('title');
		$filters = Filter::where('type', 'club')->with('categories')->has('options')->whereHas('categories', function ($query) use($club){
			return $query->where('id', $club->category->id);
		})->get();
		$step = $request->get('step');

		if($step == 'complete') {
			if(!$club->moderated) {
				$request->merge([
					'type' => 'clubForModeration',
					'recipientEmail' => $this->adminVars->adminEmail,
					'data' => $club,
				]);
				Event::fire(new SendNotificationEvent($request->all()));
			}
			return redirect()->route('profiles.index')->with('success', 'Заведение успешно сохранёно и отправлено на модерацию!');
//			return redirect()->route('billing.index', [$club->id, 'type' => 'club'])->with('success', 'Заведение успешно сохранёно и отправлено на модерацию!');
		}

		return view('clubs.createEdit', compact('club', 'categories', 'cities', 'regions', 'streets', 'filters', 'step'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param StoreClubRequest $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(StoreClubRequest $request, $id)
	{
		$request->merge([
			'moderated' => false,
		]);
		$club = Club::findOrFail($id);
		$club->update($request->all());

		if($request->get('step') == 'uploadPriceList') {
			Session::flash('success', 'Прайслист успешно загружен!');
			return ['status' => 'success', 'text' => 'Прайслист успешно загружен!'];
		}

		if($request->get('step') == 1) {
			$treners = Trener::whereIn('id', $request->get('trener_id'))->get();
			$club->treners()->sync($treners);
		}
		elseif($request->get('step') == 2) {
			if(count($request->get('filterValues')) == 0)
				return redirect()->back()->with('error', 'Необходимо выбрать хотя бы одну подкатегорию!');

			$filterValues = [];
			foreach ($request->get('filterValues') as $key => $value) {
				if($value) {
					$filterValues[] = $key;
				}
			}
			$club->filterValues()->sync($filterValues);
		}
		elseif($request->get('step') == 4) {
			$club->photos()->delete();
			if($request->get('images') != '') {
				$imgs = [];
				$images = explode(',', $request->get('images'));
//				array_push($images, $club->image);
				foreach ($images as $img) {
					$imgs[] = Photo::create(['imageable_id' => $club->id, 'path' => $img]);
				}
				$club->photos()->saveMany($imgs);
			}
		}

		return redirect()->route('clubs.edit', [$club->slug, 'step' => $request->get('step') + 1])->with('success', 'Информация о заведении обновлена!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$club = Club::findOrFail($id);
		$club->delete();

		return redirect()->back()->with('success', 'Заведение удалено!');
	}
}
