<?php

namespace App\Http\Controllers;

use App\Custom\CustomCollection;
use App\Http\Requests;
use App\Models\BaseModel;
use App\Models\Category;
use App\Models\Club;
use App\Models\Filter;
use App\Models\Region;
use App\Models\Street;
use App\Models\Trener;
use App\Models\Vars;
use DB;
use Illuminate\Http\Request;

class PushItemsController extends BaseController
{
	public function getShownItems(Request $request)
	{
		$type = $request->get('type');
		$page = $request->get('page') ? $request->get('page') : 1;
		$model = BaseModel::makeModel($type, true);

		$filteredItemsIds = $this->getFilteredItemsIds($request, $type);
		if(count($filteredItemsIds)) {
			$placeholders = implode(',', $filteredItemsIds->all());
			if($type === 'clubs.specialists')
				$items = BaseModel::makeModel('trener', true)->whereIn('id', $filteredItemsIds)
					->orderByRaw("field(id,{$placeholders})")->paginate(env('CONFIG_PAGINATE', 1));
			else
				$items = $model->whereIn('id', $filteredItemsIds)->with('street', 'region', 'photos', 'avgRatings')
					->orderByRaw("field(id,{$placeholders})")->paginate(env('CONFIG_PAGINATE', 1));
		} else
			$items = BaseModel::makeModel('club', true)->whereIn('id', $filteredItemsIds)->paginate(env('CONFIG_PAGINATE', 1));

		if($type == 'clubs.createEdit' || $type == 'treners.createEdit')
			$resultedFilteredArr = [];
		else
			$resultedFilteredArr = $this->getResultedFilteredArr($request, $type, $filteredItemsIds);

		if($type == 'clubs.index') {
			$itemsForMap = $model->whereIn('id', $filteredItemsIds)->lists('lat_lng', 'id');
			$objects = [];
			foreach ($itemsForMap as $id => $lat_lng) {
				$lat_lng = explode(',', $lat_lng);
				$objects[] = [
					'type' => 'Feature',
					'id' => $id,
					'geometry' => [
						'type' => 'Point',
						'coordinates' => [$lat_lng[0], $lat_lng[1]]
					],
				];
			}
			$points = [
				'count' => $items->total(),
				'type' => 'FeatureCollection',
				'features' => $objects
			];
		}
		$units = $type == 'clubs.index' ? trans_choice("место|места|мест", $items->total()) : trans_choice("специалист|специалиста|специалистов", $items->total());

		$dashedType = str_replace(["."], '_', $type);
		return $this->getAjaxResponseView([
			'status' => [1, null],
			'views' => ["partials.mini._unitsListFiltered-id_{$dashedType}", "partials.mini._showMoreBlock-id_{$dashedType}"],
			'params' => ['setValue' => [".b-changed_items_amount" => "- {$items->total()} {$units}"]],
			'vars' => ['items' => $items, 'points' => @$points, 'resultedFilteredArr' => $resultedFilteredArr, 'type' => $type, 'page' => $page]
		]);
	}

	protected function getFilteredItemsIds($request, $type, $withSpecialFilters = true) {
		$model = BaseModel::makeModel($type, true);
		parse_str(urldecode($request->get('filtersData')), $filters_array);
		parse_str(urldecode($request->get('sortFiltersData')), $sort_filters_array);
		$corners = $request->get('bounds');
		$city_id = $request->get('city_id');
		$category_id = $request->get('category_id');
		$specialFilters = $this->detectSpecialFilters($filters_array, 'list');
		$sortSpecialFilters = $this->detectSpecialFilters($sort_filters_array, 'list');
		$filters_array = $this->detectSpecialFilters($filters_array, 'remove');

		if($type == 'clubs.specialists') {
			if($request->get('club_id')) {
				$filteredItems = Club::findOrFail($request->get('club_id'))->treners;

				foreach($this->array_values_recursive($filters_array) as $filId) {
					$filteredItems = $filteredItems->filter(function ($item) use($filId) {
						return $item->filterValues->first() ? $item->filterValues->whereLoose('id', $filId)->first() : false;
					});
				};
			} else
				$filteredItems = new CustomCollection();
		}
		else {
			$filteredItems = $model->forCategory($category_id)->forCity($city_id);

			if ($type == 'clubs.createEdit' || $type == 'treners.createEdit')
				$filteredItems = $filteredItems->active()->forAuthUser();
			else
				$filteredItems = $filteredItems->moderated();
			if ($type == 'clubs.index' && $corners)
				$filteredItems = $filteredItems->byMapCorners(array_get($corners, 0)[0], array_get($corners, 1)[0], array_get($corners, 0)[1], array_get($corners, 1)[1]);
			if ($type == 'treners.index')
				$filteredItems = $filteredItems->forAttachedTrener();
			foreach ($this->array_values_recursive($filters_array) as $filId) {
				$filteredItems = $filteredItems->whereHas('filterValues', function ($query) use ($filId) {
					return $query->where('filter_value_id', $filId);
				});
			};
			if($withSpecialFilters)
				$filteredItems = $filteredItems->checkSpecialFilters($specialFilters)->checkSpecialFilters($sortSpecialFilters);
		}

		$filteredItemsIds = $filteredItems->lists('id', 'id');

		return $filteredItemsIds;
	}

	protected function getResultedFilteredArr($request, $type, $filteredItemsIds)
	{
		$filVars = Vars::specialFilterVarsByIds();
		$model = BaseModel::makeModel($type, true);
		$filters = ($type != 'clubs.specialists') ? Category::where('id', $request->get('category_id'))->first()->filters :
			Filter::active()->where('type', 'trener')->get();
		$specialFiltersArr = $filters->filterArrFix('id', '=', [$filVars['ratings'], $filVars['schedule'], $filVars['ratings_T']]);
//		$streets = Street::where('city_id', $city_id)->lists('title', 'id');
//		$regions = Region::where('city_id', $city_id)->lists('title', 'id');

//		$arrFilteredVal = [];
//		$arrFilteredStreetsVal = [];
//		$arrFilteredRegionsVal = [];
//		foreach ($filteredItems as $singleItem) {
//			$filterValuesArr = count($singleItem->filterValues) ? $singleItem->filterValues->lists('title', 'id')->toArray() : [];
//			foreach ($filterValuesArr as $key => $val) {
//				$arrFilteredVal[$key] = $val;
//			}
//			if($type != 'clubs.specialists') {
//				$filterStreet = $singleItem->street;
//				$filterRegion = $singleItem->region;
//				$arrFilteredStreetsVal[$filterStreet->id] = $filterStreet->title;
//				$arrFilteredRegionsVal[$filterRegion->id] = $filterRegion->title;
//			}

//		}
//
		if ($type == 'clubs.index')
			$arrFilteredVal = array_flip(\DB::table('club_filter_value')->whereIn('club_id', $filteredItemsIds)->lists('filter_value_id'));
		else
			$arrFilteredVal = array_flip(\DB::table('filter_value_trener')->whereIn('trener_id', $filteredItemsIds)->lists('filter_value_id'));

		$arrSpecialFiltersWithValues = [];
		foreach ($specialFiltersArr as $specialFilter) {
			$arrSpecialFiltersWithValues[$specialFilter->id] = $specialFilter->options->lists('title', 'id')->toArray();
		}

		foreach ($arrSpecialFiltersWithValues as $specialFilterId => $arrSpecialFilterWithValues) {
			foreach ($arrSpecialFilterWithValues as $specialFilterValueId => $arrSpecialFilterValueText) {
				if($specialFilterValueId != $filVars['with3dTour'])
					if($model->whereIn('id', $filteredItemsIds)->checkSpecialFilters([[$specialFilterId => [$specialFilterValueId]]])->count())
						$arrFilteredVal[$specialFilterValueId] = $arrSpecialFilterValueText;
					else
						$arrFilteredVal[] = null;
			}
		}
		$filteredSelectsArr = [];
		foreach ($filters->reverse() as $categoryFilter) {
			$categoryFilterValuesArr = $categoryFilter->options->lists('title', 'id')->toArray();
			$filteredSelectsArr = $this->getArrOfDisabledValues($categoryFilterValuesArr, $arrFilteredVal, $filteredSelectsArr);
		}
//		$filteredStreetsArr = $this->getArrOfDisabledValues($streets, $arrFilteredStreetsVal);
//		$filteredRegionsArr = $this->getArrOfDisabledValues($regions, $arrFilteredRegionsVal);

		$resultedFilteredArr = [];
		$resultedFilteredArr = array_add($resultedFilteredArr, 'selects', $filteredSelectsArr);
//		$resultedFilteredArr = array_add($resultedFilteredArr, 'streets', $filteredStreetsArr);
//		$resultedFilteredArr = array_add($resultedFilteredArr, 'regions', $filteredRegionsArr);

		return $resultedFilteredArr;
	}

	protected function getArrOfDisabledValues($items, $filteredItemsArr, $resultedArr = []) {
		foreach ($items as $key => $val) {
			if(array_get($filteredItemsArr, $key) !== null)
				$resultedArr[$key] = ['value' => $val, 'disabled' => false];
			else
				$resultedArr[$key] = ['value' => $val, 'disabled' => true];
		}
		return $resultedArr;
	}

	protected function detectSpecialFilters($array, $action)
	{
		$filVars = Vars::specialFilterVarsByIds();
		$flat = array();

		foreach ($array as $key => $value) {
			if($action == 'remove') {
				if ($array && $key != $filVars['ratings'] && $key != $filVars['price'] && $key != $filVars['schedule']&& $key != $filVars['ratings_T'])
					$flat[$key] = $value;
			}
			elseif($action == 'list') {
				if ($array && ($key == $filVars['ratings'] || $key == $filVars['price'] || $key == $filVars['schedule'] ||
						$key == $filVars['district'] || $key == $filVars['street'] || $key == $filVars['sort'] || $key == $filVars['ratings_T']))
					$flat[] = [$key => $value];
			}
		}

		return $flat;
	}

    protected function array_values_recursive($array)
    {
        $flat = array();

        foreach ($array as $value) {
            if (is_array($value)) {
                $flat = array_merge($flat, $this->array_values_recursive($value));
            } else {
                $flat[] = $value;
            }
        }
        return $flat;
    }

    /**
     * Returns balloon content for specified object
     *
     * @param Request $request
     * @return string
     */
    public function getBalloonContent(Request $request)
    {
        $objects = $request->get('objects');
        if (count($objects) > 1) {
            $clubs = Club::active()->whereIn('id', $objects)->get();
            $arr = [];
            foreach ($clubs as $k => $club):
                $arr[$k] = "<div class='img-wrapper'><img src='" . route('fitImage', ['80_80', $club->image]) . "' alt=''></div><div class='title'><a href='" . route('clubs.show', [$club->city->slug, $club->category->slug, $club->slug]) ."'>$club->title</a></div>";
            endforeach;
            return $arr;
        } else {
            $club = Club::active()->find(array_get($objects, 0));
            if (!$club)
                return 'Object not found!';
            return view('partials.mini._objectBalloonContent', compact('club'));
        }
    }
}
