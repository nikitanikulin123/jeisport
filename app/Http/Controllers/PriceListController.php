<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\AvgPriceList;
use App\Models\Club;
use App\Models\PriceList;
use App\Models\PriceListCategory;
use App\Models\Stock;
use Illuminate\Http\Request;
use Session;

class PriceListController extends BaseController
{
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param StoreTrenerRequest $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'title' => 'required|max:255',
			'price' => 'required|max:255',
			'currency_id' => 'required|exists:currencies,id',
			'price_for' => 'required|max:255',
		]);
		$request->merge([
			'price_list_category_id' => $request->get('priceListCategory_id'),
		]);

		$priceListCategory = PriceListCategory::findOrFail($request->get('priceListCategory_id'));
		$priceList = PriceList::create($request->all());

		$priceListCategory = PriceListCategory::findOrFail($request->get('priceListCategory_id'));
		$priceListCategory->update(['min' => $priceListCategory->priceLists->min('price'), 'max' => $priceListCategory->priceLists->max('price')]);

		return $this->ajaxResponse(1, 'Информация о кейсе прайслиста успешно сохранена!');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param StoreTrenerRequest $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'title' => 'required|max:255',
			'price' => 'required|max:255',
			'currency_id' => 'required|exists:currencies,id',
			'price_for' => 'required|max:255',
		]);

		$priceListCategory = PriceListCategory::findOrFail($request->get('priceListCategory_id'));
		$priceList = PriceList::findOrFail($id);

		$priceList->update($request->all());
		$priceListCategory->update(['min' => $priceListCategory->priceLists->min('price'), 'max' => $priceListCategory->priceLists->max('price')]);

		return $this->ajaxResponse(1, 'Информация о кейсе прайслиста успешно сохранена!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$priceList = PriceList::findOrFail($id);
		$priceList->delete();

		return redirect()->back()->with('success', 'Кейс прайслиста удалён!');
	}
}
