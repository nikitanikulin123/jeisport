<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\City;
use App\Models\Feed;
use App\Models\TrenerCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CitiesController extends BaseController
{
    public function show(Request $request, $city)
    {
	    $cities = $this->cities;
        $city = $cities->filterArrFix(['slug' => $city])->first();
        if(!$city)
            throw new NotFoundHttpException;
	    $feeds = Feed::activeOrder()->with('city')->get();

        return view('home', compact('categories', 'city', 'feeds'));
    }
}
