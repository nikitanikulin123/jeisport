<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\BaseModel;
use App\Models\BillingLog;
use Illuminate\Http\Request;
use App\Events\SendNotificationEvent;
use Event;
use Auth;
use Session;
use Request as UrlRequest;

class BillingLogsController extends BaseController
{
	public function index(Request $request, $id)
	{
		$type = $request->get('type');
		$billedItem = BaseModel::makeModel($type)->findOrFail($id);

		return view('billing.index', compact('billedItem', 'type'));
	}

	public function makePremium(Request $request, $id)
	{
		$premium = $request->get('premium');
		$duration = $request->get('duration');
		$type = $request->get('type');
		$item = BaseModel::makeModel($type)->findOrFail($id);
		$user = Auth::user();

		if(count($item->billingLog) && $item->billingLog->filterArrFix(['active' => true, 'expired' => false])->sortByDesc('created_at')->first())
			return redirect()->route('profiles.index')->with('error', 'Заведению уже присвоен премиальный план!');
		if($premium && !$duration)
			return redirect()->route('billing.index', [$id, 'type' => $type, 'premium' => 1])->with('success', 'Выберите продолжительность!');

		if($premium && $duration) {
			$rates = $this->billingVars->premiumClub;
			$sum = $rates[$duration];
			$description = 'premium';

			if($duration == 3 && !$item->used_trial) {
				$description = 'premium_trial';
				$sum = 0;
				$request->merge([
					'used_trial' => true,
				]);
			}

			if($user->balance < $sum)
				return redirect()->route('billing.index', [$id, 'type' => $type])->with('error', 'У вас недостаточно средств на счёте!');

			$balance = $user->balance - $sum;
			$billingLog = BillingLog::create(['user_id' => $user->id, 'description' => $description, 'duration' => $duration * 30,
				'change' => $sum, 'balance' => $balance, 'active' => true]);
			$item->billingLog()->save($billingLog);
			$user->update(['balance' => $balance]);
		}

		$item->update($request->all());

		return redirect()->route('profiles.index')->with('success', 'Заведению успешно присвоен тарифный план!');
	}
}
