<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Club;
use App\Models\Stock;
use Illuminate\Http\Request;
use Session;

class StocksController extends BaseController
{
	public function store(Request $request)
	{
		$this->validate($request, [
			'title3' => 'required|max:255',
			'title4' => 'required|max:255',
			'text' => 'required',
			'image' => 'required|max:255',
		]);

		$club = Club::findOrFail($request->get('club_id'));

		$stock = Stock::create($request->all());
		$club->stocks()->save($stock);

		return $this->ajaxResponse(1, 'Информация об акции успешно сохранена!');
	}

	public function show(Request $request)
	{
		$stock = Stock::findOrFail($request->get('id'));
		$club = $stock->stockable;

		return view('clubs.stocks.show', compact('stock', 'club'));
	}

	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'title3' => 'required|max:255',
			'title4' => 'required|max:255',
			'text' => 'required',
			'image' => 'required|max:255',
		]);

		$club = Club::findOrFail($request->get('club_id'));
		$stock = Stock::findOrFail($id);

		$stock->update($request->all());

		return $this->ajaxResponse(1, 'Информация об акции успешно сохранена!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$stock = Stock::findOrFail($id);
		$stock->delete();

		return redirect()->back()->with('success', 'Акция удалена!');
	}
}
