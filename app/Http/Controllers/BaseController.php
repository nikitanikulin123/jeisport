<?php

namespace App\Http\Controllers;

use App\Custom\CustomCollection;
use App\Http\Requests;
use App\Models\BaseModel;
use App\Models\Category;
use App\Models\City;
use App\Models\Club;
use App\Models\FilterValue;
use App\Models\Meta;
use App\Models\Recommend;
use App\Models\Region;
use App\Models\SeoCityCategory;
use App\Models\SeoCityFilterValue;
use App\Models\SeoRegionCategory;
use App\Models\SeoStreetCategory;
use App\Models\Street;
use App\Models\Vars;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Image;
use Request as StaticRequest;
use Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use ReflectionClass;

class BaseController extends Controller
{
	public $b_counter;
	public $currentCity;
	public $cities;
	public $categories;
	public $currentCategory;
	public $adminVars;
	public $filVars;
	public $billingVars;

	public function __construct(Request $request)
	{
		$cities = City::activeOrder()->get();
		$currentCity = $cities->where('slug', StaticRequest::segment(1))->first();
		if (!$currentCity)
			$currentCity = 'Бишкек';
		else
			$currentCity = $currentCity->title;
		$categories = Category::activeOrder()->with('subcategories')->get();
		$category = StaticRequest::segment(2) ? explode('t-', StaticRequest::segment(2)) : null;
		$currentCategory = $categories->where('slug', count($category) == 1 ? array_get($category, 0) : array_get($category, 1))->first();
		if (!$currentCategory)
			$currentCategory = 'Места и услуги';
		else
			$currentCategory = $currentCategory->title;

		$this->b_counter = 0;
		$page = Meta::where('slug', $request->path())->first();
		$adminVars = Vars::getAdminVars();
		$filVars = Vars::specialFilterVarsByIds();
		$billingVars = Vars::getBillingVars();

		$this->currentCity = $currentCity;
		$this->cities = $cities;
		$this->currentCategory = $currentCategory;
		$this->categories = $categories;
		$this->adminVars = $adminVars;
		$this->filVars = $filVars;
		$this->billingVars = $billingVars;

		view()->share([
			'b_counter' => 0,
			'cities' => $cities,
			'currentCity' => $currentCity,
			'categories' => $categories,
			'currentCategory' => $currentCategory,
			'adminVars' => $adminVars,
			'filVars' => $filVars,
			'billingVars' => $billingVars,
			'metatitle' => $page ? $page->metatitle : null,
			'metadesc' => $page ? $page->metadesc : null,
			'metakeyw' => $page ? $page->metakeyw : null,
		]);

	}

	public function fitImage(Request $request)
	{
		$keys = array_keys($request->all());
		$size = explode('_', array_get($keys, 0));
		$path = array_get($keys, 1);
		$fullPath = strpos($path, '/') === false ? config('admin.imagesUploadDirectory') . '/' . str_replace('_', '.', $path) : str_replace('_', '.', $path);
		$cachedImage = Image::cache(function($image) use($size, $fullPath) {
			$image->make($fullPath)->fit(array_get($size, 0), array_get($size, 1));
		}, 43200);

		return Response::make($cachedImage, 200, array('Content-Type' => 'image/jpeg'));
	}

	public function resizeImage(Request $request)
	{
		$keys = array_keys($request->all());
		$size = explode('_', array_get($keys, 0));
		$path = array_get($keys, 1);
		$fullPath = strpos($path, '/') === false ? config('admin.imagesUploadDirectory') . '/' . str_replace('_', '.', $path) : str_replace('_', '.', $path);
		$cachedImage = Image::cache(function($image) use($size, $fullPath) {
			$image->make($fullPath)->resize(array_get($size, 0), array_get($size, 1), function ($constraint) {
				$constraint->aspectRatio();
			});
		}, 43200);

		return Response::make($cachedImage, 200, array('Content-Type' => 'image/jpeg'));
	}

	public function getAjaxResponseView($data)
	{
		$status = array_get($data, 'status');
		$arr = [
			'status'  => $this->ajaxResponse(array_get($status, 0), array_get($status, 1), array_get($status, 2)),
			'views' => array_get($data, 'views'),
			'params' => array_get($data, 'params'),
		];
		if(count(array_get($data, 'vars')))
			foreach(array_get($data, 'vars') as $key => $var)
				$arr = array_add($arr, $key, $var);

		return view('partials.mini._getAjaxResponseBlock', $arr);
	}

	public function ajaxResponse($status, $msg, $params = [])
	{
		$arr = ['success' => $status, 'text' => $msg];
		if(count($params))
			foreach($params as $k => $v)
				$arr = array_add($arr, $k, $v);

		return $arr;
	}

	public function accessProtected($obj, $prop) {
		$reflection = new ReflectionClass($obj);
		$property = $reflection->getProperty($prop);
		$property->setAccessible(true);
		return $property->getValue($obj);
	}

	public function generateCode($length = 6)
	{
		$number = '';

		do {
			for ($i = $length; $i--; $i > 0) {
				$number = random_int(100000, 999999);
			}
		} while (!empty(User::where('personal_bill', $number)->first(['id'])));

		return $number;
	}

//--------------------------------------------------------------------------------------------------------------

	public function recommended($type, $city, $category)
	{
		$categoriesRecommends = Recommend::where(['type' => $type, 'city_id' => $city->id])->where('category_id', '!=', $category->id)->where('amount_by_category', '>', 0)
			->groupBy('category_id')->orderBy('amount_by_category', 'desc')->take(4)->get();
		$recommendedCategories = $this->addAttribute($categoriesRecommends, 'category')->sortByDesc('amount');

		$filterValuesRecommends = Recommend::where(['type' => $type, 'city_id' => $city->id, 'category_id' => $category->id])->where('amount_by_filter_value', '>', 0)
			->orderBy('amount_by_filter_value', 'desc')->take(4)->get();
		$recommendedFilterValues = $this->addAttribute($filterValuesRecommends, 'filter_value')->sortByDesc('amount');

		return [$recommendedCategories, $recommendedFilterValues];
	}

	public function getReviewsAndRatings($item)
	{
		$amountOfRates = $item->ratings->count();
		$rating = $item->ratings->filterArrFix(['user_id' => Auth::id()])->first() ? $item->ratings->filterArrFix(['user_id' => Auth::id()])->first()->value : 0;
		$avgRating = $amountOfRates > 0 ? $item->ratings->sum('value')/$amountOfRates : 0;
		$parentReviews = $item->reviews()->moderated()->where('parent_id', null)->orderBy('created_at', 'desc')->paginate(env('CONFIG_PAGINATE', 1));
		$reviews = $item->reviews()->with(['user', 'parent'])->moderated()->whereIn('ancestor_id', $parentReviews->lists('id'))->get()->toHierarchy()->sortByDesc('created_at');

		return [$amountOfRates, $rating, $avgRating, $reviews, $parentReviews->total()];
	}

	public function addAttribute($itemsRecommends, $type) {
		$model = BaseModel::makeModel($type);
		$itemsArr = $type == 'category' ? $itemsRecommends->lists('amount_by_category', 'category_id')->all() :
			$itemsRecommends->lists('amount_by_filter_value', 'filter_value_id')->all();

		$recommendedItems = $model->whereIn('id', array_keys($itemsArr))->get();
		foreach($recommendedItems as $k => $recommendedItem) {
			$recommendedItem->setAttribute('amount', $itemsArr[$recommendedItem->id]);
			$recommendedItem->setAttribute('image', $itemsRecommends->get($k)->image_filter_value);
		}

		return $recommendedItems;
	}

	public function selectFilterAndSeo($parameter, $filterType, $category, $city, $type) {
		$model = BaseModel::makeModel($type);
		$col = new CustomCollection();
		if ($filterType == 'district') {
			$searched = Region::findBySlugOrFail($parameter);
			$selected_filter_id = $searched->id;
			$seo_page = SeoRegionCategory::where('region_id', $selected_filter_id)->where('category_id', $category->id)->first();
			$items_count = $model->moderated()->forRegion($selected_filter_id)->forCategory($category->id);
		}
		elseif ($filterType == 'street') {
			$searched = Street::findBySlugOrFail($parameter);
			$selected_filter_id = $searched->id;
			$seo_page = SeoStreetCategory::where('street_id', $selected_filter_id)->where('category_id', $category->id)->first();
			$items_count = $model->moderated()->forStreet($selected_filter_id)->forCategory($category->id);
		}
		elseif ($filterType == 'type') {
			if(!$category->filters->filterFix('options.slug', '=', $parameter)->count())
				throw new NotFoundHttpException;
			$searched = FilterValue::findBySlugOrFail($parameter);
			$selected_filter_id = $searched->id;
			$seo_page = SeoCityFilterValue::where('city_id', $city->id)->where('filter_value_id', $selected_filter_id)->first();
			$items_count = $model->moderated()->forCity($city->id)->forFilterValue($selected_filter_id)->forCategory($category->id);
		}
		else {
			$selected_filter_id = null;
			$seo_page = SeoCityCategory::where('city_id', $city->id)->where('category_id', $category->id)->first();
			$items_count = $model->moderated()->forCity($city->id)->forCategory($category->id);
		}

		if($type == 'trener')
			$items_count = $items_count->forAttachedTrener()->count();
		else
			$items_count = $items_count->count();

		return $col->push(['selected_filter_id' => $selected_filter_id, 'seo_page' => $seo_page, 'items_count' => $items_count]);
	}
}
