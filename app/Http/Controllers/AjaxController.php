<?php

namespace App\Http\Controllers;

use App\Custom\CustomCollection;
use App\Http\Requests;
use App\Models\BaseModel;
use App\Models\Callback;
use App\Models\City;
use App\Models\Club;
use App\Models\PriceList;
use App\Models\PriceListCategory;
use App\Models\Review;
use App\Models\Stock;
use App\Models\Trener;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Input;
use Response;
use Validator;

class AjaxController extends PushItemsController
{
	public function uploadFile()
	{
		$validator = Validator::make(Input::all(), static::uploadFileValidationRules());
		if ($validator->fails())
		{
			return Response::make($validator->errors()->get('file'), 400);
		}
		$file = Input::file('file');
		$filename = md5(time() . $file->getClientOriginalName()) . '.' . $file->getClientOriginalExtension();
		$path = config('admin.imagesUploadDirectory');
		$fullpath = public_path($path);
		$file->move($fullpath, $filename);
		$value = $filename;
		return [
			'url'   => asset(($path . '/' . $value)),
			'value' => $value,
		];
	}

	protected static function uploadFileValidationRules()
	{
		return [
			'file' => 'mimes:jpg,jpeg,png,doc,docx,xls,xlsx,pdf|max:2000',
		];
	}

	public function uploadImage()
	{
		$validator = Validator::make(Input::all(), static::uploadImageValidationRules());
		if ($validator->fails())
		{
			return Response::make($validator->errors()->get('file'), 400);
		}
		$file = Input::file('file');
		$filename = md5(time() . $file->getClientOriginalName()) . '.' . $file->getClientOriginalExtension();
		$path = config('admin.imagesUploadDirectory');
		$fullpath = public_path($path);
		$file->move($fullpath, $filename);
		$value = $filename;
		return [
			'url'   => asset(($path . '/' . $value)),
			'value' => $value,
		];
	}

	protected static function uploadImageValidationRules()
	{
		return [
			'file' => 'mimes:jpg,jpeg,png,gif|max:2000',
		];
	}

	public function showMore(Request $request)
	{
		$type = $request->get('type');
		$page = $request->get('page');
		$sort = $request->get('sort') ? $request->get('sort') : 'id';
		$model = BaseModel::makeModel( $type === 'clubs.specialists' ? 'trener' : $type, true );

		if ($type != 'clubs.reviews' && $type != 'treners.reviews') {
			$filteredItemsIds = $this->getFilteredItemsIds( $request, $type );
			$placeholders     = implode( ',', $filteredItemsIds->all() );
			$items            = $model->whereIn( 'id', $filteredItemsIds )->with( 'street', 'region', 'photos', 'avgRatings' )
			                          ->orderByRaw( "field(id,{$placeholders})" )->paginate( env( 'CONFIG_PAGINATE', 1 ), [ '*' ], 'page', $page );
		} elseif ($type == 'clubs.reviews' || $type == 'treners.reviews') {
			$item = $model->whereId($request->get('item_id'))->first();
			$parentReviews = $model->whereId($request->get('item_id'))->first()->reviews()->moderated()->where('parent_id', null)->orderBy($sort, 'desc');
			$parentReviews = $parentReviews->orderBy('created_at', 'desc')->paginate( env( 'CONFIG_PAGINATE', 1 ), [ '*' ], 'page', $page );
			$items = $item->reviews()->with(['user', 'parent'])->moderated()->whereIn('ancestor_id', $parentReviews->lists('id'))->get()->toHierarchy()->sortByDesc($sort);
		}

		$dashedType = str_replace(["."], '_', $type);
		return $this->getAjaxResponseView([
			'status' => [1, null],
			'views' => ["partials.mini._showMoreBlock-id_{$dashedType}",
				$request->get('changedSort') ? "partials.mini._unitsListFiltered-id_{$dashedType}" : ['append' => "partials.mini._unitsListFiltered-id_{$dashedType}"]],
			'params' => ['setAttr' => [".b-show_more[data-type=\"{$type}\"]" => ['data-sort', $sort]]],
			'vars' => ['items' => $items, 'type' => $type, 'page' => $page, 'amountOfReviews' => isset($parentReviews) ? $parentReviews->total() : 0,
			           'club' => isset($item) ? $item : null, 'trener' => isset($item) ? $item : null, 'reviews' => $items]
		]);
	}

	public function getPriceList(Request $request)
	{
		$cid = $request->get('cid');
		$type = $request->get('type');
		$item_id = $request->get('item_id');
		if ($cid == "all") {
			$type == 'club' ? $item = Club::active()->findOrFail($item_id) : $item = Trener::active()->findOrFail($item_id);
			$priceLists = $item->priceLists;
		} else {
			$category = PriceListCategory::findOrFail($cid);
			$priceLists = $category->priceLists;
		}

		return view('clubs.priceList.index', compact('type', 'item_id', 'priceLists'));
	}

	public function getRegionAndCityLists(Request $request)
	{
		$city = $request->get('city_id') ? City::findOrFail($request->get('city_id')) : null;
		$regionsAndStreets = [];

		$regionsAndStreets = array_add($regionsAndStreets, 'regions', $city ? $city->regions->sortBy('title')->lists('id', 'title')->all() : null);
		$regionsAndStreets = array_add($regionsAndStreets, 'streets', $city ? $city->streets->sortBy('title')->lists('id', 'title')->all() : null);

		return $regionsAndStreets;
	}

	public function getModalContent(Request $request)
	{
		$action = $request->get('action');
		$type = $request->get('type');
		$item_id = $request->get('item_id');
		$forType = $request->get('forItem');
		$forItem_id = $request->get('forItem_id');
		$parameters = $request->get('parameters');

		if($item_id)
			$item = BaseModel::makeModel($type)->findOrFail($item_id);
		if($forItem_id)
			$forItem = BaseModel::makeModel($forType)->findOrFail($forItem_id);

		return view('partials.mini._modalContentBlock', compact('action', 'type', 'forType', 'item', 'forItem', 'parameters', 'modal_class'));
	}
}
