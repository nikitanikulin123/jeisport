<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Club;
use App\Models\PriceListCategory;
use App\Models\Stock;
use Illuminate\Http\Request;
use Session;

class PriceListCategoriesController extends BaseController
{
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param StoreTrenerRequest $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'title' => 'required|max:255',
			'file' => 'max:255',
		]);

		$club = Club::findOrFail($request->get('club_id'));

		$priceListCategory = PriceListCategory::create($request->all());
		$club->priceListCategories()->save($priceListCategory);

		return $this->ajaxResponse(1, 'Информация о прайслисте успешно сохранена!');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param StoreTrenerRequest $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'title' => 'required|max:255',
			'file' => 'max:255',
		]);

		$club = Club::findOrFail($request->get('club_id'));
		$priceListCategory = PriceListCategory::findOrFail($id);

		$priceListCategory->update($request->all());

		return $this->ajaxResponse(1, 'Информация о прайслисте успешно сохранена!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$priceListCategory = PriceListCategory::findOrFail($id);
		$priceListCategory->delete();

		return redirect()->back()->with('success', 'Прайслист удалён!');
	}
}
