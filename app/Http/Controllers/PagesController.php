<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Category;
use App\Models\Club;
use App\Models\Contact;
use App\Models\Feed;
use App\Models\Filter;
use App\Models\Page;
use App\Models\Trener;
use App\Models\TrenerCategory;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PagesController extends BaseController
{
    public function getHome(Request $request)
    {
        $feeds = Feed::activeOrder()->with('city')->get();
        return view('home', compact('feeds'));
    }

    public function getContacts()
    {
	    $contacts = Contact::first();
        return view('pages.contacts', compact('contacts'));
    }

    public function getAbout()
    {
    	$page = Page::where('type', 'about')->first();
        return view('pages.about', compact('page'));
    }

	public function getSearched(Request $request)
	{
		$query = $request->get('query');
		$clubs = Club::search($query)->moderated()->get();
		$treners = Trener::search($query)->moderated()->get();

		return view('pages.searched', compact('clubs', 'treners', 'feeds', 'query'));
	}

    public function getFeedShow(Request $request, $city, $slug)
    {
	    $city = $this->cities->where('slug', $city)->first();
        $feed = Feed::activeOrder()->whereCityId($city->id)->whereSlug($slug)->with('club_feeds.club')->first();
	    $club_feeds = $feed->club_feeds->whereLoose('active', true);
	    $similars = Feed::where('club_id', $feed->club_id)->orderByRaw("RAND()")->with('city')->get()->filter(function ($item) use($feed) {return $item['id'] != $feed->id;})->take(4);

        return view('feeds.show', compact('feed', 'club_feeds', 'similars'));
    }
}