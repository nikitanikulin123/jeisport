<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\AvgRating;
use App\Models\BaseModel;
use App\Models\Club;
use App\Models\Favourite;
use App\Models\Rating;
use App\Models\Review;
use App\Models\Trener;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReviewsController extends BaseController
{
    public function store(Request $request, $id)
    {
	    $this->validate($request, [
		    'text' => 'required'
	    ]);

	    $item = BaseModel::makeModel($request->get('type'))->findOrFail($id);
	    $request->merge([
		    'user_id' => Auth::id(),
	    ]);
	    if ($item->user->id == Auth::id())
		    $request->merge([
			    'viewed_at' => Carbon::now(),
		    ]);

	    $review = Review::create($request->all());
	    $review->ancestor_id = $request->get('parent_id') ? Review::findOrFail($request->get('parent_id'))->getRoot()->id : $review->id;
	    $item->reviews()->save($review);

	    return $this->getAjaxResponseView([
		    'status'  => [1, 'Ваш отзыв успешно добавлен и ожидает проверки администрацией!'],
	    ]);
    }

	public function likeUp(Request $request)
	{
		$review = Review::findOrFail($request->get('item_id'));
		$like = $review->likes->where('user_id', Auth::id())->where('increased', 1)->first();
		$dislike = $review->likes->where('user_id', Auth::id())->where('increased', 0)->first();

		if ($dislike) {
			$review->decrement('rating_minus');
			$dislike->delete();
		}
		if (!$like) {
			$review->increment('rating_plus');
			$review->likes()->create(['likeable_id' => $review->id, 'user_id' => Auth::id(), 'increased' => true]);
		}

		return $this->getAjaxResponseView([
			'status'  => [1, ''],
			'views' => ["partials.mini._reviewLikesBlock-id_{$review->id}"],
			'vars' => ['node' => $review]
		]);
	}

	public function likeDown(Request $request)
	{
		$review = Review::findOrFail($request->get('item_id'));
		$like = $review->likes->where('user_id', Auth::id())->where('increased', 1)->first();
		$dislike = $review->likes->where('user_id', Auth::id())->where('increased', 0)->first();

		if ($like) {
			$review->decrement('rating_plus');
			$like->delete();
		}
		if (!$dislike) {
			$review->increment('rating_minus');
			$review->likes()->create(['likeable_id' => $review->id, 'user_id' => Auth::id(), 'increased' => false]);
		}

		return $this->getAjaxResponseView([
			'status'  => [1, ''],
			'views' => ["partials.mini._reviewLikesBlock-id_{$review->id}"],
			'vars' => ['node' => $review]
		]);
	}

	public function rated(Request $request)
	{
		if (!Auth::check())
			return $this->ajaxResponse(0, 'Вы не авторизованы. Пожалуйста, авторизуйтесь!');

		$type = $request->get('type');
		$item = BaseModel::makeModel($type)->findOrFail($request->get('item_id'));

		$item->ratings()->where('user_id', Auth::id())->delete();
		$rating = Rating::create(['user_id' => Auth::id(), 'value' => $request->get('rating')]);
		$item->ratings()->save($rating);

		$item = BaseModel::makeModel($type)->findOrFail($request->get('item_id'));

		$amountOfRates = $item->ratings->count();
		$ratingValue = $item->ratings->sum('value')/$amountOfRates;
		$avgRating = AvgRating::create(['user_id' => Auth::id(), 'average_value' => $ratingValue]);
		$item->avgRatings()->delete();
		$item->avgRatings()->save($avgRating);

		list($amountOfRates, $rating, $avgRating, $reviews, $amountOfReviews) = $this->getReviewsAndRatings($item);

		return $this->getAjaxResponseView([
			'status'  => [1, ''],
			'views' => ["partials.mini._ratingsBlock"],
			'vars' => ['item' => $item, 'type' => $type, 'amountOfRates' => $amountOfRates, 'rating' => $rating, 'avgRating' => $avgRating,
			           'ratingValue' => $ratingValue, 'amountOfReviews' => $amountOfReviews]
		]);
	}

	public function toFavourites(Request $request)
	{
		if (!Auth::check())
			return $this->ajaxResponse(0, 'Вы не авторизованы. Пожалуйста, авторизуйтесь!');

		$type = $request->get('type');
		$item = BaseModel::makeModel($type)->findOrFail($request->get('item_id'));

		if ($item->favourites->where('user_id', Auth::id())->count() == 0) {
			$favourite = Favourite::create(['user_id' => Auth::id()]);
			$item->favourites()->save($favourite);
			return $this->getAjaxResponseView([
				'status'  => [1, 'Заведение добавлено в "Избранное"'],
				'params' => ['toggleClass' => ['.heart' => 'active']],
			]);
		}
		elseif ($item->favourites->where('user_id', Auth::id())->count() > 0) {
			$item->favourites->where('user_id', Auth::id())->first()->delete();
			return $this->getAjaxResponseView([
				'status'  => [1, 'Заведение удалено из "Избранного"'],
				'params' => ['toggleClass' => ['.heart' => 'active']],
			]);
		}
	}
}
