<?php

namespace App\Http\Controllers;

use App\Custom\CustomCollection;
use App\Http\Requests;
use App\Models\AvgRating;
use App\Models\BaseModel;
use App\Models\Category;
use App\Models\City;
use App\Models\Favourite;
use App\Models\Filter;
use App\Models\Photo;
use App\Models\Rating;
use App\Models\Region;
use App\Models\Street;
use App\Models\Trener;
use App\Models\Vars;
use App\Models\Visit;
use App\Models\Widget;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Models\SeoCityCategory;
use Illuminate\Http\Request;
use App\Http\Requests\StoreTrenerRequest;
use App\Events\SendNotificationEvent;
use Event;
use Session;
use Request as UrlRequest;

class TrenersController extends BaseController
{
	public function index(Request $request, $city = null, $category = null, $parameter = null)
	{
		$filVars = Vars::specialFilterVarsByIds();
		$cities = $this->cities;
		$categories = $this->categories;
		$city = $cities->filterArrFix(['slug' => $city])->first();
		$category = $categories->filterArrFix(['slug' => $category])->first();
		if (!$city)
			throw new NotFoundHttpException;
		if (!$category || stripos($category->slug, 'issyk-kul') && $city->slug != 'issyk-kul')
			throw new NotFoundHttpException;
		$treners = Trener::moderated()->forAttachedTrener()->forCategory($category->id)->forCity($city->id)->with('street', 'region', 'photos', 'avgRatings')->paginate(env('CONFIG_PAGINATE', 1));
		$filters = $category->filters;
		$sortFilters = Filter::all()->filterArrFix('id', '=', [$filVars['district'], $filVars['street']]);
		list($recommendedCategories, $recommendedFilterValues) = $this->recommended('trener', $city, $category);

		$filterType = UrlRequest::segment(3) ? UrlRequest::segment(3) : null;
		$selectFilterAndSeoArr = $this->selectFilterAndSeo($parameter, $filterType, $category, $city, 'trener')->collapse();
		$selected_filter_id = $selectFilterAndSeoArr->get('selected_filter_id');
		$seo_page = $selectFilterAndSeoArr->get('seo_page');
		$treners_count = $selectFilterAndSeoArr->get('items_count');
		if(!$parameter)
			$selected_filter_id = $request->get('filter_id');
		if ($seo_page) {
			$seoPageTitle = $seo_page->page_title;
			$metatitle = $seo_page->metatitle;
			$metakeyw = $seo_page->metakeyw;
			$metadesc = $seo_page->metadesc;
		} else
			$seoPageTitle = 'Заголовок страницы по умолчанию';

		return view('treners.index', compact('treners', 'seoPageTitle', 'filters', 'sortFilters', 'city', 'treners_count',
			'category', 'seo_page', 'metatitle', 'metakeyw', 'metadesc', 'filterType', 'selected_filter_id', 'sortFilters',
			'recommendedCategories', 'recommendedFilterValues'
		));
	}

	/**
	 * Show the form for creating a new vacancy.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(Request $request)
	{
		$regions = [];
		$streets = [];

		return view('treners.createEdit', compact('categories', 'cities', 'regions', 'streets'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param StoreTrenerRequest $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreTrenerRequest $request)
	{
		if($request->get('step') == 1) {
			$request->merge([
				'user_id' => Auth::id(),
				'email' => Auth::user()->email,
				'active' => true,
			]);
			$trener = Trener::create($request->all());

			return redirect()->route('treners.edit', [$trener->slug, 'step' => $request->get('step') + 1])->with('success', 'Информация успешно сохранена. Пожалуйста, заполните следующие данные');
		}
	}

	public function newTrenerStore(StoreTrenerRequest $request)
	{
		$user = User::where('email', $request->get('email'))->first();

		$request->merge([
			'user_id' => $user ? $user->id : 0,
			'image' => 'nophoto.jpg',
			'address' => 'Не указан',
		]);
		$trener = Trener::create($request->all());

		$request->merge([
			'type' => 'newTrener',
			'recipientEmail' => $trener->email,
			'bccEmail' => $this->adminVars->adminEmail,
			'data' => $trener,
		]);

		Event::fire(new SendNotificationEvent($request->all()));

		return $this->ajaxResponse(1, 'Информация о специалисте успешно сохранёна и отправлена на модерацию!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function show($city, $category_slug , $slug)
    {
	    $categories = $this->categories;
        $city = City::whereSlug($city)->first();
        $category = Category::whereSlug($category_slug)->first();
	    $trener = Trener::findBySlugOrFail($slug);
	    if (!$trener || (!$trener->moderated && $trener->user_id != Auth::id()))
		    throw new NotFoundHttpException;
	    if (!$city || $trener->city->id != $city->id)
		    throw new NotFoundHttpException;
	    if (!$category || $trener->category->id != $category->id)
		    throw new NotFoundHttpException;
	    if (!$trener || (!$trener->moderated && $trener->user_id != Auth::id()))
		    throw new NotFoundHttpException;
	    if(!$trener->premium)
		    list($recommendedCategories, $recommendedFilterValues) = $this->recommended('trener', $city, $category);
	    $filters = $category->filters;
	    $trener_filter_values = $trener->filterValues;
	    list($amountOfRates, $rating, $avgRating, $reviews, $amountOfReviews) = $this->getReviewsAndRatings($trener);
	    $stocks = $trener->stocks;
	    $addedToFavourites = $trener->favourites->where('user_id', Auth::id())->first();

	    $allVisits = Visit::where('visitable_id', $trener->id)->where('visitable_type', Trener::class)->orderBy('visited_at', 'asc')->get();
	    $expiredVisits = $allVisits->filter(function ($item) {return $item['visited_at'] <= Carbon::now()->subDays(30)->toDateString();});
	    if ($expiredVisits->count() > 0) {
		    foreach ($expiredVisits as $expiredVisit)
			    $expiredVisit->delete();
	    }
	    if ($visit = $allVisits->where('visited_at', Carbon::now()->toDateString())->first()) {
		    $visit->increment('visits');
	    } else {
		    $visit = Visit::create(['visits' => 1, 'visited_at' => Carbon::now()]);
		    $trener->visits()->save($visit);
	    }

	    $seoPageTitle = 'Заголовок страницы по умолчанию';
	    $workInClub = $trener->club->first();

	    return view('treners.show', compact('trener', 'filters',  'amountOfRates', 'rating', 'avgRating', 'filters', 'city', 'category',
		    'trener_filter_values', 'reviews', 'amountOfReviews', 'stocks', 'recommendedCategories', 'recommendedFilterValues', 'seoPageTitle',
		    'workInClub', 'addedToFavourites'));
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Request $request, $slug)
	{
		$trener = Trener::findBySlugOrFail($slug);
		$regions = Region::where('city_id', $trener->city_id)->get()->sortBy('title');
		$streets = Street::where('city_id', $trener->city_id)->get()->sortBy('title');
		$filters = Filter::where('type', 'trener')->with('categories')->has('options')->whereHas('categories', function ($query) use ($trener) {
			return $query->where('id', $trener->category->id);
		})->get();
		$step = $request->get('step');

		return view('treners.createEdit', compact('trener', 'categories', 'cities', 'regions', 'streets', 'filters', 'step'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param StoreTrenerRequest $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(StoreTrenerRequest $request, $id)
	{
		$request->merge([
			'moderated' => false,
		]);
		if(Auth::user()->hasRoleFix('boss'))
			$request->merge([
				'premium' => true,
			]);
		$trener = Trener::findOrFail($id);
		$trener->update($request->all());

		if($request->get('step') == 2) {
			if(count($request->get('filterValues')) < 2)
				return redirect()->back()->with('error', 'Необходимо выбрать хотя бы две подкатегории!');

			$filterValues = [];
			foreach ($request->get('filterValues') as $key => $value) {
				if($value) {
					$filterValues[] = $key;
				}
			}
			$trener->filterValues()->sync($filterValues);
		}

		if($request->get('step') == 'complete') {
			$trener->photos()->delete();
			if($request->get('images') != '') {
				$imgs = [];
				$images = explode(',', $request->get('images'));
				foreach ($images as $img) {
					$imgs[] = Photo::create(['imageable_id' => $trener->id, 'path' => $img]);
				}
				$trener->photos()->saveMany($imgs);
			}
			if($request->get('portfolio') != '') {
				$imgs = [];
				$images = explode(',', $request->get('portfolio'));
				foreach ($images as $img) {
					$imgs[] = Photo::create(['imageable_id' => $trener->id, 'path' => $img, 'type' => 'portfolio']);
				}
				$trener->photos()->saveMany($imgs);
			}

			$request->merge([
				'type' => 'trenerForModeration',
				'recipientEmail' => $this->adminVars->adminEmail,
				'data' => $trener,
			]);
			Event::fire(new SendNotificationEvent($request->all()));

			return redirect()->route('profiles.index')->with('success', 'Информация о специалисте успешно сохранёна и отправлена на модерацию!');
		}
		$nextStep = Auth::user()->hasRoleFix('boss') && $request->get('step') + 1 == 3 ? $request->get('step') + 2 : $request->get('step') + 1;

		return redirect()->route('treners.edit', [$trener->slug, 'step' => $nextStep])->with('success', 'Информация о специалисте обновлена!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$trener = Trener::findOrFail($id);
		$trener->delete();

		return redirect()->back()->with('success', 'Специалист удалён!');
	}
}
