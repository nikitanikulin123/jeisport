<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Club;
use App\Models\Trener;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function unitsUpdate(Request $request)
    {
        $value = array_get($request->get('value'), 0);
	    if($request->get('type') == 'club')
            $unit = Club::findOrFail($request->get('pk'));
	    else
            $unit = Trener::findOrFail($request->get('pk'));
        if ($value == '1')
	        $unit->active = true;
        else
	        $unit->active = false;
	    $unit->save();
    }
}
