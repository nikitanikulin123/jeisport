<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Category;
use App\Models\Club;
use App\Models\Order;
use App\Models\Trener;
use Illuminate\Http\Request;

class PopupsController extends Controller
{

    public function getAttributes(Request $request, $id)
    {
	    if($request->get('type') == 'club') {
		    $item = Club::findOrFail($id);
	        $filter_values = $item->filterValues;}
	    elseif($request->get('type') == 'trener') {
		    $item = Trener::findOrFail($id);
		    $filter_values = $item->filterValues;}
        elseif($request->get('type') == 'categories') {
	        $item = Category::findOrFail($id);
	        $filter_values = $item->filterValues;}

	    return view('admin.attributes', compact('item', 'filter_values', 'request'));
    }

    public function postAttributes(Request $request, $id)
    {
	    if($request->get('type') == 'club')
		    $item = Club::findOrFail($id);
	    elseif($request->get('type') == 'trener')
		    $item = Trener::findOrFail($id);
	    elseif($request->get('type') == 'categories')
	        $item = Category::findOrFail($id);

	    if (!$attrs = $request->get('attrs')){
	        $item->filterValues()->detach();
            return ['status' => 0, 'msg' => 'No values specified'];
        }
	    $item->filterValues()->sync($request->get('attrs'));
        return ['status' => 1, 'msg' => 'Success!'];
    }
}
