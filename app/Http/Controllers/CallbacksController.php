<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\BaseModel;
use App\Models\Callback;
use App\Models\Club;
use App\Models\Contact;
use App\Models\Vars;
use Illuminate\Http\Request;
use App\Events\SendNotificationEvent;
use Event;
use Auth;

class CallbacksController extends BaseController
{
	public function create(Request $request, $id)
	{
		$this->validate($request, [
			'name' => 'required|max:255',
			'callback_phone' => 'required|max:255',
		], [
			'name.required' => 'Укажите имя!',
			'callback_phone.required' => 'Укажите телефон!',
		]);
		$callback = Callback::create(['name' => $request->get('name'), 'phone' => $request->get('callback_phone'),
			'description' => $request->get('description') ? $request->get('description') : '']);

		if($request->get('type') == 'contacts')
			$callback->update(['callbackable_type' => Contact::class]);
		else {
			$item = BaseModel::makeModel($request->get('type'))->findOrFail($id);
			$item->callbacks()->save($callback);

			$request->merge([
				'type' => 'callback',
				'recipientEmail' => $request->get('type') === 'category' ? $this->adminVars->adminEmail : ($item->n_email ? $item->n_email : $item->email),
				'obj' => $item,
				'data' => $callback,
			]);
			if($item->email_to_admin)
				$request->merge([
					'bccEmail' => $this->adminVars->adminEmail,
				]);

			Event::fire(new SendNotificationEvent($request->all()));
		}

		return $this->ajaxResponse(1, 'Звонок заказан!');
	}

	public function checked(Request $request, $id)
	{
		$callback = Callback::findOrFail($id);
		$request->merge([
			'checked' => true,
		]);
		$callback->update($request->all());

		return redirect()->back()->with('success', 'Заявка на обратный звонок отмечена как выполненная!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$callback = Callback::findOrFail($id);
		$callback->delete();

		return redirect()->back()->with('success', 'Заявка на обратный звонок удалена!');
	}
}
