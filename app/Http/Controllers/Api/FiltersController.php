<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Models\Category;
use Illuminate\Http\Request;

class FiltersController extends ApiController
{
    public function index(Request $request)
    {
        $cid = $request->get('category_id');
        if (!$cid)
            return $this->apiResponse(0, 'Category not specified');
        $category = Category::whereActive(true)->whereId($cid)->first();
        if (!$category)
            return $this->apiResponse(0, 'Category not found');
        $count = $category->filters->count();
        return $this->apiResponse(1, "List of $count categories", $this->getCategoryAttributes($category->filters));
    }

    private function getCategoryAttributes($entries)
    {
        $arr = [];
        foreach ($entries as $entry) {
            $arr[] = [
                'id' => $entry->id,
                'title' => $entry->title,
                'slug' => $entry->slug,
                'options' => $this->getOptionAttributes($entry->options)
            ];
        }
        return $arr;
    }

    private function getOptionAttributes($entries)
    {
        $arr = [];
        foreach ($entries as $entry) {
            $arr[] = [
                'id' => $entry->id,
                'title' => $entry->title,
                'slug' => $entry->slug
            ];
        }
        return $arr;
    }
}
