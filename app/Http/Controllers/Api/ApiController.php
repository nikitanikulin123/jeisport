<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use HtmlDomParser;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function twoGisParse(Request $request)
    {
        $parser = new HtmlDomParser();
        // get html dom from file
        $html = $parser->fileGetHtml($request->get('link'));
        $items = $html->find('noscript', 0)->parent()->parent()->next_sibling();
        preg_match_all('/https?:\/\/[^ ]+?(?:\.jpg|\.png|\.gif)/', $items->innertext, $matches);
        $json = $matches;
//        foreach ($items as $item) {
//            $string = $item->style;
//
//            $pattern = '/background-image:\s*url\(\s*([\'"]*)(?P<file>[^\1]+)\1\s*\)/i';
//            $matches = array();
//            $elements[] = $item->style;
////            if (preg_match($pattern, $string, $matches)) {
////                $elements[] = $matches['file'];
////            }
//        }
//        dd($elements);
//        $finished_items = [];
//        foreach ($elements as $k => $element) {
//            $finished_items[] = str_replace('_304x', '', $element);
//        }
        return view('2gis.parsed', compact('json'));
    }

    public function twoGisForm()
    {
        return view('2gis/form');
    }

    protected function apiResponse($status, $msg, $data = [])
    {
        return ['success' => $status, 'message' => $msg, 'data' => $data];
    }
}
