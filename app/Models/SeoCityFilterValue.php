<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeoCityFilterValue extends Model
{
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function filterValue()
    {
        return $this->belongsTo(FilterValue::class);
    }
}
