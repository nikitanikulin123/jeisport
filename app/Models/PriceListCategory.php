<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PriceListCategory extends BaseModel
{
	protected $with = ['priceLists'];

    protected $fillable = [
        'title', 'min', 'max', 'file'
    ];

    public function priceLists()
    {
        return $this->hasMany(PriceList::class);
    }

	public function listable()
	{
		return $this->morphTo();
	}
}
