<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use SleepingOwl\Admin\Traits\OrderableModel;

class TrenerCategory extends BaseModel
{
    use Sluggable, SluggableScopeHelpers, OrderableModel;

    protected $fillable = [
        'title',
        'slug',
        'active'
    ];

	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'title'
			]
		];
	}
}
