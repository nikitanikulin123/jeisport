<?php namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class AvgRating extends BaseModel
{

    protected $fillable = [
	    'user_id', 'average_value'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
