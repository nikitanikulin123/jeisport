<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeoStreetCategory extends Model
{
	protected $fillable = [
		'street_id', 'category_id', 'page_title', 'page_subtitle', 'page_description',
		'metatitle', 'metakeyw', 'metadesc'
	];

	public function street()
	{
		return $this->belongsTo(Street::class);
	}

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
