<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use SleepingOwl\Admin\Traits\OrderableModel;

use Cviebrock\EloquentSluggable\Sluggable;

class Feed extends BaseModel
{
    use Sluggable, SluggableScopeHelpers, OrderableModel;

	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'title'
			]
		];
	}

	public function setAdminCaseImage1Attribute($value)
	{
		return $this->attributes['case_image1'] = str_replace(config('admin.imagesUploadDirectory') . '/', '', $value);
	}

	public function getAdminCaseImage1Attribute()
	{
		if ($this->exists && $this->attributes['case_image1'])
			return config('admin.imagesUploadDirectory') . '/' . $this->attributes['case_image1'];
		return null;
	}

	public function setAdminCaseImage2Attribute($value)
	{
		return $this->attributes['case_image2'] = str_replace(config('admin.imagesUploadDirectory') . '/', '', $value);
	}

	public function getAdminCaseImage2Attribute()
	{
		if ($this->exists && $this->attributes['case_image2'])
			return config('admin.imagesUploadDirectory') . '/' . $this->attributes['case_image2'];
		return null;
	}

	public function setAdminCaseImage3Attribute($value)
	{
		return $this->attributes['case_image3'] = str_replace(config('admin.imagesUploadDirectory') . '/', '', $value);
	}

	public function getAdminCaseImage3Attribute()
	{
		if ($this->exists && $this->attributes['case_image3'])
			return config('admin.imagesUploadDirectory') . '/' . $this->attributes['case_image3'];
		return null;
	}

	public function setAdminCaseImage4Attribute($value)
	{
		return $this->attributes['case_image4'] = str_replace(config('admin.imagesUploadDirectory') . '/', '', $value);
	}

	public function getAdminCaseImage4Attribute()
	{
		if ($this->exists && $this->attributes['case_image4'])
			return config('admin.imagesUploadDirectory') . '/' . $this->attributes['case_image4'];
		return null;
	}

	public function setTitleAttribute($value)
	{
		self::saved(function($node) {
			if(!$node->metas->first())
				Meta::updateMeta($node, $node->city->slug . '/feeds/' . $node->slug, $node->title, $node->title, $node->title);
		});
		$this->attributes['title'] = $value;
	}

	public function city()
	{
		return $this->belongsTo(City::class);
	}

	public function club()
	{
		return $this->belongsTo(Club::class);
	}

	public function metas()
	{
		return $this->morphMany(Meta::class, 'metable');
	}

	public function club_feeds()
	{
		return $this->hasMany(ClubFeed::class);
	}

	public function clubs()
	{
		return $this->belongsToMany(Club::class, 'club_feeds');
	}
}
