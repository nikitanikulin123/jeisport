<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PriceList extends Model
{
	protected $with = [];

    protected $fillable = [
        'title', 'price', 'currency_id', 'price_for', 'price_list_category_id'
    ];

	public function priceListCategory()
	{
		return $this->belongsTo(PriceListCategory::class);
	}

	public function currency()
	{
		return $this->belongsTo(Currency::class);
	}
}
