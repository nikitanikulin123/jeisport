<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use SleepingOwl\Admin\Traits\OrderableModel;

class Category extends BaseModel
{
    use Sluggable, SluggableScopeHelpers, OrderableModel;

    protected $with = [];

	protected $fillable = ['title', 'active', 'type'];

	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'title'
			]
		];
	}

//    public function filterOptions()
//    {
//        return $this->hasManyThrough(FilterValue::class, Filter::class, 'category_id', 'filter_id');
//    }

	public function filterValues()
	{
		return $this->belongsToMany(FilterValue::class);
	}

    /**
     * Relation with filters
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function filters()
    {
        return $this->belongsToMany(Filter::class);
    }

    public function subcategories()
    {
        return $this->belongsToMany(FilterValue::class);
    }

	public function callbacks()
	{
		return $this->morphMany(Callback::class, 'callbackable');
	}

    public function setSubcategoriesAttribute($subcategories)
    {
        $this->subcategories()->detach();
        if (!$subcategories) return;
        if (!$this->exists) $this->save();

        $this->subcategories()->attach($subcategories);
    }

	public function setFiltersAttribute($filter)
	{
		$this->filters()->detach();
		if (!$filter) return;
		if (!$this->exists) $this->save();

		$filVars = Vars::specialFilterVarsByIds();
		if(\Input::get('specialFilters')) {
			$this->type == 'club' ? array_push($filter, $filVars['ratings'], $filVars['price'], $filVars['schedule']) :
				array_push($filter, $filVars['gender'], $filVars['ratings_T']);
			$filter = array_unique($filter);
		}

		$this->filters()->attach($filter);
	}

	public function setSpecialFiltersAttribute($subcategories) {}
}
