<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeoRegionCategory extends Model
{
	protected $fillable = [
		'region_id', 'category_id', 'page_title', 'page_subtitle', 'page_description',
		'metatitle', 'metakeyw', 'metadesc'
	];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }
}
