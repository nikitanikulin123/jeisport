<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Callback extends BaseModel
{

	protected $fillable = [
		'name', 'phone', 'checked', 'closed', 'description', 'callbackable_type'
	];

	public function callbackable()
	{
		return $this->morphTo();
	}
}
