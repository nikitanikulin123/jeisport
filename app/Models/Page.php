<?php

namespace App\Models;

class Page extends BaseModel
{
	public function setAdminImage2Attribute($value)
	{
		return $this->attributes['image2'] = str_replace(config('admin.imagesUploadDirectory') . '/', '', $value);
	}

	public function getAdminImage2Attribute()
	{
		if ($this->exists && $this->attributes['image2'])
			return config('admin.imagesUploadDirectory') . '/' . $this->attributes['image2'];
		return null;
	}

}
