<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeoCityCategory extends Model
{
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
