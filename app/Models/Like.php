<?php

namespace App\Models;

use App\Models\Comment;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $fillable = [
        'user_id',
        'likeable_id',
	    'increased'
    ];

	public function likeable()
	{
		return $this->morphTo();
	}

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
