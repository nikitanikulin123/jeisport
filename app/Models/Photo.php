<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends BaseModel
{

    protected $fillable = [
        'type', 'path', 'photoable_id'
    ];

    public function photoable()
    {
        return $this->morphTo();
    }
}
