<?php

namespace App\Models;

use SleepingOwl\Admin\Traits\OrderableModel;

class ClubFeed extends BaseModel
{
    use OrderableModel;

    public function club()
    {
        return $this->belongsTo(Club::class);
    }

	public function feed()
	{
		return $this->belongsTo(Feed::class);
	}
}
