<?php

namespace App\Models;

class Recommend extends BaseModel
{
	protected $fillable = [
		'type', 'city_id', 'category_id', 'amount_by_category', 'filter_value_id', 'amount_by_filter_value',
		'image_filter_value', 'updated_at'
	];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	public function filter()
	{
		return $this->belongsTo(Filter::class);
	}

	public function filterValue()
	{
		return $this->belongsTo(FilterValue::class);
	}
}
