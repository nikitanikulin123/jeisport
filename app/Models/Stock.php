<?php

namespace App\Models;

use Request;

class Stock extends BaseModel
{

	protected $fillable = [
		'title3', 'title4', 'text', 'image'
	];

    public function scopeForClub($query)
    {
	    $query->get()->filterArrFix(['stockable_id' => Request::get('club_id')]);
    }

	public function stockable()
	{
		return $this->morphTo();
	}
}
