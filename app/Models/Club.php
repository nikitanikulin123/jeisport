<?php

namespace App\Models;

use App\Custom\CustomCollection;
use App\Events\SendNotificationEvent;
use App\Http\Controllers\BaseController;
use App\Role;
use App\User;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Event;
use LocalizedCarbon;
use Nicolaslopezj\Searchable\SearchableTrait;
use Request;
use Auth;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request as policyRequest;

class Club extends BaseModel
{
    use SearchableTrait, Sluggable, SluggableScopeHelpers;

    protected $with = [
        'filterValues',
        'category',
        'city',
//        'stocks',
        'reviews',
        'region',
        'ratings',
    ];

    protected $fillable = [
	    'user_id', 'title', 'prefix', 'postfix', 'phone', 'phone2', 'address', 'house', 'email', 'n_email', 'lat_lng', 'lat', 'lng', 'site',
        'description', 'priority', 'active', 'moderated', 'premium', 'duration', 'used_trial',
        'created_at', 'updated_at', 'work_in_mon', 'mon_time_from', 'mon_time_to',
        'work_in_tue', 'tue_time_from', 'tue_time_to', 'work_in_wed', 'wed_time_from',
        'wed_time_to', 'work_in_thu', 'thu_time_from', 'thu_time_to', 'work_in_fri',
        'fri_time_from', 'fri_time_to', 'work_in_sat', 'sat_time_from', 'sat_time_to',
        'work_in_sun', 'sun_time_from', 'sun_time_to', 'image', 'p_file', 'deactivation_date',
        'slug', 'category_id', 'city_id', 'region_id', 'street_id'
    ];

	protected $searchable = [
		'columns' => [
			'clubs.title' => 10,
		],
	];
	// Extended from SearchableTrait and changed 'select' to 'where'
	protected function addBindingsToQuery(Builder $query, array $bindings) {
		$count = $this->getDatabaseDriver() != 'mysql' ? 2 : 1;
		for ($i = 0; $i < $count; $i++) {
			foreach($bindings as $binding) {
				$type = $i == 0 ? 'where' : 'having';
				$query->addBinding($binding, $type);
			}
		}
	}

	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'title'
			]
		];
	}

    /**
     * Scope for filter clubs
     *
     * @param $query
     * @return mixed
     */
    public function scopeFiltered($query)
    {
        if ($ci = Request::get('category_id'))
            $query->where('category_id', $ci);
        if ($city = Request::get('city_id'))
            $query->where('city_id', $city);
        if ($title = Request::get('title'))
            $query->where('title', 'like', '%' . $title . '%');
        if ($id = Request::get('id'))
            $query->where('id', $id);
        if (Request::get('active') == '1')
            $query->where('active', 1);
        if (Request::get('active') == '0')
            $query->where('active', 0);
	    if (Request::get('moderated') == '1')
		    $query->where('moderated', 1);
	    if (Request::get('moderated') == '0')
		    $query->where('moderated', 0);
        return $query;
    }

    /**
     * Get clubs for specified map corners coordinates
     *
     * @param $query
     * @param $bound_1
     * @param $bound_2
     * @param $bound_3
     * @param $bound_4
     * @return mixed
     */
    public function scopeByMapCorners($query, $bound_1, $bound_2, $bound_3, $bound_4)
    {
        return $query->where('lat', '>', $bound_1)
            ->where('lat', '<', $bound_2)
            ->where('lng', '>', $bound_3)
            ->where('lng', '<', $bound_4);
    }


	public function getTitleAttribute($value)
	{
		return e($value);
	}

    /**
     * Return quantity of active clubs
     *
     * @return mixed
     */
    public function countActive()
    {
        return $this->where('active', true)->count();
    }

	public function setActiveAttribute($value)
	{
		if($value && $this->email && User::whereEmail($this->email)->get()->count() == 0) {
			$user = User::create(['name' => $this->title, 'email' => $this->email, 'remember_token' => str_random(50), 'limit_of_items' => 3,
				'personal_bill' => (new BaseController(new policyRequest))->generateCode()]);
			$this->update(['user_id' => $user->id]);

			$role = Role::whereName('boss')->first();
			$user->attachRole($role);

			$this->attributes['newUser'] = true;
			$this->original['newUser'] = true;

			$response = Password::sendResetLink(['email' => $user->email], function (Message $message) {
				$message->subject('Мы создали для Вас аккаунт на сайте js.kg!');
			});
		}

		$this->attributes['active'] = $value;
	}

	public function setModeratedAttribute($value)
	{
		$previouslySent = $this->moderated != $value;
		self::saved(function($item) use ($value, $previouslySent) {
			if($value && \Input::get('active') && $previouslySent) {
				$request = new CustomCollection();
				$request->push([
					'type' => 'moderatedClub',
					'recipientEmail' => $item->n_email ? $item->n_email : $item->email,
					'data' => $item,
				]);
				Event::fire(new SendNotificationEvent($request->first()));
			}
		});
		$this->attributes['moderated'] = $value;
	}

	public function setDeactivationDateAttribute($value)
	{
		if(Request::segment(1) == 'admin_panel' && !$value)
			$this->attributes['deactivation_date'] = Carbon::now();
		else
			return $value;
	}

	public function getDeactivationDateAttribute($value)
	{
		if(Request::segment(1) == 'admin_panel' && ($value == '0000-00-00' || $value == ''))
			return $this->attributes['deactivation_date'] = Carbon::now();
		else
			return $value ? $value : '';
	}

	public function setUserIdAttribute($value)
	{
		if(!$this->newUser) {
			$this->attributes['user_id'] = $value;
			if($value)
				$this->attributes['email'] = User::whereId($value)->first()->email;
		}
	}

	public function setTitleAttribute($value)
	{
		self::saved(function($node) {
			if(!$node->metas->first())
				Meta::updateMeta($node, $node->city->slug . '/' . $node->category->slug . '/' . $node->slug, $node->title, $node->title, $node->title);
		});
		$this->attributes['title'] = $value;
	}

	public function setCityRegionStreetAttribute()
	{
		$city_id = \Input::get('city_id');
		$region_id = \Input::get('region_id');
		$street_id = \Input::get('street_id');

		$this->attributes['city_id'] = $city_id ? $city_id : 1;
		$this->attributes['region_id'] = $region_id ? $region_id : 3;
		$this->attributes['street_id'] = $street_id ? $street_id : 2;
	}

	public function setTrenersAttribute($treners)
	{
		$this->treners()->detach();
		if ( ! $treners) return;
		if ( ! $this->exists) $this->save();

		$this->treners()->attach($treners);
	}

    /**
     * Stores lat, lng in lat_lng attribute
     *
     * @param $value
     */
    public function setLatLngAttribute($value)
    {
        $this->attributes['lat_lng'] = $value;
        $lat_lng = explode(',', $value);
        $this->attributes['lat'] = array_get($lat_lng, 0);
        $this->attributes['lng'] = array_get($lat_lng, 1);

    }

	/**
	 * Relation to get filter values
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function filterValues()
	{
		return $this->belongsToMany(FilterValue::class);
	}

	/**
	 * Photos relation
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\MorphMany
	 */
	public function photos()
	{
		return $this->morphMany(Photo::class, 'photoable');
	}

	/**
	 * Relation to category
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	/**
	 * Relation to city
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function city()
	{
		return $this->belongsTo(City::class);
	}

	public function street()
	{
		return $this->belongsTo(Street::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function visits()
	{
		return $this->morphMany(Visit::class, 'visitable');
	}

	public function callbacks()
	{
		return $this->morphMany(Callback::class, 'callbackable');
	}

	public function treners()
	{
		return $this->belongsToMany(Trener::class);
	}

	public function stocks()
	{
		return $this->morphMany(Stock::class, 'stockable');
	}

	public function reviews()
	{
		return $this->morphMany(Review::class, 'reviewable');
	}

    /**
     * Relation to regions table
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function priceListCategories()
    {
	    return $this->morphMany(PriceListCategory::class, 'listable');
    }

    public function priceLists()
    {
        return $this->hasManyThrough(PriceList::class, PriceListCategory::class, 'listable_id');
    }

	public function ratings()
	{
		return $this->morphMany(Rating::class, 'rateable');
	}

	public function avgRatings()
	{
		return $this->morphMany(AvgRating::class, 'rateable');
	}

	public function feeds()
	{
		return $this->hasMany(Feed::class);
	}

	public function favourites()
	{
		return $this->morphMany(Favourite::class, 'favouritable');
	}

	public function metas()
	{
		return $this->morphMany(Meta::class, 'metable');
	}

	public function billingLog()
	{
		return $this->morphMany(BillingLog::class, 'billable');
	}
}
