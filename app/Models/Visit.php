<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visit extends BaseModel
{
//	protected $dates = ['visited_at'];

	protected $fillable = [
		'visits', 'visited_at'
	];

	public function visitable()
	{
		return $this->morphTo();
	}
}
