<?php

namespace App\Models;

use App\Models\Vacancies\Vacancy;
use App\User;
use Illuminate\Database\Eloquent\Model;

class BillingLog extends BaseModel
{
    protected $fillable = [
        'user_id', 'billable_id','billable_type', 'description', 'duration',
	    'change', 'balance', 'active', 'expired', 'started_at'
    ];

    public function billable()
    {
        return $this->morphTo();
    }

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
