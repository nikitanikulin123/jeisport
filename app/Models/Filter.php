<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use SleepingOwl\Admin\Traits\OrderableModel;

class Filter extends BaseModel
{
    use OrderableModel, Sluggable, SluggableScopeHelpers;

    protected $with = ['options'];

    protected $fillable = ['id', 'title', 'description', 'active', 'type'];

	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'title'
			]
		];
	}
    /**
     * Relation with Category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * Relation with filter options
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function options()
    {
        return $this->hasMany(FilterValue::class);
    }
}
