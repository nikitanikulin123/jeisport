<?php

namespace App\Models;

use App\Custom\CustomCollection;
use App\User;

class Review extends Node
{
    protected $guarded = ['id', 'lft', 'rgt', 'depth'];

	protected $with = [
//		'likes',
//		'user',
//		'reviewable',
	];

	protected $dates = ['viewed_at'];

    protected $fillable = [
        'ancestor_id',
        'parent_id',
        'moderated',
        'name',
        'user_id',
        'club_id',
        'text',
	    'viewed_at',
    ];

    /**
     * Retrieve all moderated reviews
     *
     * @param $query
     * @return mixed
     */
    public function scopeModerated($query)
    {
        return $query->where('moderated', true);
    }

	public function likes()
	{
		return $this->morphMany(Like::class, 'likeable');
	}

    /**
     * Relation to users table
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

	public function reviewable()
	{
		return $this->morphTo();
	}

	public function listAllChildren()
	{
		$collection = new CustomCollection();

		$this->renderNode($this->children, $collection);

		return $collection->unique()->sortByDesc('created_at');
	}

	private function renderNode($node, $collection)
	{
		if($node) {
			$collection->push($this);
			foreach ($node as $child) {
				$collection->push($child);
				if($child->children)
					$this->renderNode($child->children, $collection);
			}
		}
	}
}
