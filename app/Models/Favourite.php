<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{

    protected $fillable = [
        'user_id',
    ];

//	protected $with = ['favouritable'];

	public $timestamps = false;

    public function favouritable()
    {
        return $this->morphTo();
    }
}
