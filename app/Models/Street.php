<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use SleepingOwl\Admin\Traits\OrderableModel;

class Street extends BaseModel
{
    use OrderableModel, Sluggable, SluggableScopeHelpers;

	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'title'
			]
		];
	}

	public function scopeActiveOrder($query)
	{
		$query->where('active', true)->orderBy('order', 'asc');
	}

	public function city()
	{
		return $this->belongsTo(City::class);
	}
}
