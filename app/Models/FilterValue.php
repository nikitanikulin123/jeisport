<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use SleepingOwl\Admin\Traits\OrderableModel;

class FilterValue extends BaseModel
{

    use OrderableModel, Sluggable, SluggableScopeHelpers;

	protected $fillable = ['id', 'title', 'active', 'filter_id'];

	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'title'
			]
		];
	}

	public function scopeActive($query)
	{
		$query->where('active', true)->orderBy('created_at', 'desc');
	}

    /**
     * Relation to Filter model
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function filter()
    {
        return $this->belongsTo(Filter::class);
    }
}
