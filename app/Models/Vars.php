<?php

namespace App\Models;

class Vars extends BaseModel
{
	public static function getFilteredVars($initialVars, $allowed) {
		$filteredArr = array_filter(
			$initialVars,
			function ($key) use ($allowed) {
				if(strpos($key, $allowed) !== false) return $key;
			},
			ARRAY_FILTER_USE_KEY
		);
		array_pull($filteredArr, 'b_counter'); array_pull($filteredArr, 'b_item');

		return $filteredArr;
	}

	public static function getBillingVars() {
		$col = \App::make(self::class);

		$col->premiumClub = [
			1 => 1000,
			3 => 3000,
			6 => 6000,
			12 => 12000
		];

		return $col;
	}

	public static function getAdminVars() {
		$allVars = Widget::all()->keyBy('key');
		$col = \App::make(self::class);

		foreach ($allVars as $key => $var)
			$col->{camel_case($key)} = $var->value;

		return $col;
	}

	public static function specialFilterVarsByIds() {
		$gender = 96;
//		$speciality = 97;
//		$specialization = 98;
		$ratings_T = 99;

		$male = 994;
		$female = 995;

		$withReviews_T = 996;
		$moreThen3_T = 997;
		$moreThen4_T = 998;
		$moreThen4_5_T = 999;
		

		$ratings = 100;
		$price = 101;
		$schedule = 102;
		$district = 103;
		$street = 104;
		$sort = 105;

		$withReviews = 1001;
		$withFoto = 1002;
		$with3dTour = 1003;
		$moreThen3 = 1004;
		$moreThen4 = 1005;
		$moreThen4_5 = 1006;
		$recentlyOpened = 1007;

		$lowPrice = 1008;
		$avgPrice = 1009;
		$highPrice = 1010;
		$veryHighPrice = 1011;

		$fullDay = 1012;
		$openedNow = 1013;
		$more2HoursOpened = 1014;

		$byDefault = 1015;
		$byRating = 1016;

		return ['gender' => $gender, /*'speciality' => $speciality, 'specialization' => $specialization,*/ 'ratings_T' => $ratings_T,
			'withReviews_T' => $withReviews_T,
			'male' => $male, 'female' => $female, 'moreThen3_T' => $moreThen3_T, 'moreThen4_T' => $moreThen4_T,
			'moreThen4_5_T' => $moreThen4_5_T,

			'ratings' => $ratings, 'price' => $price, 'schedule' => $schedule, 'district' => $district, 'street' => $street,
			'sort' => $sort, 'withReviews' => $withReviews, 'withFoto' => $withFoto, 'with3dTour' => $with3dTour, 'moreThen3' => $moreThen3,
			'moreThen4' => $moreThen4, 'moreThen4_5' => $moreThen4_5, 'recentlyOpened' => $recentlyOpened, 'lowPrice' => $lowPrice,
			'avgPrice' => $avgPrice, 'highPrice' => $highPrice, 'veryHighPrice' => $veryHighPrice, 'fullDay' => $fullDay, 'openedNow' => $openedNow,
			'more2HoursOpened' => $more2HoursOpened, 'byDefault' => $byDefault, 'byRating' => $byRating];
	}

	public static function specialFilterNamesByVars() {
		$filVars = Vars::specialFilterVarsByIds();

		$filters = [
			$filVars['gender'] => ['Пол', 'для специалистов', 'trener'],
//			$filVars['speciality'] => ['Специальность', 'для специалистов', 'trener'],
//			$filVars['specialization'] => ['Специализация', 'для специалистов', 'trener'],
			$filVars['ratings_T'] => ['Рейтинг', 'для специалистов'],

			$filVars['ratings'] => 'Рейтинг',
			$filVars['price'] => 'Стоимость',
			$filVars['schedule'] => 'Режим работы',
			$filVars['district'] => ['Район', 'общие для всех', 'club_trener'],
			$filVars['street'] => ['Улица', 'общие для всех', 'club_trener'],
			$filVars['sort'] => ['Сортировка', 'общие для всех', 'club_trener'],
		];

		$genderFilters = [
			$filVars['male'] => 'Мужчина',
			$filVars['female'] => 'Женщина',
		];

		$ratingFilters_T = [
			$filVars['withReviews_T'] => 'С отзывами',
			$filVars['moreThen3_T'] => 'Оценка 3+',
			$filVars['moreThen4_T'] => 'Оценка 4+',
			$filVars['moreThen4_5_T'] => 'Оценка 4.5+',
		];

		$ratingFilters = [
			$filVars['withReviews'] => 'С отзывами',
			$filVars['withFoto'] => 'С фото',
			$filVars['with3dTour'] => 'С 3d туром',
			$filVars['moreThen3'] => 'Оценка 3+',
			$filVars['moreThen4'] => 'Оценка 4+',
			$filVars['moreThen4_5'] => 'Оценка 4.5+',
			$filVars['recentlyOpened'] => 'Недавно открылись',
		];

		$priceFilters = [
			$filVars['lowPrice'] => 'Низкая',
			$filVars['avgPrice'] => 'Средняя',
			$filVars['highPrice'] => 'Высокая',
			$filVars['veryHighPrice'] => 'Очень высокая',
		];

		$scheduleFilters = [
			$filVars['fullDay'] => 'Круглосуточно',
			$filVars['openedNow'] => 'Открыто сейчас',
			$filVars['more2HoursOpened'] => 'Будет открыто ещё 2 часа',
		];

		$sortFilters = [
			$filVars['byDefault'] => 'По умолчанию',
			$filVars['byRating'] => 'По рейтингу',
		];

		$filtersOptions = [
			$filVars['gender'] => $genderFilters,
			$filVars['ratings_T'] => $ratingFilters_T,
			$filVars['ratings'] => $ratingFilters,
			$filVars['price'] => $priceFilters,
			$filVars['schedule'] => $scheduleFilters,
			$filVars['sort'] => $sortFilters,
		];

		return ['filters' => $filters, 'filtersOptions' => $filtersOptions];
	}
}
