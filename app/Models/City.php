<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Request;
use SleepingOwl\Admin\Traits\OrderableModel;

class City extends BaseModel
{
    use OrderableModel, Sluggable, SluggableScopeHelpers;

	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'title'
			]
		];
	}

    public function regions()
    {
        return $this->hasMany(Region::class);
    }

	public function streets()
	{
		return $this->hasMany(Street::class);
	}

    public function setLatLngAttribute($value)
    {
        $this->attributes['lat_lng'] = $value;
        $lat_lng = explode(',', $value);
        $this->attributes['lat'] = array_get($lat_lng, 0);
        $this->attributes['lng'] = array_get($lat_lng, 1);
    }

    public static function getDefaultCitySlug($cities)
    {
	    $city_slug = Request::segment(1);
        if($city_slug && $city_slug != 'profiles' && $city_slug != 'auth' && $city_slug != 'contacts' && $city_slug != 'searched' &&
	        $city_slug != 'about' && !Request::is('c-*') && !Request::is('t-*'))
            return $city_slug;
        $city = $cities->where('is_main', true)->first();
        return $city ? $city->slug : 'bishkek';
    }
}
