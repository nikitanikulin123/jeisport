<?php

namespace App\Models;

use Hash;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class Administrator extends Model implements AuthenticatableContract
{
    use Authenticatable, EntrustUserTrait;

    protected $fillable = [
        'username',
        'password',
        'name',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function setRolesAttribute($roles)
    {
        $this->roles()->detach();
        if ( ! $roles) return;
        if ( ! $this->exists) $this->save();

        $this->roles()->attach($roles);
    }

    public function setPasswordAttribute($value)
    {
        if ( ! empty($value))
        {
            $this->attributes['password'] = Hash::make($value);
        }
    }
}
