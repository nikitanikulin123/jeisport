<?php

namespace App\Models;

use App\Custom\CustomCollection;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use LocalizedCarbon;

class BaseModel extends Model
{
	public static function makeModel($model, $path = 'App\Models\\', $withRelations = false) {
		if(!is_string($path)) {
			$withRelations = $path;
			$path = 'App\Models\\';
		}
		if($cutModel = stristr($model, 's.', true))
			$model = $cutModel;
		$resultedModel = \App::make($path . str_replace(' ', '', ucwords(str_replace('_', ' ', $model))));
		if(!$withRelations)
			$resultedModel = $resultedModel->setEagerLoads([]);

		return $resultedModel;
	}

	/**
	 * Create a new Eloquent Collection instance.
	 *
	 * @param  array  $models
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function newCollection(array $models = [])
	{
		return new CustomCollection($models);
	}

	/**
	 * @param $query
	 */
	public function scopeForAuthUser($query)
	{
		$query->where('user_id', Auth::id());
	}

	public function scopeNotActive($query)
	{
		$query->where('active', false)->orderBy('created_at', 'desc');
	}
	public function scopeActive($query)
	{
		return $query->where('active', true)->orderBy('created_at', 'desc');
	}
	public function scopeActiveOnly($query)
	{
		return $query->where('active', true);
	}
	public function scopeOrder($query)
	{
		return $query->orderBy('order');
	}
	public function scopeActiveOrder($query)
	{
		return $query->activeOnly()->orderBy('order');
	}
	public function scopeActiveOrderDesc($query)
	{
		return $query->activeOnly()->orderBy('order', 'desc');
	}
	public function scopeModerated($query)
	{
		$query->where('moderated', true);
	}
	public function scopeActiveModerated($query)
	{
		$query->active()->where('moderated', true);
	}
	public function scopeActiveOnlyModerated($query)
	{
		$query->activeOnly()->where('moderated', true);
	}

	/**
	 * Get clubs for category
	 *
	 * @param $query
	 * @param $category
	 * @return mixed
	 */
	public function scopeForCategory($query, $category)
	{
		if ($category)
			return $query->where('category_id', $category);
		return $query;
	}

	public function scopeForCity($query, $city)
	{
		if ($city)
			return $query->where('city_id', $city);
		return $query;
	}

	public function scopeForRegion($query, $region)
	{
		if ($region)
			return $query->where('region_id', $region);
		return $query;
	}

	public function scopeForStreet($query, $street)
	{
		if ($street)
			return $query->where('street_id', $street);
		return $query;
	}

	public function scopeForFilterValue($query, $filterValue)
	{
		if ($filterValue)
			return $query->whereHas('filterValues', function ($q) use ($filterValue) {
				return $q->where('filter_value_id', $filterValue);
			});
		return $query;
	}

	public function setAdminImageAttribute($value)
	{
		return $this->attributes['image'] = str_replace(config('admin.imagesUploadDirectory') . '/', '', $value);
	}

	public function getAdminImageAttribute()
	{
		if ($this->exists && $this->attributes['image'])
			return config('admin.imagesUploadDirectory') . '/' . $this->attributes['image'];
		return null;
	}

	public function setAdminFileAttribute($value)
	{
		return $this->attributes['file'] = str_replace(config('admin.imagesUploadDirectory') . '/', '', $value);
	}

	public function getAdminFileAttribute()
	{
		if ($this->exists && $this->attributes['file'])
			return config('admin.imagesUploadDirectory') . '/' . $this->attributes['file'];
		return null;
	}

	public function setAdminImagesAttribute($images)
	{
		self::saved(function($node) use($images) {
			$node->photos()->delete();
			$imgs = [];
			foreach ($images as $img) {
				$img = str_replace(config('admin.imagesUploadDirectory') . '/', '', $img);
				$imgs[] = Photo::create(['imageable_id' => $node->id, 'path' => $img]);
			}
			$node->photos()->saveMany($imgs);
		});
	}

	public function getAdminImagesAttribute()
	{
		$imgs = [];
		if($this->photos->count() > 0)
			foreach ($this->photos as $img) {
				$imgs[] = config('admin.imagesUploadDirectory') . '/' . $img->path;
			}
		return $imgs;
	}

	public function setImage($size, $filename, $type = 'fit')
	{
		$path = strpos($filename, '.') ? $filename : $this->{$filename};
		if($path == '')
			$path = '/img/jpg/nophoto.jpg';
		elseif(strpos($path, 'https://') !== false)
			return $path;
		$fullPath = strpos($path, '/') === false ? config('admin.imagesUploadDirectory') . '/' . $path : public_path($path);
		$file_exists = file_exists($fullPath);

		if ($file_exists && $path) {
			if(strpos($path, '/') === 0)
				$path = substr($path, 1);
			return route($type. 'Image', [$size, $path]);
		}

		$size = explode('_', $size);
		$w_size = array_get($size, 0);
		$h_size = array_get($size, 1) !== 'null' ? array_get($size, 1) : 0;
		return "http://placehold.it/{$w_size}x{$h_size}";
	}

//--------------------------------------------------------------------------------------------------------------

	public function checkWorkingDays()
	{
		if($this->work_in_mon || $this->work_in_tue || $this->work_in_wed || $this->work_in_thu || $this->work_in_fri || $this->work_in_sat || $this->work_in_sun)
			return true;
		return false;
	}

	public function setDateFormat($day, $old = null)
	{
		if($old)
			return Carbon::parse($old)->format('H:i');
		else
			return Carbon::parse($this->{$day})->format('H:i');
	}

	public function getNextWorkDay() {
		$currentDayNumber = LocalizedCarbon::now()->dayOfWeek;
		$nextDayNumber = 1;
		$i = $currentDayNumber + 1;

		while ($this->{$this->getCurrentDayInfo('workToday', $i)} == 0) {
			$i == 7 ? $i = 1 : $i++;
			$nextDayNumber++;
		}

		return LocalizedCarbon::parse($this->{$this->getCurrentDayInfo('todayTimeFrom', LocalizedCarbon::now()->addDays($nextDayNumber))})->addDays($nextDayNumber);
	}

	public function getCurrentDayInfo($parameter, $specifiedDay = null)
	{
		if(!$specifiedDay)
			$specifiedDay = LocalizedCarbon::now();
		if($specifiedDay instanceof Carbon)
			$specifiedDay = $specifiedDay->dayOfWeek != 0 ? $specifiedDay->dayOfWeek : 7;

		$days = [1 => 'mon', 2 => 'tue', 3 => 'wed', 4 => 'thu', 5 => 'fri', 6 => 'sat', 7 => 'sun'];
		$russianDays = [1 => 'пн', 2 => 'вт', 3 => 'ср', 4 => 'чт', 5 => 'пт', 6 => 'сб', 7 => 'вс'];
		$singleDay = 'work_in_';
		$singleDayTimeFrom  = '_time_from';
		$singleDayTimeTo = '_time_to';

		if($parameter == 'schedule') {
			$arr = [];
			$col = new CustomCollection();
			for ($i = 1; $i <= 7; $i++) {
				if($i != 7 && $this->{$singleDay . $days[$i]} && $this->{$singleDay . $days[$i + 1]} &&
					$this->{$days[$i] . $singleDayTimeFrom} == $this->{$days[$i + 1] . $singleDayTimeFrom} &&
					$this->{$days[$i] . $singleDayTimeTo} == $this->{$days[$i + 1] . $singleDayTimeTo})
					array_push($arr, [$russianDays[$i] => $days[$i], $russianDays[$i + 1] => $days[$i + 1]]);
				elseif ($this->{$singleDay . $days[$i]} && !count($arr))
					$col->push([$russianDays[$i] => $days[$i]]);
				elseif ($this->{$singleDay . $days[$i]} && count($arr)) {
					$arr = array_unique(array_collapse($arr));
					$col->push($arr);
					$arr = [];
				}
			}
			return $col;
		}
		elseif($parameter == 'today')
			return $name = $days[$specifiedDay];
		elseif($parameter == 'workToday')
			return $day = 'work_in_' . $days[$specifiedDay];
		elseif($parameter == 'todayTimeFrom')
			return $timeFrom = $days[$specifiedDay] . '_time_from';
		elseif($parameter == 'todayTimeTo')
			return $timeTo = $days[$specifiedDay] . '_time_to';
		return false;
	}

	public function getFullAddressAttribute() {
		$result = '';
		if($this->region)
			$result = 'район ' . $this->region->title;
		if($this->street && $this->street->title != 'другая')
			$result = rtrim($result) . ', улица ' . $this->street->title;
		else
			$result = rtrim($result) . ', ' . $this->address;
		if($this->house)
			$result = rtrim($result) . ' - ' . $this->house;
		return $result;
	}

	public function scopeCheckSpecialFilters($query, $specialFilters)
	{
		$filVars = Vars::specialFilterVarsByIds();

		foreach ($specialFilters as $filters) {
			foreach ($filters as $key => $filter) {
				if ($key == $filVars['district']) {
					$query->where(function ($query) use ($filter) {
						foreach ($filter as $option) {
							$query->orWhere('region_id', $option);
						}
					});
				}
				if ($key == $filVars['street']) {
					$query->where(function ($query) use ($filter) {
						foreach ($filter as $option) {
							$query->orWhere('street_id', $option);
						}
					});
				}
				if ($key == $filVars['sort']) {
					foreach ($filter as $option) {
						if ($option == $filVars['byDefault']) {
							$query->orderBy('priority', 'desc')->orderBy('id', 'asc');
						}
						if ($option == $filVars['byRating']) {
							$query->select('clubs.*', 'avg_ratings.average_value', 'avg_ratings.rateable_id', 'avg_ratings.rateable_type')
								->leftJoin('avg_ratings', 'clubs.id', '=', 'avg_ratings.rateable_id')->orderBy('priority', 'desc')->orderBy('average_value', 'desc');
						}
					}
				}

				if ($key == $filVars['ratings']) {
					foreach ($filter as $option) {
						if ($option == $filVars['withReviews']) {
							$query->whereHas('reviews', function ($q) use ($option) {
								$q->where('moderated', true);
								return $q;
							});
						}
						if ($option == $filVars['withFoto']) {
							$query->has('photos');
						}
						if ($option == $filVars['with3dTour']) {
							//
						}
						if ($option == $filVars['moreThen3'] || $option == $filVars['moreThen4'] || $option == $filVars['moreThen4_5']) {
							$query->whereHas('avgRatings', function ($q) use ($option, $filVars) {
								if ($option == $filVars['moreThen3']) {
									$q->where('average_value', '>', 3);
								}
								if ($option == $filVars['moreThen4']) {
									$q->where('average_value', '>', 4);
								}
								if ($option == $filVars['moreThen4_5']) {
									$q->where('average_value', '>', 4.5);
								}
								return $q;
							});
						}
						if ($option == $filVars['recentlyOpened']) {
							$query->where('clubs.created_at', '>', Carbon::now()->subDay());
						}
					}
				}

				if ($key == $filVars['price']) {
					foreach ($filter as $option) {
						if ($option == $filVars['lowPrice'] || $option == $filVars['avgPrice'] || $option == $filVars['highPrice']  || $option == $filVars['veryHighPrice']) {
							$query->whereHas('priceListCategories', function ($q) use ($option, $filVars) {
								if ($option == $filVars['lowPrice']) {
									$q->where('min', '<', 500);
								}
								if ($option == $filVars['avgPrice']) {
									$q->where('min', '<', 1000);
								}
								if ($option == $filVars['highPrice']) {
									$q->where('min', '<=', 1500);
								}
								if ($option == $filVars['veryHighPrice']) {
									$q->where('min', '<=', 2000);
								}
								return $q;
							});
						}
					}
				}

				if ($key == $filVars['schedule']) {
					foreach ($filter as $option) {
						if ($option == $filVars['fullDay'] || $option == $filVars['openedNow'] || $option == $filVars['more2HoursOpened']) {
							if ($option == $filVars['fullDay']) {
								$query->where($this->getCurrentDayInfo('workToday'), true)->where($this->getCurrentDayInfo('todayTimeFrom'), '00:00:00')->where($this->getCurrentDayInfo('todayTimeTo'), '23:59:00');
							}
							if ($option == $filVars['openedNow']) {
								$query->where($this->getCurrentDayInfo('workToday'), true)->where($this->getCurrentDayInfo('todayTimeFrom'), '<', Carbon::now())->where($this->getCurrentDayInfo('todayTimeTo'), '>', Carbon::now());
							}
							if ($option == $filVars['more2HoursOpened']) {
								$query->where($this->getCurrentDayInfo('workToday'), true)->where($this->getCurrentDayInfo('todayTimeTo'), '>', Carbon::now()->addHours(2));
							}
						}
					}
				}
			}
		}
	}
}
