<?php

namespace App\Providers;

use App\Models\Club;
use App\Models\Favourite;
use App\Models\Feed;
use App\Models\Like;
use App\Models\Meta;
use App\Models\Photo;
use App\Models\PriceListCategory;
use App\Models\Rating;
use App\Models\Review;
use App\Models\Stock;
use App\Models\Trener;
use App\User;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserRegisteredEvent' => [
            'App\Listeners\UserRegisteredListener',
        ],
	    'App\Events\SendNotificationEvent' => [
		    'App\Listeners\SendNotificationListener',
	    ]
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

	    User::deleting(function ($user) {
		    foreach($user->clubs()->get() as $entry) {
			    $this->deleteNestedItems($entry);
		    }
		    $user->clubs()->delete();

		    foreach($user->treners()->get() as $entry) {
			    $this->deleteNestedItems($entry);
		    }
		    $user->treners()->delete();
	    });

	    Club::deleting(function ($entry) {
		    $this->deleteNestedItems($entry);
	    });

	    Trener::deleting(function ($entry) {
		    $this->deleteNestedItems($entry);
	    });

	    Review::deleting(function ($entry) {
		    $entry->likes()->delete();
	    });

	    Feed::deleting(function ($entry) {
		    $entry->metas()->delete();
	    });
    }

	protected function deleteNestedItems($entry)
	{
		$entry->metas()->delete();
		$entry->photos()->delete();
		$entry->priceListCategories()->delete();
		$entry->favourites()->delete();
		$entry->ratings()->delete();
		$entry->stocks()->delete();

		foreach($entry->reviews()->get() as $review) {
			$review->likes()->delete();
		}
		$entry->reviews()->delete();
	}
}
