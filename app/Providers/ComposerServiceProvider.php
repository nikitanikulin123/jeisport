<?php namespace App\Providers;

use App\Models\Category;
use App\Models\City;
use App\Models\Region;
use App\Models\Widget;
use Illuminate\Support\ServiceProvider;
use Request;
use View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
	    //
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}