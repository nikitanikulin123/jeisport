// filter-select
$(function () {
    var filter_select = $(".filter-select");

    if (filter_select.length > 0)
        filter_select.multiselect({
            buttonText: function (options, select) {
                if (options.length === 0) {
                    $(select).closest('.pull-left ').removeClass('m-checked_li');
                    return $(select).data('title');
                } else if (options.length == 1) {
                    $(select).closest('.pull-left ').addClass('m-checked_li');
                    return $(select).data('title') + ': ' + $(options).text();
                } else if (options.length > 1) {
                    $(select).closest('.pull-left ').addClass('m-checked_li');
                    return $(select).data('title') + ': (' + $(options).length + ')';
                } else {
                    var labels = [];
                    options.each(function () {
                        if ($(this).attr('label') !== undefined) {
                            labels.push($(this).attr('label'));
                        }
                        else {
                            labels.push($(this).html());
                        }
                    });
                    return labels.join(', ') + '';
                }
            },
            enableFiltering: true,
            filterPlaceholder: 'Поиск...',
            enableCaseInsensitiveFiltering: true,
            maxHeight: 400,
            //includeSelectAllOption: true,
            //selectAllText: 'Выбрать все'
            //buttonWidth: '150px'
        });

    if($('select[data-title=Сортировка] option:selected').val() == 1015)
        $('select[data-title=Сортировка]').parentsUntil('.pull-left').next().hide();

//    $(".request_add_place").on('submit', function(e){
//        e.preventDefault();
//        var self = $(this);
//        $.post(self.attr('action'), self.serialize(), function(data){
//            self.trigger('reset');
//            $('#here').modal('hide');
//            alert('После обработки (обычно в течение двух дней), заведение появится на сайте, а мы пришлем вам отчет и ссылку');
//        });
//    });
});

$(document).ready(function () {

    $('button.btn.date').click(function () {
        $('button.btn.date').removeClass("active");
        $(this).addClass("active");
    });

    $('.tabs ul li').click(function () {
        $('li').removeClass("active");
        $(this).addClass("active");
    });


    $("#otziv1").on("click", "a.otziv", function (event) {
        //отменяем стандартную обработку нажатия по ссылке
        event.preventDefault();

        //забираем идентификатор бока с атрибута href
        var id = $(this).attr('href'),

        //узнаем высоту от начала страницы до блока на который ссылается якорь
            top = $(id).offset().top;

        //анимируем переход на расстояние - top за 1500 мс
        $('body,html').animate({scrollTop: top}, 1500);
    });

    // $('.revealer').click(function () {
    //     var self = $(this);
    //     $('.dummy', $(this)).fadeOut(250, function () {
    //         $('.final', self).fadeIn(250);
    //     });
    // });
}); // end document ready


// Hide tab with items
$(function () {
    var hidden_tab = $('.hidden-tab');
    if (hidden_tab.length > 0) {
        body.on('click', '#t-list-map', function(e) {
            hidden_tab.hide();
        });
        body.on('click', '#t-list', function(e) {
            hidden_tab.show();
        });
    }
});

// Initialize select2 on selects
$(function () {
    var selects = $('.spet select');
    if (selects.length > 0)
        selects.select2();
});

// Club's address input
$(function () {
    var address_city = $('.b-address_city');
    var address_region = $('.b-address_region');
    var address_street = $('.b-address_street');
    var address_hidden = $('.b-address_hidden');


    if (address_city.length) {
        getRegionAndStreetByCity();
        address_city.select2();
        address_region.select2();
        address_street.select2();
        address_city.on('change', function (e) {
            address_hidden.hide();
            address_region.empty();
            address_street.empty();
            address_region.siblings().find('.select2-selection__rendered').text('Выберите Район');
            address_street.siblings().find('.select2-selection__rendered').text('Выберите Улицу');

            getRegionAndStreetByCity();
        });
    }

    $('select[name=city_id], select[name=region_id], select[name=street_id]').on('change', function (e) {
        setMarkerOnMap();
    });
    $('input[name=house]').on('input', function (e) {
        setMarkerOnMap();
    });
});

// Club schedule's set
$(function () {
    $('.form-groups .radio').on('click', function (e) {
        if($(this).val() == 3) {
            $('.schedule_time_from').val('00:00').prop('disabled', true);
            $('.schedule_time_to').val('23:59').prop('disabled', true);
        } else {
            $('.schedule_time_from').prop('disabled', false);
            $('.schedule_time_to').prop('disabled', false);
        }
    });

    $('.set_schedule').on('click', function (e) {
        e.preventDefault();
        var schedule_time_from = $('.schedule_time_from').val();
        var schedule_time_to = $('.schedule_time_to').val();
        var admin_panel = $('.b-admin_panel').length;
        if(admin_panel) {
            var time_from_collection = $('.row .col-lg-3 .datepicker input:even');
            var time_to_collection = $('.row .col-lg-3 .datepicker input:odd');
            var checkbox_collection = $('.row .col-lg-3 .checkbox input');
        } else {
            var time_from_collection = $('.time-from input');
            var time_to_collection = $('.time-to input');
            var checkbox_collection = $('.control--checkbox input');
        }

        if ($('.form-groups .radio:checked').val() == 1) {
            time_from_collection = time_from_collection.slice(0, 5);
            time_to_collection = time_to_collection.slice(0, 5);
            checkbox_collection = checkbox_collection.slice(0, 5);
        } else if ($('.form-groups .radio:checked').val() == 2) {
            checkbox_collection = checkbox_collection.slice(0);
        }

        time_from_collection.each(function(){ $(this).val(schedule_time_from); });
        time_to_collection.each(function(){ $(this).val(schedule_time_to); });
        checkbox_collection.each(function(){ $(this).prop('checked', true ); });
    });
});

// DateTimepicker
$(function() {
    var datetimepicker = $('.b-datetimepicker');
    var datetimepicker_input = $('.b-datetimepicker input');
    if (datetimepicker.length) {
        datetimepicker.datetimepicker({
            format: 'dd.MM.yyyy hh:mm',
            pickSeconds: false,
            language: 'ru'
        });

        // MASKED INPUT PLUGIN
        datetimepicker_input.mask("99.99.9999 99:99", {placeholder: "00.00.0000 00:00"});
    }

    var datepicker = $('.b-datepicker');
    var datepicker_input = $('.b-datepicker input');
    if (datepicker.length) {
        datepicker.datetimepicker({
            format: 'dd.MM.yyyy',
            pickTime: false,
            language: 'ru'
        });

        // MASKED INPUT PLUGIN
        datepicker_input.mask("99.99.9999", {placeholder: "00.00.0000"});
    }

    var timepicker = $('.b-timepicker');
    var timepicker_input = $('.b-timepicker input');
    if (timepicker.length) {
        timepicker.datetimepicker({
            format: 'hh:mm',
            pickDate: false,
            pickSeconds: false
        });

        // MASKED INPUT PLUGIN
        timepicker_input.mask("99:99", {placeholder: "00:00"});
    }
});

// Trainer's gender
$(function () {
    var trener_gender = $('.trener-gender .control__indicator');
    var male = trener_gender.first();
    var female = trener_gender.last();

    $(male.parent()).on('click', function (e) {
        female.prev().removeAttr('checked');
    });
    $(female.parent()).on('click', function (e) {
        male.prev().removeAttr('checked');
    });
});

// Report's plot of visitings
$(function () {
    var plot = $('#b-plot_container'), yAxisInt = [];
    if(plot[0]) {
        var xAxis = plot.attr('data-xaxis').split(',');
        var yAxis = plot.attr('data-yaxis').split(',');
        yAxis.map(function (val) {
            return yAxisInt.push(parseInt(val));
        });

        Highcharts.chart('b-plot_container', {
            title: {
                text: 'Посещаемость страницы заведений',
                x: -20 //center
            },
            subtitle: {
                //text: 'Source: WorldClimate.com',
                x: -20
            },
            xAxis: {
                //categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                categories: xAxis
            },
            yAxis: {
                title: {
                    text: 'Кол-во посещений'
                },
                plotLines: [{
                    value: 0,
                    width: 2,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ' посещений'
            },
            legend: {
                //layout: 'vertical',
                //align: 'right',
                //verticalAlign: 'middle',
                //borderWidth: 0
            },
            series: [{
                name: ' ',
                data: yAxisInt
                //}, {
                //    name: 'New York',
                //    data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
            }]
        })
    }
});

// Toggle free or premium on billing page
$(function () {
    var billing = $('section.billing');

    if (billing.length > 0) {
        var rateplans = billing.find('.rateplans');
        var free = billing.find('div.free');
        var premium = billing.find('div.premium');

        free.hover(function() {
            premium.css('z-index', '-99');
            rateplans.css('background', "url('../img/frees.png') no-repeat 75% center lightgoldenrodyellow");
        }, function() {
            premium.css('z-index', '1');
            rateplans.css('background', "white");
        });
        premium.hover(function() {
            free.css('z-index', '-99');
            rateplans.css('background', "url('../img/premiuim.png') no-repeat 10% center lightblue");
        }, function() {
            free.css('z-index', '1');
            rateplans.css('background', "white");
        });
    }
});

// Remove comma from last filter option in string
$(function () {
    var filter_options = $('.b-filter_options_with_commas');

    if (filter_options.length) {
        filter_options.each(function () {
            $(this).find('a').last().text(function(index, text) {
                return text.replace(", ", "");
            });
        });
    }
});