$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

window.body = $('body');
window.ajaxModal = $('#ajaxModal');

body.on('submit', '.ajax_form', function(e) {
    e.preventDefault();
    sendAjaxWIthValidation($(this));
});

body.on('click', '.ajax_replace', function(e) {
    e.preventDefault();
    sendAjaxWIthValidation($(this));
});

body.on('input', '.ajax_input', function(e) {
    e.preventDefault();
    if($(this).val().length >= 3)
        sendAjaxWIthValidation($(this), {query: $(this).val()});
    else
        $('.hide_window').hide();
});

body.on('click', '.ajax_showMore', function(e) {
    e.preventDefault();
    var self = $(this);
    var showMore = self.parent('.b-show_more');
    var sort = showMore.attr('data-sort');
    var type = showMore.attr('data-type');
    var page = self.attr('data-page');
    var bounds = type === 'clubs.index' ? myMap.getBounds() : null;
    var category_id = $('#ajax-category').length ? $('#ajax-category').text() : null;
    var city_id = $('#ajax-city').length ? $('#ajax-city').text() : null;
    var club_id = $('.selects').length ? $('.selects').attr('data-club-id') : null;
    var data = {
        type: type,
        page: page,
        item_id: $('.create-reviews').attr('data-id'),
        sort: sort,
        bounds: bounds,
        filtersData: $('.search-form-select').serialize(),
        sortFiltersData: $('.sort-form-select').serialize(),
        category_id: category_id,
        city_id: city_id,
        club_id: club_id
    };

    sendAjaxWIthValidation($(this), data);
});

body.on('click', '.ajax_append', function(e) {
    e.preventDefault();
    var currentUrl, url, urlObj, hashObj, getParams, hash, data;
    currentUrl = window.location.href;
    url = $(this).attr('href');
    urlObj = currentUrl.includes("?") ? $.deparam.querystring(currentUrl) : {};
    hashObj = currentUrl.includes("#") ? $.deparam.fragment(currentUrl) : {};
    urlObj = url.includes("?") ? Object.assign(urlObj, $.deparam.querystring(url)) : urlObj;
    hashObj = url.includes("#") ? Object.assign(hashObj, $.deparam.fragment(url)) : hashObj;
    data = Object.assign({}, urlObj, hashObj);
    data.ajax = true;
    getParams = urlObj && Object.keys(urlObj).length ? '?' + $.param(urlObj) : '';
    hash = hashObj && Object.keys(hashObj).length ? '#' + $.param(hashObj) : '';
    window.history.pushState('page2', 'Title', getParams + hash);
    sendAjaxWIthValidation($(this), data);
});

// MODALS
$(function () {
    body.on('click', '.ajax_modal', function(e) {
        e.preventDefault();
        getAjaxModal($(this));
    });
});

// Get info from object and send to modal window
function getAjaxModal(obj, params) {
    if(params)
        history.pushState('', 'Title', '/');
    var action = params ? params.action : obj.attr('data-action');
    var type = params ? params.type : obj.attr('data-type');
    var item_id = obj.attr('data-id') ? obj.attr('data-id') : 0;
    var forItem = obj.attr('data-forItem') ? obj.attr('data-forItem') : 0;
    var forItem_id = obj.attr('data-forItem-id') ? obj.attr('data-forItem-id') : 0;
    var parameters = params ? params.parameters : (obj.attr('data-parameters') ? obj.attr('data-parameters') : 0);
    var modal_class = obj.attr('data-modal-class') ? obj.attr('data-modal-class') : '';

    if(parameters === 'filtersModal')
        parameters = $('.sort-form-select').serialize();

    ajaxModal.remove();
    $('.modal-backdrop').remove();
    ajaxModal.load('/ajax/modal/getModalContent', {action: action, type: type, item_id: item_id, forItem: forItem,
        forItem_id: forItem_id, parameters: parameters, modal_class: modal_class}, function (data) {
        ajaxModal.modal('toggle');
        blockButton(ajaxModal);
    });

    ajaxModal.on('shown.bs.modal', function (e) {
        $(this).find('form').find('.form-group input.form-control').first().focus();

        //PRELOADER
        $('.spinner').fadeOut();
        $('#page-preloader').delay(350).fadeOut('slow');
    });

    ajaxModal.on('hidden.bs.modal', function (e) {
        ajaxModal.find('.modal-dialog').remove();
    });
}

function sendAjaxWIthValidation(self, data, method, url, callbackFunction) {
    if(!method)
        method = self.prop("tagName") === 'FORM' ? 'POST' : 'GET';
    if(self && method === 'POST') {
        url = url ? url : self.attr('action');
        data = data ? data : self.serialize();
        self.find('.form-group').removeClass('has-error');
        blockButton($('.ajax_form'));
    } else
        url = url ? url : self.attr('href');

    $.ajax({method: method, url: url, data: data }).done(function(resp) {
        var status, viewsAmount, viewName, content, action, paramsAmount;
        if($.type(resp) !== 'object') {
            status = parseJsonFromHtml(resp, "#status");
            viewsAmount = parseJsonFromHtml(resp, "#viewsAmount");
            paramsAmount = parseJsonFromHtml(resp, "#paramsAmount");
        } else
            status = resp;
        if(self && method === 'POST') {
            self.trigger('reset');
        }

        if (status.success === 1) {
            if(ajaxModal && status.modalWindow !== 'doNotHide')
                ajaxModal.modal('hide');
            if(status.text)
                alertify.success(status.text);
            if(viewsAmount)
                for (var i = 0; i < viewsAmount; i++) {
                    viewName = parseJsonFromHtml(resp, "#viewName-" + i);
                    content = parseJsonFromHtml(resp, "#content-" + i, 'notParsed');
                    action = parseJsonFromHtml(resp, "#viewName-" + i, 'getAction');
                    replaceAjaxContent(viewName, content, action, url);
                }
            if(paramsAmount) {
                implementParams(resp, ["#toggleClass", '#setValue', '#setAttr']);
            }
            if(status.location || status.history)
                setTimeout(function () {
                    if(status.location && window.location.pathname !== status.location)
                        window.location = status.location;
                    else if(status.history)
                        window.history.pushState('page2', 'Title', status.history);
                }, status.timeOut ? status.timeOut : 0);
            if(callbackFunction)
                callbackFunction(resp);
        } else
            alertify.error(status.text);
    }).error(function (resp) {
        blockButton($('.ajax_form'), false);
        var errors = $.parseJSON(resp.responseText);
        self.find('*[name=' + Object.keys(errors)[0] + ']').closest('.form-group').addClass('has-error');
        self.find('*[name=' + Object.keys(errors)[0] + ']').focus();
        alertify.error(errors[Object.keys(errors)[0]][0]);
    });
}

function implementParams(resp, arrOfParams) {
    $.each(arrOfParams, function (k, param) {
        var action = parseJsonFromHtml(resp, param);
        if(action) {
            if(param === "#toggleClass")
                $.each(action, function (selector, className) {
                    $(selector).toggleClass(className);
                });
            if(param === "#setValue")
                $.each(action, function (selector, className) {
                    if($(selector).prop("tagName") === 'INPUT')
                        $(selector).val(className);
                    else
                        $(selector).text(className);
                });
            if(param === "#setAttr")
                $.each(action, function (selector, value) {
                    $(selector).attr(value[0], value[1]);
                });
        }
    });
}

function parseJsonFromHtml(html, id, custom) {
    if(!$($.parseHTML(html)).filter(id).length)
        return null;
    var parsed = custom === 'getAction' ? $($.parseHTML(html)).filter(id).attr('data-action') : $($.parseHTML(html)).filter(id).text();

    return custom === 'getAction' || custom === 'notParsed' ? parsed : $.parseJSON(parsed);
}

function replaceAjaxContent(view, content, action) {
    var view_arr = view.split('._');
    var prefix = view_arr[view_arr.length - 1];
    var replaced_block = $('.b-replaced_block_' + prefix);

    if(action === 'append') {
        replaced_block.append(content);
    } else {
        replaced_block.empty();
        replaced_block.html(content);
    }
}

// Block/unblock buttons
function blockButton(obj, status) {
    if(status !== false)
        obj.find('.m-changed_opacity').attr('disabled', 'disabled').css('opacity', 0.5).css('pointer-events', 'none');
    else
        obj.find('.m-changed_opacity').attr('disabled', false).css('opacity', 1).css('pointer-events', 'auto');
}

// clone a JavaScript object
function clone(obj) {
    if (null === obj || "object" !== typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

// Hide modal and other windows by outside click
body.click(function() {
    $('.hide_window').hide();
});

//--------------------------------------------------------------------------------------------------------------

function getStockMap(obj) {
    var single_map = $('#stock-map'), map_obj, map_lat, map_lng;
    if (single_map.attr('data-map-obj'))
        map_obj = JSON.parse(single_map.attr('data-map-obj'));
    else if (single_map.attr('data-map-lat')) {
        map_lat = single_map.attr('data-map-lat');
        map_lng = single_map.attr('data-map-lng');
    } else {
        map_lat = 42.875989;
        map_lng = 74.603674;
    }
    ymaps.ready(function () {
        var myMap = new ymaps.Map("stock-map", {
            center: map_obj ? [map_obj.coords[0], map_obj.coords[1]] : [map_lat, map_lng],
            zoom: map_obj && map_obj.scale ? map_obj.scale : 17
        });
        myMap.behaviors.disable('scrollZoom');
        myMap.controls.remove('typeSelector');
        myMap.controls.remove('trafficControl');
        myMap.controls.remove('searchControl');
        myMap.controls.remove('scaleLine');
        myMap.controls.remove('geolocationControl');
        myMap.controls.remove('rulerControl');
        myMap.controls.remove('fullscreenControl');

        var coordsArr = map_obj ? map_obj.coords : [map_lat, map_lng], myPlacemark;
        for (var i = 0; i < coordsArr.length; i = i + 2) {
            myPlacemark = new ymaps.Placemark(
                [coordsArr[i], coordsArr[i + 1]],
                {
                    // hintContent: 'Передвиньте маркер на объект',
                    balloonContent: map_obj && map_obj.title ? map_obj.title : 'Название заведения',
                },
                {
                    iconLayout: 'default#image',
                    iconImageHref: '/img/markers/static.png'
                }
            );
            myMap.geoObjects.add(myPlacemark);
        };
    });
}

function getRegionAndStreetByCity(obj) {
    var address_city = $('.b-address_city');
    var address_region = $('.b-address_region');
    var address_street = $('.b-address_street');
    var address_hidden = $('.b-address_hidden');
    var address_region_option = $('.b-address_region option');
    var address_street_option = $('.b-address_street option');

    $.get('/ajax/address/getRegionAndCityLists', {city_id: address_city.val()}, function (items) {
        if(address_region_option.length === 0)
            address_region.append($('<option>', { value: 0, text : 'Выберите Район' }));
        if(address_region_option.length <= 1)
            $.each(items['regions'], function (item, i) {
                address_region.append($('<option>', { value: i, text : item }));
            });
        if(address_street_option.length === 0)
            address_street.append($('<option>', { value: 0, text : 'Выберите Улицу' }));
        if(address_street_option.length <= 1)
            $.each(items['streets'], function (item, i) {
                address_street.append($('<option>', { value: i, text : item }));
            });
        getOtherStreet();
    });
}

function getOtherStreet(obj) {
    var address_street = $('.b-address_street');
    var address_hidden = $('.b-address_hidden');

    if (address_street.length) {
        if ($('.b-address_street option:selected').text() === 'другая')
            address_hidden.show();
        address_street.on('change', function (e) {
            address_hidden.hide();
            if ($('.b-address_street option:selected').text() === 'другая')
                address_hidden.show();
        });
    }
}

function setMarkerOnMap() {
    var map_search = $('.b-map-search'), address;
    var city = $('.b-address_city option:selected').text();
    var region = $('.b-address_region option:selected').text();
    var street = $('.b-address_street option:selected');
    var house = $('input[name=house]').val();

    city === 'Выберите Город' ? city = '' : false;
    region === ' Выберите Район ' ? region = '' : false;
    street.text() === 'Выберите Улицу' ? street.text('') : false;
    if(street.val() && street.val() != 0 && !street.text().includes("-й") && !street.text().includes("проспект") && !street.text().includes("переулок")
        && !street.text().includes("другая"))
        street = 'ул. ' + street.text();
    else
        street = street.text();

    address = city + ' ' + region + ' ' + street + (street ? ' ' + house : '');

    map_search.val(address);
    findOnMap(address);
}

function findOnMap(text) {
    if (!$('#move_map:checked').length > 0)
        return false;
    var myGeocoder = ymaps.geocode(text);
    myGeocoder.then(
        function (res) {
            var coords = res.geoObjects.get(0).geometry.getCoordinates();
            myMap.setCenter(coords);
            myPlacemark.geometry.setCoordinates(coords);
            var corners = myMap.getBounds();
            $('#lat_lng').val(coords);
            $('#corners').val(corners);
//                        alert('Координаты объекта :' + res.geoObjects.get(0).geometry.getCoordinates());
        },
        function (err) {
            alert('Ошибка');
        }
    );
}

function refreshFilters(data) {
    window.previousData = data;
    var filterSelect, options;
    $.each(data, function (k, v) {
        if(k == 'streets') {
            filterSelect = $('.b-streets');
            options = $('.b-streets option');
        } else if(k == 'regions') {
            filterSelect = $('.b-regions');
            options = $('.b-regions option');
        } else {
            filterSelect = $('.selects .filter-select');
            options = $('.selects .filter-select option');
        }

        $.each(options, function (k, option) {
            if (v[option.value] && v[option.value].disabled)
                option.disabled = true;
            else
                option.disabled = false;
        });

        if(!$('.b-filter-apply').length)
            filterSelect.multiselect('refresh');
    });

    $('.selects').show();
    $('.sort-filters').show();
}