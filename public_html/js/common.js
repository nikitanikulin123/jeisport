$(document).ready(function () {

    if (window.matchMedia('(max-width: 767px)').matches) {
        var dropParents = $('.auth .dropdown');

        $(dropParents).each(function (i, obj) {
            $(obj).on('click', function (event) {
                event.stopPropagation();
                var target = $($(obj).attr('data-id'));
                openDropdown(target, obj);
            });
        });

        $(document).on('click', function (e) {
            openDropdown(null, null);
        });

        function openDropdown(target, parent) {
            var dropChildren = $('.dropdown-menu.custom');
            $(dropParents).each(function () {
                if ($(this).attr('data-id') === $(parent).attr('data-id')) {
                    $(this).toggleClass('open');
                } else {
                    $(this).removeClass('open');
                }
            });
            $(dropChildren).each(function (i, obj) {
                if (target !== null) {
                    if ($(obj).attr('id') === $(target).attr('id')) {
                        var left = $(parent).offset().left;
                        $(obj).toggleClass('show');
                    } else {
                        $(obj).removeClass('show');
                    }
                } else {
                    $(obj).removeClass('show');
                }

                $(obj).click('click', function (e) {
                    e.stopPropagation();
                });
            });
        }
    }
    ;


    // плавный скролл якоря
    $('.go_to').click(function () { // ловим клик по ссылке с классом go_to
        var scroll_el = $(this).attr('href'); // возьмем содержимое атрибута href, должен быть селектором, т.е. например начинаться с # или .
        if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
            $('html, body').animate({scrollTop: $(scroll_el).offset().top}, 500); // анимируем скроолинг к элементу scroll_el
        }
        return false; // выключаем стандартное действие
    });


    if ($('header.home-header').length > 0) {
        num = $('header.home-header').offset().top;
        $(window).bind('scroll', function () {
            if ($(window).scrollTop() > num) {
                if ($('header.home-header').length > 0)
                    $('header.home-header').addClass('navbar-fixed-top');
            }
            else {
                num = $('header.home-header').offset().top;
                $('header.home-header').removeClass('navbar-fixed-top');
            }
        });
    }


    if ($('.show-one-club').length > 0) {
        num = $('.show-one-club').offset().top;
        $(window).bind('scroll', function () {
            if ($(window).scrollTop() > num) {
                if ($('.show-one-club').length > 0)
                    $('.show-one-club').addClass('navbar-fixed-top');
            }
            else {
                num = $('.show-one-club').offset().top;
                $('.show-one-club').removeClass('navbar-fixed-top');
            }
        });
    }

    if ($('#macy-container').length > 0) {
        var macy = Macy({
            container: '#macy-container',
            trueOrder: false,
            waitForImages: false,
            margin: 24,
            columns: 4,
            breakAt: {
                1200: 4,
                940: 3,
                520: 2,
                400: 1
            }
        });
    }

    if ($('[data-toggle="tooltip"]').length > 0) {
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    }

    if ($('.heart').length > 0) {
        $(".heart").click(function () {
            $(".heart").toggleClass("like");
        });
    }

    if ($('.owl-carousel.mob-slider-special').length) {
        $('.owl-carousel.mob-slider-special').owlCarousel({
            loop: true,
            margin: 10,
            autoWidth:true,
            autoplay:false,
            dots:true,
            responsive: {
                0: {
                    items: 2
                },
                480: {
                    items: 3
                },
                769: {
                    items: 3
                },
                992: {
                    items: 7
                },
                1200: {
                    items: 10
                }
            }
        });
    }

    $(".button-tel").click(function() {
        $('.toggled_block').toggle();
    });
        $(document).on('click', function(e) {
            if (!$(e.target).closest(".call-back-trener").length) {
                $('.toggled_block').hide();
            }
            e.stopPropagation();
    });


    // $('#get-files').on('change', function (event) {
    //
    //     var fileReader = new FileReader();
    //     fileReader.onload = function (e) {
    //         console.log(e);
    //     }
    //     fileReader.readAsDataURL(this.files)
    //     console.log(this.files);
    // });
    if ($('.owl-carousel.photo-job-car').length > 0)
        $('.owl-carousel.photo-job-car').owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            autoplay: true,
            autoplayTimeout: 4000,
            autoplayHoverPause: true,
            smartSpeed: 700,
            responsive: {
                0: {
                    items: 2
                },
                480: {
                    items: 4
                },
                769: {
                    items: 6
                },
                992: {
                    items: 7
                },
                1200: {
                    items: 10
                }
            }
        });

    $(function(){
        $("#phone11").mask("0(999) 999-999");
    });


    // if($('.multiple-items.news-slider').length > 0)
    //     $('.multiple-items.news-slider').slick({
    //         infinite: false,
    //         slidesToShow: 3,
    //         slidesToScroll: 3,
    //         vertical: true,
    //         verticalSwiping: true
    //     });


});

// owl-carousel
$(function () {
    var owl_carousel = $(".owl-carousel");
    var owl_carousel2 = $(".owl-carousel2");
    var owl_carousel3 = $(".owl-carousel3");
    var owl_carousel_1 = $(".owl-carousel-1");
    var owl_carousel_2 = $(".owl-carousel-2");
    var owl_carousel_3 = $(".owl-carousel-3");

    if (owl_carousel.length > 0)
        owl_carousel.owlCarousel({
            center: false,
            loop: true,
            autoplay: true,
            autoplayTimeout: 3000,
            autoplaySpeed: 3000,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                600: {
                    items: 2,
                    nav: false
                },
                1000: {
                    items: 2,
                    nav: true,
                    loop: true
                }
            }
        });

    if (owl_carousel2.length > 0)
        owl_carousel2.owlCarousel({
            items: 1,
            lazyLoad: true,
            loop: true,
            nav: true,
            autoplay: false,
            autoplayTimeout: 3500,
            autoplaySpeed: 2000,

        });

    if (owl_carousel3.length > 0)
        owl_carousel3.owlCarousel({
            items: 1,
            lazyLoad: true,
            loop: true,
            nav: true,
            autoplay: true,
            autoplayTimeout: 1500,
            autoplaySpeed: 2000,

        });

    // Mobile owl-carousel
    if (owl_carousel_1.length > 0)
        owl_carousel_1.owlCarousel({
            items: 1,
            slideSpeed: 2000,
            nav: false,
            autoplay: true,
            dots: false,
            loop: true
        });
    if (owl_carousel_2.length > 0)
        owl_carousel_2.owlCarousel({
            items: 1,
            slideSpeed: 2000,
            nav: false,
            autoplay: true,
            dots: false,
            merge: true,
            //loop: true
        });
    if (owl_carousel_3.length > 0)
        owl_carousel_3.owlCarousel({
            items: 3,
            slideSpeed: 2000,
            nav: false,

            autoplay: true,
            margin: 30,
            dots: false,
            loop: true
        });

    // Mobile search dropdown
    $("#vn-click").click(function () {
        $("#vn-info").slideToggle(300);
    });

    $(function () {
        $('a.toogle').click(function (e) {
            e.preventDefault();
            $('a.toogle').removeClass('open');
            $(this).addClass('open');
        });
    });
});