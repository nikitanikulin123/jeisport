$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var filter_select = $('.filter-select');
    var show_more = $('.show_more');
    var seemore = $('.seemore'), type, page;


    if(seemore.length) {
        seemore.each(function () {
            type = $(this).attr('data-type');
            if (type !== 'clubs.reviews' && type !== 'clubs.index' && type !== 'clubs.createEdit' && type !== 'treners.createEdit')
                sidebarResolve();
        });
    }

    body.on('change', '.filter-select', function(e) {
        applyFilters($(this));
    });

    body.on('click', '.b-filter-apply', function(e) {
        e.preventDefault();
        type = $('.seemore').attr('data-type');
        page = $('[data-type="' + type + '"] a').attr('data-page');
        var selected_filters = $(this).parent().serializeArray();
        var filter_id = $(this).attr('data-filter-id');
        var current_select = $('select[name="' + filter_id + '[]"');

        current_select.find('option').prop('selected', false);
        if(selected_filters.length > 1) {
            $.each(selected_filters, function (k, obj) {
                if(obj.name !== '_token')
                    $('option[value="' + obj.value + '"]', $('select[name="' + obj.name + '"]')).prop('selected', true);
            });
        }
        applyFilters(current_select);
        filter_select.multiselect('refresh');

        ajaxModal.modal('toggle')
    });

    body.on('click', '.b-reset-filters', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).prev().find('.filter-select option:selected').each(function() {
            $(this).prop('selected', false);
        });
        $(this).hide();

        clearFilters();
        filter_select.multiselect('refresh');
    });

    function applyFilters(current_select) {
        if(current_select.find(":selected").length)
            current_select.parent('.multiselect-native-select').next().show();
        else
            current_select.parent('.multiselect-native-select').next().hide();

        clearFilters();
    }

    function clearFilters() {
        var url = window.location.href, urlType = url.split('#'), hash = '';
        if (urlType.length > 1)
            hash = urlType[1];

        if($('#ajax-baseUrl').data('has-type')){
            window.location.href = $('#ajax-baseUrl').data('url') + '?' + filter_select.serialize() + '#' + hash;
        }
        window.history.pushState('page2', 'Title', '?' + filter_select.serialize() + '#' + hash);

        if(type && type === 'clubs.index') {
            sidebarResolve(null, myMap.getBounds());
            var arr_ids = [];
            $(objectManager.objects.getAll()).each(function (k, v) {
                arr_ids.push(v.id);
            });
            objectManager.remove(arr_ids);
        } else
            sidebarResolve(type == 'clubs.reviews' ? 'clubs.specialists' : null);
    }

    function sidebarResolve(defaultType, bounds) {
        var data = {
            filtersData: $('.search-form-select').serialize(),
            sortFiltersData: $('.sort-form-select').serialize(),
            category_id: $('#ajax-category').text(),
            city_id: $('#ajax-city').text(),
            type: defaultType ? defaultType : type,
            page: page,
            club_id: $('.selects').data('club-id'),
            bounds: bounds
        };

        sendAjaxWIthValidation(null, data, "POST", '/ajax/get_shown_items', updateSidebar)
    }

    function updateSidebar(data) {
        var filters = $($.parseHTML(data)).filter("#filters").text();
        var parsedFilters = $.parseJSON(filters);
        var parsedCommonInfo = $.parseJSON($("#commonInfo").text());
        var type = parsedCommonInfo.type;
        var sidebar = $('#sidebar-' + type.replace(".", "_"));

        if(type != 'clubs.createEdit' && type != 'treners.createEdit')
            refreshFilters(parsedFilters);

        if(type && type === 'clubs.index') {
            var points = $($.parseHTML(data)).filter("#points").text();
            var parsed = $.parseJSON(points);

            $.each(parsed.features, function (k, v) {
                if (!objectManager.objects.getById(v.id))
                    objectManager.add(v);
            });

            sidebar.find('li').on('mouseover', 'a', function () {
                var objectId = $(this).attr('data-id');
                var objectState = objectManager.getObjectState(objectId);
                if (objectState.isClustered) {
                    objectManager.clusters.setClusterOptions(objectState.cluster.id, {
                        preset: 'islands#yellowClusterIcons'
                    });
                    //objectManager.clusters.state.set('activeObject', objectManager.objects.getById(objectId));
                    //objectManager.clusters.balloon.open(objectState.cluster.id);
                } else {
                    objectManager.objects.setObjectOptions(objectId, {
                        preset: 'islands#yellowIcon',
                        iconLayout: 'default#image',
                        iconImageHref: '/img/markers/on-hover.png',
                    });
                }
            }).on('mouseout', 'a', function (e) {
                var objectId = $(this).attr('data-id');
                var objectState = objectManager.getObjectState(objectId);
                if (objectState.isClustered) {
                    objectManager.clusters.setClusterOptions(objectState.cluster.id, {
                        preset: 'islands#blueClusterIcons'
                    });
                } else {
                    objectManager.objects.setObjectOptions(objectId, {
                        preset: 'islands#yellowIcon',
                        iconLayout: 'default#image',
                        iconImageHref: '/img/markers/static.png',
                    });
                }
            });
        }
    }



    if ($('#ymaps-map').length > 0) {
        ymaps.ready(function () {
            myMap = new ymaps.Map("ymaps-map", {
                center: [parseFloat($('#city-coords-lat').text()), parseFloat($('#city-coords-lng').text())],
                zoom: $('#ymaps-map').attr('data-map-zoom'),
                behaviors: ["default"]
            }, {
                minZoom: 10
            });
            //var suggestView = new ymaps.SuggestView('suggest');
            myMap.container.fitToViewport();
            myMap.behaviors.disable('scrollZoom');
            myMap.controls.remove('typeSelector');
            myMap.controls.remove('trafficControl');
            myMap.controls.remove('searchControl');
            myMap.controls.remove('scaleLine');
            myMap.controls.remove('zoomControl');
            myMap.controls.remove('geolocationControl');
            myMap.controls.remove('rulerControl');
            myMap.controls.remove('fullscreenControl');

            var zoomControl = new ymaps.control.ZoomControl({
                options: {
                    size: "small"
                }
            });

            myMap.controls.add(zoomControl, {
                float: 'none',
                position: {
                    right: 50,
                    bottom: 100
                }
            });

            var MyClusterBalloonContentLayout = ymaps.templateLayoutFactory.createClass([
                '<div class=multiballoon>',
                '<ul>',
                '{% for geoObject in properties.geoObjects %}',
                '<li>{{ geoObject.properties.content|raw }}</li>',
                '{% endfor %}',
                '</ul>',
                '</div>'
            ].join(''), {
                build: function () {
                    MyClusterBalloonContentLayout.superclass.build.call(this);
                    var geoObjects = this.getData().properties.get('geoObjects');
                    var ids = geoObjects
                        .filter(geoObject => !geoObject.properties.loaded)
                        .map(geoObject => geoObject.id);


                    if (ids.length) {
                        var self = this;
                        $.post('/ajax/balloon', {objects: ids}, function (resp) {
                            $.each(geoObjects, function (index, geoObject) {
                                geoObject.properties = {
                                    content: resp[index],
                                    properties: geoObject.properties,
                                    loaded: true
                                }
                            });
                            self.rebuild()
                        });
                    }
                }
            });

            // Переменная с описанием двух видов иконок кластеров.
            var clusterIcons = [
                {
                    href: '/img/markers/static.png',
                    size: [50, 60],
                    // Отступ, чтобы центр картинки совпадал с центром кластера.
                    offset: [-20, -45]
                }];
            // При размере кластера до 100 будет использована картинка 'small.jpg'.
            // При размере кластера больше 100 будет использована 'big.png'.
            // Сделаем макет содержимого иконки кластера,
            // в котором цифры будут раскрашены в белый цвет.

            var MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                '<div style="margin-right:15px;margin-top: -9px;">$[properties.geoObjects.length]</div>');

            objectManager = new ymaps.ObjectManager(
                {
                    clusterize: true,
                    clusterDisableClickZoom: true,
                    clusterOpenBalloonOnClick: true,
                    clusterBalloonContentLayout: MyClusterBalloonContentLayout,
                    //clusterBalloonItemContentLayout: MyClusterBalloonItemContentLayout
                    //clusterHideIconOnBalloonOpen: false,
                    //geoObjectOpenBalloonOnClick: false
                    // Если опции для кластеров задаются через кластеризатор,
                    // необходимо указывать их с префиксами "cluster".
                    clusterIcons: clusterIcons,
                    clusterIconContentLayout: MyIconContentLayout
                });
            myMap.geoObjects.add(objectManager);

            //myMap.options.set({
            //    geoObjectClusterBalloonLayout: "cluster#balloonAccordionContent",
            //});

            objectManager.objects.options.set({
                preset: 'islands#greenDotIcon',
                //balloonContentLayout:  'my#simplestBCLayout',
                iconLayout: 'default#image',
                iconImageHref: '/img/markers/static.png',
                //balloonAutoPan: false
            });

            setMapStateByHash();
            sidebarResolve(null, myMap.getBounds());

            myMap.events.add(['boundschange'], function () {
                if ($('body').scrollTop() > 512) {
                    // $('html,body').animate({
                    //     scrollTop: $('.sidebar_container').offset().top
                    // }, 700);
                }
                var params = [
                    'type=' + myMap.getType().split('#')[1],
                    'center=' + myMap.getCenter(),
                    'zoom=' + myMap.getZoom()
                ];
                window.location.hash = params.join('&');
                sidebarResolve(null, myMap.getBounds());

                // var currentUrlHash = $.deparam.fragment(window.location.href);
                // var currentUrlMapCenterArray = currentUrlHash.center ? currentUrlHash.center.split(",") : null;
                // var equalCoords = true;
                //
                // for (var i = 0; i <= 1; i++) {
                //     if(!currentUrlMapCenterArray || (parseFloat(currentUrlMapCenterArray[i]).toFixed(9) !== myMap.getCenter()[i].toFixed(9)))
                //         equalCoords = false;
                // }
                // if(!equalCoords) {
                //     window.location.hash = params.join('&');
                //     sidebarResolve(null, myMap.getBounds());
                // }
            });

            objectManager.objects.events.add(['mouseenter', 'mouseleave'], onObjectEvent);
            objectManager.clusters.events.add(['mouseenter', 'mouseleave'], onClusterEvent);

            //objectManager.clusters.events.add(['click'], function(e){
            //    var objectId = e.get('objectId');
            //
            //    //var objectState = objectManager.getObjectState(objectId);
            //
            //    //var objects = objectState.cluster.getGeoObjects();
            //    var cluster = objectManager.clusters.getById(objectId);
            //    console.log(cluster);
            //});

            objectManager.objects.events.add('click', function (e) {

                var objectId = e.get('objectId');
                var objects = [];
                objects.push(objectId);
                $.post('/ajax/balloon', {objects: objects}, function (data) {

                    var obj = objectManager.objects.getById(objectId);
                    //obj.properties.maxWidth = 10;
                    //maxHeight: 200,
                    //    maxWidth: 10,
                    obj.properties.balloonContent = data;

                    objectManager.objects.balloon.open(objectId);
                });
            });
            var layer = myMap.layers.get(0).get(0);
            // Отслеживаем событие окончания отрисовки тайлов.
            waitForTilesLoad(layer).then(function () {
                // Вызываем, например, печать документа window.print();
                //alert('Карта загружена');
            });
        });
    }

    function onObjectEvent(e) {
        var objectId = e.get('objectId');
        if (e.get('type') == 'mouseenter') {
            // Метод setObjectOptions позволяет задавать опции объекта "на лету".
            objectManager.objects.setObjectOptions(objectId, {
                preset: 'islands#yellowIcon',
                iconLayout: 'default#image',
                iconImageHref: '/img/markers/on-hover.png',
            });
        } else {
            objectManager.objects.setObjectOptions(objectId, {
                preset: 'islands#blueIcon',
                iconLayout: 'default#image',
                iconImageHref: '/img/markers/static.png',
            });
        }
    }

    function onClusterEvent(e) {
        var objectId = e.get('objectId');
        var clusterIcons = [
            {
                href: '/img/markers/on-hover.png',
                size: [50, 60],
                // Отступ, чтобы центр картинки совпадал с центром кластера.
                offset: [-20, -45]
            }];
        var clusterIconsHover = [
            {
                href: '/img/markers/static.png',
                size: [50, 60],
                // Отступ, чтобы центр картинки совпадал с центром кластера.
                offset: [-20, -45]
            }];
        // При размере кластера до 100 будет использована картинка 'small.jpg'.
        // При размере кластера больше 100 будет использована 'big.png'.
        // Сделаем макет содержимого иконки кластера,
        // в котором цифры будут раскрашены в белый цвет.

        var MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div style="margin-right:15px;margin-top: -9px;">$[properties.geoObjects.length]</div>');
        if (e.get('type') == 'mouseenter') {
            objectManager.clusters.setClusterOptions(objectId, {
                clusterIcons: clusterIcons,
                clusterIconContentLayout: MyIconContentLayout
            });
        } else {
            objectManager.clusters.setClusterOptions(objectId, {
                clusterIcons: clusterIconsHover,
                clusterIconContentLayout: MyIconContentLayout
            });
        }
    }

    // Получить слой, содержащий тайлы.
    function getTileContainer(layer) {
        for (var k in layer) {
            if (layer.hasOwnProperty(k)) {
                if (
                    layer[k] instanceof ymaps.layer.tileContainer.CanvasContainer
                    || layer[k] instanceof ymaps.layer.tileContainer.DomContainer
                ) {
                    return layer[k];
                }
            }
        }
        return null;
    }

    // Определить, все ли тайлы загружены. Возвращает Promise.
    function waitForTilesLoad(layer) {
        return new ymaps.vow.Promise(function (resolve, reject) {
            var tc = getTileContainer(layer), readyAll = true;
            tc.tiles.each(function (tile, number) {
                if (!tile.isReady()) {
                    readyAll = false;
                }
            });
            if (readyAll) {
                resolve();
            } else {
                tc.events.once("ready", function () {
                    resolve();
                });
            }
        });
    }

    function getParam(name, location) {
        location = location || window.location.hash;
        var res = location.match(new RegExp('[#&]' + name + '=([^&]*)', 'i'));
        return (res && res[1] ? res[1] : false);
    }

    // Передача параметров, описывающих состояние карты,
    // в адресную строку браузера.
    //function setLocationHash() {
    //
    //}

    // Установка состояния карты в соответствии с переданными в адресной строке
    // браузера параметрами.
    function setMapStateByHash() {
        var hashType = getParam('type'),
            hashCenter = getParam('center'),
            hashZoom = getParam('zoom'),
            open = getParam('open');
        if (hashType) {
            myMap.setType('yandex#' + hashType);
        }
        if (hashCenter) {
            myMap.setCenter(hashCenter.split(','));
        }
        if (hashZoom) {
            myMap.setZoom(hashZoom);
        }
        if (open) {
            myPlacemarkCollection.each(function (geoObj) {
                var id = geoObj.properties.get('myId');
                if (id == open) {
                    geoObj.balloon.open();

                }
            });
        }
    }

    //$("img.lazy").lazyload();
    //$(function () {
    //    $('.search-form').show();
    //});
});