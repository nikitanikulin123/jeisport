var elixir = require('laravel-elixir');
var base_src  = 'resources/assets';
var base_views  = 'resources/views';
var base_dest = 'public_html/css';

elixir.config.notifications = false;
elixir.config.sourcemaps = false;
elixir(function(mix) {
    mix.sass([base_src + '/sass'], base_src + '/css/stylesFromSass.css')
        .styles([base_src + '/css'], base_dest + '/all.min.css')
        //.version([base_dest + '/all.min.css'], './public_html')
        .browserSync({
            files: [
                base_dest + '/*',
                base_views + '/*'
            ],
            proxy 			: "jskg",
            logPrefix		: "Laravel Eixir BrowserSync",
            logConnections	: true,
            reloadOnRestart : false,
            notify 			: true
        })
});

// Не забывать менять название в proxy на название текущего проекта
// Использовать  gulp watch --production